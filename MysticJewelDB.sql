USE master
IF EXISTS(select * from sys.databases where name='MysticJewelDB')
DROP DATABASE MysticJewelDB
GO

CREATE DATABASE MysticJewelDB
GO

USE MysticJewelDB
GO

----- NHÓM 1: THÔNG TIN VỀ NGƯỜI SỬ DỤNG

CREATE TABLE AdminAccount
(
	AdminID				INT IDENTITY(1,1) PRIMARY KEY,
	AdminUserName       VARCHAR(20) UNIQUE,
	AdminPassword       VARCHAR(50),
	AdminRole			VARCHAR(20),
	DateCreated			DATETIME DEFAULT(CURRENT_TIMESTAMP),
	DateUpdated			DATETIME DEFAULT(CURRENT_TIMESTAMP)
)
GO

CREATE TABLE Customer
(
	CustomerID			INT IDENTITY(1,1) PRIMARY KEY,
	CustomerUserName    VARCHAR(20) UNIQUE,
	CustomerPassword    VARCHAR(50),
	CustomerAvatar      VARCHAR(300),
	FirstName			VARCHAR(50),
	LastName			VARCHAR(50),
	Email				VARCHAR(50) UNIQUE,
	Gender				VARCHAR(10),
	Phone1				VARCHAR(20),
	Phone2				VARCHAR(20),
	DateOfBirth			DATE,
	IsActive			BIT DEFAULT(1),
    ResetToken          VARCHAR(50) DEFAULT(NULL),
	DateCreated			DATETIME DEFAULT(CURRENT_TIMESTAMP),
	DateUpdated			DATETIME DEFAULT(CURRENT_TIMESTAMP)
)
GO

CREATE TABLE CustomerAddress
(
	AddressID			INT IDENTITY(1,1) PRIMARY KEY,
	CustomerID			INT,
	Number				VARCHAR(20),
	Street				VARCHAR(100),
	Ward				VARCHAR(20),
	District			VARCHAR(20),
	City				VARCHAR(20),
	CONSTRAINT FK_AddressCustomer FOREIGN KEY (CustomerID) REFERENCES Customer(CustomerID)
)
GO

CREATE TABLE SocialAccount
(
	SocialAccountID		INT IDENTITY(1,1) PRIMARY KEY,
	CustomerID			INT,
	ProviderCustomerID	VARCHAR(100),
	Provider			VARCHAR(50),
	CONSTRAINT FK_AccountSocial FOREIGN KEY (CustomerID) REFERENCES Customer(CustomerID)
)
GO

CREATE TABLE Category
(
	CategoryID			INT IDENTITY(1,1) PRIMARY KEY,
	CategoryName		VARCHAR(100) NOT NULL UNIQUE,
	ParentID			INT NULL,
	CONSTRAINT FK_CategoryParent FOREIGN KEY (ParentID) REFERENCES Category(CategoryID)
)
GO

CREATE TABLE AttributeGroup
(
	AttributeGroupID	INT IDENTITY(1,1) PRIMARY KEY,
	AttributeGroupName	VARCHAR(50) UNIQUE,
	IsSearchable		BIT DEFAULT(0)
)
GO

CREATE TABLE Attribute
(
	AttributeID			INT IDENTITY(1,1) PRIMARY KEY,
	AttributeValue		VARCHAR(50),
	AttributeGroupID	INT,
	CONSTRAINT FK_AttributeGroup FOREIGN KEY (AttributeGroupID) REFERENCES AttributeGroup(AttributeGroupID)
)
GO

CREATE TABLE ProductCol
(
	ColID				INT IDENTITY(1,1) PRIMARY KEY,
	ColName				VARCHAR(100) UNIQUE,
	ColBanner			VARCHAR(300),
	ColImage			VARCHAR(300),
	ColDesc				TEXT,
	IsTrending			BIT,
	IsActive			BIT
)
GO

CREATE TABLE ProductVoucher
(
	VoucherID			VARCHAR(20) PRIMARY KEY,
	VoucherDesc			TEXT,
	DiscountValue		INT,
	Quantity			INT,
	ValidFrom			DATETIME,
	ValidTo				DATETIME
)
GO

CREATE TABLE Product
(
	ProductID			INT IDENTITY(1,1) PRIMARY KEY,
	ProductName			VARCHAR(100),
	ProductDesc			TEXT,
	IsActive			BIT DEFAULT(1),
	CategoryID			INT,
	ColID				INT,
	DiscountValue		INT DEFAULT(0),
	DiscountValidFrom	DATETIME,
	DiscountValidTo		DATETIME,
	DateCreated			DATETIME DEFAULT(CURRENT_TIMESTAMP),
	DateUpdated			DATETIME DEFAULT(CURRENT_TIMESTAMP),
	CONSTRAINT FK_ProductCategory FOREIGN KEY (CategoryID) REFERENCES Category(CategoryID),
	CONSTRAINT FK_ProductCollection FOREIGN KEY (ColID) REFERENCES ProductCol(ColID)
)
GO

CREATE TABLE ProductAttribute
(
	ProductID			INT,
	AttributeID			INT,
	PRIMARY KEY (ProductID, AttributeID),
	CONSTRAINT FK_ProductAttributeLink FOREIGN KEY (ProductID) REFERENCES Product(ProductID),
	CONSTRAINT FK_AttributeProductLink FOREIGN KEY (AttributeID) REFERENCES Attribute(AttributeID)
)
GO

CREATE TABLE ProductImage
(
	ImageID				INT IDENTITY(1,1) PRIMARY KEY,
	ProductID			INT,
	ImageLink			VARCHAR(300),
	CONSTRAINT FK_ImageProduct FOREIGN KEY (ProductID) REFERENCES Product(ProductID)
)
GO

CREATE TABLE SkuSize
(
	SizeID				INT IDENTITY(1,1) PRIMARY KEY,
	SizeValue			VARCHAR(5),
	CategoryID			INT,
	CONSTRAINT FK_SizeCategory FOREIGN KEY (CategoryID) REFERENCES Category(CategoryID)
) 
GO

CREATE TABLE Sku
(
	SkuID				VARCHAR(20) PRIMARY KEY,
	ProductID			INT,
	SizeID				INT,
	BasePrice			INT,
	UnitPrice			INT,
	UnitsInStock		INT,
	UnitsOnOrder		INT,
	DateCreated			DATETIME DEFAULT(CURRENT_TIMESTAMP),
	DateUpdated			DATETIME DEFAULT(CURRENT_TIMESTAMP),
	CONSTRAINT FK_SkuProduct FOREIGN KEY (ProductID) REFERENCES Product(ProductID),
	CONSTRAINT FK_SkuSize FOREIGN KEY (SizeID) REFERENCES SkuSize(SizeID)
)
GO

CREATE TABLE Review
(
	ProductID			INT,
	CustomerID			INT,
	Title				VARCHAR(100),
	Content				TEXT,
	Rating				FLOAT,
	AdminResponse		TEXT,
	DateCreated			DATETIME DEFAULT(CURRENT_TIMESTAMP),
	DateUpdated			DATETIME DEFAULT(CURRENT_TIMESTAMP),
	PRIMARY KEY (ProductID, CustomerID),
	CONSTRAINT FK_ReviewProduct FOREIGN KEY (ProductID) REFERENCES Product(ProductID),
	CONSTRAINT FK_ReviewCustomer FOREIGN KEY (CustomerID) REFERENCES Customer(CustomerID)
)
GO

CREATE TABLE Bookmark
(
	CustomerID			INT,
	ProductID			INT,
	DateCreated			DATETIME DEFAULT(CURRENT_TIMESTAMP),
	PRIMARY KEY (CustomerID, ProductID),
	CONSTRAINT FK_BookmarkCustomer FOREIGN KEY (CustomerID) REFERENCES Customer(CustomerID),
	CONSTRAINT FK_BookmarkProduct FOREIGN KEY (ProductID) REFERENCES Product(ProductID)
)
GO

CREATE TABLE OrderMaster
(
	OrderID				INT IDENTITY(1,1) PRIMARY KEY,
	CustomerID			INT,
	VoucherID			VARCHAR(20),
	ShipDate			DATE,
	ShipFee				INT,
	ShipName			VARCHAR(100),
	ShipAddress			TEXT,
	ShipPhone			VARCHAR(20),
	ShipEmail			VARCHAR(50),
	Payment				VARCHAR(10),
	DateCreated			DATETIME DEFAULT(CURRENT_TIMESTAMP),
	DateUpdated			DATETIME DEFAULT(CURRENT_TIMESTAMP),
	OrderStatus			VARCHAR(20), --- pending, cancelled, completed, confirmed, packaging
	CONSTRAINT FK_OrderCustomer FOREIGN KEY (CustomerID) REFERENCES Customer(CustomerID),
	CONSTRAINT FK_OrderVoucher FOREIGN KEY (VoucherID) REFERENCES ProductVoucher(VoucherID)
)
GO

CREATE TABLE OrderDetail
(
	SkuID				VARCHAR(20),
	UnitPrice			INT,
	DiscountValue		INT,
	Quantity			INT,
	OrderID				INT,
	PRIMARY KEY (SkuID, OrderID),
	CONSTRAINT FK_OrderDetailSku FOREIGN KEY (SkuID) REFERENCES Sku(SkuID),
	CONSTRAINT FK_OrderDetailMaster FOREIGN KEY (OrderID) REFERENCES OrderMaster(OrderID)
)
GO

CREATE TABLE AdminLog
(
	LogID				INT IDENTITY(1,1) PRIMARY KEY,
	Username			VARCHAR(20),
	LogContent			TEXT,
	DateLogged			DATETIME DEFAULT(CURRENT_TIMESTAMP)
)
GO

---------------------------------------------------------------------------

-- ----------------------------
-- Records of AdminAccount
-- ----------------------------
SET IDENTITY_INSERT [dbo].[AdminAccount] ON
GO
INSERT INTO [dbo].[AdminAccount] ([AdminID], [AdminUserName], [AdminPassword], [AdminRole], [DateCreated], [DateUpdated]) VALUES (N'1', N'admin1', N'4297f44b13955235245b2497399d7a93', N'SuperAdmin', N'2018-03-28 15:24:32.467', N'2018-03-28 15:24:32.467')
GO
GO
INSERT INTO [dbo].[AdminAccount] ([AdminID], [AdminUserName], [AdminPassword], [AdminRole], [DateCreated], [DateUpdated]) VALUES (N'2', N'admin2', N'4297f44b13955235245b2497399d7a93', N'SuperAdmin', N'2018-03-28 15:24:32.467', N'2018-03-28 15:24:32.467')
GO
GO
INSERT INTO [dbo].[AdminAccount] ([AdminID], [AdminUserName], [AdminPassword], [AdminRole], [DateCreated], [DateUpdated]) VALUES (N'3', N'huephuong', N'4297f44b13955235245b2497399d7a93', N'Admin', N'2018-03-28 15:24:32.467', N'2018-03-28 15:24:32.467')
GO
GO
INSERT INTO [dbo].[AdminAccount] ([AdminID], [AdminUserName], [AdminPassword], [AdminRole], [DateCreated], [DateUpdated]) VALUES (N'4', N'tanphat', N'4297f44b13955235245b2497399d7a93', N'Admin', N'2018-03-28 15:24:32.467', N'2018-03-28 15:24:32.467')
GO
GO
INSERT INTO [dbo].[AdminAccount] ([AdminID], [AdminUserName], [AdminPassword], [AdminRole], [DateCreated], [DateUpdated]) VALUES (N'5', N'khainguyen', N'4297f44b13955235245b2497399d7a93', N'Admin', N'2018-03-28 15:24:32.467', N'2018-03-28 15:24:32.467')
GO
GO
INSERT INTO [dbo].[AdminAccount] ([AdminID], [AdminUserName], [AdminPassword], [AdminRole], [DateCreated], [DateUpdated]) VALUES (N'6', N'tester1', N'4297f44b13955235245b2497399d7a93', N'SuperAdmin', N'2018-03-28 15:24:32.467', N'2018-03-28 15:24:32.467')
GO
GO
INSERT INTO [dbo].[AdminAccount] ([AdminID], [AdminUserName], [AdminPassword], [AdminRole], [DateCreated], [DateUpdated]) VALUES (N'7', N'tester2', N'4297f44b13955235245b2497399d7a93', N'Admin', N'2018-03-28 15:24:32.467', N'2018-03-28 15:24:32.467')
GO
GO
SET IDENTITY_INSERT [dbo].[AdminAccount] OFF
GO

-- ----------------------------
-- Records of Customer
-- ----------------------------
SET IDENTITY_INSERT [dbo].[Customer] ON
GO
INSERT INTO [dbo].[Customer] ([CustomerID], [CustomerUserName], [CustomerPassword], [CustomerAvatar], [FirstName], [LastName], [Email], [Gender], [Phone1], [Phone2], [DateOfBirth], [IsActive], [DateCreated], [DateUpdated], [ResetToken]) VALUES ('1', 'huephuong', '4297f44b13955235245b2497399d7a93', 'ava.jpg', 'Hue Phuong', 'Du', 'huephuong@hotmail.com', 'female', '0901122345', '028384367', '1993-04-03', '1', '2018-03-28 15:24:32.467', '2018-03-28 15:24:32.467', null);
INSERT INTO [dbo].[Customer] ([CustomerID], [CustomerUserName], [CustomerPassword], [CustomerAvatar], [FirstName], [LastName], [Email], [Gender], [Phone1], [Phone2], [DateOfBirth], [IsActive], [DateCreated], [DateUpdated], [ResetToken]) VALUES ('2', 'tanphat', '4297f44b13955235245b2497399d7a93', 'ava.jpg', 'Tan Phat', 'Huynh', 'phat@gmail.com', 'male', '0904326442', null, '1995-06-11', '1', '2018-03-28 15:25:26.650', '2018-03-28 15:25:26.650', null);
INSERT INTO [dbo].[Customer] ([CustomerID], [CustomerUserName], [CustomerPassword], [CustomerAvatar], [FirstName], [LastName], [Email], [Gender], [Phone1], [Phone2], [DateOfBirth], [IsActive], [DateCreated], [DateUpdated], [ResetToken]) VALUES ('3', 'khainguyen', '4297f44b13955235245b2497399d7a93', 'ava.jpg', 'Khai Nguyen', 'Nguyen', 'nguyen@yahoo.com', 'male', '09011223344', '08273734627', '1996-06-04', '1', '2018-03-28 15:27:15.630', '2018-03-28 15:27:15.630', null);
INSERT INTO [dbo].[Customer] ([CustomerID], [CustomerUserName], [CustomerPassword], [CustomerAvatar], [FirstName], [LastName], [Email], [Gender], [Phone1], [Phone2], [DateOfBirth], [IsActive], [DateCreated], [DateUpdated], [ResetToken]) VALUES ('4', 'naruto', '4297f44b13955235245b2497399d7a93', 'ava.jpg', 'Naruto', 'Uzumaki', 'naruto@gmail.com', 'male', '09087654673', null, '1997-10-10', '0', '2018-03-28 15:28:02.590', '2018-03-28 15:28:02.590', null);
INSERT INTO [dbo].[Customer] ([CustomerID], [CustomerUserName], [CustomerPassword], [CustomerAvatar], [FirstName], [LastName], [Email], [Gender], [Phone1], [Phone2], [DateOfBirth], [IsActive], [DateCreated], [DateUpdated], [ResetToken]) VALUES ('5', 'sakura', '4297f44b13955235245b2497399d7a93', 'ava.jpg', 'Sakura', 'Haruno', 'sakura@hotmail.com', 'female', '0903341777', '0823342666', '1997-12-25', '0', '2018-03-28 15:28:46.373', '2018-03-28 15:28:46.373', null);
INSERT INTO [dbo].[Customer] ([CustomerID], [CustomerUserName], [CustomerPassword], [CustomerAvatar], [FirstName], [LastName], [Email], [Gender], [Phone1], [Phone2], [DateOfBirth], [IsActive], [DateCreated], [DateUpdated], [ResetToken]) VALUES ('6', 'sontung', '4297f44b13955235245b2497399d7a93', 'ava.jpg', 'Son Tung', 'MPT', 'sky@yahoo.com', 'male', '09011332211', null, '1994-12-09', '1', '2018-03-28 15:30:18.250', '2018-03-28 15:30:18.250', null);
INSERT INTO [dbo].[Customer] ([CustomerID], [CustomerUserName], [CustomerPassword], [CustomerAvatar], [FirstName], [LastName], [Email], [Gender], [Phone1], [Phone2], [DateOfBirth], [IsActive], [DateCreated], [DateUpdated], [ResetToken]) VALUES ('7', 'mytam', '4297f44b13955235245b2497399d7a93', 'ava.jpg', 'My Tam', 'Ca Si', 'mytam@gmail.com', 'female', '091475627364', null, '1980-01-01', '1', '2018-03-28 15:31:15.797', '2018-03-28 15:31:15.797', null);
INSERT INTO [dbo].[Customer] ([CustomerID], [CustomerUserName], [CustomerPassword], [CustomerAvatar], [FirstName], [LastName], [Email], [Gender], [Phone1], [Phone2], [DateOfBirth], [IsActive], [DateCreated], [DateUpdated], [ResetToken]) VALUES ('8', 'JackyChan', '4297f44b13955235245b2497399d7a93', 'ava.jpg', 'Jacky', 'Chan', 'jackychan@hotmail.com', 'female', '0981234567', null, '1975-02-02', '1', '2018-03-28 15:32:33.330', '2018-03-28 15:32:33.330', null);
INSERT INTO [dbo].[Customer] ([CustomerID], [CustomerUserName], [CustomerPassword], [CustomerAvatar], [FirstName], [LastName], [Email], [Gender], [Phone1], [Phone2], [DateOfBirth], [IsActive], [DateCreated], [DateUpdated], [ResetToken]) VALUES ('9', 'RockLee', '4297f44b13955235245b2497399d7a93', 'ava.jpg', 'Rock', 'Lee', 'lee@gmail.com', 'male', '0937766555', null, '1990-09-20', '1', '2018-03-28 15:33:25.740', '2018-03-28 15:33:25.740', null);
INSERT INTO [dbo].[Customer] ([CustomerID], [CustomerUserName], [CustomerPassword], [CustomerAvatar], [FirstName], [LastName], [Email], [Gender], [Phone1], [Phone2], [DateOfBirth], [IsActive], [DateCreated], [DateUpdated], [ResetToken]) VALUES ('10', 'hohohaha', '4297f44b13955235245b2497399d7a93', 'ava.jpg', 'Ha', 'Ho', 'hohohaha@yahoo.com', 'female', '09215645643', null, '1950-05-25', '1', '2018-03-28 15:35:01.180', '2018-03-28 15:35:01.180', null);
INSERT INTO [dbo].[Customer] ([CustomerID], [CustomerUserName], [CustomerPassword], [CustomerAvatar], [FirstName], [LastName], [Email], [Gender], [Phone1], [Phone2], [DateOfBirth], [IsActive], [DateCreated], [DateUpdated], [ResetToken]) VALUES ('11', 'Adam', '4297f44b13955235245b2497399d7a93', 'ava.jpg', 'Adam', 'Lambert', 'adamlambert@yahoo.com', 'male', '0921357654', null, '1982-01-29', '1', '2018-04-02 15:35:01.180', '2018-04-02 15:35:01.180', null);
INSERT INTO [dbo].[Customer] ([CustomerID], [CustomerUserName], [CustomerPassword], [CustomerAvatar], [FirstName], [LastName], [Email], [Gender], [Phone1], [Phone2], [DateOfBirth], [IsActive], [DateCreated], [DateUpdated], [ResetToken]) VALUES ('12', 'Kris', '4297f44b13955235245b2497399d7a93', 'ava.jpg', 'Kris', 'Allen', 'krisallen@gmail.com', 'female', '093645645', null, '1983-06-17', '1', '2018-04-20 15:35:01.180', '2018-04-20 15:35:01.180', null);
INSERT INTO [dbo].[Customer] ([CustomerID], [CustomerUserName], [CustomerPassword], [CustomerAvatar], [FirstName], [LastName], [Email], [Gender], [Phone1], [Phone2], [DateOfBirth], [IsActive], [DateCreated], [DateUpdated], [ResetToken]) VALUES ('13', 'Drake', '4297f44b13955235245b2497399d7a93', 'ava.jpg', 'Drake', 'Labry', 'drakelabry@hotmail.com', 'male', '09215645641', null, '1983-07-29', '1', '2018-04-20 15:35:01.180', '2018-04-20 15:35:01.180', null);
INSERT INTO [dbo].[Customer] ([CustomerID], [CustomerUserName], [CustomerPassword], [CustomerAvatar], [FirstName], [LastName], [Email], [Gender], [Phone1], [Phone2], [DateOfBirth], [IsActive], [DateCreated], [DateUpdated], [ResetToken]) VALUES ('14', 'Brad', '4297f44b13955235245b2497399d7a93', 'ava.jpg', 'Brad', 'Bell', 'brad@hotmail.com', 'female', '0921563645', null, '1983-02-24', '1', '2018-03-28 15:35:01.180', '2018-03-28 15:35:01.180', null);
INSERT INTO [dbo].[Customer] ([CustomerID], [CustomerUserName], [CustomerPassword], [CustomerAvatar], [FirstName], [LastName], [Email], [Gender], [Phone1], [Phone2], [DateOfBirth], [IsActive], [DateCreated], [DateUpdated], [ResetToken]) VALUES ('15', 'Tommy', '4297f44b13955235245b2497399d7a93', 'ava.jpg', 'Tommy Joe', 'Ratliff', 'tommy@gmail.com', 'female', '55564678', null, '1981-10-03', '1', '2018-02-28 15:35:01.180', '2018-02-28 15:35:01.180', null);
INSERT INTO [dbo].[Customer] ([CustomerID], [CustomerUserName], [CustomerPassword], [CustomerAvatar], [FirstName], [LastName], [Email], [Gender], [Phone1], [Phone2], [DateOfBirth], [IsActive], [DateCreated], [DateUpdated], [ResetToken]) VALUES ('16', 'ProfX', '4297f44b13955235245b2497399d7a93', 'ava.jpg', 'James', 'McAvoy', 'xavier@yahoo.com', 'male', '081564564', null, '1948-01-09', '1', '2018-02-28 15:35:01.180', '2018-02-28 15:35:01.180', null);
INSERT INTO [dbo].[Customer] ([CustomerID], [CustomerUserName], [CustomerPassword], [CustomerAvatar], [FirstName], [LastName], [Email], [Gender], [Phone1], [Phone2], [DateOfBirth], [IsActive], [DateCreated], [DateUpdated], [ResetToken]) VALUES ('17', 'Magneto', '4297f44b13955235245b2497399d7a93', 'ava.jpg', 'Michael', 'Fassbender', 'magneto@yahoo.com', 'male', '0755645645', null, '1945-09-12', '1', '2018-03-28 15:35:01.180', '2018-03-28 15:35:01.180', null);
INSERT INTO [dbo].[Customer] ([CustomerID], [CustomerUserName], [CustomerPassword], [CustomerAvatar], [FirstName], [LastName], [Email], [Gender], [Phone1], [Phone2], [DateOfBirth], [IsActive], [DateCreated], [DateUpdated], [ResetToken]) VALUES ('18', 'loki', '4297f44b13955235245b2497399d7a93', 'ava.jpg', 'Tom', 'Hiddleston', 'loki@asgard.com', 'male', '035645649', null, '1994-05-25', '1', '2018-03-28 15:35:01.180', '2018-03-28 15:35:01.180', null);
INSERT INTO [dbo].[Customer] ([CustomerID], [CustomerUserName], [CustomerPassword], [CustomerAvatar], [FirstName], [LastName], [Email], [Gender], [Phone1], [Phone2], [DateOfBirth], [IsActive], [DateCreated], [DateUpdated], [ResetToken]) VALUES ('19', 'thor', '4297f44b13955235245b2497399d7a93', 'ava.jpg', 'Chris', 'Hemsworth', 'thor@asgard.com', 'male', '024564567', null, '1992-02-21', '1', '2018-04-28 15:35:01.180', '2018-04-28 15:35:01.180', null);
INSERT INTO [dbo].[Customer] ([CustomerID], [CustomerUserName], [CustomerPassword], [CustomerAvatar], [FirstName], [LastName], [Email], [Gender], [Phone1], [Phone2], [DateOfBirth], [IsActive], [DateCreated], [DateUpdated], [ResetToken]) VALUES ('20', 'donny', '4297f44b13955235245b2497399d7a93', 'ava.jpg', 'Donald', 'Trump', 'donny@yahoo.com', 'female', '0921564876', null, '1939-07-20', '0', '2018-03-28 15:35:01.180', '2018-03-28 15:35:01.180', null);
SET IDENTITY_INSERT [dbo].[Customer] OFF
GO

-- ----------------------------
-- Records of CustomerAddress
-- ----------------------------
SET IDENTITY_INSERT [dbo].[CustomerAddress] ON
GO
INSERT INTO [dbo].[CustomerAddress] ([AddressID], [CustomerID], [Number], [Street], [Ward], [District], [City]) VALUES (N'1', N'1', N'134', N'tran quang khai ', N'Tan Dinh', N'1', N'HCM')
GO
GO
INSERT INTO [dbo].[CustomerAddress] ([AddressID], [CustomerID], [Number], [Street], [Ward], [District], [City]) VALUES (N'2', N'1', N'14', N'Dien Bien Phu ', N'2', N'3', N'HCM')
GO
GO
INSERT INTO [dbo].[CustomerAddress] ([AddressID], [CustomerID], [Number], [Street], [Ward], [District], [City]) VALUES (N'3', N'2', N'22', N'Nguyen Huu Tho', N'Tan Phong', N'7', N'HCM')
GO
GO
INSERT INTO [dbo].[CustomerAddress] ([AddressID], [CustomerID], [Number], [Street], [Ward], [District], [City]) VALUES (N'4', N'2', N'234', N'Nguyen Van Linh', N'Phuoc Kien', N'Nha Be', N'HCM')
GO
GO
INSERT INTO [dbo].[CustomerAddress] ([AddressID], [CustomerID], [Number], [Street], [Ward], [District], [City]) VALUES (N'5', N'3', N'31', N'Vo thi sau', N'3', N'3', N'HCM')
GO
GO
INSERT INTO [dbo].[CustomerAddress] ([AddressID], [CustomerID], [Number], [Street], [Ward], [District], [City]) VALUES (N'6', N'3', N'398', N'Tran Hung Dao', N'4', N'5', N'HCM')
GO
GO
INSERT INTO [dbo].[CustomerAddress] ([AddressID], [CustomerID], [Number], [Street], [Ward], [District], [City]) VALUES (N'7', N'4', N'486', N'Hoang Dieu', N'5', N'4', N'HCM')
GO
GO
INSERT INTO [dbo].[CustomerAddress] ([AddressID], [CustomerID], [Number], [Street], [Ward], [District], [City]) VALUES (N'8', N'5', N'52', N'Nguyen Trai', N'10', N'5', N'HCM')
GO
GO
INSERT INTO [dbo].[CustomerAddress] ([AddressID], [CustomerID], [Number], [Street], [Ward], [District], [City]) VALUES (N'9', N'6', N'61', N'Hong Bang', null, N'6', N'HCM')
GO
GO
INSERT INTO [dbo].[CustomerAddress] ([AddressID], [CustomerID], [Number], [Street], [Ward], [District], [City]) VALUES (N'10', N'7', N'19/8', N'Nguyen Huu Tho', N'Tan Phong', N'7', N'HCM')
GO
GO
INSERT INTO [dbo].[CustomerAddress] ([AddressID], [CustomerID], [Number], [Street], [Ward], [District], [City]) VALUES (N'11', N'8', N'82', N'Duong Ba Trac', N'9', N'8', N'HCM')
GO
GO
INSERT INTO [dbo].[CustomerAddress] ([AddressID], [CustomerID], [Number], [Street], [Ward], [District], [City]) VALUES (N'12', N'9', N'9E', N'Duong Dinh Hoi', N'2', N'9', N'HCM')
GO
GO
INSERT INTO [dbo].[CustomerAddress] ([AddressID], [CustomerID], [Number], [Street], [Ward], [District], [City]) VALUES (N'13', N'10', N'10bis', N'3 thang 2', N'7', N'10', N'HCM')
GO
GO
INSERT INTO [dbo].[CustomerAddress] ([AddressID], [CustomerID], [Number], [Street], [Ward], [District], [City]) VALUES (N'14', N'11', N'25', N'Tan Hung', N'12', N'5', N'HCM')
GO
GO
INSERT INTO [dbo].[CustomerAddress] ([AddressID], [CustomerID], [Number], [Street], [Ward], [District], [City]) VALUES (N'15', N'12', N'148', N'Nam Ky Khoi Nghia', N'Ben Nghe', N'1', N'HCM')
GO
GO
INSERT INTO [dbo].[CustomerAddress] ([AddressID], [CustomerID], [Number], [Street], [Ward], [District], [City]) VALUES (N'16', N'13', N'437/2/4', N'3 thang 2', N'7', N'10', N'HCM')
GO
GO
INSERT INTO [dbo].[CustomerAddress] ([AddressID], [CustomerID], [Number], [Street], [Ward], [District], [City]) VALUES (N'17', N'14', N'12', N'Quang Trung', N'Quang Trung', N'11', N'HCM')
GO
GO
INSERT INTO [dbo].[CustomerAddress] ([AddressID], [CustomerID], [Number], [Street], [Ward], [District], [City]) VALUES (N'18', N'15', N'22b', N'Ho Van Hue', N'7', N'10', N'HCM')
GO
GO
INSERT INTO [dbo].[CustomerAddress] ([AddressID], [CustomerID], [Number], [Street], [Ward], [District], [City]) VALUES (N'19', N'16', N'159', N'Ho Thi Ky', N'3', N'10', N'HCM')
GO
GO
INSERT INTO [dbo].[CustomerAddress] ([AddressID], [CustomerID], [Number], [Street], [Ward], [District], [City]) VALUES (N'20', N'17', N'98', N'Nguyen Van Troi', N'3', N'1', N'HCM')
GO
GO
INSERT INTO [dbo].[CustomerAddress] ([AddressID], [CustomerID], [Number], [Street], [Ward], [District], [City]) VALUES (N'21', N'18', N'182', N'Hai Ba Trung', N'Ben Nghe', N'1', N'HCM')
GO
GO
INSERT INTO [dbo].[CustomerAddress] ([AddressID], [CustomerID], [Number], [Street], [Ward], [District], [City]) VALUES (N'22', N'19', N'90/12', N'Luong Huu Khanh', N'12', N'1', N'HCM')
GO
GO
INSERT INTO [dbo].[CustomerAddress] ([AddressID], [CustomerID], [Number], [Street], [Ward], [District], [City]) VALUES (N'23', N'20', N'390/15', N'Ba Trieu', N'1', N'8', N'HCM')
GO
GO
SET IDENTITY_INSERT [dbo].[CustomerAddress] OFF
GO

-- ----------------------------
-- Records of Category
-- ----------------------------
SET IDENTITY_INSERT [dbo].[Category] ON
GO
INSERT INTO [dbo].[Category] ([CategoryID], [CategoryName], [ParentID]) VALUES (N'1', N'Rings', null)
GO
GO
INSERT INTO [dbo].[Category] ([CategoryID], [CategoryName], [ParentID]) VALUES (N'2', N'Bracelets', null)
GO
GO
INSERT INTO [dbo].[Category] ([CategoryID], [CategoryName], [ParentID]) VALUES (N'3', N'Necklaces', null)
GO
GO
INSERT INTO [dbo].[Category] ([CategoryID], [CategoryName], [ParentID]) VALUES (N'4', N'Earrings', null)
GO
GO
INSERT INTO [dbo].[Category] ([CategoryID], [CategoryName], [ParentID]) VALUES (N'5', N'Wedding Rings', N'1')
GO
GO
INSERT INTO [dbo].[Category] ([CategoryID], [CategoryName], [ParentID]) VALUES (N'6', N'Engagement Rings', N'1')
GO
GO
INSERT INTO [dbo].[Category] ([CategoryID], [CategoryName], [ParentID]) VALUES (N'7', N'Solitaire Rings', N'1')
GO
GO
INSERT INTO [dbo].[Category] ([CategoryID], [CategoryName], [ParentID]) VALUES (N'8', N'Multistone Rings', N'1')
GO
GO
INSERT INTO [dbo].[Category] ([CategoryID], [CategoryName], [ParentID]) VALUES (N'9', N'Bands', N'1')
GO
GO
INSERT INTO [dbo].[Category] ([CategoryID], [CategoryName], [ParentID]) VALUES (N'10', N'Pendants', N'3')
GO
GO
INSERT INTO [dbo].[Category] ([CategoryID], [CategoryName], [ParentID]) VALUES (N'11', N'Chain Necklaces', N'3')
GO
GO
INSERT INTO [dbo].[Category] ([CategoryID], [CategoryName], [ParentID]) VALUES (N'12', N'Drop Earrings', N'4')
GO
GO
INSERT INTO [dbo].[Category] ([CategoryID], [CategoryName], [ParentID]) VALUES (N'13', N'Stud Earrings', N'4')
GO
GO
INSERT INTO [dbo].[Category] ([CategoryID], [CategoryName], [ParentID]) VALUES (N'14', N'Hoop Earrings', N'4')
GO
GO
INSERT INTO [dbo].[Category] ([CategoryID], [CategoryName], [ParentID]) VALUES (N'15', N'Beaded Bracelets', N'2')
GO
GO
INSERT INTO [dbo].[Category] ([CategoryID], [CategoryName], [ParentID]) VALUES (N'17', N'Charm Bracelets', N'2')
GO
GO
INSERT INTO [dbo].[Category] ([CategoryID], [CategoryName], [ParentID]) VALUES (N'18', N'Cuff Bracelets', N'2')
GO
GO
INSERT INTO [dbo].[Category] ([CategoryID], [CategoryName], [ParentID]) VALUES (N'19', N'Bangles Bracelets', N'2')
GO
GO
INSERT INTO [dbo].[Category] ([CategoryID], [CategoryName], [ParentID]) VALUES (N'20', N'Wedding Necklaces', N'3')
GO
GO
SET IDENTITY_INSERT [dbo].[Category] OFF
GO

-- ----------------------------
-- Records of ProductCol
-- ----------------------------
SET IDENTITY_INSERT [dbo].[ProductCol] ON
GO
INSERT INTO [dbo].[ProductCol] ([ColID], [ColName], [ColBanner], [ColImage], [ColDesc], [IsTrending], [IsActive]) VALUES (N'1', N'Midnight Lover', N'colBanner7.jpg', N'colImage.jpg', N'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec hendrerit sollicitudin euismod. Vestibulum at ipsum nec diam imperdiet tempus at vel quam. Nunc est ligula, gravida consectetur pulvinar sit amet, tincidunt vitae tortor.', N'1', N'1')
GO
GO
INSERT INTO [dbo].[ProductCol] ([ColID], [ColName], [ColBanner], [ColImage], [ColDesc], [IsTrending], [IsActive]) VALUES (N'2', N'Flower Fairy', N'colBanner4.jpg', N'colImage.jpg', N'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec hendrerit sollicitudin euismod. Vestibulum at ipsum nec diam imperdiet tempus at vel quam. Nunc est ligula, gravida consectetur pulvinar sit amet, tincidunt vitae tortor.', N'1', N'1')
GO
GO
INSERT INTO [dbo].[ProductCol] ([ColID], [ColName], [ColBanner], [ColImage], [ColDesc], [IsTrending], [IsActive]) VALUES (N'3', N'Innocent Time', N'colBanner5.png', N'colImage.jpg', N'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec hendrerit sollicitudin euismod. Vestibulum at ipsum nec diam imperdiet tempus at vel quam. Nunc est ligula, gravida consectetur pulvinar sit amet, tincidunt vitae tortor.', N'1', N'1')
GO
GO
INSERT INTO [dbo].[ProductCol] ([ColID], [ColName], [ColBanner], [ColImage], [ColDesc], [IsTrending], [IsActive]) VALUES (N'4', N'Fierce Fire', N'colBanner3.png', N'colImage.jpg', N'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec hendrerit sollicitudin euismod. Vestibulum at ipsum nec diam imperdiet tempus at vel quam. Nunc est ligula, gravida consectetur pulvinar sit amet, tincidunt vitae tortor.', N'0', N'1')
GO
GO
INSERT INTO [dbo].[ProductCol] ([ColID], [ColName], [ColBanner], [ColImage], [ColDesc], [IsTrending], [IsActive]) VALUES (N'5', N'Bridal Dream', N'colBanner6.jpg', N'colImage.jpg', N'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec hendrerit sollicitudin euismod. Vestibulum at ipsum nec diam imperdiet tempus at vel quam. Nunc est ligula, gravida consectetur pulvinar sit amet, tincidunt vitae tortor.', N'1', N'1')
GO
GO
SET IDENTITY_INSERT [dbo].[ProductCol] OFF
GO

-- ----------------------------
-- Records of AttributeGroup
-- ----------------------------
SET IDENTITY_INSERT [dbo].[AttributeGroup] ON
GO
INSERT INTO [dbo].[AttributeGroup] ([AttributeGroupID], [AttributeGroupName], [IsSearchable]) VALUES (N'7', N'Gender', N'1')
GO
GO
INSERT INTO [dbo].[AttributeGroup] ([AttributeGroupID], [AttributeGroupName], [IsSearchable]) VALUES (N'1', N'Metal', N'1')
GO
GO
INSERT INTO [dbo].[AttributeGroup] ([AttributeGroupID], [AttributeGroupName], [IsSearchable]) VALUES (N'2', N'Primary Gem', N'1')
GO
GO
INSERT INTO [dbo].[AttributeGroup] ([AttributeGroupID], [AttributeGroupName], [IsSearchable]) VALUES (N'4', N'Primary Gem Color', N'1')
GO
GO
INSERT INTO [dbo].[AttributeGroup] ([AttributeGroupID], [AttributeGroupName], [IsSearchable]) VALUES (N'3', N'Primary Gem Cut', N'1')
GO
GO
INSERT INTO [dbo].[AttributeGroup] ([AttributeGroupID], [AttributeGroupName], [IsSearchable]) VALUES (N'5', N'Secondary Gem', N'0')
GO
GO
INSERT INTO [dbo].[AttributeGroup] ([AttributeGroupID], [AttributeGroupName], [IsSearchable]) VALUES (N'6', N'Secondary Gem Color', N'0')
GO
GO
SET IDENTITY_INSERT [dbo].[AttributeGroup] OFF
GO

-- ----------------------------
-- Records of Attribute
-- ----------------------------
SET IDENTITY_INSERT [dbo].[Attribute] ON
GO
INSERT INTO [dbo].[Attribute] ([AttributeID], [AttributeValue], [AttributeGroupID]) VALUES (N'1', N'Gold 18k', N'1')
GO
GO
INSERT INTO [dbo].[Attribute] ([AttributeID], [AttributeValue], [AttributeGroupID]) VALUES (N'2', N'Gold 24k', N'1')
GO
GO
INSERT INTO [dbo].[Attribute] ([AttributeID], [AttributeValue], [AttributeGroupID]) VALUES (N'3', N'Rose Gold', N'1')
GO
GO
INSERT INTO [dbo].[Attribute] ([AttributeID], [AttributeValue], [AttributeGroupID]) VALUES (N'4', N'Silver', N'1')
GO
GO
INSERT INTO [dbo].[Attribute] ([AttributeID], [AttributeValue], [AttributeGroupID]) VALUES (N'5', N'Platinum', N'1')
GO
GO
INSERT INTO [dbo].[Attribute] ([AttributeID], [AttributeValue], [AttributeGroupID]) VALUES (N'6', N'Diamond', N'2')
GO
GO
INSERT INTO [dbo].[Attribute] ([AttributeID], [AttributeValue], [AttributeGroupID]) VALUES (N'7', N'Ruby', N'2')
GO
GO
INSERT INTO [dbo].[Attribute] ([AttributeID], [AttributeValue], [AttributeGroupID]) VALUES (N'8', N'Saphire', N'2')
GO
GO
INSERT INTO [dbo].[Attribute] ([AttributeID], [AttributeValue], [AttributeGroupID]) VALUES (N'9', N'Emerald', N'2')
GO
GO
INSERT INTO [dbo].[Attribute] ([AttributeID], [AttributeValue], [AttributeGroupID]) VALUES (N'10', N'Aquamarine', N'2')
GO
GO
INSERT INTO [dbo].[Attribute] ([AttributeID], [AttributeValue], [AttributeGroupID]) VALUES (N'11', N'Pearl', N'2')
GO
GO
INSERT INTO [dbo].[Attribute] ([AttributeID], [AttributeValue], [AttributeGroupID]) VALUES (N'12', N'Round', N'3')
GO
GO
INSERT INTO [dbo].[Attribute] ([AttributeID], [AttributeValue], [AttributeGroupID]) VALUES (N'13', N'Oval', N'3')
GO
GO
INSERT INTO [dbo].[Attribute] ([AttributeID], [AttributeValue], [AttributeGroupID]) VALUES (N'14', N'Heart', N'3')
GO
GO
INSERT INTO [dbo].[Attribute] ([AttributeID], [AttributeValue], [AttributeGroupID]) VALUES (N'15', N'Pear', N'3')
GO
GO
INSERT INTO [dbo].[Attribute] ([AttributeID], [AttributeValue], [AttributeGroupID]) VALUES (N'16', N'Heart', N'3')
GO
GO
INSERT INTO [dbo].[Attribute] ([AttributeID], [AttributeValue], [AttributeGroupID]) VALUES (N'17', N'Princess', N'3')
GO
GO
INSERT INTO [dbo].[Attribute] ([AttributeID], [AttributeValue], [AttributeGroupID]) VALUES (N'18', N'White', N'4')
GO
GO
INSERT INTO [dbo].[Attribute] ([AttributeID], [AttributeValue], [AttributeGroupID]) VALUES (N'19', N'Red', N'4')
GO
GO
INSERT INTO [dbo].[Attribute] ([AttributeID], [AttributeValue], [AttributeGroupID]) VALUES (N'20', N'Blue', N'4')
GO
GO
INSERT INTO [dbo].[Attribute] ([AttributeID], [AttributeValue], [AttributeGroupID]) VALUES (N'21', N'Green', N'4')
GO
GO
INSERT INTO [dbo].[Attribute] ([AttributeID], [AttributeValue], [AttributeGroupID]) VALUES (N'22', N'Yellow', N'4')
GO
GO
INSERT INTO [dbo].[Attribute] ([AttributeID], [AttributeValue], [AttributeGroupID]) VALUES (N'23', N'Diamond', N'5')
GO
GO
INSERT INTO [dbo].[Attribute] ([AttributeID], [AttributeValue], [AttributeGroupID]) VALUES (N'24', N'Ruby', N'5')
GO
GO
INSERT INTO [dbo].[Attribute] ([AttributeID], [AttributeValue], [AttributeGroupID]) VALUES (N'25', N'Saphire', N'5')
GO
GO
INSERT INTO [dbo].[Attribute] ([AttributeID], [AttributeValue], [AttributeGroupID]) VALUES (N'26', N'Emerald', N'5')
GO
GO
INSERT INTO [dbo].[Attribute] ([AttributeID], [AttributeValue], [AttributeGroupID]) VALUES (N'27', N'Aquamarine', N'5')
GO
GO
INSERT INTO [dbo].[Attribute] ([AttributeID], [AttributeValue], [AttributeGroupID]) VALUES (N'28', N'White', N'6')
GO
GO
INSERT INTO [dbo].[Attribute] ([AttributeID], [AttributeValue], [AttributeGroupID]) VALUES (N'29', N'Red', N'6')
GO
GO
INSERT INTO [dbo].[Attribute] ([AttributeID], [AttributeValue], [AttributeGroupID]) VALUES (N'30', N'Blue', N'6')
GO
GO
INSERT INTO [dbo].[Attribute] ([AttributeID], [AttributeValue], [AttributeGroupID]) VALUES (N'31', N'Green', N'6')
GO
GO
INSERT INTO [dbo].[Attribute] ([AttributeID], [AttributeValue], [AttributeGroupID]) VALUES (N'32', N'Yellow', N'6')
GO
GO
INSERT INTO [dbo].[Attribute] ([AttributeID], [AttributeValue], [AttributeGroupID]) VALUES (N'33', N'Male', N'7')
GO
GO
INSERT INTO [dbo].[Attribute] ([AttributeID], [AttributeValue], [AttributeGroupID]) VALUES (N'34', N'Female', N'7')
GO
GO
INSERT INTO [dbo].[Attribute] ([AttributeID], [AttributeValue], [AttributeGroupID]) VALUES (N'35', N'Unisex', N'7')
GO
GO
SET IDENTITY_INSERT [dbo].[Attribute] OFF
GO

-- ----------------------------
-- Records of Product
-- ----------------------------
SET IDENTITY_INSERT [dbo].[Product] ON
GO
INSERT INTO [dbo].[Product] ([ProductID], [ProductName], [ProductDesc], [IsActive], [CategoryID], [ColID], [DiscountValue], [DiscountValidFrom], [DiscountValidTo], [DateCreated], [DateUpdated]) VALUES (N'1', N'Platinum Ruby Ring', N'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec hendrerit sollicitudin euismod. Vestibulum at ipsum nec diam imperdiet tempus at vel quam. Nunc est ligula, gravida consectetur pulvinar sit amet, tincidunt vitae tortor. Vestibulum condimentum neque ac enim ullamcorper pharetra.', N'1', N'7', N'1', N'10', N'2018-03-28 16:07:36.420', N'2018-05-28 16:07:36.420', N'2018-01-28 16:07:36.420', N'2018-03-28 16:07:36.420')
GO
GO
INSERT INTO [dbo].[Product] ([ProductID], [ProductName], [ProductDesc], [IsActive], [CategoryID], [ColID], [DiscountValue], [DiscountValidFrom], [DiscountValidTo], [DateCreated], [DateUpdated]) VALUES (N'2', N'Olive Leaf Band Ring', N'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec hendrerit sollicitudin euismod. Vestibulum at ipsum nec diam imperdiet tempus at vel quam. Nunc est ligula, gravida consectetur pulvinar sit amet, tincidunt vitae tortor. Vestibulum condimentum neque ac enim ullamcorper pharetra.', N'1', N'9', null, N'20', N'2018-03-28 16:07:36.420', N'2018-05-28 16:07:36.420', N'2018-01-28 16:07:36.420', N'2018-03-28 16:07:36.420')
GO
GO
INSERT INTO [dbo].[Product] ([ProductID], [ProductName], [ProductDesc], [IsActive], [CategoryID], [ColID], [DiscountValue], [DiscountValidFrom], [DiscountValidTo], [DateCreated], [DateUpdated]) VALUES (N'3', N'Platinum Emerald Ring', N'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec hendrerit sollicitudin euismod. Vestibulum at ipsum nec diam imperdiet tempus at vel quam. Nunc est ligula, gravida consectetur pulvinar sit amet, tincidunt vitae tortor. Vestibulum condimentum neque ac enim ullamcorper pharetra.', N'1', N'7', N'1', N'10', N'2018-03-28 16:07:36.420', N'2018-05-28 16:07:36.420', N'2018-01-28 16:07:36.420', N'2018-03-28 16:07:36.420')
GO
GO
INSERT INTO [dbo].[Product] ([ProductID], [ProductName], [ProductDesc], [IsActive], [CategoryID], [ColID], [DiscountValue], [DiscountValidFrom], [DiscountValidTo], [DateCreated], [DateUpdated]) VALUES (N'4', N'Platinum Aquamarine Ribbon Ring', N'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec hendrerit sollicitudin euismod. Vestibulum at ipsum nec diam imperdiet tempus at vel quam. Nunc est ligula, gravida consectetur pulvinar sit amet, tincidunt vitae tortor. Vestibulum condimentum neque ac enim ullamcorper pharetra.', N'1', N'8', N'1', N'0', null, null, N'2018-01-28 16:07:36.420', N'2018-03-28 16:07:36.420')
GO
GO
INSERT INTO [dbo].[Product] ([ProductID], [ProductName], [ProductDesc], [IsActive], [CategoryID], [ColID], [DiscountValue], [DiscountValidFrom], [DiscountValidTo], [DateCreated], [DateUpdated]) VALUES (N'5', N'Platinum Horizontal Ring', N'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec hendrerit sollicitudin euismod. Vestibulum at ipsum nec diam imperdiet tempus at vel quam. Nunc est ligula, gravida consectetur pulvinar sit amet, tincidunt vitae tortor. Vestibulum condimentum neque ac enim ullamcorper pharetra.', N'1', N'7', null, N'0', null, null, N'2018-01-28 16:07:36.420', N'2018-03-28 16:07:36.420')
GO
GO
INSERT INTO [dbo].[Product] ([ProductID], [ProductName], [ProductDesc], [IsActive], [CategoryID], [ColID], [DiscountValue], [DiscountValidFrom], [DiscountValidTo], [DateCreated], [DateUpdated]) VALUES (N'6', N'Gold Infinity Ring', N'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec hendrerit sollicitudin euismod. Vestibulum at ipsum nec diam imperdiet tempus at vel quam. Nunc est ligula, gravida consectetur pulvinar sit amet, tincidunt vitae tortor. Vestibulum condimentum neque ac enim ullamcorper pharetra.', N'1', N'9', N'1', N'0', null, null, N'2018-01-28 16:07:36.420', N'2018-03-28 16:07:36.420')
GO
GO
INSERT INTO [dbo].[Product] ([ProductID], [ProductName], [ProductDesc], [IsActive], [CategoryID], [ColID], [DiscountValue], [DiscountValidFrom], [DiscountValidTo], [DateCreated], [DateUpdated]) VALUES (N'7', N'Gold Diamond Infinity Ring', N'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec hendrerit sollicitudin euismod. Vestibulum at ipsum nec diam imperdiet tempus at vel quam. Nunc est ligula, gravida consectetur pulvinar sit amet, tincidunt vitae tortor. Vestibulum condimentum neque ac enim ullamcorper pharetra.', N'1', N'8', N'4', N'10', N'2018-03-28 16:07:36.420', N'2018-05-28 16:07:36.420', N'2018-01-28 16:07:36.420', N'2018-03-28 16:07:36.420')
GO
GO
INSERT INTO [dbo].[Product] ([ProductID], [ProductName], [ProductDesc], [IsActive], [CategoryID], [ColID], [DiscountValue], [DiscountValidFrom], [DiscountValidTo], [DateCreated], [DateUpdated]) VALUES (N'8', N'Gold Interlocking Ring', N'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec hendrerit sollicitudin euismod. Vestibulum at ipsum nec diam imperdiet tempus at vel quam. Nunc est ligula, gravida consectetur pulvinar sit amet, tincidunt vitae tortor. Vestibulum condimentum neque ac enim ullamcorper pharetra.', N'1', N'9', N'4', N'10', N'2018-03-28 16:07:36.420', N'2018-05-28 16:07:36.420', N'2018-01-28 16:07:36.420', N'2018-03-28 16:07:36.420')
GO
GO
INSERT INTO [dbo].[Product] ([ProductID], [ProductName], [ProductDesc], [IsActive], [CategoryID], [ColID], [DiscountValue], [DiscountValidFrom], [DiscountValidTo], [DateCreated], [DateUpdated]) VALUES (N'9', N'Gold Diamond Engagement Ring', N'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec hendrerit sollicitudin euismod. Vestibulum at ipsum nec diam imperdiet tempus at vel quam. Nunc est ligula, gravida consectetur pulvinar sit amet, tincidunt vitae tortor. Vestibulum condimentum neque ac enim ullamcorper pharetra.', N'1', N'6', N'4', N'0', null, null, N'2018-02-28 16:07:36.420', N'2018-03-28 16:07:36.420')
GO
GO
INSERT INTO [dbo].[Product] ([ProductID], [ProductName], [ProductDesc], [IsActive], [CategoryID], [ColID], [DiscountValue], [DiscountValidFrom], [DiscountValidTo], [DateCreated], [DateUpdated]) VALUES (N'10', N'Gold Luck Dog Charm Bracelet', N'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec hendrerit sollicitudin euismod. Vestibulum at ipsum nec diam imperdiet tempus at vel quam. Nunc est ligula, gravida consectetur pulvinar sit amet, tincidunt vitae tortor. Vestibulum condimentum neque ac enim ullamcorper pharetra.', N'1', N'17', N'2', N'0', null, null, N'2018-02-28 16:07:36.420', N'2018-03-28 16:07:36.420')
GO
GO
INSERT INTO [dbo].[Product] ([ProductID], [ProductName], [ProductDesc], [IsActive], [CategoryID], [ColID], [DiscountValue], [DiscountValidFrom], [DiscountValidTo], [DateCreated], [DateUpdated]) VALUES (N'11', N'Gold Lucky Frog Charm Bracelet', N'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec hendrerit sollicitudin euismod. Vestibulum at ipsum nec diam imperdiet tempus at vel quam. Nunc est ligula, gravida consectetur pulvinar sit amet, tincidunt vitae tortor. Vestibulum condimentum neque ac enim ullamcorper pharetra.', N'1', N'17', N'2', N'0', null, null, N'2018-02-28 16:07:36.420', N'2018-03-28 16:07:36.420')
GO
GO
INSERT INTO [dbo].[Product] ([ProductID], [ProductName], [ProductDesc], [IsActive], [CategoryID], [ColID], [DiscountValue], [DiscountValidFrom], [DiscountValidTo], [DateCreated], [DateUpdated]) VALUES (N'12', N'Rose Gold Charm Me Bracelet', N'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec hendrerit sollicitudin euismod. Vestibulum at ipsum nec diam imperdiet tempus at vel quam. Nunc est ligula, gravida consectetur pulvinar sit amet, tincidunt vitae tortor. Vestibulum condimentum neque ac enim ullamcorper pharetra.', N'1', N'19', N'2', N'10', N'2018-03-28 16:07:36.420', N'2018-05-28 16:07:36.420', N'2018-02-28 16:07:36.420', N'2018-03-28 16:07:36.420')
GO
GO
INSERT INTO [dbo].[Product] ([ProductID], [ProductName], [ProductDesc], [IsActive], [CategoryID], [ColID], [DiscountValue], [DiscountValidFrom], [DiscountValidTo], [DateCreated], [DateUpdated]) VALUES (N'13', N'Platinum Charm Me Bracelet', N'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec hendrerit sollicitudin euismod. Vestibulum at ipsum nec diam imperdiet tempus at vel quam. Nunc est ligula, gravida consectetur pulvinar sit amet, tincidunt vitae tortor. Vestibulum condimentum neque ac enim ullamcorper pharetra.', N'1', N'19', N'2', N'20', N'2018-03-28 16:07:36.420', N'2018-05-28 16:07:36.420', N'2018-02-28 16:07:36.420', N'2018-03-28 16:07:36.420')
GO
GO
INSERT INTO [dbo].[Product] ([ProductID], [ProductName], [ProductDesc], [IsActive], [CategoryID], [ColID], [DiscountValue], [DiscountValidFrom], [DiscountValidTo], [DateCreated], [DateUpdated]) VALUES (N'14', N'Melody Platinum Ruby Bracelet', N'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec hendrerit sollicitudin euismod. Vestibulum at ipsum nec diam imperdiet tempus at vel quam. Nunc est ligula, gravida consectetur pulvinar sit amet, tincidunt vitae tortor. Vestibulum condimentum neque ac enim ullamcorper pharetra.', N'1', N'19', N'5', N'0', null, null, N'2018-02-28 16:07:36.420', N'2018-03-28 16:07:36.420')
GO
GO
INSERT INTO [dbo].[Product] ([ProductID], [ProductName], [ProductDesc], [IsActive], [CategoryID], [ColID], [DiscountValue], [DiscountValidFrom], [DiscountValidTo], [DateCreated], [DateUpdated]) VALUES (N'15', N'Melody Platinum Emerald Bracelet', N'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec hendrerit sollicitudin euismod. Vestibulum at ipsum nec diam imperdiet tempus at vel quam. Nunc est ligula, gravida consectetur pulvinar sit amet, tincidunt vitae tortor. Vestibulum condimentum neque ac enim ullamcorper pharetra.', N'1', N'19', N'5', N'0', null, null, N'2018-02-28 16:07:36.420', N'2018-03-28 16:07:36.420')
GO
GO
INSERT INTO [dbo].[Product] ([ProductID], [ProductName], [ProductDesc], [IsActive], [CategoryID], [ColID], [DiscountValue], [DiscountValidFrom], [DiscountValidTo], [DateCreated], [DateUpdated]) VALUES (N'16', N'Melody Platinum Saphire Bracelet', N'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec hendrerit sollicitudin euismod. Vestibulum at ipsum nec diam imperdiet tempus at vel quam. Nunc est ligula, gravida consectetur pulvinar sit amet, tincidunt vitae tortor. Vestibulum condimentum neque ac enim ullamcorper pharetra.', N'1', N'19', N'5', N'50', N'2018-03-28 16:07:36.420', N'2018-05-28 16:07:36.420', N'2018-02-28 16:07:36.420', N'2018-03-28 16:07:36.420')
GO
GO
INSERT INTO [dbo].[Product] ([ProductID], [ProductName], [ProductDesc], [IsActive], [CategoryID], [ColID], [DiscountValue], [DiscountValidFrom], [DiscountValidTo], [DateCreated], [DateUpdated]) VALUES (N'17', N'Wedding Platinum Flower Necklace', N'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec hendrerit sollicitudin euismod. Vestibulum at ipsum nec diam imperdiet tempus at vel quam. Nunc est ligula, gravida consectetur pulvinar sit amet, tincidunt vitae tortor. Vestibulum condimentum neque ac enim ullamcorper pharetra.', N'1', N'20', N'1', N'0', null, null, N'2018-02-28 16:07:36.420', N'2018-03-28 16:07:36.420')
GO
GO
INSERT INTO [dbo].[Product] ([ProductID], [ProductName], [ProductDesc], [IsActive], [CategoryID], [ColID], [DiscountValue], [DiscountValidFrom], [DiscountValidTo], [DateCreated], [DateUpdated]) VALUES (N'18', N'Wedding Platinum Pearl Necklace', N'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec hendrerit sollicitudin euismod. Vestibulum at ipsum nec diam imperdiet tempus at vel quam. Nunc est ligula, gravida consectetur pulvinar sit amet, tincidunt vitae tortor. Vestibulum condimentum neque ac enim ullamcorper pharetra.', N'1', N'20', N'1', N'0', null, null, N'2018-02-28 16:07:36.420', N'2018-03-28 16:07:36.420')
GO
GO
INSERT INTO [dbo].[Product] ([ProductID], [ProductName], [ProductDesc], [IsActive], [CategoryID], [ColID], [DiscountValue], [DiscountValidFrom], [DiscountValidTo], [DateCreated], [DateUpdated]) VALUES (N'19', N'Wedding Platinum Pink Pearl Necklace', N'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec hendrerit sollicitudin euismod. Vestibulum at ipsum nec diam imperdiet tempus at vel quam. Nunc est ligula, gravida consectetur pulvinar sit amet, tincidunt vitae tortor. Vestibulum condimentum neque ac enim ullamcorper pharetra.', N'1', N'20', N'1', N'0', null, null, N'2018-02-28 16:07:36.420', N'2018-03-28 16:07:36.420')
GO
GO
INSERT INTO [dbo].[Product] ([ProductID], [ProductName], [ProductDesc], [IsActive], [CategoryID], [ColID], [DiscountValue], [DiscountValidFrom], [DiscountValidTo], [DateCreated], [DateUpdated]) VALUES (N'20', N'Wedding Platinum Orchild Necklace', N'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec hendrerit sollicitudin euismod. Vestibulum at ipsum nec diam imperdiet tempus at vel quam. Nunc est ligula, gravida consectetur pulvinar sit amet, tincidunt vitae tortor. Vestibulum condimentum neque ac enim ullamcorper pharetra.', N'1', N'20', null, N'0', null, null, N'2018-03-28 16:07:36.420', N'2018-03-28 16:07:36.420')
GO
GO
INSERT INTO [dbo].[Product] ([ProductID], [ProductName], [ProductDesc], [IsActive], [CategoryID], [ColID], [DiscountValue], [DiscountValidFrom], [DiscountValidTo], [DateCreated], [DateUpdated]) VALUES (N'21', N'Wedding Gold Orchild Necklace', N'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec hendrerit sollicitudin euismod. Vestibulum at ipsum nec diam imperdiet tempus at vel quam. Nunc est ligula, gravida consectetur pulvinar sit amet, tincidunt vitae tortor. Vestibulum condimentum neque ac enim ullamcorper pharetra.', N'1', N'20', null, N'0', null, null, N'2018-03-28 16:07:36.420', N'2018-03-28 16:07:36.420')
GO
GO
INSERT INTO [dbo].[Product] ([ProductID], [ProductName], [ProductDesc], [IsActive], [CategoryID], [ColID], [DiscountValue], [DiscountValidFrom], [DiscountValidTo], [DateCreated], [DateUpdated]) VALUES (N'22', N'Gold Diamond Heart Necklace', N'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec hendrerit sollicitudin euismod. Vestibulum at ipsum nec diam imperdiet tempus at vel quam. Nunc est ligula, gravida consectetur pulvinar sit amet, tincidunt vitae tortor. Vestibulum condimentum neque ac enim ullamcorper pharetra.', N'1', N'10', null, N'0', null, null, N'2018-03-28 16:07:36.420', N'2018-03-28 16:07:36.420')
GO
GO
INSERT INTO [dbo].[Product] ([ProductID], [ProductName], [ProductDesc], [IsActive], [CategoryID], [ColID], [DiscountValue], [DiscountValidFrom], [DiscountValidTo], [DateCreated], [DateUpdated]) VALUES (N'23', N'Platinum Clover Necklace', N'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec hendrerit sollicitudin euismod. Vestibulum at ipsum nec diam imperdiet tempus at vel quam. Nunc est ligula, gravida consectetur pulvinar sit amet, tincidunt vitae tortor. Vestibulum condimentum neque ac enim ullamcorper pharetra.', N'1', N'10', N'5', N'0', null, null, N'2018-03-28 16:07:36.420', N'2018-03-28 16:07:36.420')
GO
GO
INSERT INTO [dbo].[Product] ([ProductID], [ProductName], [ProductDesc], [IsActive], [CategoryID], [ColID], [DiscountValue], [DiscountValidFrom], [DiscountValidTo], [DateCreated], [DateUpdated]) VALUES (N'24', N'Platinum Diamond Stud Earrings', N'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec hendrerit sollicitudin euismod. Vestibulum at ipsum nec diam imperdiet tempus at vel quam. Nunc est ligula, gravida consectetur pulvinar sit amet, tincidunt vitae tortor. Vestibulum condimentum neque ac enim ullamcorper pharetra.', N'1', N'13', N'3', N'0', N'2018-03-28 16:07:36.420', N'2018-03-28 16:07:36.420', N'2018-03-28 16:07:36.420', N'2018-03-28 16:07:36.420')
GO
GO
INSERT INTO [dbo].[Product] ([ProductID], [ProductName], [ProductDesc], [IsActive], [CategoryID], [ColID], [DiscountValue], [DiscountValidFrom], [DiscountValidTo], [DateCreated], [DateUpdated]) VALUES (N'25', N'Silver Diamond Rose Earrings', N'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec hendrerit sollicitudin euismod. Vestibulum at ipsum nec diam imperdiet tempus at vel quam. Nunc est ligula, gravida consectetur pulvinar sit amet, tincidunt vitae tortor. Vestibulum condimentum neque ac enim ullamcorper pharetra.', N'1', N'13', N'3', N'20', N'2018-03-28 16:07:36.420', N'2018-05-28 16:07:36.420', N'2018-03-28 16:07:36.420', N'2018-03-28 16:07:36.420')
GO
GO
INSERT INTO [dbo].[Product] ([ProductID], [ProductName], [ProductDesc], [IsActive], [CategoryID], [ColID], [DiscountValue], [DiscountValidFrom], [DiscountValidTo], [DateCreated], [DateUpdated]) VALUES (N'26', N'Platinum Diamond Leaf Earrings', N'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec hendrerit sollicitudin euismod. Vestibulum at ipsum nec diam imperdiet tempus at vel quam. Nunc est ligula, gravida consectetur pulvinar sit amet, tincidunt vitae tortor. Vestibulum condimentum neque ac enim ullamcorper pharetra.', N'1', N'13', N'3', N'20', N'2018-03-28 16:07:36.420', N'2018-05-28 16:07:36.420', N'2018-03-28 16:07:36.420', N'2018-03-28 16:07:36.420')
GO
GO
INSERT INTO [dbo].[Product] ([ProductID], [ProductName], [ProductDesc], [IsActive], [CategoryID], [ColID], [DiscountValue], [DiscountValidFrom], [DiscountValidTo], [DateCreated], [DateUpdated]) VALUES (N'27', N'Silver Drop Earrings', N'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec hendrerit sollicitudin euismod. Vestibulum at ipsum nec diam imperdiet tempus at vel quam. Nunc est ligula, gravida consectetur pulvinar sit amet, tincidunt vitae tortor. Vestibulum condimentum neque ac enim ullamcorper pharetra.', N'1', N'12', null, N'0', null, null, N'2018-03-28 16:07:36.420', N'2018-03-28 16:07:36.420')
GO
GO
INSERT INTO [dbo].[Product] ([ProductID], [ProductName], [ProductDesc], [IsActive], [CategoryID], [ColID], [DiscountValue], [DiscountValidFrom], [DiscountValidTo], [DateCreated], [DateUpdated]) VALUES (N'28', N'Gold Pearl Earrings', N'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec hendrerit sollicitudin euismod. Vestibulum at ipsum nec diam imperdiet tempus at vel quam. Nunc est ligula, gravida consectetur pulvinar sit amet, tincidunt vitae tortor. Vestibulum condimentum neque ac enim ullamcorper pharetra.', N'1', N'12', null, N'0', null, null, N'2018-03-28 16:07:36.420', N'2018-03-28 16:07:36.420')
GO
GO
INSERT INTO [dbo].[Product] ([ProductID], [ProductName], [ProductDesc], [IsActive], [CategoryID], [ColID], [DiscountValue], [DiscountValidFrom], [DiscountValidTo], [DateCreated], [DateUpdated]) VALUES (N'29', N'Platinum Topaz Earrings', N'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec hendrerit sollicitudin euismod. Vestibulum at ipsum nec diam imperdiet tempus at vel quam. Nunc est ligula, gravida consectetur pulvinar sit amet, tincidunt vitae tortor. Vestibulum condimentum neque ac enim ullamcorper pharetra.', N'1', N'12', null, N'0', null, null, N'2018-03-28 16:07:36.420', N'2018-03-28 16:07:36.420')
GO
GO
INSERT INTO [dbo].[Product] ([ProductID], [ProductName], [ProductDesc], [IsActive], [CategoryID], [ColID], [DiscountValue], [DiscountValidFrom], [DiscountValidTo], [DateCreated], [DateUpdated]) VALUES (N'30', N'Silver Diamond Earrings', N'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec hendrerit sollicitudin euismod. Vestibulum at ipsum nec diam imperdiet tempus at vel quam. Nunc est ligula, gravida consectetur pulvinar sit amet, tincidunt vitae tortor. Vestibulum condimentum neque ac enim ullamcorper pharetra.', N'1', N'14', null, N'0', null, null, N'2018-03-28 16:07:36.420', N'2018-03-28 16:07:36.420')
GO
GO
SET IDENTITY_INSERT [dbo].[Product] OFF
GO

-- ----------------------------
-- Records of ProductAttribute
-- ----------------------------
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'1', N'5')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'1', N'7')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'1', N'12')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'1', N'19')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'1', N'23')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'1', N'28')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'1', N'34')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'2', N'1')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'2', N'35')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'3', N'5')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'3', N'9')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'3', N'12')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'3', N'21')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'3', N'23')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'3', N'28')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'3', N'34')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'4', N'5')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'4', N'10')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'4', N'13')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'4', N'20')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'4', N'23')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'4', N'28')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'4', N'34')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'5', N'5')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'5', N'6')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'5', N'17')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'5', N'18')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'5', N'33')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'6', N'1')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'6', N'33')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'7', N'1')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'7', N'6')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'7', N'12')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'7', N'18')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'7', N'34')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'8', N'1')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'8', N'33')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'9', N'1')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'9', N'6')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'9', N'12')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'9', N'18')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'9', N'23')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'9', N'28')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'9', N'34')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'10', N'1')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'10', N'35')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'11', N'1')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'11', N'35')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'12', N'3')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'12', N'6')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'12', N'12')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'12', N'18')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'12', N'34')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'13', N'5')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'13', N'6')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'13', N'12')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'13', N'18')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'13', N'34')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'14', N'5')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'14', N'7')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'14', N'12')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'14', N'19')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'14', N'34')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'15', N'5')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'15', N'9')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'15', N'12')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'15', N'21')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'15', N'34')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'16', N'5')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'16', N'8')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'16', N'12')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'16', N'20')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'16', N'34')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'17', N'5')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'17', N'6')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'17', N'12')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'17', N'18')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'17', N'23')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'17', N'28')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'17', N'34')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'18', N'5')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'18', N'11')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'18', N'18')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'18', N'34')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'19', N'5')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'19', N'11')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'19', N'18')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'19', N'34')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'20', N'5')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'20', N'6')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'20', N'13')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'20', N'18')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'20', N'34')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'21', N'2')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'21', N'6')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'21', N'13')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'21', N'18')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'21', N'34')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'22', N'2')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'22', N'6')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'22', N'14')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'22', N'18')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'22', N'23')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'22', N'28')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'22', N'34')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'23', N'5')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'23', N'6')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'23', N'12')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'23', N'18')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'23', N'34')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'24', N'5')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'24', N'6')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'24', N'12')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'24', N'18')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'24', N'34')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'25', N'4')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'25', N'6')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'25', N'13')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'25', N'18')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'25', N'34')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'26', N'5')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'26', N'6')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'26', N'15')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'26', N'18')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'26', N'23')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'26', N'28')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'26', N'34')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'27', N'4')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'27', N'10')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'27', N'15')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'27', N'20')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'27', N'34')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'28', N'1')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'28', N'11')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'28', N'18')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'28', N'34')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'29', N'5')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'29', N'22')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'29', N'34')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'30', N'4')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'30', N'6')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'30', N'12')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'30', N'18')
GO
GO
INSERT INTO [dbo].[ProductAttribute] ([ProductID], [AttributeID]) VALUES (N'30', N'34')
GO
GO

-- ----------------------------
-- Records of ProductImage
-- ----------------------------
SET IDENTITY_INSERT [dbo].[ProductImage] ON
GO
INSERT INTO [dbo].[ProductImage] ([ImageID], [ProductID], [ImageLink]) VALUES (N'1', N'1', N'PlatinumRubyRing1.png')
GO
GO
INSERT INTO [dbo].[ProductImage] ([ImageID], [ProductID], [ImageLink]) VALUES (N'2', N'1', N'PlatinumRubyRing2.jpg')
GO
GO
INSERT INTO [dbo].[ProductImage] ([ImageID], [ProductID], [ImageLink]) VALUES (N'3', N'2', N'OliveBandRing1.png')
GO
GO
INSERT INTO [dbo].[ProductImage] ([ImageID], [ProductID], [ImageLink]) VALUES (N'4', N'2', N'OliveBandRing2.jpg')
GO
GO
INSERT INTO [dbo].[ProductImage] ([ImageID], [ProductID], [ImageLink]) VALUES (N'5', N'3', N'PlatinumEmeraldRing1.png')
GO
GO
INSERT INTO [dbo].[ProductImage] ([ImageID], [ProductID], [ImageLink]) VALUES (N'6', N'4', N'PlatinumAquamarineRing.png')
GO
GO
INSERT INTO [dbo].[ProductImage] ([ImageID], [ProductID], [ImageLink]) VALUES (N'7', N'4', N'PlatinumAquamarineRing2.jpg')
GO
GO
INSERT INTO [dbo].[ProductImage] ([ImageID], [ProductID], [ImageLink]) VALUES (N'8', N'5', N'PlatinumHorizontalRing.png')
GO
GO
INSERT INTO [dbo].[ProductImage] ([ImageID], [ProductID], [ImageLink]) VALUES (N'9', N'5', N'PlatinumHorizontalRing2.jpg')
GO
GO
INSERT INTO [dbo].[ProductImage] ([ImageID], [ProductID], [ImageLink]) VALUES (N'10', N'6', N'GoldInfinityRing1.png')
GO
GO
INSERT INTO [dbo].[ProductImage] ([ImageID], [ProductID], [ImageLink]) VALUES (N'11', N'6', N'GoldInfinityRing2.jpg')
GO
GO
INSERT INTO [dbo].[ProductImage] ([ImageID], [ProductID], [ImageLink]) VALUES (N'12', N'7', N'GoldDiamondInfinityRing1.png')
GO
GO
INSERT INTO [dbo].[ProductImage] ([ImageID], [ProductID], [ImageLink]) VALUES (N'13', N'7', N'GoldDiamondInfinityRing2.jpg')
GO
GO
INSERT INTO [dbo].[ProductImage] ([ImageID], [ProductID], [ImageLink]) VALUES (N'14', N'8', N'GoldInterlockingRing1.png')
GO
GO
INSERT INTO [dbo].[ProductImage] ([ImageID], [ProductID], [ImageLink]) VALUES (N'15', N'8', N'GoldInterlockingRing2.jpg')
GO
GO
INSERT INTO [dbo].[ProductImage] ([ImageID], [ProductID], [ImageLink]) VALUES (N'16', N'9', N'GoldDiamondEngagementRing1.png')
GO
GO
INSERT INTO [dbo].[ProductImage] ([ImageID], [ProductID], [ImageLink]) VALUES (N'17', N'9', N'GoldDiamondEngagementRing2.png')
GO
GO
INSERT INTO [dbo].[ProductImage] ([ImageID], [ProductID], [ImageLink]) VALUES (N'18', N'9', N'GoldDiamondEngagementRing3.png')
GO
GO
INSERT INTO [dbo].[ProductImage] ([ImageID], [ProductID], [ImageLink]) VALUES (N'19', N'10', N'GoldLuckyDogCharmBracelet1.jpg')
GO
GO
INSERT INTO [dbo].[ProductImage] ([ImageID], [ProductID], [ImageLink]) VALUES (N'20', N'10', N'GoldLuckyDogCharmBracelet2.jpg')
GO
GO
INSERT INTO [dbo].[ProductImage] ([ImageID], [ProductID], [ImageLink]) VALUES (N'21', N'11', N'GoldLuckyFrogCharmBracelet1.png')
GO
GO
INSERT INTO [dbo].[ProductImage] ([ImageID], [ProductID], [ImageLink]) VALUES (N'22', N'11', N'GoldLuckyFrogCharmBracelet2.png')
GO
GO
INSERT INTO [dbo].[ProductImage] ([ImageID], [ProductID], [ImageLink]) VALUES (N'23', N'11', N'GoldLuckyFrogCharmBracelet3.png')
GO
GO
INSERT INTO [dbo].[ProductImage] ([ImageID], [ProductID], [ImageLink]) VALUES (N'24', N'12', N'RoseGoldCharmMeBracelet1.jpg')
GO
GO
INSERT INTO [dbo].[ProductImage] ([ImageID], [ProductID], [ImageLink]) VALUES (N'25', N'12', N'RoseGoldCharmMeBracelet2.jpg')
GO
GO
INSERT INTO [dbo].[ProductImage] ([ImageID], [ProductID], [ImageLink]) VALUES (N'26', N'12', N'RoseGoldCharmMeBracelet3.jpg')
GO
GO
INSERT INTO [dbo].[ProductImage] ([ImageID], [ProductID], [ImageLink]) VALUES (N'27', N'13', N'PlatinumCharmMeBracelet1.png')
GO
GO
INSERT INTO [dbo].[ProductImage] ([ImageID], [ProductID], [ImageLink]) VALUES (N'28', N'13', N'PlatinumCharmMeBracelet2.png')
GO
GO
INSERT INTO [dbo].[ProductImage] ([ImageID], [ProductID], [ImageLink]) VALUES (N'29', N'13', N'PlatinumCharmMeBracelet3.png')
GO
GO
INSERT INTO [dbo].[ProductImage] ([ImageID], [ProductID], [ImageLink]) VALUES (N'30', N'14', N'MelodyPlatinumRubyBracelet1.jpg')
GO
GO
INSERT INTO [dbo].[ProductImage] ([ImageID], [ProductID], [ImageLink]) VALUES (N'31', N'15', N'MelodyPlatinumEmeraldBracelet1.jpg')
GO
GO
INSERT INTO [dbo].[ProductImage] ([ImageID], [ProductID], [ImageLink]) VALUES (N'32', N'16', N'MelodyPlatinumSaphireBracelet1.jpg')
GO
GO
INSERT INTO [dbo].[ProductImage] ([ImageID], [ProductID], [ImageLink]) VALUES (N'33', N'17', N'WeddingPlatinumFlowerNecklace.png')
GO
GO
INSERT INTO [dbo].[ProductImage] ([ImageID], [ProductID], [ImageLink]) VALUES (N'34', N'18', N'WeddingPlatinumPearlNecklace.png')
GO
GO
INSERT INTO [dbo].[ProductImage] ([ImageID], [ProductID], [ImageLink]) VALUES (N'35', N'19', N'WeddingPlatinumPinkPearlNecklace.jpg')
GO
GO
INSERT INTO [dbo].[ProductImage] ([ImageID], [ProductID], [ImageLink]) VALUES (N'36', N'20', N'WeddingPlatinumOrchildNecklace.jpg')
GO
GO
INSERT INTO [dbo].[ProductImage] ([ImageID], [ProductID], [ImageLink]) VALUES (N'37', N'21', N'WeddingGoldOrchildNecklace.jpg')
GO
GO
INSERT INTO [dbo].[ProductImage] ([ImageID], [ProductID], [ImageLink]) VALUES (N'38', N'22', N'GoldDiamondHeartNecklace.jpg')
GO
GO
INSERT INTO [dbo].[ProductImage] ([ImageID], [ProductID], [ImageLink]) VALUES (N'39', N'23', N'PlatinumCloverNecklace.jpg')
GO
GO
INSERT INTO [dbo].[ProductImage] ([ImageID], [ProductID], [ImageLink]) VALUES (N'40', N'24', N'PlatinumDiamondStudEarrings.jpg')
GO
GO
INSERT INTO [dbo].[ProductImage] ([ImageID], [ProductID], [ImageLink]) VALUES (N'41', N'25', N'PlatinumDiamondStudEarrings2.jpg')
GO
GO
INSERT INTO [dbo].[ProductImage] ([ImageID], [ProductID], [ImageLink]) VALUES (N'42', N'26', N'PlatinumDiamondLeafEarrings.jpg')
GO
GO
INSERT INTO [dbo].[ProductImage] ([ImageID], [ProductID], [ImageLink]) VALUES (N'43', N'27', N'SilverDropEarrings.jpg')
GO
GO
INSERT INTO [dbo].[ProductImage] ([ImageID], [ProductID], [ImageLink]) VALUES (N'44', N'27', N'SilverDropEarrings2.jpg')
GO
GO
INSERT INTO [dbo].[ProductImage] ([ImageID], [ProductID], [ImageLink]) VALUES (N'45', N'27', N'SilverDropEarrings3.jpg')
GO
GO
INSERT INTO [dbo].[ProductImage] ([ImageID], [ProductID], [ImageLink]) VALUES (N'46', N'28', N'GoldPearlEarrings.png')
GO
GO
INSERT INTO [dbo].[ProductImage] ([ImageID], [ProductID], [ImageLink]) VALUES (N'47', N'29', N'PlatinumTopazEarrings.png')
GO
GO
INSERT INTO [dbo].[ProductImage] ([ImageID], [ProductID], [ImageLink]) VALUES (N'48', N'30', N'SilverDiamondEarrings.jpg')
GO
GO
SET IDENTITY_INSERT [dbo].[ProductImage] OFF
GO

-- ----------------------------
-- Records of SkuSize
-- ----------------------------
SET IDENTITY_INSERT [dbo].[SkuSize] ON
GO
INSERT INTO [dbo].[SkuSize] ([SizeID], [SizeValue], [CategoryID]) VALUES (N'1', N'4', N'1')
GO
GO
INSERT INTO [dbo].[SkuSize] ([SizeID], [SizeValue], [CategoryID]) VALUES (N'2', N'5', N'1')
GO
GO
INSERT INTO [dbo].[SkuSize] ([SizeID], [SizeValue], [CategoryID]) VALUES (N'3', N'6', N'1')
GO
GO
INSERT INTO [dbo].[SkuSize] ([SizeID], [SizeValue], [CategoryID]) VALUES (N'4', N'7', N'1')
GO
GO
INSERT INTO [dbo].[SkuSize] ([SizeID], [SizeValue], [CategoryID]) VALUES (N'5', N'8', N'1')
GO
GO
INSERT INTO [dbo].[SkuSize] ([SizeID], [SizeValue], [CategoryID]) VALUES (N'6', N'9', N'1')
GO
GO
INSERT INTO [dbo].[SkuSize] ([SizeID], [SizeValue], [CategoryID]) VALUES (N'7', N'10', N'1')
GO
GO
INSERT INTO [dbo].[SkuSize] ([SizeID], [SizeValue], [CategoryID]) VALUES (N'8', N'11', N'1')
GO
GO
INSERT INTO [dbo].[SkuSize] ([SizeID], [SizeValue], [CategoryID]) VALUES (N'9', N'12', N'1')
GO
GO
INSERT INTO [dbo].[SkuSize] ([SizeID], [SizeValue], [CategoryID]) VALUES (N'10', N'50', N'2')
GO
GO
INSERT INTO [dbo].[SkuSize] ([SizeID], [SizeValue], [CategoryID]) VALUES (N'11', N'51', N'2')
GO
GO
INSERT INTO [dbo].[SkuSize] ([SizeID], [SizeValue], [CategoryID]) VALUES (N'12', N'52', N'2')
GO
GO
INSERT INTO [dbo].[SkuSize] ([SizeID], [SizeValue], [CategoryID]) VALUES (N'13', N'53', N'2')
GO
GO
INSERT INTO [dbo].[SkuSize] ([SizeID], [SizeValue], [CategoryID]) VALUES (N'14', N'54', N'2')
GO
GO
INSERT INTO [dbo].[SkuSize] ([SizeID], [SizeValue], [CategoryID]) VALUES (N'15', N'55', N'2')
GO
GO
INSERT INTO [dbo].[SkuSize] ([SizeID], [SizeValue], [CategoryID]) VALUES (N'16', N'56', N'2')
GO
GO
INSERT INTO [dbo].[SkuSize] ([SizeID], [SizeValue], [CategoryID]) VALUES (N'17', N'57', N'2')
GO
GO
INSERT INTO [dbo].[SkuSize] ([SizeID], [SizeValue], [CategoryID]) VALUES (N'18', N'58', N'2')
GO
GO
INSERT INTO [dbo].[SkuSize] ([SizeID], [SizeValue], [CategoryID]) VALUES (N'19', N'59', N'2')
GO
GO
INSERT INTO [dbo].[SkuSize] ([SizeID], [SizeValue], [CategoryID]) VALUES (N'20', N'60', N'2')
GO
GO
INSERT INTO [dbo].[SkuSize] ([SizeID], [SizeValue], [CategoryID]) VALUES (N'21', N'40', N'3')
GO
GO
INSERT INTO [dbo].[SkuSize] ([SizeID], [SizeValue], [CategoryID]) VALUES (N'22', N'41', N'3')
GO
GO
INSERT INTO [dbo].[SkuSize] ([SizeID], [SizeValue], [CategoryID]) VALUES (N'23', N'42', N'3')
GO
GO
INSERT INTO [dbo].[SkuSize] ([SizeID], [SizeValue], [CategoryID]) VALUES (N'24', N'43', N'3')
GO
GO
INSERT INTO [dbo].[SkuSize] ([SizeID], [SizeValue], [CategoryID]) VALUES (N'25', N'44', N'3')
GO
GO
INSERT INTO [dbo].[SkuSize] ([SizeID], [SizeValue], [CategoryID]) VALUES (N'26', N'45', N'3')
GO
GO
SET IDENTITY_INSERT [dbo].[SkuSize] OFF
GO

-- ----------------------------
-- Records of Sku
-- ----------------------------
INSERT INTO [dbo].[Sku] ([SkuID], [ProductID], [SizeID], [BasePrice], [UnitPrice], [UnitsInStock], [UnitsOnOrder], [DateCreated], [DateUpdated]) VALUES (N'BRAC-0010-50', N'10', N'10', N'600', N'700', N'5', N'0', N'2018-03-28 19:59:07.970', N'2018-03-28 19:59:07.970')
GO
GO
INSERT INTO [dbo].[Sku] ([SkuID], [ProductID], [SizeID], [BasePrice], [UnitPrice], [UnitsInStock], [UnitsOnOrder], [DateCreated], [DateUpdated]) VALUES (N'BRAC-0011-50', N'11', N'10', N'600', N'700', N'23', N'0', N'2018-03-28 20:07:28.413', N'2018-03-28 20:07:28.413')
GO
GO
INSERT INTO [dbo].[Sku] ([SkuID], [ProductID], [SizeID], [BasePrice], [UnitPrice], [UnitsInStock], [UnitsOnOrder], [DateCreated], [DateUpdated]) VALUES (N'BRAC-0010-51', N'10', N'11', N'620', N'720', N'7', N'0', N'2018-03-28 20:00:29.720', N'2018-03-28 20:00:29.720')
GO
GO
INSERT INTO [dbo].[Sku] ([SkuID], [ProductID], [SizeID], [BasePrice], [UnitPrice], [UnitsInStock], [UnitsOnOrder], [DateCreated], [DateUpdated]) VALUES (N'BRAC-0011-51', N'11', N'11', N'610', N'710', N'12', N'0', N'2018-03-28 20:07:54.497', N'2018-03-28 20:07:54.497')
GO
GO
INSERT INTO [dbo].[Sku] ([SkuID], [ProductID], [SizeID], [BasePrice], [UnitPrice], [UnitsInStock], [UnitsOnOrder], [DateCreated], [DateUpdated]) VALUES (N'BRAC-0013-51', N'13', N'11', N'680', N'780', N'23', N'0', N'2018-03-28 20:09:46.413', N'2018-03-28 20:09:46.413')
GO
GO
INSERT INTO [dbo].[Sku] ([SkuID], [ProductID], [SizeID], [BasePrice], [UnitPrice], [UnitsInStock], [UnitsOnOrder], [DateCreated], [DateUpdated]) VALUES (N'BRAC-0010-52', N'10', N'12', N'630', N'730', N'9', N'0', N'2018-03-28 20:01:00.400', N'2018-03-28 20:01:00.400')
GO
GO
INSERT INTO [dbo].[Sku] ([SkuID], [ProductID], [SizeID], [BasePrice], [UnitPrice], [UnitsInStock], [UnitsOnOrder], [DateCreated], [DateUpdated]) VALUES (N'BRAC-0012-52', N'12', N'12', N'650', N'750', N'8', N'0', N'2018-03-28 20:08:55.100', N'2018-03-28 20:08:55.100')
GO
GO
INSERT INTO [dbo].[Sku] ([SkuID], [ProductID], [SizeID], [BasePrice], [UnitPrice], [UnitsInStock], [UnitsOnOrder], [DateCreated], [DateUpdated]) VALUES (N'BRAC-0013-52', N'13', N'12', N'690', N'790', N'45', N'0', N'2018-03-28 20:09:59.260', N'2018-03-28 20:09:59.260')
GO
GO
INSERT INTO [dbo].[Sku] ([SkuID], [ProductID], [SizeID], [BasePrice], [UnitPrice], [UnitsInStock], [UnitsOnOrder], [DateCreated], [DateUpdated]) VALUES (N'BRAC-0012-53', N'12', N'13', N'660', N'760', N'34', N'0', N'2018-03-28 20:09:05.820', N'2018-03-28 20:09:05.820')
GO
GO
INSERT INTO [dbo].[Sku] ([SkuID], [ProductID], [SizeID], [BasePrice], [UnitPrice], [UnitsInStock], [UnitsOnOrder], [DateCreated], [DateUpdated]) VALUES (N'BRAC-0013-53', N'13', N'13', N'700', N'800', N'7', N'0', N'2018-03-28 20:10:09.650', N'2018-03-28 20:10:09.650')
GO
GO
INSERT INTO [dbo].[Sku] ([SkuID], [ProductID], [SizeID], [BasePrice], [UnitPrice], [UnitsInStock], [UnitsOnOrder], [DateCreated], [DateUpdated]) VALUES (N'BRAC-0014-53', N'14', N'13', N'660', N'760', N'7', N'0', N'2018-03-28 20:10:32.110', N'2018-03-28 20:10:32.110')
GO
GO
INSERT INTO [dbo].[Sku] ([SkuID], [ProductID], [SizeID], [BasePrice], [UnitPrice], [UnitsInStock], [UnitsOnOrder], [DateCreated], [DateUpdated]) VALUES (N'BRAC-0012-54', N'12', N'14', N'670', N'770', N'2', N'0', N'2018-03-28 20:09:15.640', N'2018-03-28 20:09:15.640')
GO
GO
INSERT INTO [dbo].[Sku] ([SkuID], [ProductID], [SizeID], [BasePrice], [UnitPrice], [UnitsInStock], [UnitsOnOrder], [DateCreated], [DateUpdated]) VALUES (N'BRAC-0014-54', N'14', N'14', N'670', N'770', N'9', N'0', N'2018-03-28 20:10:42.307', N'2018-03-28 20:10:42.307')
GO
GO
INSERT INTO [dbo].[Sku] ([SkuID], [ProductID], [SizeID], [BasePrice], [UnitPrice], [UnitsInStock], [UnitsOnOrder], [DateCreated], [DateUpdated]) VALUES (N'BRAC-0016-54', N'16', N'14', N'1200', N'1300', N'9', N'0', N'2018-03-28 20:12:00.147', N'2018-03-28 20:12:00.147')
GO
GO
INSERT INTO [dbo].[Sku] ([SkuID], [ProductID], [SizeID], [BasePrice], [UnitPrice], [UnitsInStock], [UnitsOnOrder], [DateCreated], [DateUpdated]) VALUES (N'BRAC-0012-55', N'12', N'15', N'680', N'780', N'12', N'0', N'2018-03-28 20:09:24.380', N'2018-03-28 20:09:24.380')
GO
GO
INSERT INTO [dbo].[Sku] ([SkuID], [ProductID], [SizeID], [BasePrice], [UnitPrice], [UnitsInStock], [UnitsOnOrder], [DateCreated], [DateUpdated]) VALUES (N'BRAC-0014-55', N'14', N'15', N'680', N'780', N'12', N'0', N'2018-03-28 20:10:51.480', N'2018-03-28 20:10:51.480')
GO
GO
INSERT INTO [dbo].[Sku] ([SkuID], [ProductID], [SizeID], [BasePrice], [UnitPrice], [UnitsInStock], [UnitsOnOrder], [DateCreated], [DateUpdated]) VALUES (N'BRAC-0016-55', N'16', N'15', N'1270', N'1370', N'7', N'0', N'2018-03-28 20:12:14.320', N'2018-03-28 20:12:14.320')
GO
GO
INSERT INTO [dbo].[Sku] ([SkuID], [ProductID], [SizeID], [BasePrice], [UnitPrice], [UnitsInStock], [UnitsOnOrder], [DateCreated], [DateUpdated]) VALUES (N'BRAC-0015-56', N'15', N'16', N'1000', N'1100', N'3', N'0', N'2018-03-28 20:11:10.807', N'2018-03-28 20:11:10.807')
GO
GO
INSERT INTO [dbo].[Sku] ([SkuID], [ProductID], [SizeID], [BasePrice], [UnitPrice], [UnitsInStock], [UnitsOnOrder], [DateCreated], [DateUpdated]) VALUES (N'BRAC-0015-57', N'15', N'17', N'1150', N'1250', N'6', N'0', N'2018-03-28 20:11:26.713', N'2018-03-28 20:11:26.713')
GO
GO
INSERT INTO [dbo].[Sku] ([SkuID], [ProductID], [SizeID], [BasePrice], [UnitPrice], [UnitsInStock], [UnitsOnOrder], [DateCreated], [DateUpdated]) VALUES (N'BRAC-0015-58', N'15', N'18', N'1220', N'1320', N'2', N'0', N'2018-03-28 20:11:37.487', N'2018-03-28 20:11:37.487')
GO
GO
INSERT INTO [dbo].[Sku] ([SkuID], [ProductID], [SizeID], [BasePrice], [UnitPrice], [UnitsInStock], [UnitsOnOrder], [DateCreated], [DateUpdated]) VALUES (N'EARR-0024', N'24', null, N'900', N'1000', N'20', N'0', N'2018-03-28 20:34:15.560', N'2018-03-28 20:34:15.560')
GO
GO
INSERT INTO [dbo].[Sku] ([SkuID], [ProductID], [SizeID], [BasePrice], [UnitPrice], [UnitsInStock], [UnitsOnOrder], [DateCreated], [DateUpdated]) VALUES (N'EARR-0025', N'25', null, N'1000', N'2000', N'5', N'0', N'2018-03-28 20:34:38.843', N'2018-03-28 20:34:38.843')
GO
GO
INSERT INTO [dbo].[Sku] ([SkuID], [ProductID], [SizeID], [BasePrice], [UnitPrice], [UnitsInStock], [UnitsOnOrder], [DateCreated], [DateUpdated]) VALUES (N'EARR-0026', N'26', null, N'300', N'400', N'10', N'0', N'2018-03-28 20:34:53.297', N'2018-03-28 20:34:53.297')
GO
GO
INSERT INTO [dbo].[Sku] ([SkuID], [ProductID], [SizeID], [BasePrice], [UnitPrice], [UnitsInStock], [UnitsOnOrder], [DateCreated], [DateUpdated]) VALUES (N'EARR-0027', N'27', null, N'600', N'700', N'5', N'0', N'2018-03-28 20:35:07.910', N'2018-03-28 20:35:07.910')
GO
GO
INSERT INTO [dbo].[Sku] ([SkuID], [ProductID], [SizeID], [BasePrice], [UnitPrice], [UnitsInStock], [UnitsOnOrder], [DateCreated], [DateUpdated]) VALUES (N'EARR-0028', N'28', null, N'450', N'550', N'5', N'0', N'2018-03-28 20:35:25.837', N'2018-03-28 20:35:25.837')
GO
GO
INSERT INTO [dbo].[Sku] ([SkuID], [ProductID], [SizeID], [BasePrice], [UnitPrice], [UnitsInStock], [UnitsOnOrder], [DateCreated], [DateUpdated]) VALUES (N'EARR-0029', N'29', null, N'800', N'900', N'10', N'0', N'2018-03-28 20:35:36.647', N'2018-03-28 20:35:36.647')
GO
GO
INSERT INTO [dbo].[Sku] ([SkuID], [ProductID], [SizeID], [BasePrice], [UnitPrice], [UnitsInStock], [UnitsOnOrder], [DateCreated], [DateUpdated]) VALUES (N'NECK-0018-40', N'18', N'21', N'2300', N'2400', N'5', N'0', N'2018-03-28 20:19:59.670', N'2018-03-28 20:19:59.670')
GO
GO
INSERT INTO [dbo].[Sku] ([SkuID], [ProductID], [SizeID], [BasePrice], [UnitPrice], [UnitsInStock], [UnitsOnOrder], [DateCreated], [DateUpdated]) VALUES (N'NECK-0019-40', N'19', N'21', N'2750', N'2850', N'5', N'0', N'2018-03-28 20:20:41.547', N'2018-03-28 20:20:41.547')
GO
GO
INSERT INTO [dbo].[Sku] ([SkuID], [ProductID], [SizeID], [BasePrice], [UnitPrice], [UnitsInStock], [UnitsOnOrder], [DateCreated], [DateUpdated]) VALUES (N'NECK-0023-40', N'23', N'21', N'2700', N'2800', N'18', N'0', N'2018-03-28 20:25:29.920', N'2018-03-28 20:25:29.920')
GO
GO
INSERT INTO [dbo].[Sku] ([SkuID], [ProductID], [SizeID], [BasePrice], [UnitPrice], [UnitsInStock], [UnitsOnOrder], [DateCreated], [DateUpdated]) VALUES (N'NECK-0017-41', N'17', N'22', N'2300', N'2400', N'10', N'0', N'2018-03-28 20:13:12.813', N'2018-03-28 20:13:12.813')
GO
GO
INSERT INTO [dbo].[Sku] ([SkuID], [ProductID], [SizeID], [BasePrice], [UnitPrice], [UnitsInStock], [UnitsOnOrder], [DateCreated], [DateUpdated]) VALUES (N'NECK-0018-41', N'18', N'22', N'2350', N'2450', N'5', N'0', N'2018-03-28 20:20:11.507', N'2018-03-28 20:20:11.507')
GO
GO
INSERT INTO [dbo].[Sku] ([SkuID], [ProductID], [SizeID], [BasePrice], [UnitPrice], [UnitsInStock], [UnitsOnOrder], [DateCreated], [DateUpdated]) VALUES (N'NECK-0019-41', N'19', N'22', N'2800', N'2900', N'9', N'0', N'2018-03-28 20:20:53.347', N'2018-03-28 20:20:53.347')
GO
GO
INSERT INTO [dbo].[Sku] ([SkuID], [ProductID], [SizeID], [BasePrice], [UnitPrice], [UnitsInStock], [UnitsOnOrder], [DateCreated], [DateUpdated]) VALUES (N'NECK-0021-41', N'21', N'22', N'1700', N'1800', N'18', N'0', N'2018-03-28 20:22:44.733', N'2018-03-28 20:22:44.733')
GO
GO
INSERT INTO [dbo].[Sku] ([SkuID], [ProductID], [SizeID], [BasePrice], [UnitPrice], [UnitsInStock], [UnitsOnOrder], [DateCreated], [DateUpdated]) VALUES (N'NECK-0023-41', N'23', N'22', N'2800', N'2900', N'14', N'0', N'2018-03-28 20:26:27.260', N'2018-03-28 20:26:27.260')
GO
GO
INSERT INTO [dbo].[Sku] ([SkuID], [ProductID], [SizeID], [BasePrice], [UnitPrice], [UnitsInStock], [UnitsOnOrder], [DateCreated], [DateUpdated]) VALUES (N'NECK-0017-42', N'17', N'23', N'2350', N'2450', N'5', N'0', N'2018-03-28 20:19:16.957', N'2018-03-28 20:19:16.957')
GO
GO
INSERT INTO [dbo].[Sku] ([SkuID], [ProductID], [SizeID], [BasePrice], [UnitPrice], [UnitsInStock], [UnitsOnOrder], [DateCreated], [DateUpdated]) VALUES (N'NECK-0020-42', N'20', N'23', N'2400', N'2500', N'7', N'0', N'2018-03-28 20:21:21.703', N'2018-03-28 20:21:21.703')
GO
GO
INSERT INTO [dbo].[Sku] ([SkuID], [ProductID], [SizeID], [BasePrice], [UnitPrice], [UnitsInStock], [UnitsOnOrder], [DateCreated], [DateUpdated]) VALUES (N'NECK-0021-42', N'21', N'23', N'1750', N'1850', N'10', N'0', N'2018-03-28 20:23:00.123', N'2018-03-28 20:23:00.123')
GO
GO
INSERT INTO [dbo].[Sku] ([SkuID], [ProductID], [SizeID], [BasePrice], [UnitPrice], [UnitsInStock], [UnitsOnOrder], [DateCreated], [DateUpdated]) VALUES (N'NECK-0017-43', N'17', N'24', N'2400', N'2500', N'5', N'0', N'2018-03-28 20:19:27.210', N'2018-03-28 20:19:27.210')
GO
GO
INSERT INTO [dbo].[Sku] ([SkuID], [ProductID], [SizeID], [BasePrice], [UnitPrice], [UnitsInStock], [UnitsOnOrder], [DateCreated], [DateUpdated]) VALUES (N'NECK-0020-43', N'20', N'24', N'2500', N'2600', N'10', N'0', N'2018-03-28 20:21:32.773', N'2018-03-28 20:21:32.773')
GO
GO
INSERT INTO [dbo].[Sku] ([SkuID], [ProductID], [SizeID], [BasePrice], [UnitPrice], [UnitsInStock], [UnitsOnOrder], [DateCreated], [DateUpdated]) VALUES (N'NECK-0021-43', N'21', N'24', N'1800', N'1900', N'15', N'0', N'2018-03-28 20:23:17.927', N'2018-03-28 20:23:17.927')
GO
GO
INSERT INTO [dbo].[Sku] ([SkuID], [ProductID], [SizeID], [BasePrice], [UnitPrice], [UnitsInStock], [UnitsOnOrder], [DateCreated], [DateUpdated]) VALUES (N'NECK-0022-43', N'22', N'24', N'3200', N'3300', N'9', N'0', N'2018-03-28 20:24:04.417', N'2018-03-28 20:24:04.417')
GO
GO
INSERT INTO [dbo].[Sku] ([SkuID], [ProductID], [SizeID], [BasePrice], [UnitPrice], [UnitsInStock], [UnitsOnOrder], [DateCreated], [DateUpdated]) VALUES (N'NECK-0020-44', N'20', N'25', N'2600', N'2700', N'10', N'0', N'2018-03-28 20:21:42.250', N'2018-03-28 20:21:42.250')
GO
GO
INSERT INTO [dbo].[Sku] ([SkuID], [ProductID], [SizeID], [BasePrice], [UnitPrice], [UnitsInStock], [UnitsOnOrder], [DateCreated], [DateUpdated]) VALUES (N'NECK-0022-44', N'22', N'25', N'3400', N'3500', N'14', N'0', N'2018-03-28 20:24:09.550', N'2018-03-28 20:24:09.550')
GO
GO
INSERT INTO [dbo].[Sku] ([SkuID], [ProductID], [SizeID], [BasePrice], [UnitPrice], [UnitsInStock], [UnitsOnOrder], [DateCreated], [DateUpdated]) VALUES (N'NECK-0020-45', N'20', N'26', N'2700', N'2800', N'10', N'0', N'2018-03-28 20:21:55.500', N'2018-03-28 20:21:55.500')
GO
GO
INSERT INTO [dbo].[Sku] ([SkuID], [ProductID], [SizeID], [BasePrice], [UnitPrice], [UnitsInStock], [UnitsOnOrder], [DateCreated], [DateUpdated]) VALUES (N'NECK-0022-45', N'22', N'26', N'3600', N'3700', N'10', N'0', N'2018-03-28 20:24:51.300', N'2018-03-28 20:24:51.300')
GO
GO
INSERT INTO [dbo].[Sku] ([SkuID], [ProductID], [SizeID], [BasePrice], [UnitPrice], [UnitsInStock], [UnitsOnOrder], [DateCreated], [DateUpdated]) VALUES (N'RING-0001-04', N'1', N'1', N'890', N'990', N'7', N'0', N'2018-03-28 19:24:39.157', N'2018-03-28 19:24:39.157')
GO
GO
INSERT INTO [dbo].[Sku] ([SkuID], [ProductID], [SizeID], [BasePrice], [UnitPrice], [UnitsInStock], [UnitsOnOrder], [DateCreated], [DateUpdated]) VALUES (N'RING-0002-04', N'2', N'1', N'700', N'800', N'18', N'0', N'2018-03-28 19:31:23.860', N'2018-03-28 19:31:23.860')
GO
GO
INSERT INTO [dbo].[Sku] ([SkuID], [ProductID], [SizeID], [BasePrice], [UnitPrice], [UnitsInStock], [UnitsOnOrder], [DateCreated], [DateUpdated]) VALUES (N'RING-0004-04', N'4', N'1', N'1000', N'1100', N'6', N'0', N'2018-03-28 19:39:08.247', N'2018-03-28 19:39:08.247')
GO
GO
INSERT INTO [dbo].[Sku] ([SkuID], [ProductID], [SizeID], [BasePrice], [UnitPrice], [UnitsInStock], [UnitsOnOrder], [DateCreated], [DateUpdated]) VALUES (N'RING-0001-05', N'1', N'2', N'900', N'1000', N'5', N'0', N'2018-03-28 19:25:43.370', N'2018-03-28 19:25:43.370')
GO
GO
INSERT INTO [dbo].[Sku] ([SkuID], [ProductID], [SizeID], [BasePrice], [UnitPrice], [UnitsInStock], [UnitsOnOrder], [DateCreated], [DateUpdated]) VALUES (N'RING-0002-05', N'2', N'2', N'710', N'810', N'7', N'0', N'2018-03-28 19:33:19.630', N'2018-03-28 19:33:19.630')
GO
GO
INSERT INTO [dbo].[Sku] ([SkuID], [ProductID], [SizeID], [BasePrice], [UnitPrice], [UnitsInStock], [UnitsOnOrder], [DateCreated], [DateUpdated]) VALUES (N'RING-0003-05', N'3', N'2', N'850', N'950', N'5', N'0', N'2018-03-28 19:36:01.863', N'2018-03-28 19:36:01.863')
GO
GO
INSERT INTO [dbo].[Sku] ([SkuID], [ProductID], [SizeID], [BasePrice], [UnitPrice], [UnitsInStock], [UnitsOnOrder], [DateCreated], [DateUpdated]) VALUES (N'RING-0004-05', N'4', N'2', N'1100', N'1200', N'9', N'0', N'2018-03-28 19:39:08.250', N'2018-03-28 19:39:08.250')
GO
GO
INSERT INTO [dbo].[Sku] ([SkuID], [ProductID], [SizeID], [BasePrice], [UnitPrice], [UnitsInStock], [UnitsOnOrder], [DateCreated], [DateUpdated]) VALUES (N'RING-0009-05', N'9', N'2', N'870', N'970', N'14', N'0', N'2018-03-28 19:49:35.357', N'2018-03-28 19:49:35.357')
GO
GO
INSERT INTO [dbo].[Sku] ([SkuID], [ProductID], [SizeID], [BasePrice], [UnitPrice], [UnitsInStock], [UnitsOnOrder], [DateCreated], [DateUpdated]) VALUES (N'RING-0001-06', N'1', N'3', N'910', N'1010', N'5', N'0', N'2018-03-28 19:30:24.600', N'2018-03-28 19:30:24.600')
GO
GO
INSERT INTO [dbo].[Sku] ([SkuID], [ProductID], [SizeID], [BasePrice], [UnitPrice], [UnitsInStock], [UnitsOnOrder], [DateCreated], [DateUpdated]) VALUES (N'RING-0002-06', N'2', N'3', N'720', N'820', N'3', N'0', N'2018-03-28 19:35:18.853', N'2018-03-28 19:35:18.853')
GO
GO
INSERT INTO [dbo].[Sku] ([SkuID], [ProductID], [SizeID], [BasePrice], [UnitPrice], [UnitsInStock], [UnitsOnOrder], [DateCreated], [DateUpdated]) VALUES (N'RING-0003-06', N'3', N'3', N'860', N'960', N'3', N'0', N'2018-03-28 19:36:17.580', N'2018-03-28 19:36:17.580')
GO
GO
INSERT INTO [dbo].[Sku] ([SkuID], [ProductID], [SizeID], [BasePrice], [UnitPrice], [UnitsInStock], [UnitsOnOrder], [DateCreated], [DateUpdated]) VALUES (N'RING-0004-06', N'4', N'3', N'1200', N'1300', N'3', N'0', N'2018-03-28 19:40:15.440', N'2018-03-28 19:40:15.440')
GO
GO
INSERT INTO [dbo].[Sku] ([SkuID], [ProductID], [SizeID], [BasePrice], [UnitPrice], [UnitsInStock], [UnitsOnOrder], [DateCreated], [DateUpdated]) VALUES (N'RING-0009-06', N'9', N'3', N'890', N'990', N'20', N'0', N'2018-03-28 19:49:48.857', N'2018-03-28 19:49:48.857')
GO
GO
INSERT INTO [dbo].[Sku] ([SkuID], [ProductID], [SizeID], [BasePrice], [UnitPrice], [UnitsInStock], [UnitsOnOrder], [DateCreated], [DateUpdated]) VALUES (N'RING-0001-07', N'1', N'4', N'920', N'1020', N'3', N'0', N'2018-03-28 19:30:50.580', N'2018-03-28 19:30:50.580')
GO
GO
INSERT INTO [dbo].[Sku] ([SkuID], [ProductID], [SizeID], [BasePrice], [UnitPrice], [UnitsInStock], [UnitsOnOrder], [DateCreated], [DateUpdated]) VALUES (N'RING-0003-07', N'3', N'4', N'870', N'970', N'2', N'0', N'2018-03-28 19:36:32.680', N'2018-03-28 19:36:32.680')
GO
GO
INSERT INTO [dbo].[Sku] ([SkuID], [ProductID], [SizeID], [BasePrice], [UnitPrice], [UnitsInStock], [UnitsOnOrder], [DateCreated], [DateUpdated]) VALUES (N'RING-0006-07', N'6', N'4', N'890', N'990', N'5', N'0', N'2018-03-28 19:42:09.153', N'2018-03-28 19:42:09.153')
GO
GO
INSERT INTO [dbo].[Sku] ([SkuID], [ProductID], [SizeID], [BasePrice], [UnitPrice], [UnitsInStock], [UnitsOnOrder], [DateCreated], [DateUpdated]) VALUES (N'RING-0008-07', N'8', N'4', N'1200', N'1300', N'2', N'0', N'2018-03-28 19:43:50.980', N'2018-03-28 19:43:50.980')
GO
GO
INSERT INTO [dbo].[Sku] ([SkuID], [ProductID], [SizeID], [BasePrice], [UnitPrice], [UnitsInStock], [UnitsOnOrder], [DateCreated], [DateUpdated]) VALUES (N'RING-0009-07', N'9', N'4', N'900', N'1000', N'10', N'0', N'2018-03-28 19:50:01.690', N'2018-03-28 19:50:01.690')
GO
GO
INSERT INTO [dbo].[Sku] ([SkuID], [ProductID], [SizeID], [BasePrice], [UnitPrice], [UnitsInStock], [UnitsOnOrder], [DateCreated], [DateUpdated]) VALUES (N'RING-0003-08', N'3', N'5', N'880', N'980', N'8', N'0', N'2018-03-28 19:36:43.273', N'2018-03-28 19:36:43.273')
GO
GO
INSERT INTO [dbo].[Sku] ([SkuID], [ProductID], [SizeID], [BasePrice], [UnitPrice], [UnitsInStock], [UnitsOnOrder], [DateCreated], [DateUpdated]) VALUES (N'RING-0006-08', N'6', N'5', N'900', N'100', N'15', N'0', N'2018-03-28 19:42:25.757', N'2018-03-28 19:42:25.757')
GO
GO
INSERT INTO [dbo].[Sku] ([SkuID], [ProductID], [SizeID], [BasePrice], [UnitPrice], [UnitsInStock], [UnitsOnOrder], [DateCreated], [DateUpdated]) VALUES (N'RING-0008-08', N'8', N'5', N'1300', N'1400', N'5', N'0', N'2018-03-28 19:44:06.400', N'2018-03-28 19:44:06.400')
GO
GO
INSERT INTO [dbo].[Sku] ([SkuID], [ProductID], [SizeID], [BasePrice], [UnitPrice], [UnitsInStock], [UnitsOnOrder], [DateCreated], [DateUpdated]) VALUES (N'RING-0009-08', N'9', N'5', N'920', N'1020', N'2', N'0', N'2018-03-28 19:50:15.390', N'2018-03-28 19:50:15.390')
GO
GO
INSERT INTO [dbo].[Sku] ([SkuID], [ProductID], [SizeID], [BasePrice], [UnitPrice], [UnitsInStock], [UnitsOnOrder], [DateCreated], [DateUpdated]) VALUES (N'RING-0003-09', N'3', N'6', N'890', N'990', N'1', N'0', N'2018-03-28 19:36:54.673', N'2018-03-28 19:36:54.673')
GO
GO
INSERT INTO [dbo].[Sku] ([SkuID], [ProductID], [SizeID], [BasePrice], [UnitPrice], [UnitsInStock], [UnitsOnOrder], [DateCreated], [DateUpdated]) VALUES (N'RING-0005-09', N'5', N'6', N'1100', N'1200', N'12', N'0', N'2018-03-28 19:41:02.410', N'2018-03-28 19:41:02.410')
GO
GO
INSERT INTO [dbo].[Sku] ([SkuID], [ProductID], [SizeID], [BasePrice], [UnitPrice], [UnitsInStock], [UnitsOnOrder], [DateCreated], [DateUpdated]) VALUES (N'RING-0008-09', N'8', N'6', N'1400', N'1500', N'10', N'0', N'2018-03-28 19:46:15.380', N'2018-03-28 19:46:15.380')
GO
GO
INSERT INTO [dbo].[Sku] ([SkuID], [ProductID], [SizeID], [BasePrice], [UnitPrice], [UnitsInStock], [UnitsOnOrder], [DateCreated], [DateUpdated]) VALUES (N'RING-0009-09', N'9', N'6', N'930', N'1030', N'12', N'0', N'2018-03-28 19:50:29.730', N'2018-03-28 19:50:29.730')
GO
GO
INSERT INTO [dbo].[Sku] ([SkuID], [ProductID], [SizeID], [BasePrice], [UnitPrice], [UnitsInStock], [UnitsOnOrder], [DateCreated], [DateUpdated]) VALUES (N'RING-0005-10', N'5', N'7', N'1200', N'1300', N'12', N'0', N'2018-03-28 19:41:14.833', N'2018-03-28 19:41:14.833')
GO
GO
INSERT INTO [dbo].[Sku] ([SkuID], [ProductID], [SizeID], [BasePrice], [UnitPrice], [UnitsInStock], [UnitsOnOrder], [DateCreated], [DateUpdated]) VALUES (N'RING-0009-10', N'9', N'7', N'950', N'1050', N'3', N'0', N'2018-03-28 19:50:44.830', N'2018-03-28 19:50:44.830')
GO
GO
INSERT INTO [dbo].[Sku] ([SkuID], [ProductID], [SizeID], [BasePrice], [UnitPrice], [UnitsInStock], [UnitsOnOrder], [DateCreated], [DateUpdated]) VALUES (N'RING-0005-11', N'5', N'8', N'1300', N'1400', N'5', N'0', N'2018-03-28 19:41:35.890', N'2018-03-28 19:41:35.890')
GO
GO
INSERT INTO [dbo].[Sku] ([SkuID], [ProductID], [SizeID], [BasePrice], [UnitPrice], [UnitsInStock], [UnitsOnOrder], [DateCreated], [DateUpdated]) VALUES (N'RING-0007-12', N'7', N'8', N'1200', N'1300', N'8', N'0', N'2018-03-28 19:42:52.677', N'2018-03-28 19:42:52.677')
GO
GO
INSERT INTO [dbo].[Sku] ([SkuID], [ProductID], [SizeID], [BasePrice], [UnitPrice], [UnitsInStock], [UnitsOnOrder], [DateCreated], [DateUpdated]) VALUES (N'EARR-0030', N'30', null, N'500', N'600', N'5', N'0', N'2018-03-28 20:36:03.730', N'2018-03-28 20:36:03.730')
GO
GO

-- ----------------------------
-- Records of Bookmark
-- ----------------------------
INSERT INTO [dbo].[Bookmark] ([CustomerID], [ProductID], [DateCreated]) VALUES (N'1', N'1', N'2018-03-28 18:42:49.397')
GO
GO
INSERT INTO [dbo].[Bookmark] ([CustomerID], [ProductID], [DateCreated]) VALUES (N'1', N'2', N'2018-03-28 18:42:52.990')
GO
GO
INSERT INTO [dbo].[Bookmark] ([CustomerID], [ProductID], [DateCreated]) VALUES (N'1', N'3', N'2018-03-28 18:42:56.713')
GO
GO
INSERT INTO [dbo].[Bookmark] ([CustomerID], [ProductID], [DateCreated]) VALUES (N'2', N'4', N'2018-03-28 18:43:03.363')
GO
GO
INSERT INTO [dbo].[Bookmark] ([CustomerID], [ProductID], [DateCreated]) VALUES (N'2', N'5', N'2018-03-28 18:43:07.377')
GO
GO
INSERT INTO [dbo].[Bookmark] ([CustomerID], [ProductID], [DateCreated]) VALUES (N'2', N'6', N'2018-03-28 18:43:11.690')
GO
GO
INSERT INTO [dbo].[Bookmark] ([CustomerID], [ProductID], [DateCreated]) VALUES (N'3', N'1', N'2018-03-28 18:43:19.300')
GO
GO
INSERT INTO [dbo].[Bookmark] ([CustomerID], [ProductID], [DateCreated]) VALUES (N'3', N'2', N'2018-03-28 18:43:23.607')
GO
GO
INSERT INTO [dbo].[Bookmark] ([CustomerID], [ProductID], [DateCreated]) VALUES (N'3', N'3', N'2018-03-28 18:43:27.977')
GO
GO
INSERT INTO [dbo].[Bookmark] ([CustomerID], [ProductID], [DateCreated]) VALUES (N'3', N'5', N'2018-03-28 18:43:32.600')
GO
GO
INSERT INTO [dbo].[Bookmark] ([CustomerID], [ProductID], [DateCreated]) VALUES (N'3', N'13', N'2018-03-28 18:43:35.453')
GO
GO
INSERT INTO [dbo].[Bookmark] ([CustomerID], [ProductID], [DateCreated]) VALUES (N'4', N'10', N'2018-03-28 18:43:39.577')
GO
GO
INSERT INTO [dbo].[Bookmark] ([CustomerID], [ProductID], [DateCreated]) VALUES (N'4', N'14', N'2018-03-28 18:43:43.447')
GO
GO
INSERT INTO [dbo].[Bookmark] ([CustomerID], [ProductID], [DateCreated]) VALUES (N'4', N'20', N'2018-03-28 18:43:46.980')
GO
GO
INSERT INTO [dbo].[Bookmark] ([CustomerID], [ProductID], [DateCreated]) VALUES (N'6', N'7', N'2018-03-28 18:44:20.500')
GO
GO
INSERT INTO [dbo].[Bookmark] ([CustomerID], [ProductID], [DateCreated]) VALUES (N'6', N'9', N'2018-03-28 18:44:20.507')
GO
GO
INSERT INTO [dbo].[Bookmark] ([CustomerID], [ProductID], [DateCreated]) VALUES (N'8', N'22', N'2018-03-28 18:44:29.627')
GO
GO
INSERT INTO [dbo].[Bookmark] ([CustomerID], [ProductID], [DateCreated]) VALUES (N'8', N'23', N'2018-03-28 18:44:34.870')
GO
GO
INSERT INTO [dbo].[Bookmark] ([CustomerID], [ProductID], [DateCreated]) VALUES (N'10', N'15', N'2018-03-28 18:44:45.797')
GO
GO
INSERT INTO [dbo].[Bookmark] ([CustomerID], [ProductID], [DateCreated]) VALUES (N'10', N'16', N'2018-03-28 18:44:49.170')
GO
GO

-- ----------------------------
-- Records of Review
-- ----------------------------
INSERT INTO [dbo].[Review] ([ProductID], [CustomerID], [Title], [Content], [Rating], [AdminResponse], [DateCreated], [DateUpdated]) VALUES (N'1', N'1', N'Good Product', N'I love this product. Totally recommended.', N'100', N'Thank you for your response', N'2018-03-20 14:45:19.000', N'2018-03-20 14:45:27.153')
GO
GO
INSERT INTO [dbo].[Review] ([ProductID], [CustomerID], [Title], [Content], [Rating], [AdminResponse], [DateCreated], [DateUpdated]) VALUES (N'1', N'2', N'Not bad', N'The quality is fine. It''not that good though', N'60', N'Thank you for your response', N'2018-03-20 14:48:09.297', N'2018-03-20 14:48:09.297')
GO
GO
INSERT INTO [dbo].[Review] ([ProductID], [CustomerID], [Title], [Content], [Rating], [AdminResponse], [DateCreated], [DateUpdated]) VALUES (N'1', N'3', N'Wonderful', N'This is awesome. My wife loves it a lot', N'80', N'', N'2018-03-20 14:48:46.400', N'2018-03-20 14:48:46.400')
GO
GO
INSERT INTO [dbo].[Review] ([ProductID], [CustomerID], [Title], [Content], [Rating], [AdminResponse], [DateCreated], [DateUpdated]) VALUES (N'2', N'2', N'Great gift', N'I gave this to my girlfriend on her BDay and she loves it.', N'80', N'', N'2018-03-20 14:45:48.000', N'2018-03-20 14:45:54.483')
GO
GO
INSERT INTO [dbo].[Review] ([ProductID], [CustomerID], [Title], [Content], [Rating], [AdminResponse], [DateCreated], [DateUpdated]) VALUES (N'3', N'3', N'Nothing special', N'It''so so. Material is great but the style is too old-fashioned', N'60', N'', N'2018-03-20 14:46:15.847', N'2018-03-20 14:46:15.847')
GO
GO
INSERT INTO [dbo].[Review] ([ProductID], [CustomerID], [Title], [Content], [Rating], [AdminResponse], [DateCreated], [DateUpdated]) VALUES (N'4', N'4', N'I like it', N'It''suitable for many occasions', N'80', N'', N'2018-03-20 14:46:50.420', N'2018-03-20 14:46:50.420')
GO
GO
INSERT INTO [dbo].[Review] ([ProductID], [CustomerID], [Title], [Content], [Rating], [AdminResponse], [DateCreated], [DateUpdated]) VALUES (N'5', N'4', N'Nice Product', N'I like it', N'80', N'', N'2018-03-20 14:47:20.403', N'2018-03-20 14:47:20.403')
GO
GO
INSERT INTO [dbo].[Review] ([ProductID], [CustomerID], [Title], [Content], [Rating], [AdminResponse], [DateCreated], [DateUpdated]) VALUES (N'6', N'1', N'Shitty product', N'This product is terrible, the metal is off colored and the gem is fake!!! Don''buy it!', N'20', N'We will try to improve', N'2018-03-20 14:47:48.330', N'2018-03-20 14:47:48.330')
GO
GO
INSERT INTO [dbo].[Review] ([ProductID], [CustomerID], [Title], [Content], [Rating], [AdminResponse], [DateCreated], [DateUpdated]) VALUES (N'7', N'7', N'Luxury and cheap', N'Not too expensive but looks classy', N'60', N'', N'2018-03-20 14:49:24.337', N'2018-03-20 14:49:24.337')
GO
GO
INSERT INTO [dbo].[Review] ([ProductID], [CustomerID], [Title], [Content], [Rating], [AdminResponse], [DateCreated], [DateUpdated]) VALUES (N'8', N'8', N'Wow', N'Super great. I wore it to a party and everyone loved it.', N'100', N'', N'2018-03-20 14:49:58.190', N'2018-03-20 14:49:58.190')
GO
GO
INSERT INTO [dbo].[Review] ([ProductID], [CustomerID], [Title], [Content], [Rating], [AdminResponse], [DateCreated], [DateUpdated]) VALUES (N'9', N'4', N'Nice', N'Lovely gem. Love the color and the style. A bit pricy though', N'60', N'', N'2018-03-20 14:51:08.923', N'2018-03-20 14:51:08.923')
GO
GO
INSERT INTO [dbo].[Review] ([ProductID], [CustomerID], [Title], [Content], [Rating], [AdminResponse], [DateCreated], [DateUpdated]) VALUES (N'10', N'10', N'Great material', N'The material is high quality but the style is a little old', N'80', N'Thank you for your feedback', N'2018-03-20 14:51:31.670', N'2018-03-20 14:51:31.670')
GO
GO

-- ----------------------------
-- Records of ProductVoucher
-- ----------------------------
INSERT INTO [dbo].[ProductVoucher] ([VoucherID], [VoucherDesc], [DiscountValue], [Quantity], [ValidFrom], [ValidTo]) VALUES (N'VOU01', N'Happy Mother''s Day', N'20', N'10', N'2018-03-20 00:00:00.000', N'2018-03-23 00:00:00.000')
GO
GO
INSERT INTO [dbo].[ProductVoucher] ([VoucherID], [VoucherDesc], [DiscountValue], [Quantity], [ValidFrom], [ValidTo]) VALUES (N'VOU02', N'Happy Women''s Day', N'20', N'10', N'2018-03-23 00:00:00.000', N'2018-03-24 00:00:00.000')
GO
GO
INSERT INTO [dbo].[ProductVoucher] ([VoucherID], [VoucherDesc], [DiscountValue], [Quantity], [ValidFrom], [ValidTo]) VALUES (N'VOU03', N'Merry X''mas', N'20', N'10', N'2018-03-24 00:00:00.300', N'2018-03-25 00:00:00.300')
GO
GO
INSERT INTO [dbo].[ProductVoucher] ([VoucherID], [VoucherDesc], [DiscountValue], [Quantity], [ValidFrom], [ValidTo]) VALUES (N'VOU04', N'Happy New Year', N'20', N'10', N'2018-03-20 00:00:00.300', N'2018-03-23 00:00:00.300')
GO
GO
INSERT INTO [dbo].[ProductVoucher] ([VoucherID], [VoucherDesc], [DiscountValue], [Quantity], [ValidFrom], [ValidTo]) VALUES (N'VOU05', N'St.Valentine''s Day', N'20', N'10', N'2018-02-14 00:00:00.300', N'2018-02-15 00:00:00.300')
GO
GO

-- ----------------------------
-- Records of OrderMaster
-- ----------------------------
SET IDENTITY_INSERT [dbo].[OrderMaster] ON
GO
INSERT INTO [dbo].[OrderMaster] ([OrderID], [CustomerID], [VoucherID], [ShipDate], [ShipFee], [ShipName], [ShipAddress], [DateCreated], [DateUpdated], [OrderStatus], [ShipPhone], [shipEmail], [Payment]) VALUES (N'1', N'1', N'VOU01', N'2018-04-25', N'10', N'Haylee Lubowitz', N'Trang Quang Khai', N'2018-04-06 11:47:50.227', N'2018-04-06 11:47:50.227', N'Completed', N'0903341767', N'huephuong@hotmail.com', N'COD')
GO
GO
INSERT INTO [dbo].[OrderMaster] ([OrderID], [CustomerID], [VoucherID], [ShipDate], [ShipFee], [ShipName], [ShipAddress], [DateCreated], [DateUpdated], [OrderStatus], [ShipPhone], [shipEmail], [Payment]) VALUES (N'2', N'2', N'VOU01', N'2018-04-25', N'10', N'Pearline Reynolds', N'Le Quang Dinh', N'2018-04-06 11:47:50.227', N'2018-04-06 11:47:50.227', N'Completed', N'0903341767', N'phat@gmail.com', N'COD')
GO
GO
INSERT INTO [dbo].[OrderMaster] ([OrderID], [CustomerID], [VoucherID], [ShipDate], [ShipFee], [ShipName], [ShipAddress], [DateCreated], [DateUpdated], [OrderStatus], [ShipPhone], [shipEmail], [Payment]) VALUES (N'3', N'3', N'VOU02', N'2018-04-25', N'10', N'Angela Kilback', N'Phan Van Tri', N'2018-04-06 11:47:50.227', N'2018-04-06 11:47:50.227', N'Confirmed', N'0903341767', N'nguyen@yahoo.com', N'COD')
GO
GO
INSERT INTO [dbo].[OrderMaster] ([OrderID], [CustomerID], [VoucherID], [ShipDate], [ShipFee], [ShipName], [ShipAddress], [DateCreated], [DateUpdated], [OrderStatus], [ShipPhone], [shipEmail], [Payment]) VALUES (N'4', N'4', N'VOU02', N'2018-04-25', N'10', N'Zoila Dickinson', N'Bach Dang', N'2018-04-06 11:47:50.227', N'2018-04-06 11:47:50.227', N'Canceled', N'0903341767', N'naruto@gmail.com', N'CARD')
GO
GO
INSERT INTO [dbo].[OrderMaster] ([OrderID], [CustomerID], [VoucherID], [ShipDate], [ShipFee], [ShipName], [ShipAddress], [DateCreated], [DateUpdated], [OrderStatus], [ShipPhone], [shipEmail], [Payment]) VALUES (N'5', N'5', N'VOU03', N'2018-04-25', N'10', N'Elmira Wilderman', N'Dinh Bo Linh', N'2018-04-06 11:47:50.227', N'2018-04-06 11:47:50.227', N'Completed', N'0903341767', N'sakura@hotmail.com', N'COD')
GO
GO
INSERT INTO [dbo].[OrderMaster] ([OrderID], [CustomerID], [VoucherID], [ShipDate], [ShipFee], [ShipName], [ShipAddress], [DateCreated], [DateUpdated], [OrderStatus], [ShipPhone], [shipEmail], [Payment]) VALUES (N'6', N'1', N'VOU04', N'2018-04-30', N'10', N'Hue Phuong', N'W1 Phuoc Kien Nha Be', N'2018-04-06 11:59:42.410', N'2018-04-06 11:59:42.410', N'Confirmed', N'0903341767', N'huephuong@hotmail.com', N'COD')
GO
GO
INSERT INTO [dbo].[OrderMaster] ([OrderID], [CustomerID], [VoucherID], [ShipDate], [ShipFee], [ShipName], [ShipAddress], [DateCreated], [DateUpdated], [OrderStatus], [ShipPhone], [shipEmail], [Payment]) VALUES (N'7', N'2', N'VOU05', N'2018-04-30', N'10', N'Tan Phat', N'122 Cach Mang Thang 8', N'2018-04-06 12:00:17.517', N'2018-04-06 12:00:17.517', N'Pending', N'0903341767', N'phat@gmail.com', N'CARD')
GO
GO
INSERT INTO [dbo].[OrderMaster] ([OrderID], [CustomerID], [VoucherID], [ShipDate], [ShipFee], [ShipName], [ShipAddress], [DateCreated], [DateUpdated], [OrderStatus], [ShipPhone], [shipEmail], [Payment]) VALUES (N'8', N'3', null, N'2018-05-01', N'10', N'Khai Nguyen', N'Tran Binh Trong Q3', N'2018-04-06 12:01:13.620', N'2018-04-06 12:01:13.620', N'Pending', N'0903341767', N'nguyen@yahoo.com', N'CARD')
GO
GO
INSERT INTO [dbo].[OrderMaster] ([OrderID], [CustomerID], [VoucherID], [ShipDate], [ShipFee], [ShipName], [ShipAddress], [DateCreated], [DateUpdated], [OrderStatus], [ShipPhone], [shipEmail], [Payment]) VALUES (N'9', N'1', null, N'2018-04-20', N'10', N'Hue Phuong', N'Vo Van Kien', N'2018-04-06 12:32:20.277', N'2018-04-06 12:32:20.277', N'Completed', N'0903341767', N'huephuong@hotmail.com', N'COD')
GO
GO
SET IDENTITY_INSERT [dbo].[OrderMaster] OFF
GO

-- ----------------------------
-- Records of OrderDetail
-- ----------------------------
INSERT INTO [dbo].[OrderDetail] ([SkuID], [UnitPrice], [DiscountValue], [Quantity], [OrderID]) VALUES (N'BRAC-0010-50', N'700', N'10', N'2', N'1')
GO
GO
INSERT INTO [dbo].[OrderDetail] ([SkuID], [UnitPrice], [DiscountValue], [Quantity], [OrderID]) VALUES (N'BRAC-0011-51', N'710', N'10', N'1', N'3')
GO
GO
INSERT INTO [dbo].[OrderDetail] ([SkuID], [UnitPrice], [DiscountValue], [Quantity], [OrderID]) VALUES (N'BRAC-0013-52', N'790', N'10', N'1', N'5')
GO
GO
INSERT INTO [dbo].[OrderDetail] ([SkuID], [UnitPrice], [DiscountValue], [Quantity], [OrderID]) VALUES (N'BRAC-0012-53', N'760', N'10', N'1', N'1')
GO
GO
INSERT INTO [dbo].[OrderDetail] ([SkuID], [UnitPrice], [DiscountValue], [Quantity], [OrderID]) VALUES (N'BRAC-0015-57', N'1250', N'10', N'1', N'1')
GO
GO
INSERT INTO [dbo].[OrderDetail] ([SkuID], [UnitPrice], [DiscountValue], [Quantity], [OrderID]) VALUES (N'EARR-0028', N'550', N'10', N'1', N'4')
GO
GO
INSERT INTO [dbo].[OrderDetail] ([SkuID], [UnitPrice], [DiscountValue], [Quantity], [OrderID]) VALUES (N'NECK-0018-40', N'2400', N'10', N'1', N'4')
GO
GO
INSERT INTO [dbo].[OrderDetail] ([SkuID], [UnitPrice], [DiscountValue], [Quantity], [OrderID]) VALUES (N'RING-0009-07', N'1000', N'10', N'1', N'6')
GO
GO
INSERT INTO [dbo].[OrderDetail] ([SkuID], [UnitPrice], [DiscountValue], [Quantity], [OrderID]) VALUES (N'RING-0009-08', N'1020', null, N'2', N'9')
GO
GO
INSERT INTO [dbo].[OrderDetail] ([SkuID], [UnitPrice], [DiscountValue], [Quantity], [OrderID]) VALUES (N'RING-0009-10', N'1050', N'10', N'2', N'7')
GO
GO
INSERT INTO [dbo].[OrderDetail] ([SkuID], [UnitPrice], [DiscountValue], [Quantity], [OrderID]) VALUES (N'RING-0007-12', N'1300', N'10', N'2', N'2')
GO
GO
INSERT INTO [dbo].[OrderDetail] ([SkuID], [UnitPrice], [DiscountValue], [Quantity], [OrderID]) VALUES (N'EARR-0030', N'600', N'10', N'1', N'2')
GO
GO
INSERT INTO [dbo].[OrderDetail] ([SkuID], [UnitPrice], [DiscountValue], [Quantity], [OrderID]) VALUES (N'EARR-0030', N'600', null, N'2', N'8')
GO

-- ----------------------------
-- Records of AdminLog
-- ----------------------------
SET IDENTITY_INSERT [dbo].[AdminLog] ON
GO
INSERT INTO [dbo].[AdminLog] ([LogID], [Username], [LogContent], [dateLogged]) VALUES (N'1', N'huephuong', N'Initial Log', DEFAULT)
GO
SET IDENTITY_INSERT [dbo].[AdminLog] OFF
GO

-- ----------------------------
-- Records of Province
-- ----------------------------

CREATE TABLE Province (
	code	VARCHAR(6) PRIMARY KEY,
	name	VARCHAR(50) UNIQUE
)
GO

INSERT INTO province (code, name) VALUES ('VN-44', N'An Giang')
GO
INSERT INTO province (code, name) VALUES ('VN-43', N'Ba Ria-Vung Tau')
GO
INSERT INTO province (code, name) VALUES ('VN-54', N'Bac Giang')
GO
INSERT INTO province (code, name) VALUES ('VN-53', N'Bac Kan')
GO
INSERT INTO Province (code, name) VALUES ('VN-55', N'Bac Lieu')
GO
INSERT INTO Province (code, name) VALUES ('VN-56', N'Bac Ninh')
GO
INSERT INTO Province (code, name) VALUES ('VN-50', N'Ben Tre')
GO
INSERT INTO Province (code, name) VALUES ('VN-31', N'Binh Dinh')
GO
INSERT INTO Province (code, name) VALUES ('VN-57', N'Binh Duong')
GO
INSERT INTO Province (code, name) VALUES ('VN-58', N'Binh Phuoc')
GO
INSERT INTO Province (code, name) VALUES ('VN-40', N'Binh Thuan')
GO
INSERT INTO Province (code, name) VALUES ('VN-59', N'Ca Mau')
GO
INSERT INTO Province (code, name) VALUES ('VN-CT', N'Can Tho')
GO
INSERT INTO Province (code, name) VALUES ('VN-04', N'Cao Bang')
GO
INSERT INTO Province (code, name) VALUES ('VN-DN', N'Da Nang')
GO
INSERT INTO Province (code, name) VALUES ('VN-33', N'Dak Lak')
GO
INSERT INTO Province (code, name) VALUES ('VN-72', N'Dak Nong')
GO
INSERT INTO Province (code, name) VALUES ('VN-71', N'Dien Bien')
GO
INSERT INTO Province (code, name) VALUES ('VN-39', N'Dong Nai')
GO
INSERT INTO Province (code, name) VALUES ('VN-45', N'Dong Thap')
GO
INSERT INTO Province (code, name) VALUES ('VN-30', N'Gia Lai')
GO
INSERT INTO Province (code, name) VALUES ('VN-03', N'Ha Giang')
GO
INSERT INTO Province (code, name) VALUES ('VN-63', N'Ha Nam')
GO
INSERT INTO Province (code, name) VALUES ('VN-HN', N'Ha Noi')
GO
INSERT INTO Province (code, name) VALUES ('VN-23', N'Ha Tinh')
GO
INSERT INTO Province (code, name) VALUES ('VN-61', N'Hai Duong')
GO
INSERT INTO Province (code, name) VALUES ('VN-HP', N'Hai Phong')
GO
INSERT INTO Province (code, name) VALUES ('VN-73', N'Hau Giang')
GO
INSERT INTO Province (code, name) VALUES ('VN-SG', N'Ho Chi Minh')
GO
INSERT INTO Province (code, name) VALUES ('VN-14', N'Hoa Binh')
GO
INSERT INTO Province (code, name) VALUES ('VN-66', N'Hung Yen')
GO
INSERT INTO Province (code, name) VALUES ('VN-34', N'Khanh Hoa')
GO
INSERT INTO Province (code, name) VALUES ('VN-47', N'Kien Giang')
GO
INSERT INTO Province (code, name) VALUES ('VN-28', N'Kon Tum')
GO
INSERT INTO Province (code, name) VALUES ('VN-01', N'Lai Chau')
GO
INSERT INTO Province (code, name) VALUES ('VN-35', N'Lam Dong')
GO
INSERT INTO Province (code, name) VALUES ('VN-09', N'Lang Son')
GO
INSERT INTO Province (code, name) VALUES ('VN-02', N'Lao Cai')
GO
INSERT INTO Province (code, name) VALUES ('VN-41', N'Long An')
GO
INSERT INTO Province (code, name) VALUES ('VN-67', N'Nam Dinh')
GO
INSERT INTO Province (code, name) VALUES ('VN-22', N'Nghe An')
GO
INSERT INTO Province (code, name) VALUES ('VN-18', N'Ninh Binh')
GO
INSERT INTO Province (code, name) VALUES ('VN-36', N'Ninh Thuan')
GO
INSERT INTO Province (code, name) VALUES ('VN-68', N'Phu Tho')
GO
INSERT INTO Province (code, name) VALUES ('VN-32', N'Phu Yen')
GO
INSERT INTO Province (code, name) VALUES ('VN-24', N'Quang Binh')
GO
INSERT INTO Province (code, name) VALUES ('VN-27', N'Quang Nam')
GO
INSERT INTO Province (code, name) VALUES ('VN-29', N'Quang Ngai')
GO
INSERT INTO Province (code, name) VALUES ('VN-13', N'Quang Ninh')
GO
INSERT INTO Province (code, name) VALUES ('VN-25', N'Quang Tri')
GO
INSERT INTO Province (code, name) VALUES ('VN-52', N'Soc Trang')
GO
INSERT INTO Province (code, name) VALUES ('VN-05', N'Son La')
GO
INSERT INTO Province (code, name) VALUES ('VN-37', N'Tay Ninh')
GO
INSERT INTO Province (code, name) VALUES ('VN-20', N'Thai Binh')
GO
INSERT INTO Province (code, name) VALUES ('VN-69', N'Thai Nguyen')
GO
INSERT INTO Province (code, name) VALUES ('VN-21', N'Thanh Hoa')
GO
INSERT INTO Province (code, name) VALUES ('VN-26', N'Thua Thien Hue')
GO
INSERT INTO Province (code, name) VALUES ('VN-46', N'Tien Giang')
GO
INSERT INTO Province (code, name) VALUES ('VN-51', N'Tra Vinh')
GO
INSERT INTO Province (code, name) VALUES ('VN-07', N'Tuyen Quang')
GO
INSERT INTO Province (code, name) VALUES ('VN-49', N'Vinh Long')
GO
INSERT INTO Province (code, name) VALUES ('VN-70', N'Vinh Phuc')
GO
INSERT INTO Province (code, name) VALUES ('VN-06', N'Yen Bai')
GO