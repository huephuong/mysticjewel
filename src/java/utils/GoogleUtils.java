/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import java.io.IOException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.fluent.Form;
import org.apache.http.client.fluent.Request;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author nknguyengl
 */
public class GoogleUtils {

    public static final String GOOGLE_CLIENT_ID = "1080859875559-3b9ik19m23o1p3un1uufmv6cqvtljl13.apps.googleusercontent.com";
    public static final String GOOGLE_CLIENT_SECRET = "csTyWEbgsedg5kzDa2K3DY5p";
    public static final String GOOGLE_REDIRECT_URI = "http://localhost:8080/MysticJewel/gg-login";
    public static final String GOOGLE_LINK_GET_TOKEN = "https://accounts.google.com/o/oauth2/token";
    public static final String GOOGLE_LINK_GET_USER_INFO = "https://www.googleapis.com/oauth2/v1/userinfo?access_token=";
    public static final String GOOGLE_GRANT_TYPE = "authorization_code";

    public String getGGAuthUrl() {
        String ggLoginUrl = "";
        try {
            ggLoginUrl = "https://accounts.google.com/o/oauth2/auth?" + "client_id="
                    + GOOGLE_CLIENT_ID + "&redirect_uri="
                    + URLEncoder.encode(GOOGLE_REDIRECT_URI, "UTF-8")
                    + "&scope=email&response_type=code&approval_prompt=force";
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return ggLoginUrl;
    }

    public static String getToken(final String code) throws ClientProtocolException, IOException {
        String response = Request.Post(GOOGLE_LINK_GET_TOKEN)
                .bodyForm(Form.form().add("client_id", GOOGLE_CLIENT_ID)
                        .add("client_secret", GOOGLE_CLIENT_SECRET)
                        .add("redirect_uri", GOOGLE_REDIRECT_URI)
                        .add("code", code)
                        .add("grant_type", GOOGLE_GRANT_TYPE).build())
                .execute().returnContent().asString();
        JsonObject jobj = new Gson().fromJson(response, JsonObject.class);
        String accessToken = jobj.get("access_token").toString().replaceAll("\"", "");
        return accessToken;
    }

    public static Map<String, String> getData(final String accessToken) throws ClientProtocolException, IOException {
        String link = GOOGLE_LINK_GET_USER_INFO + accessToken;
        Map<String, String> ggProfile = new HashMap<String, String>();
        try {
            String response = Request.Get(link).execute().returnContent().asString();
            JSONObject json = new JSONObject(response);
            ggProfile.put("id", json.getString("id"));
            ggProfile.put("given_name", json.getString("given_name"));
            ggProfile.put("family_name", json.getString("family_name"));
            if (json.has("email")) {
                ggProfile.put("email", json.getString("email"));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return ggProfile;
    }

}
