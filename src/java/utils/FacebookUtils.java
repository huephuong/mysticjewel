/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import org.apache.http.client.fluent.Form;
import org.apache.http.client.fluent.Request;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author nknguyengl
 */
public class FacebookUtils {

    public static final String FACEBOOK_APP_ID = "152800598660318";
    public static final String FACEBOOK_APP_SECRET = "09ae433ee16555a591fbb50e5e6fa9dd";
    public static final String FACEBOOK_REDIRECT_URI = "http://localhost:8080/MysticJewel/fb-login";
    public static final String FACEBOOK_LINK_GET_TOKEN = "https://graph.facebook.com/oauth/access_token";
    public static final String FACEBOOK_LINK_GET_USER_INFO = "https://graph.facebook.com/me?access_token=";

    public String getFBAuthUrl() {
        String fbLoginUrl = "";
        try {
            fbLoginUrl = "http://www.facebook.com/dialog/oauth?" + "client_id="
                    + FACEBOOK_APP_ID + "&redirect_uri="
                    + URLEncoder.encode(FACEBOOK_REDIRECT_URI, "UTF-8")
                    + "&scope=email";
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return fbLoginUrl;
    }

    public String getToken(String code) throws IOException {
        String response = Request.Post(FACEBOOK_LINK_GET_TOKEN)
                .bodyForm(Form.form().add("client_id", FACEBOOK_APP_ID)
                        .add("client_secret", FACEBOOK_APP_SECRET)
                        .add("redirect_uri", FACEBOOK_REDIRECT_URI)
                        .add("code", code).build())
                .execute().returnContent().asString();
        JsonObject jobj = new Gson().fromJson(response, JsonObject.class);
        String accessToken = jobj.get("access_token").toString().replaceAll("\"", "");
        return accessToken;
    }

    public Map<String, String> getData(String accessToken) throws IOException {
        String link = FACEBOOK_LINK_GET_USER_INFO + accessToken + "&fields=id,first_name,last_name,email,gender";
        Map<String, String> fbProfile = new HashMap<String, String>();
        try {
            String response = Request.Get(link).execute().returnContent().asString();
            JSONObject json = new JSONObject(response);
            fbProfile.put("id", json.getString("id"));
            fbProfile.put("first_name", json.getString("first_name"));
            fbProfile.put("last_name", json.getString("last_name"));
            if (json.has("email")) {
                fbProfile.put("email", json.getString("email"));
            }
            if (json.has("gender")) {
                fbProfile.put("gender", json.getString("gender"));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return fbProfile;
    }
}
