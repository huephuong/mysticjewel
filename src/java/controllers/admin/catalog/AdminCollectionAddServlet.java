package controllers.admin.catalog;

import beans.ProductColFacadeLocal;
import entities.ProductCol;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "AdminCollectionAddServlet", urlPatterns = {"/admin-page/catalog/collection/edit"})
public class AdminCollectionAddServlet extends HttpServlet {

    @EJB
    private ProductColFacadeLocal productColFacade;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {

        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        ProductCol col = productColFacade.find(id);
        List<ProductCol> list = productColFacade.findAll();
        request.setAttribute("colList", list);
        request.setAttribute("currentCol", col);
        request.setAttribute("saveFn", "Update");
        request.getRequestDispatcher("/admin/catalog/collection_list.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getParameter("saveFn");
        if ("Add".equals(action)) {
            String name = request.getParameter("inputName");
            String desc = request.getParameter("inputDesc");
            String banner = request.getParameter("inputBanner");
            String image = request.getParameter("inputImage");
            boolean isTrending = "on".equals(request.getParameter("inputTrending"));
            boolean isActive = "on".equals(request.getParameter("inputActive"));
            ProductCol newCol = new ProductCol(name, banner, image, desc, isTrending, isActive);
            productColFacade.createProductCol(newCol);
        } else if ("Update".equals(action)) {
            int id = Integer.parseInt(request.getParameter("id"));
            ProductCol currentCol = productColFacade.find(id);
            String name = request.getParameter("inputName");
            String desc = request.getParameter("inputDesc");
            String banner = request.getParameter("inputBanner");
            String image = request.getParameter("inputImage");
            boolean isTrending = "on".equals(request.getParameter("inputTrending"));
            boolean isActive = "on".equals(request.getParameter("inputActive"));

            currentCol.setColName(name);
            currentCol.setColBanner(banner);
            currentCol.setColImage(image);
            currentCol.setColDesc(desc);
            currentCol.setIsTrending(isTrending);
            currentCol.setIsActive(isActive);
            if (banner == null) {
                currentCol.setColBanner(request.getParameter("oldBanner"));
            } else {
                currentCol.setColBanner(banner);
            }
            if (image == null) {
                currentCol.setColImage(request.getParameter("oldImage"));
            } else {
                currentCol.setColImage(image);
            }
            productColFacade.editProductCol(currentCol);
        }
        response.sendRedirect(request.getContextPath() + "/admin-page/catalog/collection");
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
