package controllers.admin.catalog;

import beans.AdminLogFacadeLocal;
import beans.AttributeFacadeLocal;
import beans.CategoryFacadeLocal;
import entities.AdminAccount;
import entities.Category;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(name = "AdminCategoryAddServlet", urlPatterns = {"/admin-page/catalog/category/edit"})
public class AdminCategoryAddServlet extends HttpServlet {
    @EJB
    private AdminLogFacadeLocal adminLogFacade;
    @EJB
    private CategoryFacadeLocal categoryFacade;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        Category cat = categoryFacade.find(id);
        List<Category> catList = categoryFacade.findAll();
        List<Category> mainList = categoryFacade.getMainCategory();
        request.setAttribute("currentCat", cat);
        request.setAttribute("catList", catList);
        request.setAttribute("mainList", mainList);
        request.setAttribute("saveFn", "Update");
        request.getRequestDispatcher("/admin/catalog/category_list.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        AdminAccount admin = (AdminAccount) session.getAttribute("admin");
        String action = request.getParameter("saveFn");
        if ("Add".equals(action)) {
            String name = request.getParameter("inputName");
            int parentID = Integer.parseInt(request.getParameter("inputParent"));
            Category parent = categoryFacade.find(parentID);
            Category newCat = new Category(name, parent);
            // trả về Category object
            categoryFacade.createCategory(newCat);
            adminLogFacade.createLog(admin.getAdminUserName(), "Add new category: " + newCat.getCategoryName());
        } else if ("Update".equals(action)) {
            int id = Integer.parseInt(request.getParameter("id"));
            String name = request.getParameter("inputName");
            int parentID = Integer.parseInt(request.getParameter("inputParent"));
            Category parent = categoryFacade.find(parentID);
            Category cat = categoryFacade.find(id);
            cat.setCategoryName(name);
            cat.setParentID(parent);
            categoryFacade.editCategory(cat);
            adminLogFacade.createLog(admin.getAdminUserName(), "Update category: " + cat.getCategoryName());
        }
        response.sendRedirect(request.getContextPath() + "/admin-page/catalog/category");
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
