package controllers.admin.catalog;

import beans.AdminLogFacadeLocal;
import beans.AttributeFacadeLocal;
import beans.AttributeGroupFacadeLocal;
import entities.AdminAccount;
import entities.Attribute;
import entities.AttributeGroup;
import java.io.IOException;
import java.io.PrintWriter;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(name = "AdminAttributeAddServlet", urlPatterns = {"/admin-page/catalog/attributes/edit"})
public class AdminAttributeAddServlet extends HttpServlet {
    @EJB
    private AdminLogFacadeLocal adminLogFacade;
    @EJB
    private AttributeGroupFacadeLocal attributeGroupFacade;
    @EJB
    private AttributeFacadeLocal attributeFacade;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {

        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        Attribute currentAtt = attributeFacade.find(id);
        request.setAttribute("currentAtt", currentAtt);
        request.setAttribute("attList", attributeFacade.findAll());
        request.setAttribute("groupList", attributeGroupFacade.findAll());
        request.setAttribute("saveFn", "Update");
        request.getRequestDispatcher("/admin/catalog/attribute_group.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getParameter("saveFn");
        HttpSession session = request.getSession();
        AdminAccount admin = (AdminAccount) session.getAttribute("admin");
        if ("Add".equals(action)) {
            String value = request.getParameter("inputAttValue");
            String isNew = request.getParameter("isNew");
            if ("on".equals(isNew)) {
                String parent = request.getParameter("inputNewGroup");
                AttributeGroup newGroup = attributeGroupFacade.createAttributeGroup(parent);
                Attribute newAtt = attributeFacade.createAttribute(value, newGroup);
                // admin log
                adminLogFacade.createLog(admin.getAdminUserName(), "Add new attribute group: " + newGroup.getAttributeGroupName() + ", new attribute: " + newAtt.getAttributeValue());
            } else {
                int parentID = Integer.parseInt(request.getParameter("inputParent"));
                AttributeGroup oldGroup = attributeGroupFacade.find(parentID);
                Attribute newAtt = attributeFacade.createAttribute(value, oldGroup);
                // admin log
                adminLogFacade.createLog(admin.getAdminUserName(), "Add new attribute: " + newAtt.getAttributeValue() + " to group: " + oldGroup.getAttributeGroupName());
            }
        } else if ("Update".equals(action)) {
            int id = Integer.parseInt(request.getParameter("id"));
            Attribute att = attributeFacade.find(id);
            String value = request.getParameter("inputAttValue");
            String isNew = request.getParameter("isNew");
            if ("on".equals(isNew)) {
                String parent = request.getParameter("inputNewGroup");
                AttributeGroup newGroup = attributeGroupFacade.createAttributeGroup(parent);
                att.setAttributeValue(value);
                att.setAttributeGroupID(newGroup);
            } else {
                int parentID = Integer.parseInt(request.getParameter("inputParent"));
                AttributeGroup oldGroup = attributeGroupFacade.find(parentID);
                att.setAttributeValue(value);
                att.setAttributeGroupID(oldGroup);
            }
            Attribute updateAtt = attributeFacade.editAttribute(att);
            // admin log
            adminLogFacade.createLog(admin.getAdminUserName(), "Update attribute with ID: " + updateAtt.getAttributeID() + ", value: " + updateAtt.getAttributeValue());
        }
        response.sendRedirect(request.getContextPath() + "/admin-page/catalog/attributes");
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
