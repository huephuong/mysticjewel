package controllers.admin.catalog;

import beans.ProductFacadeLocal;
import beans.ProductImageFacadeLocal;
import beans.SkuFacadeLocal;
import beans.SkuSizeFacadeLocal;
import entities.Product;
import entities.ProductImage;
import entities.Sku;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "AdminProductDetailAddServlet", urlPatterns = {"/admin-page/catalog/product-detail/add"})
public class AdminProductDetailAddServlet extends HttpServlet {
    @EJB
    private SkuSizeFacadeLocal skuSizeFacade;
    @EJB
    private ProductFacadeLocal productFacade;
    @EJB
    private ProductImageFacadeLocal productImageFacade;
    @EJB
    private SkuFacadeLocal skuFacade;
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            int productID = Integer.parseInt(request.getParameter("productID"));
            Product currentProd = productFacade.find(productID);
            
            String[] codeArr = request.getParameterValues("inputCode");
            String[] sizeArr = request.getParameterValues("inputSize");
            String[] basePriceArr = request.getParameterValues("inputBasePrice");
            String[] unitPriceArr = request.getParameterValues("inputUnitPrice");
            String[] unitsInStockArr = request.getParameterValues("inputUnitsInStock");
            Date dateCreated = new Date(System.currentTimeMillis());
            Date dateUpdated = new Date(System.currentTimeMillis());
            
            for (int i = 0; i < codeArr.length; i++) {
                Sku sku = new Sku();
                sku.setSkuID(codeArr[i]);
                sku.setProductID(currentProd);
                if (sizeArr == null) {
                    sku.setSizeID(null);
                } else {
                    sku.setSizeID(skuSizeFacade.find(Integer.parseInt(sizeArr[i])));
                }
                sku.setBasePrice(Integer.parseInt(basePriceArr[i]));
                sku.setUnitPrice(Integer.parseInt(unitPriceArr[i]));
                sku.setUnitsInStock(Integer.parseInt(unitsInStockArr[i]));
                sku.setUnitsOnOrder(0);
                sku.setDateCreated(dateCreated);
                sku.setDateUpdated(dateUpdated);
                // trả về sku object
                skuFacade.createSku(sku);
            }

            String[] imageArr = request.getParameterValues("inputImage");
            for (int i = 0; i < imageArr.length; i++) {
                ProductImage img = new ProductImage();
                img.setImageLink(imageArr[i]);
                img.setProductID(currentProd);
                // trả về image object
                productImageFacade.createImage(img);
            }
            
            // chuyển vào product detail sau
            response.sendRedirect(request.getContextPath() + "/admin-page/catalog/product");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
