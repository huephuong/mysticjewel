package controllers.admin.catalog;

import beans.AttributeFacadeLocal;
import beans.AttributeGroupFacadeLocal;
import beans.CategoryFacadeLocal;
import beans.ProductColFacadeLocal;
import beans.ProductFacadeLocal;
import entities.Attribute;
import entities.AttributeGroup;
import entities.Category;
import entities.Product;
import entities.ProductCol;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "AdminProductUpdateServlet", urlPatterns = {"/admin-page/catalog/product/update"})
public class AdminProductUpdateServlet extends HttpServlet {
    @EJB
    private AttributeFacadeLocal attributeFacade;
    @EJB
    private AttributeGroupFacadeLocal attributeGroupFacade;
    @EJB
    private ProductFacadeLocal productFacade;
    @EJB
    private ProductColFacadeLocal productColFacade;
    @EJB
    private CategoryFacadeLocal categoryFacade;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("productid"));
        List<Category> catList = categoryFacade.getSubCategory();
        List<ProductCol> colList = productColFacade.findAll();
        List<AttributeGroup> attList = attributeGroupFacade.findAll();
        request.setAttribute("prod", productFacade.find(id));
        request.setAttribute("catList", catList);
        request.setAttribute("colList", colList);
        request.setAttribute("attList", attList);
        request.getRequestDispatcher("/admin/catalog/product_update.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        String name = request.getParameter("inputName");
        String description = request.getParameter("inputDescription");
        Category cat = categoryFacade.find(Integer.parseInt(request.getParameter("inputCategory")));
        ProductCol col = null;
        if (!(request.getParameter("inputCollection")).equals("None")) {
            col = productColFacade.find(Integer.parseInt(request.getParameter("inputCollection")));
        }
        int discountValue = Integer.parseInt(request.getParameter("inputDiscountValue"));
        Date validFromDate = null;
        Date validToDate = null;
        // check discount value
        if (discountValue > 0) {
            String validDate = request.getParameter("inputValidDate");
            String validFrom = validDate.substring(0, 19);
            String validTo = validDate.substring(22);
            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm a");
            try {
                validFromDate = formatter.parse(validFrom);
                validToDate = formatter.parse(validTo);
            } catch (ParseException ex) {
                Logger.getLogger(AdminProductAddServlet.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        Date dateUpdated = new Date(System.currentTimeMillis());
        
        // Add attribute
        String[] attArray = request.getParameterValues("inputAttribute");
        List<Attribute> attributeList = new ArrayList<>();
        for (String s : attArray) {
            if (!"none".equals(s)) {
                Attribute temp = attributeFacade.find(Integer.parseInt(s));
                attributeList.add(temp);
            }
        }
        
        Product prod = productFacade.find(id);
        prod.setProductName(name);
        prod.setProductDesc(description);
        prod.setCategoryID(cat);
        prod.setColID(col);
        prod.setDiscountValue(discountValue);
        prod.setDiscountValidFrom(validFromDate);
        prod.setDiscountValidTo(validToDate);
        prod.setDateUpdated(dateUpdated);

        productFacade.editProduct(prod, attributeList);
        response.sendRedirect(request.getContextPath() + "/admin-page/catalog/product-detail?id=" + id);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
