/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package controllers.admin.catalog;

import beans.AdminLogFacadeLocal;
import beans.AttributeFacadeLocal;
import entities.AdminAccount;
import entities.Attribute;
import java.io.IOException;
import java.io.PrintWriter;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author HuePhuong
 */
@WebServlet(name = "AdminAttributeDeleteServlet", urlPatterns = {"/admin-page/catalog/attributes/delete"})
public class AdminAttributeDeleteServlet extends HttpServlet {
    @EJB
    private AdminLogFacadeLocal adminLogFacade;
    @EJB
    private AttributeFacadeLocal attributeFacade;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            HttpSession session = request.getSession();
            AdminAccount admin = (AdminAccount) session.getAttribute("admin");
            int id = Integer.parseInt(request.getParameter("id"));
            Attribute att = attributeFacade.find(id);
            attributeFacade.deleteAttribute(att);
            adminLogFacade.createLog(admin.getAdminUserName(), "Delete attribute - id: " + String.valueOf(id));
            response.sendRedirect(request.getContextPath() + "/admin-page/catalog/attributes");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
