package controllers.admin.catalog;

import beans.AdminLogFacadeLocal;
import beans.AttributeGroupFacadeLocal;
import entities.AdminAccount;
import entities.AttributeGroup;
import java.io.IOException;
import java.io.PrintWriter;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(name = "AdminAttributeGroupDeleteServlet", urlPatterns = {"/admin-page/catalog/attributes/deleteGroup"})
public class AdminAttributeGroupDeleteServlet extends HttpServlet {
    @EJB
    private AdminLogFacadeLocal adminLogFacade;
    @EJB
    private AttributeGroupFacadeLocal attributeGroupFacade;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            HttpSession session = request.getSession();
            AdminAccount admin = (AdminAccount) session.getAttribute("admin");
            int id = Integer.parseInt(request.getParameter("id"));
            AttributeGroup group = attributeGroupFacade.find(id);
            attributeGroupFacade.remove(group);
            adminLogFacade.createLog(admin.getAdminUserName(), "Delete attribute group - id: " + String.valueOf(id));
            response.sendRedirect(request.getContextPath() + "/admin-page/catalog/attributes");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
