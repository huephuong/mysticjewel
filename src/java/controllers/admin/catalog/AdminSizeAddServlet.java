/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers.admin.catalog;

import beans.CategoryFacadeLocal;
import beans.SkuSizeFacadeLocal;
import entities.Category;
import entities.SkuSize;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "AdminSizeAddServlet", urlPatterns = {"/admin-page/catalog/size/edit"})
public class AdminSizeAddServlet extends HttpServlet {

    @EJB
    private CategoryFacadeLocal categoryFacade;
    @EJB
    private SkuSizeFacadeLocal skuSizeFacade;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {

        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        List<SkuSize> sizeList = skuSizeFacade.findAll();
        List<Category> catList = categoryFacade.getMainCategory();
        int id = Integer.parseInt(request.getParameter("id"));
        SkuSize size = skuSizeFacade.find(id);
        request.setAttribute("currentSize", size);
        request.setAttribute("catList", catList);
        request.setAttribute("sizeList", sizeList);
        request.setAttribute("saveFn", "Update");
        request.getRequestDispatcher("/admin/catalog/size_list.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getParameter("saveFn");
        if ("Add".equals(action)) {
            String sizeValue = request.getParameter("inputValue");
            int categoryID = Integer.parseInt(request.getParameter("inputCategory"));
            Category cat = categoryFacade.find(categoryID);
            SkuSize newSize = new SkuSize(sizeValue, cat);
            // create trả về object
            skuSizeFacade.createSkuSize(newSize);
        } else if ("Update".equals(action)) {
            int id = Integer.parseInt(request.getParameter("id"));
            SkuSize size = skuSizeFacade.find(id);
            String sizeValue = request.getParameter("inputValue");
            int categoryID = Integer.parseInt(request.getParameter("inputCategory"));
            Category cat = categoryFacade.find(categoryID);
            size.setSizeValue(sizeValue);
            size.setCategoryID(cat);
            // create trả về object
            skuSizeFacade.editSkuSize(size);
        }
        response.sendRedirect(request.getContextPath() + "/admin-page/catalog/size");
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
