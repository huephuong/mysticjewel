package controllers.admin.review;

import beans.ReviewFacadeLocal;
import entities.Review;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author htp06
 */
@WebServlet(name = "ReviewServlet", urlPatterns = {"/admin-page/catalog/reviews"})
public class ReviewServlet extends HttpServlet {

    @EJB
    private ReviewFacadeLocal reviewFacade;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            if (request.getParameter("cusID") != null && request.getParameter("proID") != null) {
                int cusID = Integer.parseInt(request.getParameter("cusID"));
                int proID = Integer.parseInt(request.getParameter("proID"));
                Review rmReview = reviewFacade.findByReviewPK(cusID, proID);
                // delete review
                reviewFacade.removeReview(rmReview);
                response.sendRedirect(request.getContextPath() + "/admin-page/catalog/reviews");
            } else {
                List<Review> list = reviewFacade.findAll();
                request.setAttribute("reviewList", list);
                request.getRequestDispatcher("/admin/catalog/review_list.jsp").forward(request, response);
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
