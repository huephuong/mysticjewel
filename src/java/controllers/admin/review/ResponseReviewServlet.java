package controllers.admin.review;

import beans.ReviewFacadeLocal;
import entities.Review;
import entities.ReviewPK;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author htp06
 */
@WebServlet(name = "ResponseReviewServlet", urlPatterns = {"/admin-page/catalog/reviews/response"})
public class ResponseReviewServlet extends HttpServlet {
    @EJB
    private ReviewFacadeLocal reviewFacade;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ResponseReviewServlet</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ResponseReviewServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int cusID = Integer.parseInt(request.getParameter("cusID"));
        int proID = Integer.parseInt(request.getParameter("proID"));
        Review review = reviewFacade.findByReviewPK(cusID, proID);
        request.setAttribute("responseReview", review);
        request.getRequestDispatcher("/admin/catalog/review_response.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int cusID = Integer.parseInt(request.getParameter("cusID"));
        int proID = Integer.parseInt(request.getParameter("proID"));
        String adminResponse = request.getParameter("adminResponse");
        Date dateUpdated = new Date(System.currentTimeMillis());

        Review responseReview = reviewFacade.findByReviewPK(cusID, proID);
        
        responseReview.setAdminResponse(adminResponse);
        responseReview.setDateUpdated(dateUpdated);
        int newID = reviewFacade.editReview(responseReview);

        System.out.println("response review product's ID: " + newID);
        System.out.println("Response Review successfully !!!");
        
        request.setAttribute("msg", "Response Review successfully !!!");
        response.sendRedirect(request.getContextPath() + "/admin-page/catalog/reviews");
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
