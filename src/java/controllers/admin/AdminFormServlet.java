/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers.admin;

import beans.AdminAccountFacadeLocal;
import entities.AdminAccount;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import utils.PasswordMD5Utils;

/**
 *
 * @author nknguyengl
 */
@WebServlet(name = "AdminFormServlet", urlPatterns = {"/admin-page/admin-account/form"})
public class AdminFormServlet extends HttpServlet {

    @EJB
    private AdminAccountFacadeLocal adminAccountFacade;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        if (request.getParameterMap().containsKey("adminID")) {
            int adminID = Integer.parseInt(request.getParameter("adminID"));
            request.setAttribute("adminID", adminID);
            request.setAttribute("adminAccount", adminAccountFacade.find(adminID));
        }
        request.getRequestDispatcher("/admin/admin_account/form.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        String role = request.getParameter("role");
        Date dateCreated = new Date(System.currentTimeMillis());
        Date dateUpdated = new Date(System.currentTimeMillis());
        PasswordMD5Utils passwordMD5Util = new PasswordMD5Utils();
        if (request.getParameterMap().containsKey("adminID")) {
            int adminID = Integer.parseInt(request.getParameter("adminID"));
            AdminAccount aaOld = adminAccountFacade.find(adminID);
            aaOld.setAdminUserName(username);
            aaOld.setAdminPassword(passwordMD5Util.passwordToHash(password));
            aaOld.setAdminRole(role);
            aaOld.setDateUpdated(dateUpdated);
            adminAccountFacade.edit(aaOld);
            response.sendRedirect(request.getContextPath() + "/admin-page/admin-account");
        } else {
            AdminAccount aaOld = adminAccountFacade.findAdminByUserName(username);
            if (aaOld == null) {
                AdminAccount aaNew = new AdminAccount();
                aaNew.setAdminUserName(username);
                aaNew.setAdminPassword(passwordMD5Util.passwordToHash(password));
                aaNew.setAdminRole(role);
                aaNew.setDateCreated(dateCreated);
                aaNew.setDateUpdated(dateUpdated);
                adminAccountFacade.create(aaNew);
                response.sendRedirect(request.getContextPath() + "/admin-page/admin-account");
            } else {
                session.setAttribute("res_message", "Username alrealy exits");
                response.sendRedirect(request.getContextPath() + "/admin-page/admin-account/add");
            }
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
