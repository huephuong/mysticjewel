/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers.admin;

import beans.CategoryFacadeLocal;
import beans.CustomerFacadeLocal;
import beans.OrderDetailFacadeLocal;
import beans.OrderMasterFacadeLocal;
import beans.ProductFacadeLocal;
import entities.Category;
import entities.OrderDetail;
import entities.OrderMaster;
import entities.Product;
import entities.Sku;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "AdminServlet", urlPatterns = {"/admin-page"})
public class AdminServlet extends HttpServlet {

    @EJB
    private CategoryFacadeLocal categoryFacade;
    @EJB
    private ProductFacadeLocal productFacade;
    @EJB
    private OrderDetailFacadeLocal orderDetailFacade;
    @EJB
    private CustomerFacadeLocal customerFacade;
    @EJB
    private OrderMasterFacadeLocal orderMasterFacade;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            int totalOrder = orderMasterFacade.findAll().size();
            int totalCustomer = customerFacade.findAll().size();
            int revenue = 0;
            for (OrderDetail o : orderDetailFacade.findAll()) {
                revenue += (o.getUnitPrice() - o.getSku().getBasePrice()) * o.getQuantity();
            }
            request.setAttribute("totalOrder", totalOrder);
            request.setAttribute("totalCustomer", totalCustomer);
            request.setAttribute("totalRevenue", revenue);
            request.setAttribute("newOrders", orderMasterFacade.findAll().subList(0, 6));
            request.setAttribute("newProducts", productFacade.findAll().subList(0, 4));
            request.setAttribute("mainCategorys", categoryFacade.getMainCategory());
            request.getRequestDispatcher("/admin/dashboard.jsp").forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
