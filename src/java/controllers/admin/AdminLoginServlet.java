/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers.admin;

import beans.AdminAccountFacadeLocal;
import entities.AdminAccount;
import java.io.IOException;
import java.io.PrintWriter;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import utils.PasswordMD5Utils;

/**
 *
 * @author nkngu
 */
@WebServlet(name = "AdminLoginServlet", urlPatterns = {"/admin-login"})
public class AdminLoginServlet extends HttpServlet {

    @EJB
    private AdminAccountFacadeLocal adminAccountFacade;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        if (session.getAttribute("admin") != null) {
            response.sendRedirect(request.getContextPath() + "/admin-page");
        } else {
            request.getRequestDispatcher("/admin/login.jsp").forward(request, response);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String userName = request.getParameter("userName");
        String password = request.getParameter("password");
        String remember = request.getParameter("remember");
        HttpSession session = request.getSession();
        PasswordMD5Utils passwordToHash = new PasswordMD5Utils();
        if (adminAccountFacade.login(userName, passwordToHash.passwordToHash(password))) {
            AdminAccount aa = adminAccountFacade.findAdminByUserName(userName);
            session.setAttribute("admin", aa);
            if (remember != null) {
                Cookie ckUserNameAa = new Cookie("userNameAa", userName);
                ckUserNameAa.setMaxAge(24 * 60 * 60);
                ckUserNameAa.setPath(request.getContextPath());
                response.addCookie(ckUserNameAa);
                Cookie ckPasswordAa = new Cookie("passwordAa", passwordToHash.passwordToHash(password));
                ckPasswordAa.setMaxAge(24 * 60 * 60);
                ckPasswordAa.setPath(request.getContextPath());
                response.addCookie(ckPasswordAa);
            }
            response.sendRedirect(request.getContextPath() + "/admin-page");
        } else {
            request.setAttribute("admin_login_message", "Username or password not valid");
            request.getRequestDispatcher("/admin/login.jsp").forward(request, response);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
