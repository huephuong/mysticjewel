package controllers.admin.sales;

import beans.OrderMasterFacadeLocal;
import java.io.IOException;
import java.io.PrintWriter;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "OrderUpdateStatusServlet", urlPatterns = {"/admin-page/sales/order/status"})
public class OrderUpdateStatusServlet extends HttpServlet {
    @EJB
    private OrderMasterFacadeLocal orderMasterFacade;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            int id = Integer.parseInt(request.getParameter("id"));
            String s = request.getParameter("status");
            String status = null;
            switch (s) {
                case "CNF":
                    status = "Confirmed";
                    break;
                case "PCK":
                    status = "Packaging";
                    break;
                case "DLV":
                    status = "Delivering";
                    break;
                case "CMP":
                    status = "Completed";
                    break;
                case "CNC":
                    status = "Canceled";
                    break;
            }
            orderMasterFacade.updateOrderStatus(orderMasterFacade.find(id), status);
            response.sendRedirect(request.getContextPath() + "/admin-page/sales/order");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
