package controllers.admin.sales;

import beans.ProductVoucherFacadeLocal;
import entities.ProductVoucher;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author htp06
 */
@WebServlet(name = "ProductVoucherServlet", urlPatterns = {"/admin-page/sales/voucher"})
public class ProductVoucherServlet extends HttpServlet {

    @EJB
    private ProductVoucherFacadeLocal productVoucherFacade;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            if (request.getParameter("voucherID") != null) {
                String voucherID = request.getParameter("voucherID");
                ProductVoucher proVouDelete = productVoucherFacade.find(voucherID);
                if (productVoucherFacade.removeProductVoucher(proVouDelete)) {
                    request.setAttribute("msg", "delete success!!!");
                    System.out.println("delete Voucher Successfully !!!");
//                    response.sendRedirect(request.getContextPath() + "/admin-page/product-voucher");
                } else {
                    request.setAttribute("msg", "can not delete this  voucher !!!");
                    System.out.println("can not delete this  voucher !!!");
//                    response.sendRedirect(request.getContextPath() + "/admin-page/product-voucher");
                }
            }  
//            else {
                List<ProductVoucher> list = productVoucherFacade.findAll();
                request.setAttribute("voucherList", list);
                request.getRequestDispatcher("/admin/sales/voucher_list.jsp").forward(request, response);
//            }

        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
