package controllers.admin.sales;

import beans.ProductVoucherFacadeLocal;
import entities.ProductVoucher;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author htp06
 */
@WebServlet(name = "FormProductVoucherServlet", urlPatterns = {"/admin-page/sales/voucher/form"})
public class FormProductVoucherServlet extends HttpServlet {

    @EJB
    private ProductVoucherFacadeLocal productVoucherFacade;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet FormProductVoucherServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet FormProductVoucherServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        if (request.getParameter("voucherID") != null) {
            String voucherID = request.getParameter("voucherID");
            ProductVoucher voucherUpdate = productVoucherFacade.find(voucherID);
            request.setAttribute("voucherUpdate", voucherUpdate);
        }
        request.getRequestDispatcher("/admin/sales/voucher_form.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            String formName = request.getParameter("formName"); // type of form (Add/Edit)
            System.out.println("Form Name: " + formName);
            String voucherID = request.getParameter("inputVoucherID");
            String voucherDesc = request.getParameter("inputDescription");
            int discountValue = Integer.parseInt(request.getParameter("inputDiscountValue"));
            int quantity = Integer.parseInt(request.getParameter("inputQuantity"));
            Date validFromDate = null;
            Date validToDate = null;
            String validDate = request.getParameter("inputValidDate");
            String validFrom = validDate.substring(0, 19);
            String validTo = validDate.substring(22);
            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm a");
            validFromDate = formatter.parse(validFrom);
            validToDate = formatter.parse(validTo);

            ProductVoucher voucher = new ProductVoucher();
            voucher.setVoucherID(voucherID);
            voucher.setVoucherDesc(voucherDesc);
            voucher.setDiscountValue(discountValue);
            voucher.setQuantity(quantity);
            voucher.setValidFrom(validFromDate);
            voucher.setValidTo(validToDate);

            if (formName.equals("Edit")) {
                productVoucherFacade.editProductVoucher(voucher);
            } else {
                if(productVoucherFacade.find(voucherID) != null) {
                    voucher.setVoucherID(null);
                    request.setAttribute("voucherUpdate", voucher);
                    request.setAttribute("msg", "This id has exists already !");
                    request.getRequestDispatcher("/admin/sales/voucher_form.jsp").forward(request, response);
                    return;
                }
                else {
                    productVoucherFacade.create(voucher);
                }
            }
            response.sendRedirect(request.getContextPath() + "/admin-page/sales/voucher");
        } catch (ParseException ex) {
            Logger.getLogger(FormProductVoucherServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
