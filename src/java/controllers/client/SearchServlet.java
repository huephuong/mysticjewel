/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers.client;

import beans.ProductFacadeLocal;
import entities.Product;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author htp06
 */
@WebServlet(name = "SearchServlet", urlPatterns = {"/catalogsearch", "/catalogsearch/result"})
public class SearchServlet extends HttpServlet {

    @EJB
    private ProductFacadeLocal productFacade;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        String searchTitle = request.getParameter("searchTitle");
        List<Product> resultList = productFacade.findByName(searchTitle);
        if (resultList.size() <= 15) {
            resultList.subList(0, resultList.size());
        } else {
            resultList.subList(0, 15);
        }
        if (resultList.size() > 0) {
            String resultHTML = "<ul>"
                    + "<li style=\"display:none\"></li>";
            for (Product product : resultList) {
                resultHTML += "<li title=\"" + product.getProductName() + "\" class=\"even first\">"
                        + "<a href=\"" + request.getContextPath() + "/product-detail?skuid=" + product.getSkuList().get(0).getSkuID() + "\">" + product.getProductName() + "</a>"
                        + "</li>";
            }
            resultHTML += "</ul>";
            out.print(resultHTML);
        } else {
            String resultHTML = "<ul><li style=\"color: red\">No matched result!!!</li></ul>";
            out.print(resultHTML);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String searchTitle = request.getParameter("searchTitle");
        List<Product> resultList = productFacade.findByName(searchTitle);
        request.setAttribute("prodList", resultList);
        request.getRequestDispatcher("/client/product/product.jsp").forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
