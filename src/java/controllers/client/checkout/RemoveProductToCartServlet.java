/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers.client.checkout;

import beans.SkuFacadeLocal;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import entities.Cart;
import entities.Sku;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author nknguyengl
 */
@WebServlet(name = "RemoveProductToCartServlet", urlPatterns = {"/remove-product-to-cart"})
public class RemoveProductToCartServlet extends HttpServlet {

    @EJB
    private SkuFacadeLocal skuFacade;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            HttpSession session = request.getSession();
            Cart cart = (Cart) session.getAttribute("cart");
            String skuID = request.getParameter("skuID");
            if (skuID.isEmpty()) {
                cart.clearCart();
            } else {
                cart.removeToCart(skuFacade.find(skuID));
            }
            HashMap<Sku, Integer> items = cart.getCartItems();
            session.setAttribute("cartItems", items);
            session.setAttribute("subTotal", cart.subTotal());

            HashMap<String, String> cartmini = new HashMap<>();
            cartmini.put("status", "SUCCESS");
            cartmini.put("message", "Product was removed from your shopping cart.");
            String cartminiHTML = "<div class=\"mini-cart is-not-empty\">\n"
                    + "    <div class=\"mini-cart-trigger\">\n"
                    + "        <a href=\"" + request.getContextPath() + "/checkout/cart\" class=\"cart-summary\" title=\"CART\">\n"
                    + "            <span class=\"cart-badge\">\n"
                    + "                <span class=\"badge-icon icon-royal icon-soppingbag-large theme-color\"></span>\n"
                    + "                <span class=\"badge-number\">" + items.size() + "</span>\n"
                    + "            </span>\n"
                    + "            <span class=\"cart-label\">Cart : </span>\n"
                    + "            <span class=\"subtotal\">\n"
                    + "                <span class=\"amount\">\n"
                    + "                    <span class=\"price\">$" + cart.subTotal() + "</span>\n"
                    + "                </span>\n"
                    + "            </span>\n"
                    + "        </a>\n"
                    + "    </div>\n"
                    + "    <div class=\"mini-cart-content\">\n"
                    + "        <h4 class=\"block-subtitle\">Recently Added Item(s)</h4>\n"
                    + "        <ol class=\"mini-products-list\">\n";
            for (Sku item : items.keySet()) {
                cartminiHTML += "<li class=\"item\">\n"
                        + "                <div class=\"media-section\">\n"
                        + "                    <a href=\"" + request.getContextPath() + "/product-detail?skuid=" + item.getSkuID() + "\" title=\"" + item.getProductID().getProductName() + "\" class=\"product-image\">\n"
                        + "                        <img src=\"" + request.getContextPath() + "/resource/images/" + item.getProductID().getProductImageList().get(0).getImageLink() + "\" alt=\"" + item.getProductID().getProductName() + "\" />\n"
                        + "                    </a>\n"
                        + "                </div>\n"
                        + "                <div class=\"product-details clearfix\">\n"
                        + "                    <a href=\"javascript:void(0);\" title=\"Remove This Item\" onclick=\"confirm('Are you sure you would like to remove this item from the shopping cart?');EGAjaxObject.ajaxCall('" + request.getContextPath() + "/remove-product-to-cart" + "', '.block-cart', 'cart', 'skuID=" + item.getSkuID() + "');\" class=\"btn-remove\"></a>\n"
                        + "                    <p class=\"product-name\">\n"
                        + "                        <a href=\"" + request.getContextPath() + "/product-detail?skuid=" + item.getSkuID() + "\">" + item.getProductID().getProductName() + "</a>\n"
                        + "                    </p>\n"
                        + "                    <span class=\"mini-qty\">" + items.get(item) + "</span> x <span class=\"price\">$" + item.getUnitPrice() + "</span>\n"
                        + "                </div>\n"
                        + "            </li>\n";
            }
            cartminiHTML += "</ol>\n"
                    + "        <div class=\"clearfix\"></div>\n"
                    + "        <span class=\"subtotal-wrapper\">\n"
                    + "            <span class=\"subtotal-title\">Subtotal: </span>\n"
                    + "            <a class=\"summary\" href=\"" + request.getContextPath() + "/checkout/cart\" title=\"View all items in your shopping cart\">\n"
                    + "                <span class=\"subtotal\">\n"
                    + "                    <span class=\"price\">$" + cart.subTotal() + "</span>\n"
                    + "                </span>\n"
                    + "            </a>\n"
                    + "        </span>\n"
                    + "        <script type=\"text/javascript\">\n"
                    + "            decorateList('cart-sidebar', 'none-recursive')\n"
                    + "        </script>\n"
                    + "        <div class=\"clearfix\"></div>\n"
                    + "        <div class=\"buttons-set\">\n"
                    + "            <div class=\"view-cart grid-full no-padding\">\n"
                    + "                <a title=\"Cart\" class=\"button btn-cart btn-grey\" href=\"" + request.getContextPath() + "/checkout/cart\">\n"
                    + "                    <span><span>Cart</span></span>\n"
                    + "                </a>\n"
                    + "            </div>\n"
                    + "            <div class=\"checkout grid-full no-padding\">\n"
                    + "                <a title=\"Checkout\" class=\"button btn-checkout\" href=\"" + request.getContextPath() + "/checkout/onepage\">\n"
                    + "                    <span><span>Checkout</span></span>\n"
                    + "                </a>\n"
                    + "            </div>\n"
                    + "        </div>\n"
                    + "    </div>\n"
                    + "</div>";
            cartmini.put("cartmini", cartminiHTML);
            cartmini.put("stickycart", cartminiHTML);
            cartmini.put("sidebar", cartminiHTML);
            Gson gson = new GsonBuilder().disableHtmlEscaping().create();
            String json = gson.toJson(cartmini);
            out.print(json);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
