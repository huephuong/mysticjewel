package controllers.client.checkout;

import beans.CustomerAddressFacadeLocal;
import beans.CustomerFacadeLocal;
import beans.OrderMasterFacadeLocal;
import beans.ProvinceFacadeLocal;
import entities.Cart;
import entities.Customer;
import entities.CustomerAddress;
import entities.OrderMaster;
import entities.ProductVoucher;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(name = "OrderReviewServlet", urlPatterns = {"/order-finish"})
public class OrderReviewServlet extends HttpServlet {

    @EJB
    private CustomerAddressFacadeLocal customerAddressFacade;
    @EJB
    private ProvinceFacadeLocal provinceFacade;
    @EJB
    private OrderMasterFacadeLocal orderMasterFacade;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            // session values
            HttpSession session = request.getSession();
            Cart cart = (Cart) session.getAttribute("cart");
            Customer customer = (Customer) session.getAttribute("customer");
            ProductVoucher voucher = (ProductVoucher) session.getAttribute("voucher");
            int currentFee = 0;
            if (session.getAttribute("currentFee") != null) {
                currentFee = (int) session.getAttribute("currentFee");
            }

            String number = request.getParameter("shipping_add_number");
            String street = request.getParameter("shipping_add_street");
            String ward = request.getParameter("shipping_add_ward");
            String district = request.getParameter("shipping_add_district");
            String city = provinceFacade.find(request.getParameter("shipping_add_city")).getName();

            // Add new address on request
            String addAddress = request.getParameter("addAddress");
            if ("addAddress".equals(addAddress) && customer != null) {
                CustomerAddress newAdd = new CustomerAddress();
                newAdd.setNumber(number);
                newAdd.setStreet(street);
                newAdd.setWard(ward);
                newAdd.setDistrict(district);
                newAdd.setCity(city);
                newAdd.setCustomerID(customer);
                customerAddressFacade.createAddress(newAdd);
            }

            // place order
            String shipName = request.getParameter("billing_first_name").trim() + " " + request.getParameter("billing_last_name").trim();
            String shipPhone = request.getParameter("billing_phone").trim();
            String shipEmail = request.getParameter("billing_email").trim();
            String shipAddress = number + " " + street + ", Ward " + ward + ", District " + district + ", " + city;
            String payment = request.getParameter("payment_method");

            OrderMaster order = new OrderMaster();
            order.setCustomerID(customer);
            order.setVoucherID(voucher);
            order.setShipName(shipName);
            order.setShipPhone(shipPhone);
            order.setShipEmail(shipEmail);
            if ("CARD".equals(payment)) {
                order.setOrderStatus("Awaiting Payment");
            } else {
                order.setOrderStatus("Pending");
            }
            order.setPayment(payment);
            order.setShipAddress(shipAddress);
            order.setShipFee(currentFee);
            order.setShipDate(null);
            order.setDateCreated(new Date(System.currentTimeMillis()));
            order.setDateUpdated(new Date(System.currentTimeMillis()));

            // Add new Order
            OrderMaster newOrder = orderMasterFacade.createOrder(order, cart);
            request.setAttribute("newOrder", newOrder);

            // Erase cart
            session.removeAttribute("cart");
            session.removeAttribute("cartItems");
            session.removeAttribute("subTotal");
            session.removeAttribute("currentVoucher");
            session.removeAttribute("currentFee");
            session.removeAttribute("currentAddress");
            session.removeAttribute("purchaseCustomer");
            if ("CARD".equals(payment)) {
                // Erase cart
                response.sendRedirect(request.getContextPath() + "/place-order/payment?orderid=" + newOrder.getOrderID());
            } else {
                request.getRequestDispatcher("/client/checkout/order-success.jsp").forward(request, response);
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
