package controllers.client.checkout;

import beans.CustomerAddressFacadeLocal;
import beans.CustomerFacadeLocal;
import beans.OrderMasterFacadeLocal;
import beans.ProvinceFacadeLocal;
import com.google.gson.Gson;
import com.google.maps.DirectionsApi;
import com.google.maps.GeoApiContext;
import com.google.maps.model.DistanceMatrix;
import entities.Cart;
import entities.Customer;
import entities.CustomerAddress;
import entities.OrderMaster;
import entities.ProductVoucher;
import entities.PurchaseCustomer;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.joda.time.DateTime;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import utils.DistanceMatrixUtils;
import utils.PasswordMD5Utils;

@WebServlet(name = "PlaceOrderServlet", urlPatterns = {"/place-order"})
public class PlaceOrderServlet extends HttpServlet {
    @EJB
    private CustomerAddressFacadeLocal customerAddressFacade;
    @EJB
    private CustomerFacadeLocal customerFacade;
    @EJB
    private ProvinceFacadeLocal provinceFacade;
    @EJB
    private OrderMasterFacadeLocal orderMasterFacade;
    
    private static final String API_KEY = "AIzaSyDUIKjMFHWDcKTHBTVOJwJpQiXSR-e2gJ0";
    private static final GeoApiContext context = new GeoApiContext().setApiKey(API_KEY);
    private static final String ORIGINS = "590 Cách Mạng Tháng 8, Quận 11, Hồ Chí Minh";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            request.setAttribute("provinceList", provinceFacade.findAll());
            // session values
            HttpSession session = request.getSession();
            Cart cart = (Cart) session.getAttribute("cart");
            Customer customer = (Customer) session.getAttribute("customer");
            ProductVoucher voucher = (ProductVoucher) session.getAttribute("voucher");
            String createAccount = request.getParameter("createaccount");
            String number = request.getParameter("shipping_add_number");
            String street = request.getParameter("shipping_add_street");
            String ward = request.getParameter("shipping_add_ward");
            String district = request.getParameter("shipping_add_district");
            String city = provinceFacade.find(request.getParameter("shipping_add_city")).getName();
            if (customer == null && createAccount != null) {
                PasswordMD5Utils passwordToHash = new PasswordMD5Utils();
                String firstname = request.getParameter("billing_first_name");
                String lastname = request.getParameter("billing_last_name");
                String username = request.getParameter("account_username");
                String email = request.getParameter("billing_email");
                String password = request.getParameter("account_password");
                String cpassword = request.getParameter("confirm_password");
                if (!password.equals(cpassword)) {
                    session.setAttribute("res_message", "Confirm password must match password!");
                    request.getRequestDispatcher("/client/checkout/checkout.jsp").forward(request, response);
                }
                String phone = request.getParameter("billing_phone");
                Date dateCreated = new Date(System.currentTimeMillis());
                Date dateUpdated = new Date(System.currentTimeMillis());

                // add new customer
                Customer newCustomer = new Customer();
                newCustomer.setFirstName(firstname);
                newCustomer.setLastName(lastname);
                newCustomer.setCustomerUserName(username);
                newCustomer.setEmail(email);
                newCustomer.setCustomerPassword(passwordToHash.passwordToHash(password));
                newCustomer.setPhone1(phone);
                newCustomer.setCustomerAvatar("avt.jpg");
                newCustomer.setIsActive(Boolean.TRUE);
                newCustomer.setDateCreated(dateCreated);
                newCustomer.setDateUpdated(dateUpdated);

                if (customerFacade.findCustomerByEmail(email) == null && customerFacade.findCustomerByUserName(username) == null) {
                    customerFacade.createCustomer(newCustomer);
                    customer = customerFacade.findCustomerByEmail(email);
                    session.setAttribute("customer", customer);
                } else {
                    session.setAttribute("res_message", "This email or username is already registered!");
                    request.getRequestDispatcher("/client/checkout/checkout.jsp").forward(request, response);
                }
            }

            // recalculate shipping fee
            String choice = request.getParameter("chooseAddress");
            request.setAttribute("isNewAddress", choice);
            CustomerAddress cusAdd;
            if ("new".equals(choice)) {
                cusAdd = new CustomerAddress();
                cusAdd.setNumber(number);
                cusAdd.setStreet(street);
                cusAdd.setWard(ward);
                cusAdd.setDistrict(district);
                cusAdd.setCity(city);
            } else {
                int addressID = Integer.parseInt(choice);
                cusAdd = customerAddressFacade.find(addressID);
            }
            session.setAttribute("currentAddress", cusAdd);

            String googleAddress = cusAdd.getNumber() + " " + cusAdd.getStreet() + ", Phường " + cusAdd.getWard() + ", Quận " + cusAdd.getDistrict() + ", " + cusAdd.getCity();
            int shipFee = 5;

            // calculate shipping fee
            Gson gson = new Gson();
            DistanceMatrixUtils matrixUtil = new DistanceMatrixUtils();
            DistanceMatrix trix = matrixUtil.estimateRouteTime(new DateTime(), Boolean.FALSE, DirectionsApi.RouteRestriction.TOLLS, ORIGINS, googleAddress);
            String trixJson = gson.toJson(trix);
            try {
                JSONParser parser = new JSONParser();
                Object obj = parser.parse(trixJson);
                JSONObject distanceMatrix = (JSONObject) obj;
                JSONArray rows = (JSONArray) distanceMatrix.get("rows");
                for (int i = 0; i < rows.size(); i++) {
                    JSONObject row = (JSONObject) rows.get(i);
                    JSONArray elements = (JSONArray) row.get("elements");
                    for (int j = 0; j < elements.size(); j++) {
                        JSONObject element = (JSONObject) elements.get(j);
                        //duration
                        JSONObject duration = (JSONObject) element.get("duration");
                        String seconds = (String) duration.get("inSeconds").toString();
                        System.out.println(seconds);
                        //distance
                        JSONObject distance = (JSONObject) element.get("distance");
                        long meters = (long) distance.get("inMeters");
                        if (meters > 5000) {
                            for (long m = meters - 10000; m > 0; m -= 10000) {
                                shipFee += 3;
                            }
                        }
                    }
                }
            } catch (ParseException ex) {
                ex.printStackTrace();
            }
            session.setAttribute("currentFee", shipFee);
            
            // get PurchaseCustomer
            PurchaseCustomer purchaseCustomer = (PurchaseCustomer) session.getAttribute("purchaseCustomer");
            purchaseCustomer.setFirstName(request.getParameter("billing_first_name").trim());
            purchaseCustomer.setLastName(request.getParameter("billing_last_name").trim());
            purchaseCustomer.setEmail(request.getParameter("billing_email").trim());
            purchaseCustomer.setPhone(request.getParameter("billing_phone").trim());
            purchaseCustomer.setPayment(request.getParameter("payment_method"));
            session.setAttribute("purchaseCustomer", purchaseCustomer);
            
            request.getRequestDispatcher("/client/checkout/order-review.jsp").forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
