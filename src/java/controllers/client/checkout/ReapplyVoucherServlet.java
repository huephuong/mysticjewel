package controllers.client.checkout;

import beans.ProductVoucherFacadeLocal;
import beans.ProvinceFacadeLocal;
import entities.Customer;
import entities.OrderMaster;
import entities.ProductVoucher;
import java.io.IOException;
import java.io.PrintWriter;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(name = "ReapplyVoucherServlet", urlPatterns = {"/checkout/reapply-voucher"})
public class ReapplyVoucherServlet extends HttpServlet {

    @EJB
    private ProductVoucherFacadeLocal productVoucherFacade;
    @EJB
    private ProvinceFacadeLocal provinceFacade;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            request.setAttribute("provinceList", provinceFacade.findAll());
            String voucherID = request.getParameter("coupon_code");
            ProductVoucher voucher = productVoucherFacade.find(voucherID);
            if (voucher != null) {
                if (voucher.getQuantity() > 0) {
                    HttpSession session = request.getSession();
                    Customer currentCustomer = (Customer) session.getAttribute("customer");
                    for (OrderMaster order : currentCustomer.getOrderMasterList()) {
                        if (order.getVoucherID() != null) {
                            if (voucherID.equals(order.getVoucherID().getVoucherID())) {
                                request.setAttribute("voucherError", "You have already used this voucher");
                                request.getRequestDispatcher("/client/checkout/checkout.jsp").forward(request, response);
                                
                            }
                        }
                    }
                    // temporary remove a voucher (to prevent other from using it)
                    voucher.setQuantity(voucher.getQuantity() - 1);
                    productVoucherFacade.edit(voucher);
                    session.setAttribute("currentVoucher", productVoucherFacade.find(voucherID));
                } else {
                    request.setAttribute("voucherError", "Invalid voucher code");
                }
            } else {
                request.setAttribute("voucherError", "Invalid voucher code");
            }
            request.getRequestDispatcher("/client/checkout/checkout.jsp").forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
