package controllers.client.checkout;

import beans.CustomerAddressFacadeLocal;
import beans.ProvinceFacadeLocal;
import com.google.gson.Gson;
import com.google.maps.DirectionsApi;
import com.google.maps.GeoApiContext;
import com.google.maps.model.DistanceMatrix;
import entities.CustomerAddress;
import java.io.IOException;
import java.io.PrintWriter;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.joda.time.DateTime;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import utils.DistanceMatrixUtils;

@WebServlet(name = "ShippingFeeServlet", urlPatterns = {"/checkout/estimate-shipping-fee"})
public class ShippingFeeServlet extends HttpServlet {

    @EJB
    private CustomerAddressFacadeLocal customerAddressFacade;
    @EJB
    private ProvinceFacadeLocal provinceFacade;

    private static final String API_KEY = "AIzaSyDUIKjMFHWDcKTHBTVOJwJpQiXSR-e2gJ0";
    private static final GeoApiContext context = new GeoApiContext().setApiKey(API_KEY);
    private static final String ORIGINS = "590 Cách Mạng Tháng 8, Quận 11, Hồ Chí Minh";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            request.setAttribute("provinceList", provinceFacade.findAll());
            HttpSession session = request.getSession();
            String choice = request.getParameter("chooseAddress");
            CustomerAddress cusAdd;
            if ("new".equals(choice)) {
                String number = request.getParameter("billing-number");
                String street = request.getParameter("billing-street");
                String ward = request.getParameter("billing-ward");
                String district = request.getParameter("billing-district");
                String city = request.getParameter("billing-city");
                cusAdd = new CustomerAddress();
                cusAdd.setNumber(number);
                cusAdd.setStreet(street);
                cusAdd.setWard(ward);
                cusAdd.setDistrict(district);
                cusAdd.setCity(provinceFacade.findByCode(city).getName());
            } else {
                int addressID = Integer.parseInt(choice);
                cusAdd = customerAddressFacade.find(addressID);
            }
            session.setAttribute("currentAddress", cusAdd);

            String googleAddress = cusAdd.getNumber() + " " + cusAdd.getStreet() + ", Phường " + cusAdd.getWard() + ", Quận " + cusAdd.getDistrict() + ", " + cusAdd.getCity();
            int shipFee = 5;

            // calculate shipping fee
            Gson gson = new Gson();
            DistanceMatrixUtils matrixUtil = new DistanceMatrixUtils();
            DistanceMatrix trix = matrixUtil.estimateRouteTime(new DateTime(), Boolean.FALSE, DirectionsApi.RouteRestriction.TOLLS, ORIGINS, googleAddress);
            String trixJson = gson.toJson(trix);
            try {
                JSONParser parser = new JSONParser();
                Object obj = parser.parse(trixJson);
                JSONObject distanceMatrix = (JSONObject) obj;
                JSONArray rows = (JSONArray) distanceMatrix.get("rows");
                for (int i = 0; i < rows.size(); i++) {
                    JSONObject row = (JSONObject) rows.get(i);
                    JSONArray elements = (JSONArray) row.get("elements");
                    for (int j = 0; j < elements.size(); j++) {
                        JSONObject element = (JSONObject) elements.get(j);
                        //duration
                        JSONObject duration = (JSONObject) element.get("duration");
                        String seconds = (String) duration.get("inSeconds").toString();
                        System.out.println(seconds);
                        //distance
                        JSONObject distance = (JSONObject) element.get("distance");
                        long meters = (long) distance.get("inMeters");
                        if (meters > 5000) {
                            for (long m = meters - 10000; m > 0; m -= 10000) {
                                shipFee += 3;
                            }
                        }
                    }
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            
            session.setAttribute("currentFee", shipFee);
            response.sendRedirect(request.getContextPath() + "/checkout/cart");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
