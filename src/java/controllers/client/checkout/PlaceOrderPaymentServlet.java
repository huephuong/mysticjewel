package controllers.client.checkout;

import beans.OrderMasterFacadeLocal;
import entities.OrderMaster;
import java.io.IOException;
import java.io.PrintWriter;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "PlaceOrderPaymentServlet", urlPatterns = {"/place-order/payment"})
public class PlaceOrderPaymentServlet extends HttpServlet {
    @EJB
    private OrderMasterFacadeLocal orderMasterFacade;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int orderID = Integer.parseInt(request.getParameter("orderid"));
        OrderMaster order = orderMasterFacade.find(orderID);
        request.setAttribute("currentOrder", order);
        request.getRequestDispatcher("/client/checkout/order-payment.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int orderID = Integer.parseInt(request.getParameter("orderid"));
        OrderMaster order = orderMasterFacade.find(orderID);
        order = orderMasterFacade.updateOrderStatus(order, "Pending");
        request.setAttribute("newOrder", order);
        request.getRequestDispatcher("/client/checkout/order-success.jsp").forward(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
