/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers.client.customer.account;

import beans.BookmarkFacadeLocal;
import entities.Bookmark;
import entities.Customer;
import java.io.IOException;
import java.io.PrintWriter;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import static utils.MailUtils.sendMail;
import static utils.MailUtils.getBaseUrl;

/**
 *
 * @author nknguyengl
 */
@WebServlet(name = "ShareWishlistServlet", urlPatterns = {"/customer/wishlist/share"})
public class ShareWishlistServlet extends HttpServlet {

    @EJB
    private BookmarkFacadeLocal bookmarkFacade;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.getRequestDispatcher("/client/customer/account/share_wishlist.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        Customer c = (Customer) session.getAttribute("customer");
        String email = request.getParameter("email");
        String message = request.getParameter("message");
        String content = "<table border=\"1\">\n"
                + "    <thead>\n"
                + "        <tr>\n"
                + "            <th></th>\n"
                + "            <th>Product Details</th>\n"
                + "        </tr>\n"
                + "    </thead>\n"
                + "    <tbody>\n";
        for (Bookmark b : bookmarkFacade.findByCustomerID(c.getCustomerID())) {
            content += "           <tr>\n"
                    + "                <td width=\"1\">\n"
                    + "                    <a href=\"" + getBaseUrl(request) + "/product-detail?skuid=" + b.getProduct().getSkuList().get(0).getSkuID() + "\" target=\"blank\" title=\"" + b.getProduct().getProductName() + "\"> <img width=\"149\" height=\"198\" src=\"" + getBaseUrl(request) + "/resource/images/" + b.getProduct().getProductImageList().get(0).getImageLink() + "\" alt=\"" + b.getProduct().getProductName() + "\" /> </a>\n"
                    + "                </td>\n"
                    + "                <td>\n"
                    + "                    <h3><a href=\"" + getBaseUrl(request) + "/product-detail?skuid=" + b.getProduct().getSkuList().get(0).getSkuID() + "\" target=\"blank\" title=\"" + b.getProduct().getProductName() + "\">" + b.getProduct().getProductName() + "</a></h3>\n"
                    + "                    <div>\n"
                    + "                        <div>" + b.getProduct().getProductDesc() + "</div>\n"
                    + "                    </div>\n"
                    + "                </td>\n"
                    + "            </tr>\n";
        }
        content += "    </tbody>\n"
                + "</table>"
                + "<p><strong>Message to you:</strong> " + message + "</p>";
        sendMail(email, "My wishlist in Mystic Jewel", content);
        session.setAttribute("res_message", "Wishlist sharing success!");
        response.sendRedirect(request.getContextPath() + "/customer/wishlist/share");
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
