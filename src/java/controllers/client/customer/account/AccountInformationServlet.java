/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package controllers.client.customer.account;

import beans.CustomerFacadeLocal;
import entities.Customer;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author nknguyengl
 */
@WebServlet(name = "AccountInformationServlet", urlPatterns = {"/customer/account/information"})
public class AccountInformationServlet extends HttpServlet {
    @EJB
    private CustomerFacadeLocal customerFacade;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.getRequestDispatcher("/client/customer/account/information.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        Customer cOld = (Customer) session.getAttribute("customer");
        Customer cNew = customerFacade.find(cOld.getCustomerID());
        String changePassword = request.getParameter("change_password");
        if (changePassword != null) {
            String currentPassword = request.getParameter("current_password");
            String password = request.getParameter("password");
            if (currentPassword.equals(cNew.getCustomerPassword())) {
                cNew.setCustomerPassword(password);
                customerFacade.edit(cNew);
                session.setAttribute("customer", cNew);
            } else {
                session.setAttribute("res_message", "Invalid current password.");
                response.sendRedirect(request.getContextPath() + "/customer/account/information");
            }
        } else {
            String firstname = request.getParameter("firstname");
            String lastname = request.getParameter("lastname");
            String gender = request.getParameter("gender");
            String dateOfBirth = request.getParameter("dateOfBirth");
            String phone = request.getParameter("phone");
            try {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                Date date = sdf.parse(dateOfBirth);
                cNew.setFirstName(firstname);
                cNew.setLastName(lastname);
                cNew.setGender(gender);
                cNew.setDateOfBirth(date);
                cNew.setPhone1(phone);
                customerFacade.edit(cNew);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            session.setAttribute("customer", cNew);
            session.setAttribute("res_message", "The account information has been saved.");
            response.sendRedirect(request.getContextPath() + "/customer/account");
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
