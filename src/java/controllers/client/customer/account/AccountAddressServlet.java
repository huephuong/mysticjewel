/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers.client.customer.account;

import beans.CustomerAddressFacadeLocal;
import beans.ProvinceFacadeLocal;
import entities.Customer;
import entities.CustomerAddress;
import entities.Province;
import java.io.IOException;
import java.io.PrintWriter;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author nknguyengl
 */
@WebServlet(name = "AddressServlet", urlPatterns = {"/customer/account/address"})
public class AccountAddressServlet extends HttpServlet {

    @EJB
    private CustomerAddressFacadeLocal customerAddressFacade;
    @EJB
    private ProvinceFacadeLocal provinceFacade;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        if (request.getParameterMap().containsKey("addressID")) {
            int addressID = Integer.parseInt(request.getParameter("addressID"));
            request.setAttribute("address", customerAddressFacade.find(addressID));
        }
        request.setAttribute("provinceList", provinceFacade.findAll());
        request.getRequestDispatcher("/client/customer/account/address.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        Customer c = (Customer) session.getAttribute("customer");
        String number = request.getParameter("number");
        String street = request.getParameter("street");
        String ward = request.getParameter("ward");
        String district = request.getParameter("district");
        String city = request.getParameter("city");
        CustomerAddress ca = new CustomerAddress();
        if (request.getParameterMap().containsKey("addressID")) {
            int addressID = Integer.parseInt(request.getParameter("addressID"));
            ca = customerAddressFacade.find(addressID);
            ca.setCustomerID(c);
            ca.setNumber(number);
            ca.setStreet(street);
            ca.setWard(ward);
            ca.setDistrict(district);
            Province province = provinceFacade.find(city);
            ca.setCity(province.getName());
            customerAddressFacade.edit(ca);
            session.setAttribute("res_message", "Edit address successfully!");
            response.sendRedirect(request.getContextPath() + "/customer/account/address?addressID=" + addressID);
        } else {
            ca.setCustomerID(c);
            ca.setNumber(number);
            ca.setStreet(street);
            ca.setWard(ward);
            ca.setDistrict(district);
            Province province = provinceFacade.find(city);
            ca.setCity(province.getName());
            customerAddressFacade.createAddress(ca);
            session.setAttribute("res_message", "Add new address successfully!");
            request.getRequestDispatcher("/client/customer/account/address.jsp").forward(request, response);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
