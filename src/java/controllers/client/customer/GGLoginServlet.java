/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers.client.customer;

import beans.CustomerFacadeLocal;
import beans.SocialAccountFacadeLocal;
import entities.Customer;
import entities.SocialAccount;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.Map;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import utils.PasswordMD5Utils;
import utils.GoogleUtils;

/**
 *
 * @author nknguyengl
 */
@WebServlet(name = "GGLoginServlet", urlPatterns = {"/gg-login"})
public class GGLoginServlet extends HttpServlet {

    @EJB
    private SocialAccountFacadeLocal socialAccountFacade;
    @EJB
    private CustomerFacadeLocal customerFacade;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            HttpSession session = request.getSession();
            String code = request.getParameter("code");
            if (code == null || code.isEmpty()) {
                response.sendRedirect(request.getContextPath() + "/login");
            } else {
                GoogleUtils googleUtils = new GoogleUtils();
                String accessToken = googleUtils.getToken(code);
                Map<String, String> ggProfileData = googleUtils.getData(accessToken);
                if (ggProfileData.get("email") == null) {
                    session.setAttribute("res_status", "error");
                    session.setAttribute("res_message", "Email can not blank!");
                } else {
                    PasswordMD5Utils passwordToHash = new PasswordMD5Utils();
                    String id = ggProfileData.get("id");
                    String firstname = ggProfileData.get("given_name");
                    String lastname = ggProfileData.get("family_name");
                    String email = ggProfileData.get("email");
                    String[] emailSplit = email.split("@");
                    String username = emailSplit[0];
                    String password = "123123";
                    Date dateCreated = new Date(System.currentTimeMillis());
                    Date dateUpdated = new Date(System.currentTimeMillis());
                    Customer c;
                    if (customerFacade.findCustomerByEmail(email) == null && socialAccountFacade.findByProviderCustomerID(id) == null) {
                        if (customerFacade.findCustomerByEmail(email) == null && customerFacade.findCustomerByUserName(username) == null) {
                            Customer newCustomer = new Customer();
                            newCustomer.setFirstName(firstname);
                            newCustomer.setLastName(lastname);
                            newCustomer.setCustomerUserName(username);
                            newCustomer.setEmail(email);
                            newCustomer.setCustomerPassword(passwordToHash.passwordToHash(password));
                            newCustomer.setCustomerAvatar("avt.jpg");
                            newCustomer.setIsActive(Boolean.TRUE);
                            newCustomer.setDateCreated(dateCreated);
                            newCustomer.setDateUpdated(dateUpdated);
                            c = customerFacade.createCustomer(newCustomer);
                            SocialAccount sa = new SocialAccount();
                            sa.setCustomerID(c);
                            sa.setProviderCustomerID(id);
                            sa.setProvider("google");
                            socialAccountFacade.createSocialAccount(sa);
                            session.setAttribute("customer", c);
                            session.setAttribute("res_status", "success");
                            session.setAttribute("res_message", "Thank you for registering with Mystic Jewel.");
                            //response.sendRedirect(request.getContextPath() + "/customer/account");
                        } else {
                            session.setAttribute("res_status", "error");
                            session.setAttribute("res_message", "This email or username is already registered!");
                            //response.sendRedirect(request.getContextPath() + "/login");
                        }
                    } else {
                        c = customerFacade.findCustomerByEmail(email);
                        if (socialAccountFacade.findByProviderCustomerID(id) == null) {
                            SocialAccount sa = new SocialAccount();
                            sa.setCustomerID(c);
                            sa.setProviderCustomerID(id);
                            sa.setProvider("google");
                            socialAccountFacade.createSocialAccount(sa);
                        }
                        session.setAttribute("customer", c);
                        //response.sendRedirect(request.getContextPath() + "/customer/account");
                    }
                }
            }
            out.println("<script type=\"text/javascript\">");
            out.println("window.opener.location.reload(true);");
            out.println("window.close();");
            out.println("</script>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
