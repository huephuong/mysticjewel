/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package controllers.client.customer;

import beans.CustomerFacadeLocal;
import entities.Customer;
import java.io.IOException;
import java.io.PrintWriter;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import utils.PasswordMD5Utils;

/**
 *
 * @author nknguyengl
 */
@WebServlet(name = "ResetPasswordServlet", urlPatterns = {"/reset-password"})
public class ResetPasswordServlet extends HttpServlet {
    @EJB
    private CustomerFacadeLocal customerFacade;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        String token = request.getParameter("token");
        Customer c = customerFacade.findCustomerByResetToken(token);
        if (c != null) {
            session.setAttribute("resetToken", token);
            request.getRequestDispatcher("client/customer/reset-password.jsp").forward(request, response);
        } else {
            session.setAttribute("res_status", "error");
            session.setAttribute("res_message", "Oops!  This is an invalid password reset link.");
            response.sendRedirect(request.getContextPath() + "/forgot-password");
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        String resetToken = session.getAttribute("resetToken").toString();
        String password = request.getParameter("password");
        Customer c = customerFacade.findCustomerByResetToken(resetToken);
        PasswordMD5Utils passwordToHash = new PasswordMD5Utils();
        if (c != null) {
            c.setCustomerPassword(passwordToHash.passwordToHash(password));
            c.setResetToken(null);
            customerFacade.edit(c);
            session.removeAttribute("resetToken");
            session.setAttribute("res_status", "success");
            session.setAttribute("res_message", "You have successfully reset your password.  You may now login.");
            response.sendRedirect(request.getContextPath() + "/login");
        } else {
            session.setAttribute("res_status", "error");
            session.setAttribute("res_message", "Oops!  This is an invalid password reset link.");
            response.sendRedirect(request.getContextPath() + "/forgot-password");
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
