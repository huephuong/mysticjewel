/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package controllers.client.customer;

import beans.CustomerFacadeLocal;
import entities.Customer;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import utils.PasswordMD5Utils;

/**
 *
 * @author nkngu
 */
@WebServlet(name = "RegisterServlet", urlPatterns = {"/register"})
public class RegisterServlet extends HttpServlet {
    @EJB
    private CustomerFacadeLocal customerFacade;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        if (session.getAttribute("customer") != null) {
            response.sendRedirect(request.getContextPath());
        } else {
            request.getRequestDispatcher("/client/customer/register.jsp").forward(request, response);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        HttpSession session = request.getSession();
        PasswordMD5Utils passwordToHash = new PasswordMD5Utils();
        String ajax = request.getParameter("ajax");
        String firstname = request.getParameter("firstname");
        String lastname = request.getParameter("lastname");
        String username = request.getParameter("username");
        String email = request.getParameter("email");
        String password = request.getParameter("password");
        Date dateCreated = new Date(System.currentTimeMillis());
        Date dateUpdated = new Date(System.currentTimeMillis());
        Customer newCustomer = new Customer();
        newCustomer.setFirstName(firstname);
        newCustomer.setLastName(lastname);
        newCustomer.setCustomerUserName(username);
        newCustomer.setEmail(email);
        newCustomer.setCustomerPassword(passwordToHash.passwordToHash(password));
        newCustomer.setCustomerAvatar("avt.jpg");
        newCustomer.setIsActive(Boolean.TRUE);
        newCustomer.setDateCreated(dateCreated);
        newCustomer.setDateUpdated(dateUpdated);
        if (customerFacade.findCustomerByEmail(email) == null && customerFacade.findCustomerByUserName(username) == null) {
            Customer c = customerFacade.createCustomer(newCustomer);
            session.setAttribute("customer", c);
            session.setAttribute("res_message", "Thank you for registering with Mystic Jewel.");
            if (request.getParameterMap().containsKey("ajax") && ajax.equals("register")) {
                out.print("success");
            } else {
                response.sendRedirect(request.getContextPath() + "/customer/account");
            }
        } else {
            session.setAttribute("res_message", "failed");
            if (request.getParameterMap().containsKey("ajax") && ajax.equals("register")) {
                if (customerFacade.findCustomerByUserName(username) != null) {
                    out.print("usernameisexist,");
                }
                if (customerFacade.findCustomerByEmail(email) != null) {
                    out.print("emailisexist,");
                }
            } else {
                response.sendRedirect(request.getContextPath() + "/register");
            }
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
