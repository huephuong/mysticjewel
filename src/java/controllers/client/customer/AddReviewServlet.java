package controllers.client.customer;

import beans.ReviewFacadeLocal;
import entities.Customer;
import entities.Review;
import entities.ReviewPK;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author htp06
 */
@WebServlet(name = "AddReviewServlet", urlPatterns = {"/review-product"})
public class AddReviewServlet extends HttpServlet {

    @EJB
    private ReviewFacadeLocal reviewFacade;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            HttpSession session = request.getSession();
            Customer c = (Customer) session.getAttribute("customer");
            int cusID = c.getCustomerID();
            int proID = Integer.parseInt(request.getParameter("prodID"));
            String skuID = request.getParameter("skuID");
            String title = request.getParameter("title");
            String content = request.getParameter("detail");
            double rating = Double.parseDouble(request.getParameter("inputRating"));
            Date dateCreated = new Date(System.currentTimeMillis());
            Date dateUpdated = new Date(System.currentTimeMillis());
            ReviewPK reviewPk = new ReviewPK(proID, cusID);
            Review newReview = new Review();
            newReview.setReviewPK(reviewPk);
            newReview.setTitle(title);
            newReview.setContent(content);
            newReview.setRating(rating);
            newReview.setDateCreated(dateCreated);
            newReview.setDateUpdated(dateUpdated);
            reviewFacade.createReview(newReview);
            request.setAttribute("skuid", skuID);
            response.sendRedirect(request.getContextPath() + "/product-detail?skuid=" + skuID);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
