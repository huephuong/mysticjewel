/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers.client.customer;

import beans.CustomerFacadeLocal;
import entities.Customer;
import java.io.IOException;
import java.io.PrintWriter;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import utils.PasswordMD5Utils;

/**
 *
 * @author nkngu
 */
@WebServlet(name = "LoginServlet", urlPatterns = {"/login"})
public class LoginServlet extends HttpServlet {

    @EJB
    private CustomerFacadeLocal customerFacade;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        if (session.getAttribute("customer") != null) {
            response.sendRedirect(request.getContextPath());
        } else {
            request.getRequestDispatcher("/client/customer/login.jsp").forward(request, response);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        String ajax = request.getParameter("ajax");
        String email = request.getParameter("email");
        String password = request.getParameter("password");
        String remember = request.getParameter("remember");
        HttpSession session = request.getSession();
        PasswordMD5Utils passwordToHash = new PasswordMD5Utils();
        if (request.getParameterMap().containsKey("ajax") && ajax.equals("login")) {
            if (customerFacade.login(email, passwordToHash.passwordToHash(password))) {
                Customer c = customerFacade.findCustomerByEmail(email);
                if (c.getIsActive() == false) {
                    out.print("baned,");
                } else {
                    session.setAttribute("customer", c);
                    if (remember != null) {
                        Cookie ckCustomerEmail = new Cookie("customerEmail", email);
                        ckCustomerEmail.setMaxAge(24 * 60 * 60);
                        ckCustomerEmail.setPath(request.getContextPath());
                        response.addCookie(ckCustomerEmail);
                        Cookie ckCustomerPassword = new Cookie("customerPassword", passwordToHash.passwordToHash(password));
                        ckCustomerPassword.setMaxAge(24 * 60 * 60);
                        ckCustomerPassword.setPath(request.getContextPath());
                        response.addCookie(ckCustomerPassword);
                    }
                    out.print("success");
                }
            } else {
                out.print("wronglogin,");
            }
        } else {
            if (customerFacade.login(email, passwordToHash.passwordToHash(password))) {
                Customer c = customerFacade.findCustomerByEmail(email);
                if (c.getIsActive() == false) {
                    session.setAttribute("res_status", "error");
                    session.setAttribute("res_message", "The account is locked, please contact the administrator.");
                    response.sendRedirect(request.getContextPath() + "/login");
                } else {
                    session.setAttribute("customer", c);
                    if (remember != null) {
                        Cookie ckCustomerEmail = new Cookie("customerEmail", email);
                        ckCustomerEmail.setMaxAge(24 * 60 * 60);
                        ckCustomerEmail.setPath(request.getContextPath());
                        response.addCookie(ckCustomerEmail);
                        Cookie ckCustomerPassword = new Cookie("customerPassword", passwordToHash.passwordToHash(password));
                        ckCustomerPassword.setMaxAge(24 * 60 * 60);
                        ckCustomerPassword.setPath(request.getContextPath());
                        response.addCookie(ckCustomerPassword);
                    }
                    response.sendRedirect(request.getContextPath() + "/customer/account");
                }
            } else {
                session.setAttribute("res_status", "error");
                session.setAttribute("res_message", "Invalid login or password.");
                response.sendRedirect(request.getContextPath() + "/login");
            }
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
