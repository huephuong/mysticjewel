package controllers.client.customer;

import beans.BookmarkFacadeLocal;
import beans.CustomerFacadeLocal;
import beans.ProductFacadeLocal;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import entities.Bookmark;
import entities.BookmarkPK;
import entities.ComparedProduct;
import entities.Customer;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author htp06
 */
@WebServlet(name = "AddWishlistServlet", urlPatterns = {"/add-wishlist/*"})
public class AddWishlistServlet extends HttpServlet {

    @EJB
    private BookmarkFacadeLocal bookmarkFacade;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            HttpSession session = request.getSession();
            Customer c = (Customer) session.getAttribute("customer");
            ComparedProduct compared = (ComparedProduct) session.getAttribute("compared");
            HashMap<String, String> wishlist = new HashMap<>();
            Gson gson = new GsonBuilder().disableHtmlEscaping().create();
            if (c == null) {
                session.setAttribute("res_status", "error");
                session.setAttribute("res_message", "Please Login First.");
                wishlist.put("status", "LOGIN");
                wishlist.put("href", request.getContextPath() + "/login");
            } else {
                String[] pathParts = request.getPathInfo().split("/");
                int proID = Integer.parseInt(pathParts[1]);
                Bookmark b = bookmarkFacade.findByBookmarkPK(c.getCustomerID(), proID);
                if (b == null) {
                    Date date = new Date();
                    Timestamp time = new Timestamp(date.getTime());
                    BookmarkPK newBookmarkPK = new BookmarkPK(c.getCustomerID(), proID);
                    Bookmark newBookmark = new Bookmark();
                    newBookmark.setBookmarkPK(newBookmarkPK);
                    newBookmark.setDateCreated(time);
                    bookmarkFacade.createBookmark(newBookmark);

                    b = bookmarkFacade.findByBookmarkPK(c.getCustomerID(), proID);
                    int count = bookmarkFacade.findByCustomerID(c.getCustomerID()).size();
                    int countComp = 0;
                    if (compared != null) {
                        countComp = compared.getComparedProducts().size();
                    }
                    wishlist.put("status", "SUCCESS");
                    wishlist.put("message", "" + b.getProduct().getProductName() + " was added to your wishlist.");
                    String toplinkHTML = "<div class=\"top-links\">"
                            + "<ul class=\"links\">\n"
                            + "    <li class=\"first \"><a href=\"" + request.getContextPath() + "customer/account\" title=\"My Account\">My Account</a> </li>\n"
                            + "    <li><a href=\"" + request.getContextPath() + "customer/wishlist\" title=\"My Wishlist (" + count + ")\">My Wishlist (" + count + ")</a> </li>\n"
                            + "    <li class=\"eg-top-links\"><a href=\"" + request.getContextPath() + "product-compare\" title=\"Compare\">Compare List(" + countComp + ")</a></li>"
                            + "    <li class=\" \"><a href=\"" + request.getContextPath() + "checkout\" title=\"Checkout\" class=\"top-link-checkout\">Checkout</a> </li>\n"
                            + "    <li class=\" last \"><a href=\"" + request.getContextPath() + "customer/account/logout\" title=\"Log Out\" class=\"eg-logout-link\">Log Out</a> </li>\n"
                            + "</ul>"
                            + "</div>";
                    String sidebarHTML = "<div class=\"block block-wishlist\">\n"
                            + "    <div class=\"block-title\"> <strong><span>My Wishlist <small>(" + count + ")</small></span></strong> </div>\n"
                            + "    <div class=\"block-content\">\n"
                            + "        <p class=\"block-subtitle\">Last Added Items</p>\n"
                            + "        <ol class=\"mini-products-list\" id=\"wishlist-sidebar\">";
                    for (Bookmark bookmark : bookmarkFacade.findByCustomerID(proID)) {
                        sidebarHTML += "<li class=\"item\">\n"
                                + "                <a href=\"men-jewellery.html\" title=\"" + bookmark.getProduct().getProductName() + "\" class=\"product-image\"><img src=\"\" width=\"50\" height=\"50\" alt=\"" + bookmark.getProduct().getProductName() + "\" /> </a>\n"
                                + "                <div class=\"product-details\"> <a href=\"wishlist/index/remove/item/34/\" title=\"Remove This Item\" onclick=\"return confirm('Are you sure you would like to remove this item from the wishlist?');\" class=\"btn-remove\">Remove This Item</a>\n"
                                + "                    <p class=\"product-name\"><a href=\"men-jewellery.html\">" + bookmark.getProduct().getProductName() + "</a> </p>\n"
                                + "                    <div class=\"price-box\">\n"
                                + "                        <p class=\"old-price\"> <span class=\"price-label\">Regular Price:</span> <span class=\"price\" id=\"old-price-139-wishlist\"> $125.00 </span> </p>\n"
                                + "                        <p class=\"special-price\"> <span class=\"price-label\">Special Price</span> <span class=\"price\" id=\"product-price-139-wishlist\"> $120.00 </span> </p>\n"
                                + "                    </div> <a href=\"wishlist/index/cart/item/34\" class=\"link-cart\">Add to Cart</a>\n"
                                + "                </div>\n"
                                + "            </li>";
                    }
                    sidebarHTML += "        </ol>\n"
                            + "        <script type=\"text/javascript\">\n"
                            + "            decorateList('wishlist-sidebar');\n"
                            + "        </script>\n"
                            + "        <div class=\"actions\"> <a href=\"" + request.getContextPath() + "customer/wishlist\">Go to Wishlist</a> </div>\n"
                            + "    </div>\n"
                            + "</div>";
                    wishlist.put("toplink", toplinkHTML);
                    wishlist.put("sidebar", sidebarHTML);
                } else {
                    wishlist.put("status", "SUCCESS");
                    wishlist.put("message", "" + b.getProduct().getProductName() + " was added to your wishlist.");
                    b = bookmarkFacade.findByBookmarkPK(c.getCustomerID(), proID);
                    int count = bookmarkFacade.findByCustomerID(c.getCustomerID()).size();
                    int countComp = 0;
                    wishlist.put("status", "SUCCESS");
                    wishlist.put("message", "" + b.getProduct().getProductName() + " was added to your wishlist.");
                    String toplinkHTML = "<div class=\"top-links\">"
                            + "<ul class=\"links\">\n"
                            + "    <li class=\"first \"><a href=\"" + request.getContextPath() + "customer/account\" title=\"My Account\">My Account</a> </li>\n"
                            + "    <li><a href=\"" + request.getContextPath() + "customer/wishlist\" title=\"My Wishlist (" + count + ")\">My Wishlist (" + count + ")</a> </li>\n"
                            + "    <li class=\"eg-top-links\"><a href=\"" + request.getContextPath() + "product-compare\" title=\"Compare\">Compare List(" + countComp + ")</a></li>"
                            + "    <li class=\" \"><a href=\"" + request.getContextPath() + "checkout\" title=\"Checkout\" class=\"top-link-checkout\">Checkout</a> </li>\n"
                            + "    <li class=\" last \"><a href=\"" + request.getContextPath() + "customer/account/logout\" title=\"Log Out\" class=\"eg-logout-link\">Log Out</a> </li>\n"
                            + "</ul>"
                            + "</div>";
                    String sidebarHTML = "<div class=\"block block-wishlist\">\n"
                            + "    <div class=\"block-title\"> <strong><span>My Wishlist <small>(" + count + ")</small></span></strong> </div>\n"
                            + "    <div class=\"block-content\">\n"
                            + "        <p class=\"block-subtitle\">Last Added Items</p>\n"
                            + "        <ol class=\"mini-products-list\" id=\"wishlist-sidebar\">";
                    for (Bookmark bookmark : bookmarkFacade.findByCustomerID(proID)) {
                        sidebarHTML += "<li class=\"item\">\n"
                                + "                <a href=\"men-jewellery.html\" title=\"" + bookmark.getProduct().getProductName() + "\" class=\"product-image\"><img src=\"\" width=\"50\" height=\"50\" alt=\"" + bookmark.getProduct().getProductName() + "\" /> </a>\n"
                                + "                <div class=\"product-details\"> <a href=\"wishlist/index/remove/item/34/\" title=\"Remove This Item\" onclick=\"return confirm('Are you sure you would like to remove this item from the wishlist?');\" class=\"btn-remove\">Remove This Item</a>\n"
                                + "                    <p class=\"product-name\"><a href=\"men-jewellery.html\">" + bookmark.getProduct().getProductName() + "</a> </p>\n"
                                + "                    <div class=\"price-box\">\n"
                                + "                        <p class=\"old-price\"> <span class=\"price-label\">Regular Price:</span> <span class=\"price\" id=\"old-price-139-wishlist\"> $125.00 </span> </p>\n"
                                + "                        <p class=\"special-price\"> <span class=\"price-label\">Special Price</span> <span class=\"price\" id=\"product-price-139-wishlist\"> $120.00 </span> </p>\n"
                                + "                    </div> <a href=\"wishlist/index/cart/item/34\" class=\"link-cart\">Add to Cart</a>\n"
                                + "                </div>\n"
                                + "            </li>";
                    }
                    sidebarHTML += "        </ol>\n"
                            + "        <script type=\"text/javascript\">\n"
                            + "            decorateList('wishlist-sidebar');\n"
                            + "        </script>\n"
                            + "        <div class=\"actions\"> <a href=\"" + request.getContextPath() + "customer/wishlist\">Go to Wishlist</a> </div>\n"
                            + "    </div>\n"
                            + "</div>";
                    wishlist.put("toplink", toplinkHTML);
                    wishlist.put("sidebar", sidebarHTML);
                }
            }
            String json = gson.toJson(wishlist);
            out.print(json);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
