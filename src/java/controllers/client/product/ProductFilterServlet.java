/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers.client.product;

import beans.AttributeFacadeLocal;
import beans.AttributeGroupFacadeLocal;
import beans.CategoryFacadeLocal;
import beans.ProductColFacadeLocal;
import beans.ProductFacadeLocal;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import entities.Attribute;
import entities.AttributeGroup;
import entities.Category;
import entities.Product;
import entities.ProductCol;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author nknguyengl
 */
@WebServlet(name = "ProductFilterServlet", urlPatterns = {"/product-filter"})
public class ProductFilterServlet extends HttpServlet {

    @EJB
    private AttributeFacadeLocal attributeFacade;
    @EJB
    private AttributeGroupFacadeLocal attributeGroupFacade;
    @EJB
    private ProductColFacadeLocal productColFacade;
    @EJB
    private CategoryFacadeLocal categoryFacade;
    @EJB
    private ProductFacadeLocal productFacade;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            String catParam = request.getParameter("category");
            String colParam = request.getParameter("collection");
            int categoryID = 0;
            int colID = 0;
            if (catParam != null) {
                categoryID = Integer.parseInt(catParam);
            }
            if (colParam != null) {
                colID = Integer.parseInt(colParam);
            }

            String mode = request.getParameterMap().containsKey("mode") ? request.getParameter("mode") : "grid";

            // Filter
            int attr = request.getParameterMap().containsKey("attr") && !request.getParameter("attr").equals("clear") ? Integer.parseInt(request.getParameter("attr")) : 0;
            String price = request.getParameterMap().containsKey("price") && !request.getParameter("price").equals("clear") ? request.getParameter("price") : "-2,-1";
            String[] split = price.split(",");
            int priceFrom = Integer.parseInt(split[0]);
            int priceTo = Integer.parseInt(split[1]);

            // Sort
            String dir = request.getParameterMap().containsKey("dir") ? request.getParameter("dir") : "asc";
            String order = request.getParameterMap().containsKey("order") ? request.getParameter("order") : "name";

            // Pagination Max Result 9
            int page = request.getParameter("p") != null ? Integer.parseInt(request.getParameter("p")) : 1;
            int maxResult = request.getParameterMap().containsKey("limit") ? Integer.parseInt(request.getParameter("limit")) : 6;
            int firstResult = (page - 1) * maxResult;
            List<Product> list;
            if (request.getParameterMap().containsKey("attr") && !request.getParameter("attr").equals("clear")
                    || request.getParameterMap().containsKey("price") && !request.getParameter("price").equals("clear")
                    || request.getParameterMap().containsKey("order") || request.getParameterMap().containsKey("dir")) {
                list = productFacade.findByFilter(categoryID, colID, attr, priceFrom, priceTo, dir, order);
            } else {
                list = productFacade.findByCatCol(categoryID, colID);
            }
            int totalPage = list.size();
            int pageLimit = (int) Math.floor((totalPage / maxResult) + 1);

            int from = Math.max(0, firstResult);
            int to = Math.min(list.size(), from + maxResult);
            list = list.subList(from, to);

            HashMap<String, String> filterJson = new HashMap<>();
            String layerHTML = "<div class=\"eg-layerednav\">\n"
                    + "    <div class=\"block block-categories\">\n"
                    + "        <div class=\"block-title\"> <strong><span>Find Jewellery</span></strong> </div>\n"
                    + "        <div class=\"block-content\">\n"
                    + "            <ul class=\"eg-categories-accordion\">\n"
                    + "                <!-- Category -->\n"
                    + "                <li id=\"category-3\" class=\"level0 active current parent first last\">\n"
                    + "                    <a class=\"level0\" href=\"index.php/fashion.html\"><span>Categories</span></a><span class=\"opener\"><span class=\"icon-royal icon-plus\"></span><span class=\"icon-royal icon-minus\"></span></span>\n"
                    + "                    <ul class=\"level0 parent clearfix\">\n";
            for (Category c : categoryFacade.getMainCategory()) {
                layerHTML += "                         <li class=\"level1 parent\">\n"
                        + "                                <a class=\"level1\" href=\"" + request.getContextPath() + "/products?category=" + c.getCategoryID() + "\"><span>" + c.getCategoryName() + "</span></a>\n"
                        + "                                <span class=\"opener\">\n"
                        + "                                    <span class=\"icon-royal icon-plus\"></span><span class=\"icon-royal icon-minus\"></span>\n"
                        + "                                </span>\n"
                        + "                                <ul class=\"level1 parent clearfix\">\n";

                for (Category sc : c.getCategoryList()) {
                    layerHTML += "                                     <li class=\"level2\">\n"
                            + "                                            <a class=\"level2\" href=\"" + request.getContextPath() + "/products?category=" + sc.getCategoryID() + "\"><span>" + sc.getCategoryName() + "</span></a>\n"
                            + "                                        </li>\n";
                }
                layerHTML += "                             </ul>\n"
                        + "                            </li>\n";
            }
            layerHTML += "                 </ul>\n"
                    + "                </li>\n"
                    + "                <!-- Collection -->\n"
                    + "                <li id=\"category-3\" class=\"level0 active current parent first last\">\n"
                    + "                    <a class=\"level0\" href=\"index.php/fashion.html\"><span>Collections</span></a><span class=\"opener\"><span class=\"icon-royal icon-plus\"></span><span class=\"icon-royal icon-minus\"></span></span>\n"
                    + "                    <ul class=\"level0 parent clearfix\">\n";
            for (ProductCol pc : productColFacade.findAll()) {
                layerHTML += "                         <li class=\"level1 parent first\">\n"
                        + "                                <a class=\"level1\" href=\"" + request.getContextPath() + "/products?collection=" + pc.getColID() + "\"><span>" + pc.getColName() + "</span></a>\n"
                        + "                            </li>\n";
            }
            layerHTML += "                 </ul>\n"
                    + "                </li>\n"
                    + "            </ul>\n"
                    + "        </div>\n"
                    + "    </div>\n"
                    + "    <script type=\"text/javascript\">\n"
                    + "        jQuery('.eg-categories-accordion').accordion();\n"
                    + "        jQuery('.eg-categories-accordion').each(function(index) {\n"
                    + "            var activeItems = jQuery(this).find('li.active');\n"
                    + "            activeItems.each(function(i) {\n"
                    + "                jQuery(this).children('ul').css('display', 'block');\n"
                    + "                if (i == activeItems.length - 1) {\n"
                    + "                    jQuery(this).addClass(\"current\");\n"
                    + "                }\n"
                    + "            });\n"
                    + "        });\n"
                    + "    </script>\n"
                    + "    <script type=\"text/javascript\">\n"
                    + "        var categoryUrl = \"<c:url value=\" / \" />\";\n"
                    + "        var categoryUrlParams = \"\";\n"
                    + "        var layerednavAjaxUrl = \"<c:url value=\" / \" />product-filter\";\n"
                    + "    </script>\n"
                    + "    <div class=\"block block-layered-nav\">\n"
                    + "        <div class=\"block-title\">\n"
                    + "            <strong><span>Shop by</span></strong>\n"
                    + "        </div>\n"
                    + "        <div class=\"block-content\">\n"
                    + "            <div class=\"layers-container\">\n"
                    + "                <dl id=\"eglayerednav_dl\">\n";
            for (AttributeGroup ag : attributeGroupFacade.findAll()) {
                layerHTML += "             <dt class=\"eglayerednav_dt\">";
                if (attr != 0) {
                    if (ag.getAttributeGroupID() == attributeFacade.find(attr).getAttributeGroupID().getAttributeGroupID()) {
                        layerHTML += "         <a id=\"attr-clear\" href=\"?attr=clear\" class=\"btn-remove eglayerednav_clear\" title=\"Remove This Item\">Remove This Item</a>";
                    }
                }
                layerHTML += "             " + ag.getAttributeGroupName() + ""
                        + "                </dt>\n"
                        + "                <dd id=\"eglayerednav_attr\" style=\"display: " + (attr != 0 ? ag.getAttributeGroupID() == attributeFacade.find(attr).getAttributeGroupID().getAttributeGroupID() ? "block" : "none" : "none") + "\">\n";
                for (Attribute a : ag.getAttributeList()) {
                    layerHTML += "                 <ol class=\"clearfix\">\n"
                            + "                        <li class=\"grid-full no-padding-right clearfix\"> <a href=\"#\" id=\"attr-" + a.getAttributeID() + "\" class=\"eglayerednav_attribute " + (a.getAttributeID() == attr ? "eglayerednav_attribute_selected" : "") + "\">" + a.getAttributeValue() + "</a> <span class=\"f-right\">" + a.getProductList().size() + "</span> </li>\n"
                            + "                    </ol>\n";
                }
                layerHTML += "             </dd>\n";
            }
            layerHTML += "                 <dt class=\"eglayerednav_dt\">Price Filter</dt>\n"
                    + "                    <dd id=\"eglayerednav_price\">\n"
                    + "                        <ol>\n"
                    + "                            <li>\n"
                    + "                                <div id=\"eglayerednav_price_slider\" class=\"price_slider\"> </div> <span>Price: </span>\n"
                    + "                                <input class=\"input-text\" type=\"text\" maxlength=\"12\" size=\"4\" id=\"price_from\" value=\"$" + (priceFrom == -2 ? 100 : priceFrom) + "\" onclick=\"if (this.value == this.name)\n"
                    + "                                            this.value = '';\" name=\"From\" /> <span>-</span>\n"
                    + "                                <input class=\"input-text\" type=\"text\" maxlength=\"12\" size=\"4\" id=\"price_to\" value=\"$" + (priceTo == -1 ? 10000 : priceTo) + "\" onclick=\"if (this.value == this.name)\n"
                    + "                                            this.value = '';\" name=\"To\" />\n"
                    + "                                <br />\n"
                    + "                                <button type=\"button\" class=\"button\" id=\"price_button_ok\">\n"
                    + "                                    <span><span>OK</span></span>\n"
                    + "                                </button>\n";
            if (priceFrom != -2 && priceTo != -1) {
                layerHTML += "                         <button type=\"button\" class=\"button eglayerednav_clear\">\n"
                        + "                                <span><span>CLEAR</span></span>\n"
                        + "                            </button>\n";
            }
            layerHTML += "                         </li>\n"
                    + "                        </ol>\n"
                    + "                        <script type=\"text/javascript\">\n"
                    + "                            var priceQueryKey = \"price\";\n"
                    + "                            jQuery(function($) {\n"
                    + "                                $(\"#eglayerednav_price_slider\").slider({\n"
                    + "                                    range: true,\n"
                    + "                                    min: 100,\n"
                    + "                                    max: 10000,\n"
                    + "                                    values: [" + (priceFrom == -2 ? 100 : priceFrom) + ", " + (priceTo == -1 ? 10000 : priceTo) + "],\n"
                    + "                                    slide: function(event, ui) {\n"
                    + "                                        $(\"#price_from\").val(\"$\" + ui.values[0]);\n"
                    + "                                        $(\"#price_to\").val(\"$\" + ui.values[1]);\n"
                    + "                                    },\n"
                    + "                                    change: function(event, ui) {\n"
                    + "                                        eglayerednav_add_params(priceQueryKey, ui.values[0] + ',' + ui.values[1], true);\n"
                    + "                                        eglayerednav_send_request();\n"
                    + "                                    }\n"
                    + "                                });\n"
                    + "                            });\n"
                    + "                        </script>\n"
                    + "                    </dd>\n"
                    + "                </dl>\n"
                    + "            </div>\n"
                    + "        </div>\n"
                    + "        <!-- <div class=\"layerednav_loading\" style=\"display:none\"></div> --></div>\n"
                    + "    <script type=\"text/javascript\">\n"
                    + "        eglayerednav_init();\n"
                    + "    </script>\n"
                    + "    <script type=\"text/javascript\">\n"
                    + "        decorateDataList('eglayer-filters');\n"
                    + "        catalog_toolbar_init();\n"
                    + "    </script>\n"
                    + "</div>";
            String productsHTML = "<div id=\"eglayerednav_container\">\n"
                    + "    <div class=\"category-products\">\n"
                    + "        <div class=\"toolbar clearfix\">\n"
                    + "            <div class=\"toolbar-upside\">\n"
                    + "                <div class=\"sort-by\">\n"
                    + "                    <label>Sort By:</label>\n"
                    + "                    <select onchange=\"catalog_toolbar_make_request(this.value)\">\n"
                    + "                        <option value=\"?order=name&dir=" + dir + "\" " + (order.equals("name") ? "selected" : "") + "> Name </option>\n"
                    + "                        <option value=\"?order=price&dir=" + dir + "\" " + (order.equals("price") ? "selected" : "") + "> Price </option>\n"
                    + "                        <option value=\"?order=new&dir=" + dir + "\" " + (order.equals("new") ? "selected" : "") + "> New </option>\n"
                    + "                    </select>\n"
                    + "                    <a href=\"?order=" + order + "&dir=" + (dir.equals("desc") ? "asc" : "desc") + "\" title=\"Set Descending Direction\" class=\"fa fa-long-arrow-" + (dir.equals("desc") ? "down" : "up") + "\"></a>\n"
                    + "                </div>\n"
                    + "                <div class=\"view-mode\">\n";
            if (mode.equals("grid")) {
                productsHTML += "              <strong title=\"Grid\" class=\"grid\">\n"
                        + "                        <i class=\"fa fa-th-large\"></i>\n"
                        + "                    </strong>\n"
                        + "                    <a href=\"?mode=list\" title=\"List\" class=\"list\">\n"
                        + "                        <i class=\"fa fa-th-list\"></i>\n"
                        + "                    </a>\n";
            } else {
                productsHTML += "              <a href=\"?mode=grid\" title=\"Grid\" class=\"List\">\n"
                        + "                        <i class=\"fa fa-th-large\"></i>\n"
                        + "                    </a>\n"
                        + "                    <strong title=\"List\" class=\"grid\">\n"
                        + "                        <i class=\"fa fa-th-list\"></i>\n"
                        + "                    </strong>\n";
            }
            productsHTML += "          </div>\n"
                    + "                <div class=\"limiter\">\n"
                    + "                    <label>Show:</label>\n"
                    + "                    <select onchange=\"catalog_toolbar_make_request(this.value)\">\n"
                    + "                        <option value=\"?limit=6\" " + (maxResult == 6 ? "selected" : "") + "> 6 </option>\n"
                    + "                        <option value=\"?limit=9\" " + (maxResult == 9 ? "selected" : "") + "> 9 </option>\n"
                    + "                        <option value=\"?limit=12\" " + (maxResult == 12 ? "selected" : "") + "> 12 </option>\n"
                    + "                    </select>\n"
                    + "                </div>\n"
                    + "            </div>\n"
                    + "            <div class=\"toolbar-downside no-display\">\n"
                    + "                <div class=\"pages gen-direction-arrows1\"> <strong>Page:</strong>\n"
                    + "                    <ol>\n";
            int i = 0;
            for (i = 1; i <= pageLimit; i++) {
                if (i == page) {
                    productsHTML += "                                            <li class=\"current\">" + i + "</li>\n";
                } else {
                    productsHTML += "                                            <li><a href=\"?p=" + i + "\">" + i + "</a> </li>\n";
                }
            }
            if (page < pageLimit) {
                productsHTML += "                  <li class=\"next\">\n"
                        + "                            <a class=\"next i-next\" href=\"?p=" + (page + 1) + "\" title=\"Next\">\n"
                        + "                                Next                        \n"
                        + "                            </a>\n"
                        + "                        </li>\n";
            }
            productsHTML += "              </ol>\n"
                    + "                </div>\n"
                    + "            </div>\n"
                    + "        </div>\n";
            if (mode.equals("list")) {
                productsHTML += "<ol class=\"products-list clearfix\" id=\"products-list\">\n";
                for (Product p : list) {
                    productsHTML += "    <li class=\"item\">\n"
                            + "        <div class=\"media-section eg-image-slider\">\n"
                            + "            <a href=\"" + request.getContextPath() + "/product-detail?skuid=" + p.getSkuList().get(0).getSkuID() + "\" title=\"" + p.getProductName() + "\" class=\"product-image\" data-images=\"" + request.getContextPath() + "/resource/images/" + p.getProductImageList().get(0).getImageLink() + "\"> <img src=\"" + request.getContextPath() + "/resource/images/" + p.getProductImageList().get(0).getImageLink() + "\" alt=\"" + p.getProductName() + "\" style=\"width:100%\" width=\"350\" height=\"455\" /> </a>\n"
                            + "            <div class=\"product-labels\">\n";
                    if (p.getIsNew()) {
                        productsHTML += "              <span class=\"new-product\">\n"
                                + "                        <span>New</span>\n"
                                + "                    </span>\n";
                    }
                    if (p.getDiscountValue() != 0) {
                        productsHTML += "              <span class=\"discount-code\">\n"
                                + "                        <span>-" + p.getDiscountValue() + "%</span>\n"
                                + "                    </span>\n";
                    }
                    productsHTML += "      </div>\n"
                            + "            <div class=\"hover-section\">\n"
                            + "                <div class=\"hover-links-wrapper a-center\">\n"
                            + "                    <div class=\"eg-tac link-icon\">\n"
                            + "                        <a href=\"" + request.getContextPath() + "/add-wishlist/" + p.getProductID() + "\" class=\"link-wishlist\" title=\"Wishlist\"> <span class=\"link-ico\"><span class=\"fa fa-heart\"></span></span> <span class=\"link-label\">Wishlist</span> </a>\n"
                            + "                    </div>\n"
                            + "                    <div class=\"eg-tac link-icon\">\n"
                            + "                        <a href=\"" + request.getContextPath() + "/product-compare/add?productid=" + p.getProductID() + "\" class=\"\" title=\"Compare\"> <span class=\"link-ico\"><span class=\"fa fa-exchange\"></span></span> <span class=\"link-label\">Compare</span> </a>\n"
                            + "                    </div>\n"
                            + "                    <div class=\"eg-tac link-both\">\n"
                            + "                        <a href=\"" + request.getContextPath() + "/quickview?skuid=" + p.getSkuList().get(0).getSkuID() + "\" class=\"link-quickview\" title=\"Quick View\"> <span class=\"link-ico\"><span class=\"fa fa-search\"></span></span> <span class=\"link-label\">Quickview</span> </a>\n"
                            + "                    </div>\n"
                            + "                </div>\n"
                            + "            </div>\n"
                            + "        </div>\n"
                            + "        <div class=\"product-shop\">\n"
                            + "            <div class=\"f-fix\">\n"
                            + "                <div class=\"clearfix\"></div>\n"
                            + "                <div class=\"product-category\">\n"
                            + "                    <a href=\"#\" title=\"" + p.getCategoryID().getCategoryName() + "\">" + p.getCategoryID().getCategoryName() + "</a>\n"
                            + "                </div>\n"
                            + "                <h2 class=\"product-name\">\n"
                            + "                    <a href=\"" + request.getContextPath() + "/product-detail?skuid=" + p.getSkuList().get(0).getSkuID() + "\" title=\"" + p.getProductName() + "\">" + p.getProductName() + "</a>\n"
                            + "                </h2>\n"
                            + "                <div class=\"review rating-only\">\n"
                            + "                    <div class=\"ratings\">\n"
                            + "                        <div class=\"rating-box\">\n";
                    if (p.getRating() != 0) {
                        productsHTML += "                            <div class=\"rating theme-color\" style=\"width:" + p.getRating() + "%\"></div>\n";
                    }
                    if (p.getRating() == 0) {
                        productsHTML += "                            <div class=\"rating theme-color\" style=\"width:0%\"></div>\n";
                    }
                    productsHTML += "                  </div>\n"
                            + "                    </div>\n"
                            + "                </div>\n"
                            + "                <div class=\"price-box\">\n";
                    if (p.getDiscountValue() != 0) {
                        productsHTML += "              <p class=\"old-price\"> <span class=\"price-label\">Regular Price:</span>\n"
                                + "                        <span class=\"price\">\n"
                                + "                            $" + p.getSkuList().get(0).getUnitPrice() + ""
                                + "                        </span>\n"
                                + "                    </p>\n"
                                + "                    <p class=\"special-price\"> <span class=\"price-label\">Special Price</span>\n"
                                + "                        <span class=\"price\" style=\"color: #cda85c\">\n"
                                + "                            $" + (p.getSkuList().get(0).getUnitPrice() - (p.getSkuList().get(0).getUnitPrice() * p.getDiscountValue() / 100)) + ""
                                + "                        </span>\n"
                                + "                    </p>\n";
                    }
                    if (p.getDiscountValue() == 0) {
                        productsHTML += "              <span class=\"regular-price\">\n"
                                + "                        <span class=\"price\">\n"
                                + "                            $" + p.getSkuList().get(0).getUnitPrice() + ""
                                + "                        </span> \n"
                                + "                    </span>\n";
                    }
                    productsHTML += "              </div>\n"
                            + "                <div class=\"desc std\"> " + p.getProductDesc() + " <a href=\"" + request.getContextPath() + "/product-detail?skuid=" + p.getSkuList().get(0).getSkuID() + "\" title=\"" + p.getProductName() + "\" class=\"link-learn\">Learn More</a> </div>\n"
                            + "            </div>\n"
                            + "        </div>\n"
                            + "    </li>\n";
                }
                productsHTML += "</ol>\n"
                        + "<script type=\"text/javascript\">\n"
                        + "    decorateList('products-list', 'none-recursive')\n"
                        + "</script>\n"
                        + "<script type=\"text/javascript\">\n"
                        + "    decorateGeneric($$('ul.products-grid'), ['odd', 'even', 'first', 'last'])\n"
                        + "</script> ";
            } else {
                productsHTML += "  <!-- PRODUCT GRIDS -->\n"
                        + "        <div class=\"grid-container\">\n"
                        + "            <ul class=\"products-grid\">";
                for (Product p : list) {
                    productsHTML += "<li class=\"grid-columns cols-3 item grid12-sm-4 grid12-xs-6 grid12-sxs-12\">\n"
                            + "        <div class=\"media-section eg-image-slider\">\n"
                            + "            <a href=\"" + request.getContextPath() + "/product-detail?skuid=" + p.getSkuList().get(0).getSkuID() + "\" title=\"" + p.getProductName() + "\" class=\"product-image\" data-images=\"" + p.getProductImageList().get(0).getImageLink() + "\"> <img src=\"" + request.getContextPath() + "/resource/images/" + p.getProductImageList().get(0).getImageLink() + "\" alt=\"" + p.getProductName() + "\" style=\"width:100%\" width=\"350\" height=\"455\" /> </a>\n"
                            + "            <div class=\"product-labels\">\n";
                    if (p.getIsNew()) {
                        productsHTML += "              <span class=\"new-product\">\n"
                                + "                        <span>New</span>\n"
                                + "                    </span>\n";
                    }
                    if (p.getDiscountValue() != 0) {
                        productsHTML += "              <span class=\"discount-code\">\n"
                                + "                        <span>-" + p.getDiscountValue() + "%</span>\n"
                                + "                    </span>\n";
                    }
                    productsHTML += "      </div>\n"
                            + "            <div class=\"hover-section\">\n"
                            + "                <div class=\"hover-links-wrapper a-center\">\n"
                            + "                    <div class=\"eg-tac link-icon\">\n"
                            + "                        <a href=\"" + request.getContextPath() + "/add-wishlist/" + p.getProductID() + "\" class=\"link-wishlist\" title=\"Wishlist\"> <span class=\"link-ico\"><span class=\"fa fa-heart\"></span></span> <span class=\"link-label\">Wishlist</span> </a>\n"
                            + "                    </div>\n"
                            + "                    <div class=\"eg-tac link-icon\">\n"
                            + "                        <a href=\"" + request.getContextPath() + "/product-compare/add?productid=" + p.getProductID() + "\" class=\"\" title=\"Compare\"> <span class=\"link-ico\"><span class=\"fa fa-exchange\"></span></span> <span class=\"link-label\">Compare</span> </a>\n"
                            + "                    </div>\n"
                            + "                    <div class=\"eg-tac link-both\">\n"
                            + "                        <a href=\"javascript:void(0);\" data-target=\"" + request.getContextPath() + "/quickview?skuid=" + p.getSkuList().get(0).getSkuID() + "\" class=\"link-quickview\" title=\"Quick View\"> <span class=\"link-ico\"><span class=\"fa fa-search\"></span></span> <span class=\"link-label\">Quickview</span> </a>\n"
                            + "                    </div>\n"
                            + "                </div>\n"
                            + "            </div>\n"
                            + "        </div>\n"
                            + "        <div class=\"details-section\">\n"
                            + "            <div class=\"product-category\"> <a href=\"#\" title=\"" + p.getCategoryID().getCategoryName() + "\">" + p.getCategoryID().getCategoryName() + "</a> </div>\n"
                            + "            <h2 class=\"product-name\">\n"
                            + "                <a href=\"" + request.getContextPath() + "/product-detail?skuid=" + p.getSkuList().get(0).getSkuID() + "\" title=\"" + p.getProductName() + "\">" + p.getProductName() + "</a>\n"
                            + "            </h2>\n"
                            + "            <div class=\"review rating-only\">\n"
                            + "                <div class=\"ratings clearfix\">\n"
                            + "                    <div class=\"rating-box\">\n";
                    if (p.getRating() != 0) {
                        productsHTML += "                            <div class=\"rating theme-color\" style=\"width:" + p.getRating() + "%\"></div>\n";
                    }
                    if (p.getRating() == 0) {
                        productsHTML += "                            <div class=\"rating theme-color\" style=\"width:0%\"></div>\n";
                    }
                    productsHTML += "              </div>\n"
                            + "                </div>\n"
                            + "            </div>\n"
                            + "            <div class=\"price-box\">\n";
                    if (p.getDiscountValue() != 0) {
                        productsHTML += "              <p class=\"old-price\"> <span class=\"price-label\">Regular Price:</span>\n"
                                + "                        <span class=\"price\">\n"
                                + "                            $" + p.getSkuList().get(0).getUnitPrice() + ""
                                + "                        </span>\n"
                                + "                    </p>\n"
                                + "                    <p class=\"special-price\"> <span class=\"price-label\">Special Price</span>\n"
                                + "                        <span class=\"price\" style=\"color: #cda85c\">\n"
                                + "                            $" + (p.getSkuList().get(0).getUnitPrice() - (p.getSkuList().get(0).getUnitPrice() * p.getDiscountValue() / 100)) + ""
                                + "                        </span>\n"
                                + "                    </p>\n";
                    }
                    if (p.getDiscountValue() == 0) {
                        productsHTML += "              <span class=\"regular-price\">\n"
                                + "                        <span class=\"price\">\n"
                                + "                            $" + p.getSkuList().get(0).getUnitPrice() + ""
                                + "                        </span> \n"
                                + "                    </span>\n";
                    }
                    productsHTML += "      </div>\n"
                            + "        </div>\n"
                            + "    </li>\n";
                }
                productsHTML += "</ul>\n"
                        + "        </div>\n"
                        + "        <script type=\"text/javascript\">\n"
                        + "            decorateGeneric($$('ul.products-grid'), ['odd', 'even', 'first', 'last'])\n"
                        + "        </script>\n"
                        + "        <div class=\"toolbar-bottom\">\n"
                        + "            <div class=\"toolbar clearfix\">\n"
                        + "                <div class=\"toolbar-upside\">\n"
                        + "                    <div class=\"sort-by\">\n"
                        + "                        <label>Sort By:</label>\n"
                        + "                        <select onchange=\"catalog_toolbar_make_request(this.value)\">\n"
                        + "                            <option value=\"?order=name&dir=" + dir + "\" " + (order.equals("name") ? "selected" : "") + "> Name </option>\n"
                        + "                            <option value=\"?order=price&dir=" + dir + "\" " + (order.equals("price") ? "selected" : "") + "> Price </option>\n"
                        + "                            <option value=\"?order=new&dir=" + dir + "\" " + (order.equals("new") ? "selected" : "") + "> New </option>\n"
                        + "                        </select>\n"
                        + "                        <a href=\"?order=" + order + "&dir=" + (dir.equals("desc") ? "asc" : "desc") + "\" title=\"Set Descending Direction\" class=\"fa fa-long-arrow-" + (dir.equals("desc") ? "down" : "up") + "\"></a>\n"
                        + "                    </div>\n";
                if (mode.equals("grid")) {
                    productsHTML += "              <strong title=\"Grid\" class=\"grid\">\n"
                            + "                        <i class=\"fa fa-th-large\"></i>\n"
                            + "                    </strong>\n"
                            + "                    <a href=\"?mode=list\" title=\"List\" class=\"list\">\n"
                            + "                        <i class=\"fa fa-th-list\"></i>\n"
                            + "                    </a>\n";
                } else {
                    productsHTML += "              <a href=\"?mode=grid\" title=\"Grid\" class=\"List\">\n"
                            + "                        <i class=\"fa fa-th-large\"></i>\n"
                            + "                    </a>\n"
                            + "                    <strong title=\"List\" class=\"grid\">\n"
                            + "                        <i class=\"fa fa-th-list\"></i>\n"
                            + "                    </strong>\n";
                }
                productsHTML += "              <div class=\"limiter\">\n"
                        + "                        <label>Show:</label>\n"
                        + "                        <select onchange=\"catalog_toolbar_make_request(this.value)\">\n"
                        + "                            <option value=\"?limit=6\" " + (maxResult == 6 ? "selected" : "") + "> 6 </option>\n"
                        + "                            <option value=\"?limit=9\" " + (maxResult == 9 ? "selected" : "") + "> 9 </option>\n"
                        + "                            <option value=\"?limit=12\" " + (maxResult == 12 ? "selected" : "") + "> 12 </option>\n"
                        + "                        </select>\n"
                        + "                    </div>\n"
                        + "                </div>\n"
                        + "                <div class=\"toolbar-downside no-display\">\n"
                        + "                    <div class=\"pager\">\n"
                        + "                        <div class=\"pages gen-direction-arrows1\"> <strong>Page:</strong>\n"
                        + "                            <ol>\n";
                for (i = 1; i <= pageLimit; i++) {
                    if (i == page) {
                        productsHTML += "                                            <li class=\"current\">" + i + "</li>\n";
                    } else {
                        productsHTML += "                                            <li><a href=\"?p=" + i + "\">" + i + "</a> </li>\n";
                    }
                }
                if (page < pageLimit) {
                    productsHTML += "                          <li class=\"next\">\n"
                            + "                                    <a class=\"next i-next\" href=\"?p=" + (page + 1) + "\" title=\"Next\">\n"
                            + "                                        Next                        \n"
                            + "                                    </a>\n"
                            + "                                </li>\n";
                }
                productsHTML += "                      </ol>\n"
                        + "                        </div>\n"
                        + "                    </div>\n"
                        + "                </div>\n"
                        + "            </div>\n"
                        + "        </div>\n"
                        + "    </div>\n"
                        + "    <div class=\"ajaxcart_loading\" style=\"display:none\"> <img src=\"resource/client/skin/frontend/base/default/images/elegento/ajax/ajax-loader.gif\" alt=\"Loading\" /> </div>\n"
                        + "    <script type=\"text/javascript\">\n"
                        + "        //<![CDATA[\n"
                        + "        jQuery(function($) {\n"
                        + "            var columnCount = 3;\n"
                        + "            var getColumnCount = function(wrapper) {\n"
                        + "                if (Modernizr.mq(\"screen and (min-width:768px) and (max-width:991px)\")) {\n"
                        + "                    return 3;\n"
                        + "                } else if (Modernizr.mq(\"screen and (min-width:481px) and (max-width:767px)\")) {\n"
                        + "                    return 2;\n"
                        + "                } else if (Modernizr.mq(\"screen and (max-width:480px)\")) {\n"
                        + "                    return 1;\n"
                        + "                } else {\n"
                        + "                    return columnCount;\n"
                        + "                }\n"
                        + "            };\n"
                        + "            var resizeGrid = function() {\n"
                        + "                $('.category-products .products-grid').each(function() {\n"
                        + "                    var $grid = $(this);\n"
                        + "                    var colCount = getColumnCount();\n"
                        + "                    $grid.find('.product-image img').imgpreload({\n"
                        + "                        all: function() {\n"
                        + "                            var h = 0;\n"
                        + "                            var index = 0;\n"
                        + "                            var size = $grid.find('.item').length;\n"
                        + "                            var items = new Array();\n"
                        + "                            $grid.find('.item').each(function() {\n"
                        + "                                index++;\n"
                        + "                                $(this).css('height', 'auto');\n"
                        + "                                if (h < $(this).innerHeight() + 12)\n"
                        + "                                    h = $(this).innerHeight() + 12;\n"
                        + "                                if (index % colCount == 0 || index == size) {\n"
                        + "                                    items.push(this);\n"
                        + "                                    $.each(items, function(index, value) {\n"
                        + "                                        $(value).css({\n"
                        + "                                            height: h + 'px'\n"
                        + "                                        });\n"
                        + "                                    });\n"
                        + "                                    h = 0;\n"
                        + "                                    items = new Array();\n"
                        + "                                } else {\n"
                        + "                                    items.push(this);\n"
                        + "                                }\n"
                        + "                            })\n"
                        + "                        }\n"
                        + "                    });\n"
                        + "                });\n"
                        + "            };\n"
                        + "            resizeGrid();\n"
                        + "            $(window).on('delayed-resize', function(e, resizeEvent) {\n"
                        + "                resizeGrid();\n"
                        + "            });\n"
                        + "            EG.commonfns();\n"
                        + "        });\n"
                        + "        //]]>\n"
                        + "    </script>\n"
                        + "</div>";
            }
            filterJson.put("layer", layerHTML);
            filterJson.put("products", productsHTML);
            Gson gson = new GsonBuilder().disableHtmlEscaping().create();
            String json = gson.toJson(filterJson);
            out.print(json);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
