package controllers.client.product;

import beans.OrderMasterFacadeLocal;
import beans.ProductFacadeLocal;
import beans.ReviewFacadeLocal;
import beans.SkuFacadeLocal;
import entities.Customer;
import entities.OrderDetail;
import entities.OrderMaster;
import entities.Product;
import entities.Sku;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.lang.ArrayUtils;

@WebServlet(name = "ProductDetailServlet", urlPatterns = {"/product-detail"})
public class ProductDetailServlet extends HttpServlet {

    @EJB
    private OrderMasterFacadeLocal orderMasterFacade;
    @EJB
    private ReviewFacadeLocal reviewFacade;
    @EJB
    private SkuFacadeLocal skuFacade;
    @EJB
    private ProductFacadeLocal productFacade;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            // get product
            String skuid = request.getParameter("skuid");
            Sku curSku = skuFacade.find(skuid);
            Product prod = curSku.getProductID();
            request.setAttribute("curSku", curSku);
            request.setAttribute("prod", prod);
            
            // get related Product List
            List<Product> relatedList = productFacade.getRelatedProduct(prod);
            request.setAttribute("relatedList", relatedList);

            // save cookie
            Cookie[] ck = request.getCookies();
            if (ck != null) {
                List<String> prodId = new ArrayList<>();
                String ssValue = "";
                for (Cookie c : ck) {
                    if (c.getName().equals("ssValue")) {
                        ssValue = c.getValue();
                    }
                }
                String joined = "";
                if (ssValue.isEmpty()) {
                    joined = prod.getProductID().toString();
                } else {
                    String[] split = ssValue.split(",");
                    if (!ArrayUtils.contains(split, prod.getProductID().toString())) {
                        prodId.add(prod.getProductID().toString());
                    }
                    prodId.add(ssValue);
                    joined = String.join(",", prodId);
                }
                Cookie ckSsValue = new Cookie("ssValue", joined);
                ckSsValue.setMaxAge(24 * 60 * 60);
                response.addCookie(ckSsValue);
            }
            
            // get cookie
            String ckValue = "";
            for (Cookie c : ck) {
                if (c.getName().equals("ssValue")) {
                    ckValue = c.getValue();
                }
            }
            if (ckValue.isEmpty()) {
                request.setAttribute("ckProductHistory", productFacade.getHotProduct().subList(0, 6));
            } else {
                request.setAttribute("ckProductHistory", productFacade.findByCustomerHistory(ckValue));
            }
            
            // check customer has buy product & THIS PRODUCT HAS REVIEW OR CUSTOMER NOT LOGIN YET
            HttpSession session = request.getSession();
            Customer c = (Customer) session.getAttribute("customer");
            if (c != null) {
                List<OrderMaster> listOM = orderMasterFacade.findByCustomerID(c);
                int prodID = 0;
                for (int i = 0; i < listOM.size(); i++) {
                    List<OrderDetail> listOD = listOM.get(i).getOrderDetailList();
                    for (int j = 0; j < listOD.size(); j++) {
                        prodID = listOD.get(j).getSku().getProductID().getProductID();
                        if (prodID == curSku.getProductID().getProductID() && reviewFacade.findByReviewPK(c.getCustomerID(), prod.getProductID()) == null) {
                            request.setAttribute("submitComment", "yes");
                            break;
                        }
                    }
                }
                
                // get recommend product (IF USER HAS LOGGED IN)
                Product recProd = productFacade.getReccommendedProduct(c);
                request.setAttribute("recProd", recProd);
            }
            
            if (c != null) {
                // get recommend product (IF USER HAS LOGGED IN)
                Product recProd = productFacade.getReccommendedProduct(c);
                request.setAttribute("recProd", recProd);
            }
            request.getRequestDispatcher("/client/product/product-detail.jsp").forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
