package controllers.client.product;

import beans.AttributeFacadeLocal;
import beans.AttributeGroupFacadeLocal;
import beans.ProductFacadeLocal;
import entities.Customer;
import entities.Product;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(name = "ProductServlet", urlPatterns = {"/products"})
public class ProductServlet extends HttpServlet {

    @EJB
    private AttributeFacadeLocal attributeFacade;
    @EJB
    private AttributeGroupFacadeLocal attributeGroupFacade;
    @EJB
    private ProductFacadeLocal productFacade;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            String catParam = request.getParameter("category");
            String colParam = request.getParameter("collection");
            int categoryID = 0;
            int colID = 0;
            if (catParam != null) {
                categoryID = Integer.parseInt(catParam);
            }
            if (colParam != null) {
                colID = Integer.parseInt(colParam);
            }
            
            // save cookie
            Cookie[] ck = request.getCookies();

            // get cookie
            String ckValue = "";
            for (Cookie c : ck) {
                if (c.getName().equals("ssValue")) {
                    ckValue = c.getValue();
                }
            }
            if (ckValue.isEmpty()) {
                request.setAttribute("ckProductHistory", productFacade.getHotProduct().subList(0, 6));
            } else {
                request.setAttribute("ckProductHistory", productFacade.findByCustomerHistory(ckValue));
            }
            
            // rec product
            HttpSession session = request.getSession();
            Customer c = (Customer) session.getAttribute("customer");
            if (c != null) {
                // get recommend product (IF USER HAS LOGGED IN)
                Product recProd = productFacade.getReccommendedProduct(c);
                request.setAttribute("recProd", recProd);
            }
            
            String mode = request.getParameterMap().containsKey("mode") ? request.getParameter("mode") : "grid";

            // Filter
            int attr = request.getParameterMap().containsKey("attr") && !request.getParameter("attr").equals("clear") ? Integer.parseInt(request.getParameter("attr")) : 0;
            String price = request.getParameterMap().containsKey("price") && !request.getParameter("price").equals("clear") ? request.getParameter("price") : "-2,-1";
            String[] split = price.split(",");
            int priceFrom = Integer.parseInt(split[0]);
            int priceTo = Integer.parseInt(split[1]);

            // Sort
            String dir = request.getParameterMap().containsKey("dir") ? request.getParameter("dir") : "asc";
            String order = request.getParameterMap().containsKey("order") ? request.getParameter("order") : "name";

            // Pagination Max Result 9
            int page = request.getParameter("p") != null ? Integer.parseInt(request.getParameter("p")) : 1;
            int maxResult = request.getParameterMap().containsKey("limit") ? Integer.parseInt(request.getParameter("limit")) : 6;
            int firstResult = (page - 1) * maxResult;
            List<Product> list;
            if (request.getParameterMap().containsKey("attr") && !request.getParameter("attr").equals("clear")
                    || request.getParameterMap().containsKey("price") && !request.getParameter("price").equals("clear")
                    || request.getParameterMap().containsKey("order") || request.getParameterMap().containsKey("dir")) {
                list = productFacade.findByFilter(categoryID, colID, attr, priceFrom, priceTo, dir, order);
            } else {
                list = productFacade.findByCatCol(categoryID, colID);
            }
            int totalPage = list.size();
            int pageLimit = (int) Math.floor((totalPage / maxResult) + 1);

            int from = Math.max(0, firstResult);
            int to = Math.min(list.size(), from + maxResult);
            list = list.subList(from, to);

            request.setAttribute("attrGroupList", attributeGroupFacade.getSearchableGroup());
            request.setAttribute("attr", attributeFacade.find(attr));
            request.setAttribute("prodList", list);
            request.setAttribute("mode", mode);
            // Pagination
            request.setAttribute("page", page);
            request.setAttribute("pageLimit", pageLimit);
            // Filter
            request.setAttribute("categoryID", categoryID);
            request.setAttribute("colID", colID);
            request.setAttribute("attrID", attr);
            request.setAttribute("priceFrom", priceFrom == -2 ? 100 : priceFrom);
            request.setAttribute("priceTo", priceTo == -1 ? 10000 : priceTo);
            // Sort
            request.setAttribute("dir", dir);
            request.setAttribute("order", order);
            request.setAttribute("limit", maxResult);
            request.getRequestDispatcher("/client/product/product.jsp").forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
