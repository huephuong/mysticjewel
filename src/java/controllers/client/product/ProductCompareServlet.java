/* To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package controllers.client.product;

import beans.AttributeGroupFacadeLocal;
import beans.CategoryFacadeLocal;
import beans.ProductColFacadeLocal;
import beans.ProductFacadeLocal;
import java.io.IOException;
import java.io.PrintWriter;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "ProductCompareServlet", urlPatterns = {"/product-compare"})
public class ProductCompareServlet extends HttpServlet {
    @EJB
    private ProductFacadeLocal productFacade;
    @EJB
    private ProductColFacadeLocal productColFacade;
    @EJB
    private CategoryFacadeLocal categoryFacade;
    @EJB
    private AttributeGroupFacadeLocal attributeGroupFacade;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            // header
            request.setAttribute("catList", categoryFacade.getMainCategory());
            request.setAttribute("colList", productColFacade.findAll());

            // footer
            request.setAttribute("newProdFooter", productFacade.getNewProduct().subList(0, 3));
            request.setAttribute("hotProdFooter", productFacade.getHotProduct().subList(0, 3));
            request.setAttribute("saleProdFooter", productFacade.getOnSaleProduct().subList(0, 3));
            request.setAttribute("colFooter", productColFacade.getSingleCol());
            
            request.setAttribute("attGroupList", attributeGroupFacade.findAll());
            request.getRequestDispatcher("/client/product/product-compare.jsp").forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
