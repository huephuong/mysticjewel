package entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

@Entity
@Table(name = "Attribute", catalog = "MysticJewelDB", schema = "dbo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Attribute.findAll", query = "SELECT a FROM Attribute a"),
    @NamedQuery(name = "Attribute.findByAttributeID", query = "SELECT a FROM Attribute a WHERE a.attributeID = :attributeID"),
    @NamedQuery(name = "Attribute.findByAttributeValue", query = "SELECT a FROM Attribute a WHERE a.attributeValue = :attributeValue")})
public class Attribute implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "AttributeID", nullable = false)
    private Integer attributeID;
    @Size(max = 50)
    @Column(name = "AttributeValue", length = 50)
    private String attributeValue;
    @JoinTable(name = "ProductAttribute", joinColumns = {
        @JoinColumn(name = "AttributeID", referencedColumnName = "AttributeID", nullable = false)}, inverseJoinColumns = {
        @JoinColumn(name = "ProductID", referencedColumnName = "ProductID", nullable = false)})
    @ManyToMany
    private List<Product> productList;
    @JoinColumn(name = "AttributeGroupID", referencedColumnName = "AttributeGroupID")
    @ManyToOne
    private AttributeGroup attributeGroupID;

    public Attribute() {
    }

    public Attribute(String attributeValue, AttributeGroup attributeGroupID) {
        this.attributeValue = attributeValue;
        this.attributeGroupID = attributeGroupID;
    }

    public Attribute(Integer attributeID) {
        this.attributeID = attributeID;
    }

    public Integer getAttributeID() {
        return attributeID;
    }

    public void setAttributeID(Integer attributeID) {
        this.attributeID = attributeID;
    }

    public String getAttributeValue() {
        return attributeValue;
    }

    public void setAttributeValue(String attributeValue) {
        this.attributeValue = attributeValue;
    }

    @XmlTransient
    public List<Product> getProductList() {
        return productList;
    }

    public void setProductList(List<Product> productList) {
        this.productList = productList;
    }

    public AttributeGroup getAttributeGroupID() {
        return attributeGroupID;
    }

    public void setAttributeGroupID(AttributeGroup attributeGroupID) {
        this.attributeGroupID = attributeGroupID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (attributeID != null ? attributeID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Attribute)) {
            return false;
        }
        Attribute other = (Attribute) object;
        if ((this.attributeID == null && other.attributeID != null) || (this.attributeID != null && !this.attributeID.equals(other.attributeID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Attribute[ attributeID=" + attributeID + " ]";
    }
    
}
