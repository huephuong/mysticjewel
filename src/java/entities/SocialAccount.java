/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author nknguyengl
 */
@Entity
@Table(name = "SocialAccount", catalog = "MysticJewelDB", schema = "dbo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SocialAccount.findAll", query = "SELECT s FROM SocialAccount s"),
    @NamedQuery(name = "SocialAccount.findBySocialAccountID", query = "SELECT s FROM SocialAccount s WHERE s.socialAccountID = :socialAccountID"),
    @NamedQuery(name = "SocialAccount.findByProviderCustomerID", query = "SELECT s FROM SocialAccount s WHERE s.providerCustomerID = :providerCustomerID"),
    @NamedQuery(name = "SocialAccount.findByProvider", query = "SELECT s FROM SocialAccount s WHERE s.provider = :provider")})
public class SocialAccount implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "SocialAccountID", nullable = false)
    private Integer socialAccountID;
    @Size(max = 100)
    @Column(name = "ProviderCustomerID", length = 100)
    private String providerCustomerID;
    @Size(max = 50)
    @Column(name = "Provider", length = 50)
    private String provider;
    @JoinColumn(name = "CustomerID", referencedColumnName = "CustomerID")
    @ManyToOne
    private Customer customerID;

    public SocialAccount() {
    }

    public SocialAccount(Integer socialAccountID) {
        this.socialAccountID = socialAccountID;
    }

    public Integer getSocialAccountID() {
        return socialAccountID;
    }

    public void setSocialAccountID(Integer socialAccountID) {
        this.socialAccountID = socialAccountID;
    }

    public String getProviderCustomerID() {
        return providerCustomerID;
    }

    public void setProviderCustomerID(String providerCustomerID) {
        this.providerCustomerID = providerCustomerID;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public Customer getCustomerID() {
        return customerID;
    }

    public void setCustomerID(Customer customerID) {
        this.customerID = customerID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (socialAccountID != null ? socialAccountID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SocialAccount)) {
            return false;
        }
        SocialAccount other = (SocialAccount) object;
        if ((this.socialAccountID == null && other.socialAccountID != null) || (this.socialAccountID != null && !this.socialAccountID.equals(other.socialAccountID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.SocialAccount[ socialAccountID=" + socialAccountID + " ]";
    }
    
}
