package entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author HuePhuong
 */
@Entity
@Table(name = "ProductCol", catalog = "MysticJewelDB", schema = "dbo", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"ColName"})})
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ProductCol.findAll", query = "SELECT p FROM ProductCol p"),
    @NamedQuery(name = "ProductCol.findByColID", query = "SELECT p FROM ProductCol p WHERE p.colID = :colID"),
    @NamedQuery(name = "ProductCol.findByColName", query = "SELECT p FROM ProductCol p WHERE p.colName = :colName"),
    @NamedQuery(name = "ProductCol.findByColBanner", query = "SELECT p FROM ProductCol p WHERE p.colBanner = :colBanner"),
    @NamedQuery(name = "ProductCol.findByColImage", query = "SELECT p FROM ProductCol p WHERE p.colImage = :colImage"),
    @NamedQuery(name = "ProductCol.findByIsTrending", query = "SELECT p FROM ProductCol p WHERE p.isTrending = :isTrending"),
    @NamedQuery(name = "ProductCol.findByIsActive", query = "SELECT p FROM ProductCol p WHERE p.isActive = :isActive")})
public class ProductCol implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ColID", nullable = false)
    private Integer colID;
    @Size(max = 100)
    @Column(name = "ColName", length = 100)
    private String colName;
    @Size(max = 300)
    @Column(name = "ColBanner", length = 300)
    private String colBanner;
    @Size(max = 300)
    @Column(name = "ColImage", length = 300)
    private String colImage;
    @Lob
    @Size(max = 2147483647)
    @Column(name = "ColDesc", length = 2147483647)
    private String colDesc;
    @Column(name = "IsTrending")
    private Boolean isTrending;
    @Column(name = "IsActive")
    private Boolean isActive;
    @OneToMany(mappedBy = "colID")
    private List<Product> productList;

    public ProductCol() {
    }

    public ProductCol(String colName, String colBanner, String colImage, String colDesc, Boolean isTrending, Boolean isActive) {
        this.colName = colName;
        this.colBanner = colBanner;
        this.colImage = colImage;
        this.colDesc = colDesc;
        this.isTrending = isTrending;
        this.isActive = isActive;
    }
    
    public ProductCol(Integer colID) {
        this.colID = colID;
    }

    public Integer getColID() {
        return colID;
    }

    public void setColID(Integer colID) {
        this.colID = colID;
    }

    public String getColName() {
        return colName;
    }

    public void setColName(String colName) {
        this.colName = colName;
    }

    public String getColBanner() {
        return colBanner;
    }

    public void setColBanner(String colBanner) {
        this.colBanner = colBanner;
    }

    public String getColImage() {
        return colImage;
    }

    public void setColImage(String colImage) {
        this.colImage = colImage;
    }

    public String getColDesc() {
        return colDesc;
    }

    public void setColDesc(String colDesc) {
        this.colDesc = colDesc;
    }

    public Boolean getIsTrending() {
        return isTrending;
    }

    public void setIsTrending(Boolean isTrending) {
        this.isTrending = isTrending;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    @XmlTransient
    public List<Product> getProductList() {
        return productList;
    }

    public void setProductList(List<Product> productList) {
        this.productList = productList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (colID != null ? colID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProductCol)) {
            return false;
        }
        ProductCol other = (ProductCol) object;
        if ((this.colID == null && other.colID != null) || (this.colID != null && !this.colID.equals(other.colID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.ProductCol[ colID=" + colID + " ]";
    }
    
}
