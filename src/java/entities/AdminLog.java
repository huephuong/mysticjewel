/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author NATSAMA
 */
@Entity
@Table(name = "AdminLog", catalog = "MysticJewelDB", schema = "dbo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AdminLog.findAll", query = "SELECT a FROM AdminLog a"),
    @NamedQuery(name = "AdminLog.findByLogID", query = "SELECT a FROM AdminLog a WHERE a.logID = :logID"),
    @NamedQuery(name = "AdminLog.findByUsername", query = "SELECT a FROM AdminLog a WHERE a.username = :username"),
    @NamedQuery(name = "AdminLog.findByDateLogged", query = "SELECT a FROM AdminLog a WHERE a.dateLogged = :dateLogged")})
public class AdminLog implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "LogID", nullable = false)
    private Integer logID;
    @Size(max = 20)
    @Column(name = "Username", length = 20)
    private String username;
    @Lob
    @Size(max = 2147483647)
    @Column(name = "LogContent", length = 2147483647)
    private String logContent;
    @Column(name = "DateLogged")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateLogged;

    public AdminLog() {
    }

    public AdminLog(Integer logID) {
        this.logID = logID;
    }

    public Integer getLogID() {
        return logID;
    }

    public void setLogID(Integer logID) {
        this.logID = logID;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getLogContent() {
        return logContent;
    }

    public void setLogContent(String logContent) {
        this.logContent = logContent;
    }

    public Date getDateLogged() {
        return dateLogged;
    }

    public void setDateLogged(Date dateLogged) {
        this.dateLogged = dateLogged;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (logID != null ? logID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AdminLog)) {
            return false;
        }
        AdminLog other = (AdminLog) object;
        if ((this.logID == null && other.logID != null) || (this.logID != null && !this.logID.equals(other.logID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.AdminLog[ logID=" + logID + " ]";
    }
    
}
