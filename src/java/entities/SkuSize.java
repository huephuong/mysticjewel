/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author NATSAMA
 */
@Entity
@Table(name = "SkuSize", catalog = "MysticJewelDB", schema = "dbo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SkuSize.findAll", query = "SELECT s FROM SkuSize s"),
    @NamedQuery(name = "SkuSize.findBySizeID", query = "SELECT s FROM SkuSize s WHERE s.sizeID = :sizeID"),
    @NamedQuery(name = "SkuSize.findBySizeValue", query = "SELECT s FROM SkuSize s WHERE s.sizeValue = :sizeValue")})
public class SkuSize implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "SizeID", nullable = false)
    private Integer sizeID;
    @Size(max = 5)
    @Column(name = "SizeValue", length = 5)
    private String sizeValue;
    @JoinColumn(name = "CategoryID", referencedColumnName = "CategoryID")
    @ManyToOne
    private Category categoryID;
    @OneToMany(mappedBy = "sizeID")
    private List<Sku> skuList;

    public SkuSize() {
    }

    public SkuSize(String sizeValue, Category categoryID) {
        this.sizeValue = sizeValue;
        this.categoryID = categoryID;
    }

    public SkuSize(Integer sizeID) {
        this.sizeID = sizeID;
    }

    public Integer getSizeID() {
        return sizeID;
    }

    public void setSizeID(Integer sizeID) {
        this.sizeID = sizeID;
    }

    public String getSizeValue() {
        return sizeValue;
    }

    public void setSizeValue(String sizeValue) {
        this.sizeValue = sizeValue;
    }

    public Category getCategoryID() {
        return categoryID;
    }

    public void setCategoryID(Category categoryID) {
        this.categoryID = categoryID;
    }

    @XmlTransient
    public List<Sku> getSkuList() {
        return skuList;
    }

    public void setSkuList(List<Sku> skuList) {
        this.skuList = skuList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (sizeID != null ? sizeID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SkuSize)) {
            return false;
        }
        SkuSize other = (SkuSize) object;
        if ((this.sizeID == null && other.sizeID != null) || (this.sizeID != null && !this.sizeID.equals(other.sizeID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.SkuSize[ sizeID=" + sizeID + " ]";
    }
    
}
