/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author NATSAMA
 */
@Entity
@Table(name = "Sku", catalog = "MysticJewelDB", schema = "dbo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Sku.findAll", query = "SELECT s FROM Sku s"),
    @NamedQuery(name = "Sku.findBySkuID", query = "SELECT s FROM Sku s WHERE s.skuID = :skuID"),
    @NamedQuery(name = "Sku.findByBasePrice", query = "SELECT s FROM Sku s WHERE s.basePrice = :basePrice"),
    @NamedQuery(name = "Sku.findByUnitPrice", query = "SELECT s FROM Sku s WHERE s.unitPrice = :unitPrice"),
    @NamedQuery(name = "Sku.findByUnitsInStock", query = "SELECT s FROM Sku s WHERE s.unitsInStock = :unitsInStock"),
    @NamedQuery(name = "Sku.findByUnitsOnOrder", query = "SELECT s FROM Sku s WHERE s.unitsOnOrder = :unitsOnOrder"),
    @NamedQuery(name = "Sku.findByDateCreated", query = "SELECT s FROM Sku s WHERE s.dateCreated = :dateCreated"),
    @NamedQuery(name = "Sku.findByDateUpdated", query = "SELECT s FROM Sku s WHERE s.dateUpdated = :dateUpdated")})
public class Sku implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "SkuID", nullable = false, length = 20)
    private String skuID;
    @Column(name = "BasePrice")
    private Integer basePrice;
    @Column(name = "UnitPrice")
    private Integer unitPrice;
    @Column(name = "UnitsInStock")
    private Integer unitsInStock;
    @Column(name = "UnitsOnOrder")
    private Integer unitsOnOrder;
    @Column(name = "DateCreated")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreated;
    @Column(name = "DateUpdated")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateUpdated;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "sku")
    private List<OrderDetail> orderDetailList;
    @JoinColumn(name = "ProductID", referencedColumnName = "ProductID")
    @ManyToOne
    private Product productID;
    @JoinColumn(name = "SizeID", referencedColumnName = "SizeID")
    @ManyToOne
    private SkuSize sizeID;

    public Sku() {
    }

    public Sku(String skuID, Integer basePrice, Integer unitPrice, Integer unitsInStock, Integer unitsOnOrder, Date dateCreated, Date dateUpdated, Product productID, SkuSize sizeID) {
        this.skuID = skuID;
        this.basePrice = basePrice;
        this.unitPrice = unitPrice;
        this.unitsInStock = unitsInStock;
        this.unitsOnOrder = unitsOnOrder;
        this.dateCreated = dateCreated;
        this.dateUpdated = dateUpdated;
        this.productID = productID;
        this.sizeID = sizeID;
    }

    public Sku(String skuID) {
        this.skuID = skuID;
    }

    public String getSkuID() {
        return skuID;
    }

    public void setSkuID(String skuID) {
        this.skuID = skuID;
    }

    public Integer getBasePrice() {
        return basePrice;
    }

    public void setBasePrice(Integer basePrice) {
        this.basePrice = basePrice;
    }

    public Integer getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(Integer unitPrice) {
        this.unitPrice = unitPrice;
    }

    public Integer getUnitsInStock() {
        return unitsInStock;
    }

    public void setUnitsInStock(Integer unitsInStock) {
        this.unitsInStock = unitsInStock;
    }

    public Integer getUnitsOnOrder() {
        return unitsOnOrder;
    }

    public void setUnitsOnOrder(Integer unitsOnOrder) {
        this.unitsOnOrder = unitsOnOrder;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Date getDateUpdated() {
        return dateUpdated;
    }

    public void setDateUpdated(Date dateUpdated) {
        this.dateUpdated = dateUpdated;
    }

    @XmlTransient
    public List<OrderDetail> getOrderDetailList() {
        return orderDetailList;
    }

    public void setOrderDetailList(List<OrderDetail> orderDetailList) {
        this.orderDetailList = orderDetailList;
    }

    public Product getProductID() {
        return productID;
    }

    public void setProductID(Product productID) {
        this.productID = productID;
    }

    public SkuSize getSizeID() {
        return sizeID;
    }

    public void setSizeID(SkuSize sizeID) {
        this.sizeID = sizeID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (skuID != null ? skuID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Sku)) {
            return false;
        }
        Sku other = (Sku) object;
        if ((this.skuID == null && other.skuID != null) || (this.skuID != null && !this.skuID.equals(other.skuID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Sku[ skuID=" + skuID + " ]";
    }
    
}
