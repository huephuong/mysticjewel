package entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ComparedProduct implements Serializable {
    List<Product> comparedProducts;

    public List<Product> getComparedProducts() {
        return comparedProducts;
    }

    public void setComparedProducts(List<Product> comparedProducts) {
        this.comparedProducts = comparedProducts;
    }

    public ComparedProduct() {
        this.comparedProducts = new ArrayList<>();
    }
    
    public void addToComparedList(Product prod) {
        if (!comparedProducts.contains(prod)) {
            comparedProducts.add(prod);
        }
    }
    
    public void removeFromComparedList(Product prod) {
        if (comparedProducts.contains(prod)) {
            comparedProducts.remove(prod);
        }
    }
    
    public void clearList() {
        comparedProducts.clear();
    }
    
}
