/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author NATSAMA
 */
@Entity
@Table(name = "AttributeGroup", catalog = "MysticJewelDB", schema = "dbo", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"AttributeGroupName"})})
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AttributeGroup.findAll", query = "SELECT a FROM AttributeGroup a"),
    @NamedQuery(name = "AttributeGroup.findByAttributeGroupID", query = "SELECT a FROM AttributeGroup a WHERE a.attributeGroupID = :attributeGroupID"),
    @NamedQuery(name = "AttributeGroup.findByAttributeGroupName", query = "SELECT a FROM AttributeGroup a WHERE a.attributeGroupName = :attributeGroupName")})
public class AttributeGroup implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "AttributeGroupID", nullable = false)
    private Integer attributeGroupID;
    @Size(max = 50)
    @Column(name = "AttributeGroupName", length = 50)
    private String attributeGroupName;
    @OneToMany(mappedBy = "attributeGroupID")
    private List<Attribute> attributeList;
    @Column(name = "IsSearchable")
    private Boolean isSearchable;

    public AttributeGroup() {
    }

    public AttributeGroup(String attributeGroupName) {
        this.attributeGroupName = attributeGroupName;
    }

    public AttributeGroup(Integer attributeGroupID) {
        this.attributeGroupID = attributeGroupID;
    }

    public Integer getAttributeGroupID() {
        return attributeGroupID;
    }

    public void setAttributeGroupID(Integer attributeGroupID) {
        this.attributeGroupID = attributeGroupID;
    }

    public String getAttributeGroupName() {
        return attributeGroupName;
    }

    public void setAttributeGroupName(String attributeGroupName) {
        this.attributeGroupName = attributeGroupName;
    }

    @XmlTransient
    public List<Attribute> getAttributeList() {
        return attributeList;
    }

    public void setAttributeList(List<Attribute> attributeList) {
        this.attributeList = attributeList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (attributeGroupID != null ? attributeGroupID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AttributeGroup)) {
            return false;
        }
        AttributeGroup other = (AttributeGroup) object;
        if ((this.attributeGroupID == null && other.attributeGroupID != null) || (this.attributeGroupID != null && !this.attributeGroupID.equals(other.attributeGroupID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.AttributeGroup[ attributeGroupID=" + attributeGroupID + " ]";
    }

    public Boolean getIsSearchable() {
        if (this.attributeList == null || this.attributeList.size() == 0) {
            isSearchable = false;
        }
        return isSearchable;
    }

    public void setIsSearchable(Boolean isSearchable) {
        this.isSearchable = isSearchable;
    }

}
