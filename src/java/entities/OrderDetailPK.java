/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author NATSAMA
 */
@Embeddable
public class OrderDetailPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "SkuID", nullable = false, length = 20)
    private String skuID;
    @Basic(optional = false)
    @NotNull
    @Column(name = "OrderID", nullable = false)
    private int orderID;

    public OrderDetailPK() {
    }
    
    public OrderDetailPK(String skuID, int orderID) {
        this.skuID = skuID;
        this.orderID = orderID;
    }

    public String getSkuID() {
        return skuID;
    }

    public void setSkuID(String skuID) {
        this.skuID = skuID;
    }

    public int getOrderID() {
        return orderID;
    }

    public void setOrderID(int orderID) {
        this.orderID = orderID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (skuID != null ? skuID.hashCode() : 0);
        hash += (int) orderID;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof OrderDetailPK)) {
            return false;
        }
        OrderDetailPK other = (OrderDetailPK) object;
        if ((this.skuID == null && other.skuID != null) || (this.skuID != null && !this.skuID.equals(other.skuID))) {
            return false;
        }
        if (this.orderID != other.orderID) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.OrderDetailPK[ skuID=" + skuID + ", orderID=" + orderID + " ]";
    }
    
}
