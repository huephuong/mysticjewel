/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.HashMap;

/**
 *
 * @author nknguyengl
 */
public class Cart implements Serializable {

    public HashMap<Sku, Integer> cartItems;

    public HashMap<Sku, Integer> getCartItems() {
        return cartItems;
    }

    public void setCartItems(HashMap<Sku, Integer> cartItems) {
        this.cartItems = cartItems;
    }

    public Cart() {
        cartItems = new HashMap<>();
    }

    public void addToCart(Sku skuID, int qty) {
        if (cartItems.containsKey(skuID)) {
            qty += cartItems.get(skuID);
            cartItems.put(skuID, qty);
        } else {
            cartItems.put(skuID, qty);
        }
    }

    public void updateCart(Sku skuID, int qty) {
        if (cartItems.containsKey(skuID)) {
            if (qty == 0) {
                cartItems.remove(skuID);
            } else {
                cartItems.put(skuID, qty);
            }
        }
    }

    public void removeToCart(Sku skuID) {
        if (cartItems.containsKey(skuID)) {
            cartItems.remove(skuID);
        }
    }

    public void clearCart() {
        cartItems.clear();
    }

    public double subTotal() {
        int total = 0;
        HashMap<Sku, Integer> items = getCartItems();
        for (HashMap.Entry<Sku, Integer> item : items.entrySet()) {
            if (item.getKey().getProductID().getDiscountValue() > 0) {
                total += ((item.getKey().getUnitPrice() - (item.getKey().getUnitPrice() * item.getKey().getProductID().getDiscountValue() / 100)) * item.getValue());
            } else {
                total += item.getKey().getUnitPrice() * item.getValue();
            } 
        }
        return total;
    }
}
