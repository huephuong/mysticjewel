package entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

@Entity
@Table(name = "OrderMaster", catalog = "MysticJewelDB", schema = "dbo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "OrderMaster.findAll", query = "SELECT o FROM OrderMaster o"),
    @NamedQuery(name = "OrderMaster.findByOrderID", query = "SELECT o FROM OrderMaster o WHERE o.orderID = :orderID"),
    @NamedQuery(name = "OrderMaster.findByShipDate", query = "SELECT o FROM OrderMaster o WHERE o.shipDate = :shipDate"),
    @NamedQuery(name = "OrderMaster.findByShipFee", query = "SELECT o FROM OrderMaster o WHERE o.shipFee = :shipFee"),
    @NamedQuery(name = "OrderMaster.findByShipName", query = "SELECT o FROM OrderMaster o WHERE o.shipName = :shipName"),
    @NamedQuery(name = "OrderMaster.findByDateCreated", query = "SELECT o FROM OrderMaster o WHERE o.dateCreated = :dateCreated"),
    @NamedQuery(name = "OrderMaster.findByDateUpdated", query = "SELECT o FROM OrderMaster o WHERE o.dateUpdated = :dateUpdated"),
    @NamedQuery(name = "OrderMaster.findByOrderStatus", query = "SELECT o FROM OrderMaster o WHERE o.orderStatus = :orderStatus")})
public class OrderMaster implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "OrderID", nullable = false)
    private Integer orderID;
    @Column(name = "ShipDate")
    @Temporal(TemporalType.DATE)
    private Date shipDate;
    @Column(name = "ShipFee")
    private Integer shipFee;
    @Size(max = 100)
    @Column(name = "ShipName", length = 100)
    private String shipName;
    @Lob
    @Size(max = 2147483647)
    @Column(name = "ShipAddress", length = 2147483647)
    private String shipAddress;
    @Column(name = "DateCreated")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreated;
    @Column(name = "DateUpdated")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateUpdated;
    @Size(max = 20)
    @Column(name = "OrderStatus", length = 20)
    private String orderStatus;
    @JoinColumn(name = "CustomerID", referencedColumnName = "CustomerID")
    @ManyToOne
    private Customer customerID;
    @JoinColumn(name = "VoucherID", referencedColumnName = "VoucherID")
    @ManyToOne
    private ProductVoucher voucherID;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "orderMaster")
    private List<OrderDetail> orderDetailList;
    @Size(max = 10)
    @Column(name = "Payment", length = 10)
    private String payment;
    @Size(max = 20)
    @Column(name = "ShipPhone", length = 20)
    private String shipPhone;
    @Size(max = 50)
    @Column(name = "ShipEmail", length = 50)
    private String shipEmail;
    
    @Transient
    private int subTotal;
    
    @Transient
    private int grandTotal;
    
    @Transient
    private int orderRevenue;

    public int getSubTotal() {
        this.subTotal = 0;
        for (OrderDetail item : this.orderDetailList) {
            if (item.getDiscountValue() != null) {
                this.subTotal += (item.getUnitPrice() - (item.getUnitPrice() * item.getDiscountValue() / 100)) * item.getQuantity();
            } else {
                this.subTotal += item.getUnitPrice() * item.getQuantity();
            }
        }
        return subTotal;
    }

    public void setSubTotal(int subTotal) {
        this.subTotal = subTotal;
    }

    public int getGrandTotal() {
        int sub = this.getSubTotal();
        this.grandTotal = (int) (sub + (sub * 0.1)) + this.shipFee;
        return grandTotal;
    }

    public void setGrandTotal(int grandTotal) {
        this.grandTotal = grandTotal;
    }

    public int getOrderRevenue() {
        this.orderRevenue = 0;
        for (OrderDetail item : this.orderDetailList) {
            if (item.getDiscountValue() != null) {
                this.orderRevenue += item.getUnitPrice() - (item.getUnitPrice() * item.getDiscountValue() / 100) - item.getSku().getBasePrice();
            } else {
                this.orderRevenue += item.getUnitPrice() - item.getSku().getBasePrice();
            }
        }
        return orderRevenue;
    }

    public void setOrderRevenue(int orderRevenue) {
        this.orderRevenue = orderRevenue;
    }

    public OrderMaster() {
    }

    public OrderMaster(Integer orderID) {
        this.orderID = orderID;
    }

    public Integer getOrderID() {
        return orderID;
    }

    public void setOrderID(Integer orderID) {
        this.orderID = orderID;
    }

    public Date getShipDate() {
        return shipDate;
    }

    public void setShipDate(Date shipDate) {
        this.shipDate = shipDate;
    }

    public Integer getShipFee() {
        return shipFee;
    }

    public void setShipFee(Integer shipFee) {
        this.shipFee = shipFee;
    }

    public String getShipName() {
        return shipName;
    }

    public void setShipName(String shipName) {
        this.shipName = shipName;
    }

    public String getShipAddress() {
        return shipAddress;
    }

    public void setShipAddress(String shipAddress) {
        this.shipAddress = shipAddress;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Date getDateUpdated() {
        return dateUpdated;
    }

    public void setDateUpdated(Date dateUpdated) {
        this.dateUpdated = dateUpdated;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public Customer getCustomerID() {
        return customerID;
    }

    public void setCustomerID(Customer customerID) {
        this.customerID = customerID;
    }

    public ProductVoucher getVoucherID() {
        return voucherID;
    }

    public void setVoucherID(ProductVoucher voucherID) {
        this.voucherID = voucherID;
    }

    @XmlTransient
    public List<OrderDetail> getOrderDetailList() {
        return orderDetailList;
    }

    public void setOrderDetailList(List<OrderDetail> orderDetailList) {
        this.orderDetailList = orderDetailList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (orderID != null ? orderID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof OrderMaster)) {
            return false;
        }
        OrderMaster other = (OrderMaster) object;
        if ((this.orderID == null && other.orderID != null) || (this.orderID != null && !this.orderID.equals(other.orderID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.OrderMaster[ orderID=" + orderID + " ]";
    }

    public String getPayment() {
        return payment;
    }

    public void setPayment(String payment) {
        this.payment = payment;
    }

    public String getShipPhone() {
        return shipPhone;
    }

    public void setShipPhone(String shipPhone) {
        this.shipPhone = shipPhone;
    }

    public String getShipEmail() {
        return shipEmail;
    }

    public void setShipEmail(String shipEmail) {
        this.shipEmail = shipEmail;
    }

}
