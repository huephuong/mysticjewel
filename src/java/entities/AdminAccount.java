/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author HuePhuong
 */
@Entity
@Table(name = "AdminAccount", catalog = "MysticJewelDB", schema = "dbo", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"AdminUserName"})})
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AdminAccount.findAll", query = "SELECT a FROM AdminAccount a"),
    @NamedQuery(name = "AdminAccount.findByAdminID", query = "SELECT a FROM AdminAccount a WHERE a.adminID = :adminID"),
    @NamedQuery(name = "AdminAccount.findByAdminUserName", query = "SELECT a FROM AdminAccount a WHERE a.adminUserName = :adminUserName"),
    @NamedQuery(name = "AdminAccount.findByAdminPassword", query = "SELECT a FROM AdminAccount a WHERE a.adminPassword = :adminPassword"),
    @NamedQuery(name = "AdminAccount.findByAdminRole", query = "SELECT a FROM AdminAccount a WHERE a.adminRole = :adminRole"),
    @NamedQuery(name = "AdminAccount.findByDateCreated", query = "SELECT a FROM AdminAccount a WHERE a.dateCreated = :dateCreated"),
    @NamedQuery(name = "AdminAccount.findByDateUpdated", query = "SELECT a FROM AdminAccount a WHERE a.dateUpdated = :dateUpdated")})
public class AdminAccount implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "AdminID", nullable = false)
    private Integer adminID;
    @Size(max = 20)
    @Column(name = "AdminUserName", length = 20)
    private String adminUserName;
    @Size(max = 50)
    @Column(name = "AdminPassword", length = 50)
    private String adminPassword;
    @Size(max = 20)
    @Column(name = "AdminRole", length = 20)
    private String adminRole;
    @Column(name = "DateCreated")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreated;
    @Column(name = "DateUpdated")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateUpdated;

    public AdminAccount() {
    }

    public AdminAccount(Integer adminID) {
        this.adminID = adminID;
    }

    public Integer getAdminID() {
        return adminID;
    }

    public void setAdminID(Integer adminID) {
        this.adminID = adminID;
    }

    public String getAdminUserName() {
        return adminUserName;
    }

    public void setAdminUserName(String adminUserName) {
        this.adminUserName = adminUserName;
    }

    public String getAdminPassword() {
        return adminPassword;
    }

    public void setAdminPassword(String adminPassword) {
        this.adminPassword = adminPassword;
    }

    public String getAdminRole() {
        return adminRole;
    }

    public void setAdminRole(String adminRole) {
        this.adminRole = adminRole;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Date getDateUpdated() {
        return dateUpdated;
    }

    public void setDateUpdated(Date dateUpdated) {
        this.dateUpdated = dateUpdated;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (adminID != null ? adminID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AdminAccount)) {
            return false;
        }
        AdminAccount other = (AdminAccount) object;
        if ((this.adminID == null && other.adminID != null) || (this.adminID != null && !this.adminID.equals(other.adminID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.AdminAccount[ adminID=" + adminID + " ]";
    }
    
}
