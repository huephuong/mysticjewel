package entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author NATSAMA
 */
@Entity
@Table(name = "ProductVoucher", catalog = "MysticJewelDB", schema = "dbo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ProductVoucher.findAll", query = "SELECT p FROM ProductVoucher p"),
    @NamedQuery(name = "ProductVoucher.findByVoucherID", query = "SELECT p FROM ProductVoucher p WHERE p.voucherID = :voucherID"),
    @NamedQuery(name = "ProductVoucher.findByDiscountValue", query = "SELECT p FROM ProductVoucher p WHERE p.discountValue = :discountValue"),
    @NamedQuery(name = "ProductVoucher.findByQuantity", query = "SELECT p FROM ProductVoucher p WHERE p.quantity = :quantity"),
    @NamedQuery(name = "ProductVoucher.findByValidFrom", query = "SELECT p FROM ProductVoucher p WHERE p.validFrom = :validFrom"),
    @NamedQuery(name = "ProductVoucher.findByValidTo", query = "SELECT p FROM ProductVoucher p WHERE p.validTo = :validTo")})
public class ProductVoucher implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "VoucherID", nullable = false, length = 20)
    private String voucherID;
    @Lob
    @Size(max = 2147483647)
    @Column(name = "VoucherDesc", length = 2147483647)
    private String voucherDesc;
    @Column(name = "DiscountValue")
    private Integer discountValue;
    @Column(name = "Quantity")
    private Integer quantity;
    @Column(name = "ValidFrom")
    @Temporal(TemporalType.TIMESTAMP)
    private Date validFrom;
    @Column(name = "ValidTo")
    @Temporal(TemporalType.TIMESTAMP)
    private Date validTo;
    @OneToMany(mappedBy = "voucherID")
    private List<OrderMaster> orderMasterList;

    public ProductVoucher() {
    }

    public ProductVoucher(String voucherID) {
        this.voucherID = voucherID;
    }

    public String getVoucherID() {
        return voucherID;
    }

    public void setVoucherID(String voucherID) {
        this.voucherID = voucherID;
    }

    public String getVoucherDesc() {
        return voucherDesc;
    }

    public void setVoucherDesc(String voucherDesc) {
        this.voucherDesc = voucherDesc;
    }

    public Integer getDiscountValue() {
        return discountValue;
    }

    public void setDiscountValue(Integer discountValue) {
        this.discountValue = discountValue;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Date getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(Date validFrom) {
        this.validFrom = validFrom;
    }

    public Date getValidTo() {
        return validTo;
    }

    public void setValidTo(Date validTo) {
        this.validTo = validTo;
    }

    @XmlTransient
    public List<OrderMaster> getOrderMasterList() {
        return orderMasterList;
    }

    public void setOrderMasterList(List<OrderMaster> orderMasterList) {
        this.orderMasterList = orderMasterList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (voucherID != null ? voucherID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProductVoucher)) {
            return false;
        }
        ProductVoucher other = (ProductVoucher) object;
        if ((this.voucherID == null && other.voucherID != null) || (this.voucherID != null && !this.voucherID.equals(other.voucherID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.ProductVoucher[ voucherID=" + voucherID + " ]";
    }
    
}
