/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author NATSAMA
 */
@Entity
@Table(name = "Product", catalog = "MysticJewelDB", schema = "dbo", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"ProductName"})})
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Product.findAll", query = "SELECT p FROM Product p"),
    @NamedQuery(name = "Product.findByProductID", query = "SELECT p FROM Product p WHERE p.productID = :productID"),
    @NamedQuery(name = "Product.findByProductName", query = "SELECT p FROM Product p WHERE p.productName = :productName"),
    @NamedQuery(name = "Product.findByIsActive", query = "SELECT p FROM Product p WHERE p.isActive = :isActive"),
    @NamedQuery(name = "Product.findByDiscountValue", query = "SELECT p FROM Product p WHERE p.discountValue = :discountValue"),
    @NamedQuery(name = "Product.findByDiscountValidFrom", query = "SELECT p FROM Product p WHERE p.discountValidFrom = :discountValidFrom"),
    @NamedQuery(name = "Product.findByDiscountValidTo", query = "SELECT p FROM Product p WHERE p.discountValidTo = :discountValidTo"),
    @NamedQuery(name = "Product.findByDateCreated", query = "SELECT p FROM Product p WHERE p.dateCreated = :dateCreated"),
    @NamedQuery(name = "Product.findByDateUpdated", query = "SELECT p FROM Product p WHERE p.dateUpdated = :dateUpdated")})
public class Product implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ProductID", nullable = false)
    private Integer productID;
    @Size(max = 100)
    @Column(name = "ProductName", length = 100)
    private String productName;
    @Lob
    @Size(max = 2147483647)
    @Column(name = "ProductDesc", length = 2147483647)
    private String productDesc;
    @Column(name = "IsActive")
    private Boolean isActive;
    @Column(name = "DiscountValue")
    private Integer discountValue;
    @Column(name = "DiscountValidFrom")
    @Temporal(TemporalType.TIMESTAMP)
    private Date discountValidFrom;
    @Column(name = "DiscountValidTo")
    @Temporal(TemporalType.TIMESTAMP)
    private Date discountValidTo;
    @Column(name = "DateCreated")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreated;
    @Column(name = "DateUpdated")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateUpdated;
    @ManyToMany(mappedBy = "productList")
    private List<Attribute> attributeList;
    @OneToMany(mappedBy = "productID")
    private List<ProductImage> productImageList;
    @JoinColumn(name = "CategoryID", referencedColumnName = "CategoryID")
    @ManyToOne
    private Category categoryID;
    @JoinColumn(name = "ColID", referencedColumnName = "ColID")
    @ManyToOne
    private ProductCol colID;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "product")
    private List<Review> reviewList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "product")
    private List<Bookmark> bookmarkList;
    @OneToMany(mappedBy = "productID")
    private List<Sku> skuList;

    // tìm rating
    @Transient
    private int rating;

    // is new 
    @Transient
    private boolean isNew;

    // sale counts
    @Transient
    private int soldCount;
    
    // revenue
    @Transient
    private double revenue;
    
    // favoured score for recommendation
    @Transient
    private int favScore;

    public Product() {
    }

    public Product(String productName, String productDesc, Boolean isActive, Integer discountValue, Date discountValidFrom, Date discountValidTo, Date dateCreated, Date dateUpdated, Category categoryID, ProductCol colID) {
        this.productName = productName;
        this.productDesc = productDesc;
        this.isActive = isActive;
        this.discountValue = discountValue;
        this.discountValidFrom = discountValidFrom;
        this.discountValidTo = discountValidTo;
        this.dateCreated = dateCreated;
        this.dateUpdated = dateUpdated;
        this.categoryID = categoryID;
        this.colID = colID;
    }

    public Product(Integer productID) {
        this.productID = productID;
    }

    public Integer getProductID() {
        return productID;
    }

    public void setProductID(Integer productID) {
        this.productID = productID;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductDesc() {
        return productDesc;
    }

    public void setProductDesc(String productDesc) {
        this.productDesc = productDesc;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public Integer getDiscountValue() {
        if (getDiscountValidTo() != null) {
            if (getDiscountValidTo().before(new Date())) {
                return 0;
            }
        }
        return discountValue;
    }

    public void setDiscountValue(Integer discountValue) {
        this.discountValue = discountValue;
    }

    public Date getDiscountValidFrom() {
        return discountValidFrom;
    }

    public void setDiscountValidFrom(Date discountValidFrom) {
        this.discountValidFrom = discountValidFrom;
    }

    public Date getDiscountValidTo() {
        return discountValidTo;
    }

    public void setDiscountValidTo(Date discountValidTo) {
        this.discountValidTo = discountValidTo;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Date getDateUpdated() {
        return dateUpdated;
    }

    public void setDateUpdated(Date dateUpdated) {
        this.dateUpdated = dateUpdated;
    }

    @XmlTransient
    public List<Attribute> getAttributeList() {
        return attributeList;
    }

    public void setAttributeList(List<Attribute> attributeList) {
        this.attributeList = attributeList;
    }

    @XmlTransient
    public List<ProductImage> getProductImageList() {
        return productImageList;
    }

    public void setProductImageList(List<ProductImage> productImageList) {
        this.productImageList = productImageList;
    }

    public Category getCategoryID() {
        return categoryID;
    }

    public void setCategoryID(Category categoryID) {
        this.categoryID = categoryID;
    }

    public ProductCol getColID() {
        return colID;
    }

    public void setColID(ProductCol colID) {
        this.colID = colID;
    }
    
    @XmlTransient
    public int getRating() {
        int sum = 0;
        this.rating = 0;
        if (reviewList.size() > 0) {
            for (Review r : reviewList) {
                sum += r.getRating();
            }
            this.rating = (int) sum / reviewList.size();
        }
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    @XmlTransient
    public boolean getIsNew() {
        Date today = new Date();
        long diff = today.getTime() - dateCreated.getTime();
        long diffDays = diff / (24 * 60 * 60 * 1000);
        if (diffDays <= 30) {
            isNew = true;
        } else {
            isNew = false;
        }
        return isNew;
    }

    public void setIsNew(boolean isNew) {
        this.isNew = isNew;
    }

    @XmlTransient
    public int getSoldCount() {
        this.soldCount = 0;
        for (Sku sku : skuList) {
            if (sku.getOrderDetailList() != null) {
                for (OrderDetail det : sku.getOrderDetailList()) {
                    this.soldCount += det.getQuantity();
                }
            }
        }
        return soldCount;
    }

    public void setSoldCount(int soldCount) {
        this.soldCount = soldCount;
    }
    
    @XmlTransient
    public double getRevenue() {
        return revenue;
    }

    public void setRevenue(double revenue) {
        this.revenue = revenue;
    }
    
    @XmlTransient
    public int getFavScore() {
        return favScore;
    }

    public void setFavScore(int favScore) {
        this.favScore = favScore;
    }
    
    @XmlTransient
    public List<Review> getReviewList() {
        return reviewList;
    }

    public void setReviewList(List<Review> reviewList) {
        this.reviewList = reviewList;
    }

    @XmlTransient
    public List<Bookmark> getBookmarkList() {
        return bookmarkList;
    }

    public void setBookmarkList(List<Bookmark> bookmarkList) {
        this.bookmarkList = bookmarkList;
    }

    @XmlTransient
    public List<Sku> getSkuList() {
        return skuList;
    }

    public void setSkuList(List<Sku> skuList) {
        this.skuList = skuList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (productID != null ? productID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Product)) {
            return false;
        }
        Product other = (Product) object;
        if ((this.productID == null && other.productID != null) || (this.productID != null && !this.productID.equals(other.productID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Product[ productID=" + productID + " ]";
    }

}
