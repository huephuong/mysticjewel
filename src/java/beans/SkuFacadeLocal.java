/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package beans;

import entities.Product;
import entities.Sku;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author nknguyengl
 */
@Local
public interface SkuFacadeLocal {

    void create(Sku sku);

    void edit(Sku sku);

    void remove(Sku sku);

    Sku find(Object id);

    List<Sku> findAll();

    List<Sku> findRange(int[] range);

    int count();

    Sku createSku(Sku sku);

}
