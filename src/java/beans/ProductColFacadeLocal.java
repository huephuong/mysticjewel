package beans;

import entities.Product;
import entities.ProductCol;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author nknguyengl
 */
@Local
public interface ProductColFacadeLocal {

    void create(ProductCol productCol);

    void edit(ProductCol productCol);

    void remove(ProductCol productCol);

    ProductCol find(Object id);

    List<ProductCol> findAll();

    List<ProductCol> findRange(int[] range);

    int count();


    int removeProductCol(ProductCol removeProductCol);

    ProductCol getSingleCol();

    ProductCol createProductCol(ProductCol col);

    ProductCol editProductCol(ProductCol col);

    void addProductToCol(Product prod, ProductCol col);

    void removeProductFromCol(int id, int prodid);
    
}
