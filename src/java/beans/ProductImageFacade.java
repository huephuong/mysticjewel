/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package beans;

import entities.Product;
import entities.ProductImage;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author nknguyengl
 */
@Stateless
public class ProductImageFacade extends AbstractFacade<ProductImage> implements ProductImageFacadeLocal {
    @PersistenceContext(unitName = "MysticJewelPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ProductImageFacade() {
        super(ProductImage.class);
    }

    @Override
    public ProductImage createImage(ProductImage img) {
        create(img);
        Product p = img.getProductID();
        p.getProductImageList().add(img);
        em.merge(p);
        return img;
    }
    
}
