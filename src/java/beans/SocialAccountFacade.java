/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import entities.Customer;
import entities.SocialAccount;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author nknguyengl
 */
@Stateless
public class SocialAccountFacade extends AbstractFacade<SocialAccount> implements SocialAccountFacadeLocal {

    @PersistenceContext(unitName = "MysticJewelPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public SocialAccountFacade() {
        super(SocialAccount.class);
    }

    @Override
    public SocialAccount createSocialAccount(SocialAccount socialAccount) {
        Customer c = socialAccount.getCustomerID();
        c.getSocialAccountList().add(socialAccount);
        em.merge(c);
        return socialAccount;
    }

    @Override
    public SocialAccount findByProviderCustomerID(String providerCustomerID) {
        Query q = em.createQuery("SELECT s FROM SocialAccount s WHERE s.providerCustomerID = :providerCustomerID");
        q.setParameter("providerCustomerID", providerCustomerID);
        try {
            return (SocialAccount) q.getSingleResult();
        } catch (Exception e) {
            return null;
        }
    }

}
