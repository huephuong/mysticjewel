/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package beans;

import entities.Attribute;
import entities.AttributeGroup;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author nknguyengl
 */
@Stateless
public class AttributeFacade extends AbstractFacade<Attribute> implements AttributeFacadeLocal {
    @EJB
    private AttributeGroupFacadeLocal attributeGroupFacade;
    @PersistenceContext(unitName = "MysticJewelPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public AttributeFacade() {
        super(Attribute.class);
    }

    @Override
    public Attribute createAttribute(String name, AttributeGroup parent) {
        Attribute newAtt = new Attribute(name, parent);
        create(newAtt);
        AttributeGroup group = newAtt.getAttributeGroupID();
        group.getAttributeList().add(newAtt);
        em.merge(group);
        return newAtt;
    }

    @Override
    public Attribute editAttribute(Attribute att) {
        edit(att);
        return att;
    }

    @Override
    public int deleteAttribute(Attribute att) {
        int id = att.getAttributeID();
        AttributeGroup group = att.getAttributeGroupID();
        group.getAttributeList().remove(att);
        em.merge(group);
        remove(att);
        return id;
    }
    
}
