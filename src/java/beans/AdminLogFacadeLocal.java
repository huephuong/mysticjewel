/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package beans;

import entities.AdminLog;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author NATSAMA
 */
@Local
public interface AdminLogFacadeLocal {

    void create(AdminLog adminLog);

    void edit(AdminLog adminLog);

    void remove(AdminLog adminLog);

    AdminLog find(Object id);

    List<AdminLog> findAll();

    List<AdminLog> findRange(int[] range);

    int count();

    void createLog(String username, String logContent);

    List<AdminLog> getNewestLog();
    
}
