/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package beans;

import entities.Cart;
import entities.Customer;
import entities.OrderDetail;
import entities.OrderDetailPK;
import entities.OrderMaster;
import java.util.List;
import entities.Sku;
import java.util.HashMap;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author nknguyengl
 */
@Stateless
public class OrderMasterFacade extends AbstractFacade<OrderMaster> implements OrderMasterFacadeLocal {
    @EJB
    private OrderDetailFacadeLocal orderDetailFacade;
    @PersistenceContext(unitName = "MysticJewelPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public OrderMasterFacade() {
        super(OrderMaster.class);
    }

    @Override
    public List<OrderMaster> findByCustomerID(Customer customerID) {
        Query q = em.createQuery("SELECT o FROM OrderMaster o WHERE o.customerID = :customerID ORDER BY o.orderID DESC");
        q.setParameter("customerID", customerID);
        return q.getResultList();
    }

    public OrderMaster createOrder(OrderMaster order, Cart cart) {
        create(order);
        if (order.getCustomerID() != null) {
            Customer cus = order.getCustomerID();
            cus.getOrderMasterList().add(order);
            em.merge(cus);
        }
        HashMap<Sku, Integer> items = cart.getCartItems();
        for (HashMap.Entry<Sku, Integer> item : items.entrySet()) {
            OrderDetail detail = new OrderDetail();
            OrderDetailPK pk = new OrderDetailPK(item.getKey().getSkuID(), order.getOrderID());
            detail.setOrderDetailPK(pk);
            detail.setOrderMaster(order);
            detail.setSku(item.getKey());
            item.getKey().setUnitsInStock(item.getKey().getUnitsInStock() - item.getValue());
            item.getKey().setUnitsOnOrder(item.getKey().getUnitsOnOrder() + item.getValue());
            em.merge(item.getKey());
            detail.setQuantity(item.getValue());
            detail.setUnitPrice(item.getKey().getUnitPrice());
            if (order.getVoucherID() != null) {
                detail.setDiscountValue(order.getVoucherID().getDiscountValue());
            } else {
                detail.setDiscountValue(item.getKey().getProductID().getDiscountValue());
            }
            orderDetailFacade.createOrderDetail(detail);
            order.getOrderDetailList().add(detail);
            em.merge(order);
        }
        return order;
    }

    @Override
    public OrderMaster updateOrderStatus(OrderMaster order, String status) {
        if ("Canceled".equals(status)) {
            for (OrderDetail detail : order.getOrderDetailList()) {
                detail.getSku().setUnitsInStock(detail.getSku().getUnitsInStock() + detail.getQuantity());
                detail.getSku().setUnitsOnOrder(detail.getSku().getUnitsOnOrder() - detail.getQuantity());
                em.merge(detail.getSku());
            }
        }
        if ("Completed".equals(status) || "Delivering".equals(status)) {
            for (OrderDetail detail : order.getOrderDetailList()) {
                detail.getSku().setUnitsOnOrder(detail.getSku().getUnitsOnOrder() - detail.getQuantity());
                em.merge(detail.getSku());
            }
        }
        order.setOrderStatus(status);
        edit(order);
        return order;
    }

}
