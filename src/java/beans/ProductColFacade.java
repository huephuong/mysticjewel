package beans;

import entities.Product;
import entities.ProductCol;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author nknguyengl
 */
@Stateless
public class ProductColFacade extends AbstractFacade<ProductCol> implements ProductColFacadeLocal {
    @EJB
    private ProductFacadeLocal productFacade;
    @PersistenceContext(unitName = "MysticJewelPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ProductColFacade() {
        super(ProductCol.class);
    }

    @Override
    public int removeProductCol(ProductCol removeProductCol) {
        remove(removeProductCol);
        List<Product> productList = removeProductCol.getProductList();
        for (Product p : productList) {
            p.setColID(null);
            em.merge(p);
        }
        return removeProductCol.getColID();
    }

    public ProductCol getSingleCol() {
        Query q = em.createQuery("SELECT pc FROM ProductCol pc WHERE pc.isTrending = TRUE AND pc.isActive = TRUE ORDER BY pc.colID DESC");
        q.setMaxResults(1);
        ProductCol col = (ProductCol) q.getSingleResult();
        return col;
    }

    @Override
    public ProductCol createProductCol(ProductCol col) {
        create(col);
        return col;
    }

    @Override
    public ProductCol editProductCol(ProductCol col) {
        edit(col);
        return col;
    }

    @Override
    public void addProductToCol(Product prod, ProductCol col) {
        prod.setColID(col);
        col.getProductList().add(prod);
        em.merge(prod);
        em.merge(col);
    }

    @Override
    public void removeProductFromCol(int id, int prodid) {
        Product prod = productFacade.find(prodid);
        ProductCol col = find(id);
        col.getProductList().remove(prod);
        prod.setColID(null);
        em.merge(col);
        em.merge(prod);
    }
    
}
