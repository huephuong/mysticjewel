package beans;

import entities.Category;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Stateless
public class CategoryFacade extends AbstractFacade<Category> implements CategoryFacadeLocal {
    @PersistenceContext(unitName = "MysticJewelPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public CategoryFacade() {
        super(Category.class);
    }

    @Override
    public List<Category> getMainCategory() {
        Query q = em.createQuery("SELECT c FROM Category c WHERE c.parentID IS NULL");
        List<Category> list = q.getResultList();
        return list;
    }

    @Override
    public List<Category> getCategoryByParentID(int parentID) {
        Category parent = find(parentID);
        Query q = em.createQuery("SELECT c FROM Category c WHERE c.parentID = :parentID");
        q.setParameter("parentID", parent);
        List<Category> list = q.getResultList();
        return list;
    }

    @Override
    public List<Category> getSubCategory() {
        List<Category> subCat;
        Query q = em.createQuery("SELECT c FROM Category c WHERE c.parentID != NULL");
        subCat = q.getResultList();
        return subCat;
    }

    @Override
    public Category createCategory(Category newCat) {
        create(newCat);
        Category parent = newCat.getParentID();
        parent.getCategoryList().add(newCat);
        em.merge(parent);
        return newCat;
    }

    @Override
    public Category editCategory(Category cat) {
        edit(cat);
        return cat;
    }

    @Override
    public int deleteSubCategory(Category cat) {
        int id = cat.getCategoryID();
        Category parent = cat.getParentID();
        parent.getCategoryList().remove(cat);
        em.merge(parent);
        remove(cat);
        return id;
    }
    
}
