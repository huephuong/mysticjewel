/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package beans;

import entities.Attribute;
import entities.AttributeGroup;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author nknguyengl
 */
@Local
public interface AttributeGroupFacadeLocal {

    void create(AttributeGroup attributeGroup);

    void edit(AttributeGroup attributeGroup);

    void remove(AttributeGroup attributeGroup);

    AttributeGroup find(Object id);

    List<AttributeGroup> findAll();

    List<AttributeGroup> findRange(int[] range);

    int count();

    AttributeGroup createAttributeGroup(String name);

    List<AttributeGroup> getEmptyGroup();

    List<AttributeGroup> getSearchableGroup();

    void updateGroupStatus(AttributeGroup group, boolean searchable);
    
}
