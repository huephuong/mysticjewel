/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package beans;

import entities.Customer;
import entities.Product;
import entities.Review;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author nknguyengl
 */
@Stateless
public class ReviewFacade extends AbstractFacade<Review> implements ReviewFacadeLocal {
    @EJB
    private ProductFacadeLocal productFacade;
    @EJB
    private CustomerFacadeLocal customerFacade;
    @PersistenceContext(unitName = "MysticJewelPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ReviewFacade() {
        super(Review.class);
    }

    @Override
    public int createReview(Review newReview) {
        // Customer/Product: One to Many
        create(newReview);
        int cusID = newReview.getReviewPK().getCustomerID();
        Customer cus = customerFacade.find(cusID);
        cus.getReviewList().add(newReview);
        em.merge(cus);

        int proID = newReview.getReviewPK().getProductID();
        Product pro = productFacade.find(proID);
        pro.getReviewList().add(newReview);
        em.merge(pro);
        
        // return id of reveiewed product
        return newReview.getReviewPK().getProductID();
    }

    @Override
    public int editReview(Review editReview) {
        edit(editReview);
        return editReview.getReviewPK().getProductID();
    }
    
    @Override
    public int removeReview(Review rmReview) {
        // Customer/Product: One to Many
        remove(rmReview);
        int cusID = rmReview.getReviewPK().getCustomerID();
        Customer cus = customerFacade.find(cusID);
        cus.getReviewList().remove(rmReview);
        em.merge(cus);

        int proID = rmReview.getReviewPK().getProductID();
        Product pro = productFacade.find(proID);
        pro.getReviewList().remove(rmReview);
        em.merge(pro);
        
        // return id of product removed review
        return rmReview.getReviewPK().getProductID();
    }

    @Override
    public List<Review> findByCustomerID(int cusID) {
        String query = "SELECT r FROM Review r WHERE r.reviewPK.customerID = :customerID";
        Query q = em.createQuery(query);
        q.setParameter("customerID", cusID);
        return q.getResultList();
    }
    
    @Override
    public List<Review> findByProductID(int proID) {
        String query = "SELECT r FROM Review r WHERE r.reviewPK.productID = :productID";
        Query q = em.createQuery(query);
        q.setParameter("productID", proID);
        return q.getResultList();
    }
    
    @Override
    public Review findByReviewPK(int cusID, int proID) {
        String query = "SELECT r FROM Review r WHERE r.reviewPK.customerID = :customerID AND r.reviewPK.productID = :productID";
        Query q = em.createQuery(query);
        q.setParameter("customerID", cusID);
        q.setParameter("productID", proID);
        
        try {
            return (Review) q.getSingleResult();
        } catch (Exception e) {
            return null;
        }
    }

    
}
