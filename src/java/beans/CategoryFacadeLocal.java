/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package beans;

import entities.Category;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author nknguyengl
 */
@Local
public interface CategoryFacadeLocal {

    void create(Category category);

    void edit(Category category);

    void remove(Category category);

    Category find(Object id);

    List<Category> findAll();

    List<Category> findRange(int[] range);

    int count();

    List<Category> getMainCategory();

    List<Category> getCategoryByParentID(int parentID);

    List<Category> getSubCategory();

    Category createCategory(Category newCat);

    Category editCategory(Category cat);

    int deleteSubCategory(Category cat);
    
}
