/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package beans;

import entities.Category;
import entities.SkuSize;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author NATSAMA
 */
@Stateless
public class SkuSizeFacade extends AbstractFacade<SkuSize> implements SkuSizeFacadeLocal {
    @PersistenceContext(unitName = "MysticJewelPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public SkuSizeFacade() {
        super(SkuSize.class);
    }

    @Override
    public SkuSize createSkuSize(SkuSize newSize) {
        create(newSize);
        Category parent = newSize.getCategoryID();
        parent.getSkuSizeList().add(newSize);
        em.merge(parent);
        return newSize;
    }

    @Override
    public SkuSize editSkuSize(SkuSize skuSize) {
        edit(skuSize);
        return skuSize;
    }  
}
