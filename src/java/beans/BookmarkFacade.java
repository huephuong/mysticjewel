package beans;

import entities.Bookmark;
import entities.Customer;
import entities.Product;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author nknguyengl
 */
@Stateless
public class BookmarkFacade extends AbstractFacade<Bookmark> implements BookmarkFacadeLocal {

    @EJB
    private ProductFacadeLocal productFacade;
    @EJB
    private CustomerFacadeLocal customerFacade;

    @PersistenceContext(unitName = "MysticJewelPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public BookmarkFacade() {
        super(Bookmark.class);
    }

    @Override
    public int createBookmark(Bookmark newBookmark) {
        // Customer/Product: One to Many
        create(newBookmark);
//        int cusID = newBookmark.getBookmarkPK().getCustomerID();
//        Customer cus = customerFacade.find(cusID);
        Customer cus = newBookmark.getCustomer();
        cus.getBookmarkList().add(newBookmark);
        em.merge(cus);

        int proID = newBookmark.getBookmarkPK().getProductID();
        Product pro = productFacade.find(proID);
        pro.getBookmarkList().add(newBookmark);
        em.merge(pro);

        //em.persist(newBookmark);
        //em.merge(newBookmark);
        return newBookmark.getBookmarkPK().getProductID();
    }

    @Override
    public int removeBookmark(Bookmark rmBookmark) {
        remove(rmBookmark);
        Customer cus = rmBookmark.getCustomer();
        Product pro = rmBookmark.getProduct();
        cus.getBookmarkList().remove(rmBookmark);
        pro.getBookmarkList().remove(rmBookmark);
        em.merge(cus);
        em.merge(pro);
        return rmBookmark.getProduct().getProductID();
    }

    @Override
    public List<Bookmark> findByCustomerID(int cusID) {
        String query = "SELECT b FROM Bookmark b WHERE b.bookmarkPK.customerID = :customerID";
        Query q = em.createQuery(query);
        q.setParameter("customerID", cusID);
        return q.getResultList();
    }

    @Override
    public List<Bookmark> findByProductID(int proID) {
        String query = "SELECT b FROM Bookmark b WHERE b.bookmarkPK.productID = :productID";
        Query q = em.createQuery(query);
        q.setParameter("productID", proID);
        return q.getResultList();
    }

    @Override
    public Bookmark findByBookmarkPK(int cusID, int proID) {
        String query = "SELECT b FROM Bookmark b WHERE b.bookmarkPK.customerID = :customerID AND b.bookmarkPK.productID = :productID";
        Query q = em.createQuery(query);
        q.setParameter("customerID", cusID);
        q.setParameter("productID", proID);
        try {
            return (Bookmark) q.getSingleResult();
        } catch (Exception e) {
            return null;
        }
    }

}
