/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import entities.Product;
import entities.Sku;
import entities.SkuSize;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author nknguyengl
 */
@Stateless
public class SkuFacade extends AbstractFacade<Sku> implements SkuFacadeLocal {

    @PersistenceContext(unitName = "MysticJewelPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public SkuFacade() {
        super(Sku.class);
    }

    @Override
    public Sku createSku(Sku sku) {
        create(sku);
        Product p = sku.getProductID();
        p.getSkuList().add(sku);
        em.merge(p);
        SkuSize s = sku.getSizeID();
        if (s != null) {
            s.getSkuList().add(sku);
            em.merge(s);
        }
        return sku;
    }

    
}
