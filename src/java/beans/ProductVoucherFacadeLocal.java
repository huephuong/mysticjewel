/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package beans;

import entities.ProductVoucher;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author nknguyengl
 */
@Local
public interface ProductVoucherFacadeLocal {

    void create(ProductVoucher productVoucher);

    void edit(ProductVoucher productVoucher);

    void remove(ProductVoucher productVoucher);

    ProductVoucher find(Object id);

    List<ProductVoucher> findAll();

    List<ProductVoucher> findRange(int[] range);

    int count();

    String createProductVoucher(ProductVoucher newProductVoucher);

    String editProductVoucher(ProductVoucher editProductVoucher);

    boolean removeProductVoucher(ProductVoucher removeProductVoucher);
    
}
