/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import entities.Attribute;
import entities.Customer;
import entities.Product;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author nknguyengl
 */
@Local
public interface ProductFacadeLocal {

    void create(Product product);

    void edit(Product product);

    void remove(Product product);

    Product find(Object id);

    List<Product> findAll();

    List<Product> findRange(int[] range);

    int count();

    List<Product> findByCatCol(int categoryID, int colID);

    Product createProduct(Product newProd, List<Attribute> attributeList);

    List<Product> getHotProduct();

    List<Product> getNewProduct();

    List<Product> getOnSaleProduct();

    Product editProduct(Product prod, List<Attribute> attList);

    List<Product> findByCustomerHistory(String value);

    List<Product> findByName(String searchTitle);

    List<Product> findByFilter(int categoryID, int colID, int attrID, int priceFrom, int priceTo, String dir, String order);

    List<Product> getRelatedProduct(Product prod);

    Product getReccommendedProduct(Customer cus);

    List<Product> getNewProductPagination(int firstResult, int maxResult);

    List<Product> getOnSaleProductPagination(int firstResult, int maxResult);

    List<Product> findByFilterNew(int attrID, int priceFrom, int priceTo, int firstResult, int maxResult, String dir, String order);

    List<Product> findByFilterSale(int attrID, int priceFrom, int priceTo, int firstResult, int maxResult, String dir, String order);

}
