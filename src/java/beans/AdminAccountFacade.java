/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package beans;

import entities.AdminAccount;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author nknguyengl
 */
@Stateless
public class AdminAccountFacade extends AbstractFacade<AdminAccount> implements AdminAccountFacadeLocal {
    @PersistenceContext(unitName = "MysticJewelPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public AdminAccountFacade() {
        super(AdminAccount.class);
    }

    @Override
    public AdminAccount findAdminByUserName(String userName) {
        Query q = em.createQuery("SELECT a FROM AdminAccount a WHERE a.adminUserName = :adminUserName");
        q.setParameter("adminUserName", userName);
        try {
            return (AdminAccount) q.getSingleResult();
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public boolean login(String userName, String password) {
        AdminAccount aa = findAdminByUserName(userName);
        if (aa == null) {
            return false;
        } else {
            if (aa.getAdminPassword().equals(password)) {
                return true;
            }
        }
        return false;
    }
    
}
