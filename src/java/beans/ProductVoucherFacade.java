/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import entities.OrderMaster;
import entities.ProductVoucher;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author nknguyengl
 */
@Stateless
public class ProductVoucherFacade extends AbstractFacade<ProductVoucher> implements ProductVoucherFacadeLocal {

    @PersistenceContext(unitName = "MysticJewelPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ProductVoucherFacade() {
        super(ProductVoucher.class);
    }

    @Override
    public String createProductVoucher(ProductVoucher newProductVoucher) {
        create(newProductVoucher);
        return newProductVoucher.getVoucherID();
    }

    @Override
    public String editProductVoucher(ProductVoucher editProductVoucher) {
        edit(editProductVoucher);
        List<OrderMaster> orderMasterList = editProductVoucher.getOrderMasterList();
        for (OrderMaster om : orderMasterList) {
            om.setVoucherID(editProductVoucher);
            em.merge(om);
        }
        return editProductVoucher.getVoucherID();
    }

    @Override
    public boolean removeProductVoucher(ProductVoucher removeProductVoucher) {
        List<OrderMaster> orderMasterList = removeProductVoucher.getOrderMasterList();
        if (orderMasterList.size() >= 1) {
            return false;
        }
        remove(removeProductVoucher);
        return true;
    }

}
