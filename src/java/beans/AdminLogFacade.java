/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package beans;

import entities.AdminLog;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author NATSAMA
 */
@Stateless
public class AdminLogFacade extends AbstractFacade<AdminLog> implements AdminLogFacadeLocal {
    @PersistenceContext(unitName = "MysticJewelPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public AdminLogFacade() {
        super(AdminLog.class);
    }

    @Override
    public void createLog(String username, String logContent) {
        AdminLog log = new AdminLog();
        Date dateLogged = new Date(System.currentTimeMillis());
        log.setUsername(username);
        log.setLogContent(logContent);
        log.setDateLogged(dateLogged);
        create(log);
    }

    @Override
    public List<AdminLog> getNewestLog() {
        Query q = em.createQuery("SELECT l FROM AdminLog l ORDER BY l.logID DESC");
        List<AdminLog> list = q.getResultList();
        return list;
    }
    
}
