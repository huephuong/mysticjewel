/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package beans;

import entities.SkuSize;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author NATSAMA
 */
@Local
public interface SkuSizeFacadeLocal {

    void create(SkuSize skuSize);

    void edit(SkuSize skuSize);

    void remove(SkuSize skuSize);

    SkuSize find(Object id);

    List<SkuSize> findAll();

    List<SkuSize> findRange(int[] range);

    int count();

    SkuSize createSkuSize(SkuSize newSize);

    SkuSize editSkuSize(SkuSize skuSize);
    
}
