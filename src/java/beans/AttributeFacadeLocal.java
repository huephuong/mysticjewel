/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package beans;

import entities.Attribute;
import entities.AttributeGroup;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author nknguyengl
 */
@Local
public interface AttributeFacadeLocal {

    void create(Attribute attribute);

    void edit(Attribute attribute);

    void remove(Attribute attribute);

    Attribute find(Object id);

    List<Attribute> findAll();

    List<Attribute> findRange(int[] range);

    int count();

    Attribute createAttribute(String name, AttributeGroup parent);

    Attribute editAttribute(Attribute att);

    int deleteAttribute(Attribute att);
    
}
