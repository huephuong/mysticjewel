/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package beans;

import entities.Customer;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author nknguyengl
 */
@Local
public interface CustomerFacadeLocal {

    void create(Customer customer);

    void edit(Customer customer);

    void remove(Customer customer);

    Customer find(int id);

    List<Customer> findAll();

    List<Customer> findRange(int[] range);

    int count();
    
    Customer findCustomerByUserName(String userName);
    
    Customer findCustomerByEmail(String email);
    
    boolean login(String userName, String password);

    Customer createCustomer(Customer newCustomer);

    Customer editCustomer(Customer newCustomer);

    Customer findCustomerByResetToken(String resetToken);
    
}
