/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package beans;

import entities.Province;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author NATSAMA
 */
@Stateless
public class ProvinceFacade extends AbstractFacade<Province> implements ProvinceFacadeLocal {
    @PersistenceContext(unitName = "MysticJewelPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ProvinceFacade() {
        super(Province.class);
    }

    @Override
    public Province findByCode(String code) {
        Query q = em.createQuery("SELECT pr FROM Province pr WHERE pr.code = :code");
        q.setParameter("code", code);
        return (Province) q.getSingleResult();
    }
    
}
