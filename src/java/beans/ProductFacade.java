/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import entities.Attribute;
import entities.Category;
import entities.Customer;
import entities.Product;
import entities.ProductCol;
import entities.Review;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author nknguyengl
 */
@Stateless
public class ProductFacade extends AbstractFacade<Product> implements ProductFacadeLocal {

    @EJB
    private CustomerFacadeLocal customerFacade;

    @EJB
    private ProductColFacadeLocal productColFacade;
    @EJB
    private CategoryFacadeLocal categoryFacade;
    @PersistenceContext(unitName = "MysticJewelPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ProductFacade() {
        super(Product.class);
    }

    @Override
    public List<Product> findByCatCol(int categoryID, int colID) {
        Category cat = categoryFacade.find(categoryID);
        ProductCol col = productColFacade.find(colID);
        Query q;
        List<Product> list = new ArrayList<Product>();
        if (colID == 0 && categoryID == 0) {
            q = em.createQuery("SELECT p FROM Product p WHERE p.isActive = TRUE ORDER BY p.productID DESC");
            list = q.getResultList();
        } else if (colID > 0 && categoryID > 0) {
            q = em.createQuery("SELECT p FROM Product p WHERE p.categoryID = :categoryID AND p.colID = :colID AND p.isActive = TRUE ORDER BY p.productID DESC");
            q.setParameter("categoryID", cat);
            q.setParameter("colID", col);
            list = q.getResultList();
        } else if (categoryID == 0 && colID > 0) {
            q = em.createQuery("SELECT p FROM Product p WHERE p.colID = :colID AND p.isActive = TRUE ORDER BY p.productID DESC");
            q.setParameter("colID", col);
            list = q.getResultList();
        } else if (colID == 0 && categoryID > 0) {
            // tìm theo main Cat: parent = NULL
            // parentID là object Category
            if (cat.getParentID() == null) {
                q = em.createQuery("SELECT p FROM Product p, Category c WHERE p.categoryID = c AND c.parentID = :parent AND p.isActive = TRUE ORDER BY p.productID DESC");
                q.setParameter("parent", cat);
            } else {
                q = em.createQuery("SELECT p FROM Product p WHERE p.categoryID = :categoryID AND p.isActive = TRUE ORDER BY p.productID DESC");
                q.setParameter("categoryID", cat);
            }
            list = q.getResultList();
        } else {
            q = em.createQuery("SELECT p FROM Product p WHERE p.isActive = TRUE ORDER BY p.productID DESC");
            list = q.getResultList();
        }
        return list;
    }

    @Override
    public Product createProduct(Product newProd, List<Attribute> attributeList) {
        // Product - Attribute: Many to Many
        // Col/Cat: One to Many
        create(newProd);
        newProd.setAttributeList(attributeList);
        for (Attribute a : attributeList) {
            if (a.getProductList() == null) {
                a.setProductList(new ArrayList<>());
            }
            a.getProductList().add(newProd);
            em.merge(a);
        }
        Category cat = newProd.getCategoryID();
        ProductCol col = newProd.getColID();
        cat.getProductList().add(newProd);
        em.merge(cat);
        if (newProd.getColID() != null) {
            col.getProductList().add(newProd);
            em.merge(col);
        }
        return newProd;
    }

    @Override
    public List<Product> getHotProduct() {
        Query q = em.createQuery("SELECT p FROM Product p WHERE p.isActive = TRUE ORDER BY p.productID DESC");
        List<Product> list = new ArrayList<>();
        List<Product> full = q.getResultList();
        for (Product p : full) {
            if (p.getRating() >= 70) {
                list.add(p);
            }
        }
        return list;
    }

    @Override
    public List<Product> getNewProduct() {
        Query q = em.createQuery("SELECT p FROM Product p WHERE p.isActive = TRUE ORDER BY p.productID DESC");
        List<Product> list = q.getResultList();
        return list;
    }

    @Override
    public List<Product> getOnSaleProduct() {
        Query q = em.createQuery("SELECT p FROM Product p WHERE p.discountValue > 0 AND p.isActive = TRUE ORDER BY p.discountValue DESC");
        List<Product> list = q.getResultList();
        return list;
    }

    @Override
    public Product editProduct(Product prod, List<Attribute> attList) {
        prod.setAttributeList(attList);
        for (Attribute a : attList) {
            a.getProductList().remove(prod);
            em.merge(a);
        }
        edit(prod);
        return prod;
    }

    @Override
    public List<Product> findByCustomerHistory(String value) {
        Query q = em.createQuery("SELECT p FROM Product p WHERE p.productID IN (" + value + ")");
        return q.getResultList();
    }

    @Override
    public List<Product> findByName(String productName) {
        Query q = em.createQuery("SELECT p FROM Product p WHERE p.productName LIKE :productName AND p.isActive = TRUE ORDER BY p.productID DESC");
        q.setParameter("productName", "%" + productName + "%");
        try {
            return q.getResultList();
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public List<Product> findByFilter(int categoryID, int colID, int attrID, int priceFrom, int priceTo, String dir, String order) {
        Category cat = categoryFacade.find(categoryID);
        ProductCol col = productColFacade.find(colID);
        Query q;
        List<Product> list = new ArrayList<>();
        String fjoin = "";
        if (colID == 0) {
            if (cat.getParentID() == null) {
                fjoin += " , Category c ";
            }
        }
        if (attrID != 0) {
            fjoin += " JOIN p.attributeList a ";
        }
        String query = "SELECT p FROM Product p " + fjoin + " WHERE 1 = 1 ";
        if (colID > 0 && categoryID > 0) {
            query += " AND p.categoryID = :categoryID AND p.colID = :colID ";
        } else if (categoryID == 0) {
            query += " AND p.colID = :colID ";
        } else if (colID == 0) {
            if (cat.getParentID() == null) {
                query += " AND p.categoryID = c AND c.parentID = :parent ";
            } else {
                query += " AND p.categoryID = :categoryID ";
            }
        }
        if (attrID != 0) {
            query += " AND a.attributeID = :attrID ";
        }
        query += " AND p.isActive = TRUE ";
        q = em.createQuery(query);
        if (colID > 0 && categoryID > 0) {
            q.setParameter("categoryID", cat);
            q.setParameter("colID", col);
        } else if (categoryID == 0) {
            q.setParameter("colID", col);
        } else if (colID == 0) {
            if (cat.getParentID() == null) {
                q.setParameter("parent", cat);
            } else {
                q.setParameter("categoryID", cat);
            }
        }
        if (attrID != 0) {
            q.setParameter("attrID", attrID);
        }
        list = q.getResultList();
        List<Product> listPrice = new ArrayList<>();
        if (priceFrom != -2 && priceTo != -1) {
            for (Product p : list) {
                if (!listPrice.contains(p.getProductID())) {
                    if (p.getSkuList().get(0).getUnitPrice() >= priceFrom && p.getSkuList().get(0).getUnitPrice() <= priceTo) {
                        listPrice.add(p);
                    }
                }
            }
            list = listPrice;
        }
        if (order.equals("price")) {
            Collections.sort(list, new Comparator<Product>() {
                @Override
                public int compare(Product o1, Product o2) {
                    return o1.getSkuList().get(0).getUnitPrice().compareTo(o2.getSkuList().get(0).getUnitPrice());
                }
            });
        } else if (order.equals("new")) {
            Collections.sort(list, new Comparator<Product>() {
                @Override
                public int compare(Product o1, Product o2) {
                    return o1.getDateCreated().compareTo(o2.getDateCreated());
                }
            });
        } else {
            Collections.sort(list, new Comparator<Product>() {
                @Override
                public int compare(Product o1, Product o2) {
                    return o1.getProductName().compareTo(o2.getProductName());
                }
            });
        }
        if (dir.equals("desc")) {
            Collections.reverse(list);
        }
        return list;
    }

    @Override
    public List<Product> getRelatedProduct(Product prod) {
        Product bestMatch = null;
        List<Product> list = new ArrayList<>();
        List<Product> fullList = findAll();
        fullList.remove(prod);

        for (int i = 0; i <= 3; i++) {
            int bestScore = 0;
            if (bestMatch != null) {
                fullList.remove(bestMatch);
            }
            for (Product p : fullList) {
                int matchScore = 0;
                if (p.getColID() != null && prod.getColID() != null) {
                    if (p.getColID().getColID() == prod.getColID().getColID()) {
                        matchScore += 2;
                    }
                }
                if (p.getCategoryID().getCategoryID() == prod.getCategoryID().getCategoryID()) {
                    matchScore += 3;
                }
                for (Attribute att : p.getAttributeList()) {
                    for (Attribute prodAtt : prod.getAttributeList()) {
                        if (att.getAttributeID() == prodAtt.getAttributeID()) {
                            matchScore++;
                        }
                    }
                }
                if (bestScore <= matchScore) {
                    bestScore = matchScore;
                    bestMatch = p;
                }
            }
            list.add(bestMatch);
        }
        return list;
    }

    @Override
    public Product getReccommendedProduct(Customer cus) {
        Product bestMatch = null;

        // Based-on Customer's preference
        if (!cus.getReviewList().isEmpty()) {
            List<Integer> reviewList = new ArrayList<>();
            for (Review rev : cus.getReviewList()) {
                reviewList.add(rev.getProduct().getProductID());
            }
            // Get customer's most liked product
            Product mostLiked = null;
            double highest = 10;
            for (Review r : cus.getReviewList()) {
                if (highest < r.getRating()) {
                    highest = r.getRating();
                    mostLiked = r.getProduct();
                }
            }
            // Find matching reference from other customer
            int bestScore = 0;
            List<Product> fullList = findAll();
            Query q = em.createQuery("SELECT c FROM Customer c WHERE c.reviewList IS NOT EMPTY");
            List<Customer> reviewCus = q.getResultList();
            for (Product p : fullList) {
                p.setFavScore(0);
            }
            for (Customer c : reviewCus) {
                for (Review rev : c.getReviewList()) {
                    if (mostLiked.getProductID() == rev.getProduct().getProductID()) {
                        for (Review r : c.getReviewList()) {
                            r.getProduct().setFavScore(r.getProduct().getFavScore() + 1);
                        }
                    }
                }
            }
            // get Recommended product
            for (Product p : fullList) {
                if (!reviewList.contains(p.getProductID())) {
                    if (bestScore <= p.getFavScore()) {
                        bestScore = p.getFavScore();
                        bestMatch = p;
                    }
                }
            }
        } else {
            // If customer doesn't have a reference
            List<Product> hotList = getHotProduct();
            int bestScore = 0;
            for (Product p : hotList) {
                if (bestScore <= p.getRating()) {
                    bestScore = p.getRating();
                    bestMatch = p;
                }
            }
        }

        return bestMatch;
    }

    @Override
    public List<Product> getNewProductPagination(int firstResult, int maxResult) {
        Query q = em.createQuery("SELECT p FROM Product p WHERE p.isActive = TRUE ORDER BY p.productID DESC");
        if (firstResult != -2 || maxResult != -1) {
            q.setFirstResult(firstResult);
            q.setMaxResults(maxResult);
        }
        List<Product> list = q.getResultList();
        return list;
    }

    @Override
    public List<Product> getOnSaleProductPagination(int firstResult, int maxResult) {
        Query q = em.createQuery("SELECT p FROM Product p WHERE p.discountValue > 0 AND p.isActive = TRUE ORDER BY p.discountValue DESC");
        if (firstResult != -2 || maxResult != -1) {
            q.setFirstResult(firstResult);
            q.setMaxResults(maxResult);
        }
        List<Product> list = q.getResultList();
        return list;
    }

    @Override
    public List<Product> findByFilterNew(int attrID, int priceFrom, int priceTo, int firstResult, int maxResult, String dir, String order) {
        Query q;
        List<Product> list = new ArrayList<>();
        String fjoin = "";
        if (attrID != 0) {
            fjoin += " JOIN p.attributeList a ";
        }
        String query = "SELECT p FROM Product p JOIN p.skuList s " + fjoin + " WHERE 1 = 1 ";
        if (attrID != 0) {
            query += " AND a.attributeID = :attrID ";
        }
        if (priceFrom != -2 && priceTo != -1) {
            query += " AND s.unitPrice BETWEEN :priceFrom AND :priceTo ";
        }
        query += " AND p.isActive = TRUE ";
        if (order.equals("price")) {
            query += " ORDER BY p.productID DESC, s.unitPrice ";
        } else {
            query += " ORDER BY p.productID DESC, p.productName ";
        }
        query += dir;
        q = em.createQuery(query);
        if (attrID != 0) {
            q.setParameter("attrID", attrID);
        }
        if (priceFrom != -2 && priceTo != -1) {
            q.setParameter("priceFrom", priceFrom);
            q.setParameter("priceTo", priceTo);
        }
        if (firstResult != -2 || maxResult != -1) {
            q.setFirstResult(firstResult);
            q.setMaxResults(maxResult);
        }
        list = q.getResultList();
        list = list.stream().distinct().collect(Collectors.toList());
        System.out.println(list);
        return list;
    }

    @Override
    public List<Product> findByFilterSale(int attrID, int priceFrom, int priceTo, int firstResult, int maxResult, String dir, String order) {
        Query q;
        List<Product> list = new ArrayList<>();
        String fjoin = "";
        if (attrID != 0) {
            fjoin += " JOIN p.attributeList a ";
        }
        String query = "SELECT p FROM Product p JOIN p.skuList s " + fjoin + " WHERE p.discountValue > 0 ";
        if (attrID != 0) {
            query += " AND a.attributeID = :attrID ";
        }
        if (priceFrom != -2 && priceTo != -1) {
            query += " AND s.unitPrice BETWEEN :priceFrom AND :priceTo ";
        }
        query += " AND p.isActive = TRUE ";
        if (order.equals("price")) {
            query += " ORDER BY s.unitPrice ";
        } else {
            query += " ORDER BY p.productName ";
        }
        query += dir;
        q = em.createQuery(query);

        if (attrID != 0) {
            q.setParameter("attrID", attrID);
        }
        if (priceFrom != -2 && priceTo != -1) {
            q.setParameter("priceFrom", priceFrom);
            q.setParameter("priceTo", priceTo);
        }
        if (firstResult != -2 || maxResult != -1) {
            q.setFirstResult(firstResult);
            q.setMaxResults(maxResult);
        }
        list = q.getResultList();
        list = list.stream().distinct().collect(Collectors.toList());
        System.out.println(list);
        return list;
    }

}
