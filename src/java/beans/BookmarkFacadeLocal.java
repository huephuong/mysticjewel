package beans;

import entities.Bookmark;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author nknguyengl
 */
@Local
public interface BookmarkFacadeLocal {

    void create(Bookmark bookmark);

    void edit(Bookmark bookmark);

    void remove(Bookmark bookmark);

    Bookmark find(Object id);

    List<Bookmark> findAll();

    List<Bookmark> findRange(int[] range);

    int count();

    int createBookmark(Bookmark newBookmark);

    int removeBookmark(Bookmark rmBookmark);

    List<Bookmark> findByCustomerID(int cusID);

    List<Bookmark> findByProductID(int proID);

    Bookmark findByBookmarkPK(int cusID, int proID);
    
}
