/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import entities.SocialAccount;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author nknguyengl
 */
@Local
public interface SocialAccountFacadeLocal {

    void create(SocialAccount socialAccount);

    void edit(SocialAccount socialAccount);

    void remove(SocialAccount socialAccount);

    SocialAccount find(Object id);

    List<SocialAccount> findAll();

    List<SocialAccount> findRange(int[] range);

    int count();

    SocialAccount createSocialAccount(SocialAccount socialAccount);

    SocialAccount findByProviderCustomerID(String providerCustomerID);

}
