/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package beans;

import entities.AdminAccount;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author nknguyengl
 */
@Local
public interface AdminAccountFacadeLocal {

    void create(AdminAccount adminAccount);

    void edit(AdminAccount adminAccount);

    void remove(AdminAccount adminAccount);

    AdminAccount find(Object id);

    List<AdminAccount> findAll();

    List<AdminAccount> findRange(int[] range);

    int count();
    
    AdminAccount findAdminByUserName(String userName);
    
    boolean login(String userName, String password);
    
}
