/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package beans;

import entities.Attribute;
import entities.AttributeGroup;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author nknguyengl
 */
@Stateless
public class AttributeGroupFacade extends AbstractFacade<AttributeGroup> implements AttributeGroupFacadeLocal {
    @PersistenceContext(unitName = "MysticJewelPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public AttributeGroupFacade() {
        super(AttributeGroup.class);
    }

    @Override
    public AttributeGroup createAttributeGroup(String name) {
        AttributeGroup newGroup = new AttributeGroup(name);
        newGroup.setIsSearchable(false);
        create(newGroup);
        return newGroup;
    }

    @Override
    public List<AttributeGroup> getEmptyGroup() {
        Query q = em.createQuery("SELECT a FROM AttributeGroup a WHERE a.attributeList IS EMPTY");
        List<AttributeGroup> list = q.getResultList();
        return list;
    }

    @Override
    public List<AttributeGroup> getSearchableGroup() {
        Query q = em.createQuery("SELECT a FROM AttributeGroup a WHERE a.isSearchable = TRUE");
        List<AttributeGroup> list = q.getResultList();
        return list;
    }

    @Override
    public void updateGroupStatus(AttributeGroup group, boolean searchable) {
        group.setIsSearchable(searchable);
        edit(group);
    }
}
