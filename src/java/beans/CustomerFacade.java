/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package beans;

import entities.Customer;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author nknguyengl
 */
@Stateless
public class CustomerFacade extends AbstractFacade<Customer> implements CustomerFacadeLocal {
    @PersistenceContext(unitName = "MysticJewelPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public CustomerFacade() {
        super(Customer.class);
    }

    @Override
    public Customer find(int id) {
        Customer c = em.find(Customer.class, id);
        return c;
    }

    @Override
    public Customer findCustomerByUserName(String userName) {
        Query q = em.createQuery("SELECT c FROM Customer c WHERE c.customerUserName = :customerUserName");
        q.setParameter("customerUserName", userName);
        try {
            return (Customer) q.getSingleResult();
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public Customer findCustomerByEmail(String email) {
        Query q = em.createQuery("SELECT c FROM Customer c WHERE c.email = :email");
        q.setParameter("email", email);
        try {
            return (Customer) q.getSingleResult();
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public boolean login(String userName, String password) {
        Customer c = findCustomerByEmail(userName);
        if (c == null) {
            return false;
        } else {
            if (c.getCustomerPassword().equals(password)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public Customer createCustomer(Customer newCustomer) {
        create(newCustomer);
        return newCustomer;
    }

    @Override
    public Customer editCustomer(Customer newCustomer) {
        edit(newCustomer);
        return newCustomer;
    }

    @Override
    public Customer findCustomerByResetToken(String resetToken) {
        Query q = em.createQuery("SELECT c FROM Customer c WHERE c.resetToken = :resetToken");
        q.setParameter("resetToken", resetToken);
        try {
            return (Customer) q.getSingleResult();
        } catch (Exception e) {
            return null;
        }
    }
    
}
