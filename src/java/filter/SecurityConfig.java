/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package filter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author nkngu
 */
public class SecurityConfig {

    public static final String ROLE_MANAGER = "SuperAdmin";
    public static final String ROLE_EMPLOYEE = "Admin";

    private static final Map<String, List<String>> mapConfig = new HashMap<String, List<String>>();

    static {
        init();
    }

    private static void init() {
        /**
         * SuperAdmin role config
         */
        List<String> urlPatterns1 = new ArrayList<>();

        urlPatterns1.add("/admin-page");
        urlPatterns1.add("/admin-page/dashboard");
        urlPatterns1.add("/admin-page/catalog/category");
        urlPatterns1.add("/admin-page/catalog/size");
        urlPatterns1.add("/admin-page/catalog/product");
        urlPatterns1.add("/admin-page/catalog/product/inventory");
        urlPatterns1.add("/admin-page/catalog/product/add");
        urlPatterns1.add("/admin-page/catalog/product/discount");
        urlPatterns1.add("/admin-page/catalog/collection");
        urlPatterns1.add("/admin-page/catalog/attributes");
        urlPatterns1.add("/admin-page/catalog/reviews");
        urlPatterns1.add("/admin-page/sales/order");
        urlPatterns1.add("/admin-page/sales/voucher");
        urlPatterns1.add("/admin-page/sales/voucher/form");
        urlPatterns1.add("/admin-page/admin-account");
        urlPatterns1.add("/admin-page/admin-account/form");
        urlPatterns1.add("/admin-page/customer");
        urlPatterns1.add("/admin-page/customer/review");

        mapConfig.put(ROLE_MANAGER, urlPatterns1);

        /**
         * Admin role config
         */
        List<String> urlPatterns2 = new ArrayList<>();

        urlPatterns2.add("/admin-page");
        urlPatterns2.add("/admin-page/dashboard");
        urlPatterns2.add("/admin-page/sales/order");
        urlPatterns2.add("/admin-page/sales/voucher");
        urlPatterns2.add("/admin-page/sales/voucher/form");
        urlPatterns2.add("/admin-page/customer");
        urlPatterns2.add("/admin-page/customer/review");

        mapConfig.put(ROLE_EMPLOYEE, urlPatterns2);
    }

    public static Set<String> getAllAppRoles() {
        return mapConfig.keySet();
    }

    public static List<String> getUrlPatternsForRole(String role) {
        return mapConfig.get(role);
    }
}
