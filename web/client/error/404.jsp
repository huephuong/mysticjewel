<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="/admin/layout/taglib.jsp"%>
<tiles:insertDefinition name="page-client">
    <tiles:putAttribute name="title" value="404" />
    <tiles:putAttribute name="body-class" value=" catalog-product-view catalog-product-view product-awesome-jewel-product categorypath-fashion-html category-fashion  wide" />
    <tiles:putAttribute name="page-css">
        <link rel="stylesheet" type="text/css" href="<c:url value="/" />resource/client/media/css/c78a949c76158a4d75f94587ee9c6aab.css" media="all" />
    </tiles:putAttribute>
    <tiles:putAttribute name="page-js" value="" />
    <tiles:putAttribute name="content">
        <div class="main-container col1-layout">
            <div class="main-before-top-container"></div>
            <div class="main container">
                <div class="preface"></div>
                <div class="col-main">
                    <div class="clearfix"></div>
                    <div class="std">
                        <div class="grid-full eg-tac" style="font-size: 13px;">
                            <h1 class="eg-ttu" style="font-size: 72px;color: #333;">404</h1>
                            <h1 class="eg-ttu" style="color: #333;">Oops! Page not found</h1>
                            <div class="horizontal-separator separator-5" style="margin: 30px auto;">&nbsp;</div>
                            <p style="color: #767676;">Sorry, but the page you are looking for is not found. Please, make sure you have typed the current URL. </p> <a href="<c:url value="/" />" class="button eg-ttu" style="padding: 13px 28px;margin-top: 20px;">Go to home page</a>
                        </div>
                    </div>
                </div>
                <div class="postscript"></div>
            </div>
        </div>
    </tiles:putAttribute>
</tiles:insertDefinition>
