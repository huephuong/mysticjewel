<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="/admin/layout/taglib.jsp"%>
<tiles:insertDefinition name="page-client">
    <tiles:putAttribute name="title" value="Forgot password" />
    <tiles:putAttribute name="body-class" value=" customer-account-forgotpassword  wide" />
    <tiles:putAttribute name="page-css">
        <link rel="stylesheet" type="text/css" href="resource/client/media/css/c78a949c76158a4d75f94587ee9c6aab.css" media="all" />
    </tiles:putAttribute>
    <tiles:putAttribute name="page-js">
        <script type="text/javascript" src="resource/client/js/mage/captcha.js"></script>
    </tiles:putAttribute>
    <tiles:putAttribute name="content">
        <div class="main-container col1-layout">
            <div class="main-before-top-container"></div>
            <div class="main container">
                <div class="preface"></div>
                <div class="col-main">
                    <div class="clearfix"></div>
                    <tiles:insertTemplate template="/client/layout/toggle.jsp" />
                    <div class="page-title">
                        <h1>Forgot Your Password?</h1>
                    </div>
                    <c:if test="${not empty res_message}">
                        <ul class="messages">
                            <li class="${res_status}-msg">
                                <ul>
                                    <li>
                                        <span>${res_message}</span>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                        <c:set var="res_message" value="" scope="session"/>
                    </c:if>
                    <form action="<c:url value="/" />forgot-password" method="post" id="form-validate">
                        <div class="fieldset">
                            <h2 class="legend">Retrieve your password here</h2>
                            <p>Please enter your email address below. You will receive a link to reset your password.</p>
                            <ul class="form-list">
                                <li>
                                    <label for="email_address" class="required"><em>*</em>Email Address</label>
                                    <div class="input-box">
                                        <input type="text" name="email" alt="email" id="email_address" class="input-text required-entry validate-email" value="" />
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="buttons-set">
                            <p class="required">* Required Fields</p>
                            <p class="back-link"><a href="<c:url value="/" />login"><small>&laquo; </small>Back to Login</a> </p>
                            <button type="submit" title="Submit" class="button">
                                <span><span>Submit</span></span>
                            </button>
                        </div>
                    </form>
                    <script type="text/javascript">
                        //<![CDATA[
                        var dataForm = new VarienForm('form-validate', true);
                        //]]>
                    </script>
                </div>
                <div class="postscript"></div>
            </div>
        </div>
    </tiles:putAttribute>
</tiles:insertDefinition>
