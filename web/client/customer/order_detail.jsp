<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="/admin/layout/taglib.jsp"%>
<tiles:insertDefinition name="page-client">
    <tiles:putAttribute name="title" value="Order # 1600000009" />
    <tiles:putAttribute name="body-class" value=" customer-account-index  wide cat-sidebar-popup-enabled" />
    <tiles:putAttribute name="page-css">
        <link rel="stylesheet" type="text/css" href="<c:url value="/" />resource/client/media/css/c78a949c76158a4d75f94587ee9c6aab.css" media="all" />
    </tiles:putAttribute>
    <tiles:putAttribute name="page-js" value="" />
    <tiles:putAttribute name="content">
        <div class="top-container">
            <div class="breadcrumbs bctype2">
                <div class="container">
                    <div class="breadcrumbs-inner">
                        <div class="bc-page-title">
                            <h1><span>Order #${order.orderID}</span></h1> </div>
                        <ul>
                            <li class="home"> <a href="<c:url value="/"/>" title="Go to Home Page">Home</a> <span> / </span> </li>
                            <li class="my-account"> <strong>Order Details</strong> </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="main-container col2-left-layout">
            <div class="main-before-top-container"></div>
            <div class="main container">
                <div class="preface"></div>
                <div class="col-main">
                    <div class="clearfix"></div>
                    <tiles:insertTemplate template="/client/layout/toggle.jsp" />
                    <div class="my-account">
                        <dl class="order-info" style="font-size: 18px;">
                            <dt>Your order's status:</dt>
                            <dd>${order.orderStatus}</dd>
                            <c:if test="${order.orderStatus == 'Awaiting Payment'}">
                                <dd><a href="/place-order/payment?orderid=${order.orderID}">Complete payment for this order</a></dd>
                            </c:if>
                        </dl>
                        <h2>
                            Order Details #${order.orderID}
                        </h2>
                        <div class="col2-set order-info-box">
                            <div class="col-xs-3">
                                <div class="box">
                                    <div class="box-content" style="font-size: 14px;">
                                        <p><strong>Date Ordered:</strong></p>
                                        <p><strong>Payment Method:</strong></p>
                                        <p><strong>Full Name:</strong></p>
                                        <p><strong>Email:</strong></p>
                                        <p><strong>Phone:</strong></p>
                                        <p><strong>Address:</strong></p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-9">
                                <div class="box">
                                    <div class="box-content" style="font-size: 14px;"> 
                                        <p><fmt:formatDate type = "both" dateStyle = "short" timeStyle = "short" value = "${order.dateCreated}" /></p>
                                        <p>${order.payment}</p>
                                        <p>${order.shipName}</p>
                                        <p>${order.shipEmail}</p>
                                        <p>${order.shipPhone}</p>
                                        <p>${order.shipAddress}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="order-items order-details">
                            <h2>
                                Items Ordered
                            </h2>
                            <table class="data-table" id="my-orders-table" summary="Items Ordered">
                                <thead>
                                    <tr>
                                        <th class="text-center">Product Name</th>
                                        <th class="text-center">SKU</th>
                                        <th class="text-center">Price</th>
                                        <th class="text-center">Qty</th>
                                        <th class="text-center">Subtotal</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr class="subtotal">
                                        <td colspan="4" class="a-right"> Subtotal </td>
                                        <td class="last a-right">
                                            <span class="price">
                                                <fmt:setLocale value="en_US"/>
                                                <fmt:formatNumber value="${order.subTotal}" type="currency"/>
                                            </span>
                                        </td>
                                    </tr>
                                    <tr class="shipping">
                                        <td colspan="4" class="a-right"> VAT </td>
                                        <td class="last a-right">
                                            <span class="price">
                                                <fmt:setLocale value="en_US"/>
                                                <fmt:formatNumber value="${order.subTotal * 0.1}" type="currency"/>
                                            </span>
                                        </td>
                                    </tr>
                                    <tr class="shipping">
                                        <td colspan="4" class="a-right"> Shipping </td>
                                        <td class="last a-right">
                                            <span class="price">
                                                <fmt:setLocale value="en_US"/>
                                                <fmt:formatNumber value="${order.shipFee}" type="currency"/>
                                            </span>
                                        </td>
                                    </tr>
                                    <tr class="grand_total">
                                        <td colspan="4" class="a-right"> <strong>Grand Total</strong> </td>
                                        <td class="last a-right">
                                            <strong>
                                                <span class="price">
                                                    <fmt:setLocale value="en_US"/>
                                                    <fmt:formatNumber value="${order.grandTotal}" type="currency"/>
                                                </span>
                                            </strong> 
                                        </td>
                                    </tr>
                                </tfoot>
                                <tbody>
                                    <c:forEach items="${order.orderDetailList}" var="item">
                                        <tr class="border" id="order-item-row-88">
                                            <td>
                                                <h3 class="product-name">${item.sku.productID.productName}</h3>
                                            </td>
                                            <td class="a-center">${item.sku.skuID}</td>
                                            <td class="a-center"> <span class="price-excl-tax">
                                                    <span class="cart-price">
                                                        <span class="price">
                                                            <c:choose>
                                                                <c:when test="${item.discountValue gt 0}">
                                                                    <fmt:setLocale value="en_US"/>
                                                                    <fmt:formatNumber value="${item.unitPrice * item.discountValue / 100}" type="currency"/>
                                                                    (- ${item.discountValue}%)
                                                                </c:when>
                                                                <c:otherwise>
                                                                    <fmt:setLocale value="en_US"/>
                                                                    <fmt:formatNumber value="${item.unitPrice}" type="currency"/>
                                                                </c:otherwise>
                                                            </c:choose>
                                                        </span>
                                                    </span>
                                                </span>
                                                <br/>
                                            </td>
                                            <td class="a-center">
                                                <span class="nobr">
                                                    Ordered: <strong>${item.quantity}</strong><br/>
                                                </span> </td>
                                            <td class="a-center">
                                                <span class="price-excl-tax">
                                                    <span class="cart-price">
                                                        <span class="price">
                                                            <c:choose>
                                                                <c:when test="${item.discountValue gt 0}">
                                                                    <fmt:setLocale value="en_US"/>
                                                                    <fmt:formatNumber value="${(item.unitPrice * item.discountValue / 100) * item.quantity}" type="currency"/>
                                                                </c:when>
                                                                <c:otherwise>
                                                                    <fmt:setLocale value="en_US"/>
                                                                    <fmt:formatNumber value="${item.unitPrice * item.quantity}" type="currency"/>
                                                                </c:otherwise>
                                                            </c:choose>
                                                        </span>
                                                    </span>
                                                </span>
                                                <br/>
                                            </td>
                                        </tr>
                                    </c:forEach>
                                </tbody>
                            </table>
                            <script type="text/javascript">
                                decorateTable('my-orders-table', {
                                    'tbody': ['odd', 'even'],
                                    'tbody tr': ['first', 'last']
                                })
                            </script>
                            <div style="margin-top: 20px;">
                                <c:if test="${not empty customer}">
                                    <p class="pull-left"><a href="<c:url value="/customer/order/history"/>">&laquo;&nbsp;&nbsp;Back to My Orders</a></p>
                                </c:if>
                                <p class="pull-right"><a href="<c:url value="/print-receipt?orderid=${order.orderID}"/>">Print Receipt&nbsp;&nbsp;&raquo;</a></p>
                            </div>
                        </div>
                    </div>
                </div>
                <c:if test="${empty customer}">
                    <div class="col-left sidebar">
                        <div class="block block-account">
                            <div class="block-title"> <strong><span>Sign Up For More</span></strong> </div>
                            <div class="block-content">
                                <ul>
                                    <li class="first"><a href="<c:url value="/register"/>">Sign up</a> </li>
                                    <li class="last"><a href="<c:url value="/login"/>">Sign in</a> </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </c:if>
                <c:if test="${not empty customer}">
                    <tiles:insertTemplate template="/client/customer/account/sidebar.jsp" />
                </c:if>
                <div class="postscript"></div>
            </div>
        </div>
    </tiles:putAttribute>
</tiles:insertDefinition>
