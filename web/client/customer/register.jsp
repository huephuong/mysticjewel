<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="/admin/layout/taglib.jsp"%>
<tiles:insertDefinition name="page-client">
    <tiles:putAttribute name="title" value="Register" />
    <tiles:putAttribute name="body-class" value=" customer-account-create  wide" />
    <tiles:putAttribute name="page-css">
        <link rel="stylesheet" type="text/css" href="resource/client/media/css/c78a949c76158a4d75f94587ee9c6aab.css" media="all" />
    </tiles:putAttribute>
    <tiles:putAttribute name="page-js">
        <script type="text/javascript" src="resource/client/js/mage/captcha.js"></script>
    </tiles:putAttribute>
    <tiles:putAttribute name="content">
        <div class="top-container">
            <div class="breadcrumbs bctype2">
                <div class="container">
                    <div class="breadcrumbs-inner">
                        <div class="bc-page-title">
                            <h1><span>Create New Customer Account</span></h1>
                        </div>
                        <ul>
                            <li class="home"> <a href="/" title="Go to Home Page">Home</a> <span> / </span> </li>
                            <li class="account-create"> <strong>Create Account</strong> </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="main-container col1-layout">
            <div class="main-before-top-container"></div>
            <div class="main container">
                <div class="preface"></div>
                <div class="col-main">
                    <div class="clearfix"></div>
                    <tiles:insertTemplate template="/client/layout/toggle.jsp" />
                    <div class="account-create">
                        <div class="page-title">
                            <h1>Create an Account</h1>
                        </div>
                        <c:if test="${not empty res_message}">
                            <ul class="messages">
                                <li class="error-msg">
                                    <ul>
                                        <li>
                                            <span>There is already an account with this email address. If you are sure that it is your email address, <a href="<c:url value="/" />customer/account/forgot-password">click here</a> to get your password and access your account.</span>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                            <c:set var="res_message" value="" scope="session"/>
                        </c:if>
                        <form action="<c:url value="/" />register" method="post" id="form-validate">
                            <div class="fieldset grid12-6 no-padding-left grid-xs-full no-padding-right-xs">
                                <div class="personal-info">
                                    <input type="hidden" name="success_url" value="" />
                                    <input type="hidden" name="error_url" value="" />
                                    <h2 class="r-separator"><span>Personal Information</span></h2>
                                    <ul class="form-list">
                                        <li class="fields">
                                            <div class="customer-name">
                                                <div class="field name-firstname">
                                                    <label for="firstname" class="required"><em>*</em>First Name</label>
                                                    <div class="input-box">
                                                        <input type="text" id="firstname" name="firstname" value="" title="First Name" maxlength="255" class="input-text required-entry" />
                                                    </div>
                                                </div>
                                                <div class="field name-lastname">
                                                    <label for="lastname" class="required"><em>*</em>Last Name</label>
                                                    <div class="input-box">
                                                        <input type="text" id="lastname" name="lastname" value="" title="Last Name" maxlength="255" class="input-text required-entry" />
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="fields">
                                            <div>
                                                <label for="username" class="required"><em>*</em>User Name</label>
                                                <div class="input-box">
                                                    <input type="text" name="username" id="username" value="" title="User Name" class="input-text required-entry" />
                                                </div>
                                            </div>
                                        </li>
                                        <li class="fields">
                                            <div>
                                                <label for="email_address" class="required"><em>*</em>Email Address</label>
                                                <div class="input-box">
                                                    <input type="text" name="email" id="email_address" value="" title="Email Address" class="input-text validate-email required-entry" />
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="fieldset grid12-6 no-padding-right grid-xs-full no-padding-left-xs">
                                <h2 class="r-separator"><span>Login Information</span></h2>
                                <ul class="form-list">
                                    <li class="fields">
                                        <div class="field">
                                            <label for="password" class="required"><em>*</em>Password</label>
                                            <div class="input-box">
                                                <input type="password" name="password" id="password" title="Password" class="input-text required-entry validate-password" />
                                            </div>
                                        </div>
                                        <div class="field">
                                            <label for="confirmation" class="required"><em>*</em>Confirm Password</label>
                                            <div class="input-box">
                                                <input type="password" name="confirmation" id="confirmation" title="Confirm Password" class="input-text required-entry validate-cpassword" />
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                                <div id="window-overlay" class="window-overlay" style="display:none;"></div>
                                <div id="remember-me-popup" class="remember-me-popup" style="display:none;">
                                    <div class="remember-me-popup-head">
                                        <h3>What's this?</h3> <a href="#" class="remember-me-popup-close" title="Close">Close</a>
                                    </div>
                                    <div class="remember-me-popup-body">
                                        <p>Checking &quot;Remember Me&quot; will let you access your shopping cart on this computer when you are logged out</p>
                                        <div class="remember-me-popup-close-button a-right"> <a href="#" class="remember-me-popup-close button" title="Close"><span>Close</span></a> </div>
                                    </div>
                                </div>
                                <script type="text/javascript">
                                    //<![CDATA[
                                    function toggleRememberMepopup(event) {
                                        if ($('remember-me-popup')) {
                                            var viewportHeight = document.viewport.getHeight(),
                                                    docHeight = $$('body')[0].getHeight(),
                                                    height = docHeight > viewportHeight ? docHeight : viewportHeight;
                                            $('remember-me-popup').toggle();
                                            $('window-overlay').setStyle({
                                                height: height + 'px'
                                            }).toggle();
                                        }
                                        Event.stop(event);
                                    }
                                    document.observe("dom:loaded", function() {
                                        new Insertion.Bottom($$('body')[0], $('window-overlay'));
                                        new Insertion.Bottom($$('body')[0], $('remember-me-popup'));
                                        $$('.remember-me-popup-close').each(function(element) {
                                            Event.observe(element, 'click', toggleRememberMepopup);
                                        })
                                        $$('#remember-me-box a').each(function(element) {
                                            Event.observe(element, 'click', toggleRememberMepopup);
                                        });
                                    });
                                    //]]>
                                </script>
                            </div>
                            <div class="buttons-set">
                                <p class="required">* Required Fields</p>
                                <p class="back-link"><a href="<c:url value="/" />login" class="back-link"><small>&laquo; </small>Back</a> </p>
                                <button type="submit" title="Submit" class="button">
                                    <span><span>Submit</span></span>
                                </button>
                            </div>
                        </form>
                        <script type="text/javascript">
                            //<![CDATA[
                            var dataForm = new VarienForm('form-validate', true);
                            //]]>
                        </script>
                    </div>
                </div>
                <div class="postscript"></div>
            </div>
        </div>
    </tiles:putAttribute>
</tiles:insertDefinition>
