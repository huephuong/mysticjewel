<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="/admin/layout/taglib.jsp"%>
<div class="col-left sidebar">
    <div class="block block-account">
        <div class="block-title"> <strong><span>My Account</span></strong> </div>
        <div class="block-content">
            <ul>
                <li><a href="<c:url value="/" />customer/account">Account Dashboard</a> </li>
                <li><a href="<c:url value="/" />customer/account/information">Account Information</a> </li>
                <li><a href="<c:url value="/" />customer/account/address">Address Book</a> </li>
                <li><a href="<c:url value="/" />customer/order/history">My Orders</a> </li>
                <li class="last"><a href="<c:url value="/" />customer/wishlist">My Wishlist</a> </li>
            </ul>
        </div>
    </div>
</div>
