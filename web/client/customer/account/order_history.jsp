<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="/admin/layout/taglib.jsp"%>
<tiles:insertDefinition name="page-client">
    <tiles:putAttribute name="title" value="My Orders" />
    <tiles:putAttribute name="body-class" value=" customer-account-index  wide cat-sidebar-popup-enabled" />
    <tiles:putAttribute name="page-css">
        <link rel="stylesheet" type="text/css" href="<c:url value="/" />resource/client/media/css/c78a949c76158a4d75f94587ee9c6aab.css" media="all" />
    </tiles:putAttribute>
    <tiles:putAttribute name="page-js" value="" />
    <tiles:putAttribute name="content">
        <div class="top-container">
            <div class="breadcrumbs bctype2">
                <div class="container">
                    <div class="breadcrumbs-inner">
                        <div class="bc-page-title">
                            <h1><span>My Orders</span></h1>
                        </div>
                        <ul>
                            <li class="home"> <a href="/" title="Go to Home Page">Home</a> <span> / </span> </li>
                            <li class="my-account"> <strong>My Account</strong> </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="main-container col2-left-layout">
            <div class="main-before-top-container"></div>
            <div class="main container">
                <div class="preface"></div>
                <div class="col-main">
                    <div class="clearfix"></div>
                    <tiles:insertTemplate template="/client/layout/toggle.jsp" />
                    <div class="my-account">
                        <div class="dashboard">
                            <div class="page-title">
                                <h1>My Orders</h1>
                            </div>
                            <div class="my-orders-title">
                                <h2>My Orders</h2>
                            </div>
                            <div class="my-orders-table-wrapper">
                                <table class="data-table" id="my-orders-table">
                                    <thead>
                                        <tr>
                                            <th>Order #</th>
                                            <th>Date</th>
                                            <th>Ship To</th>
                                            <th><span class="nobr">Order Total</span> </th>
                                            <th><span class="nobr">Order Status</span> </th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <c:forEach items="${orderMaster}" var="order">
                                            <tr>
                                                <td>${order.orderID}</td>
                                                <td><span class="nobr">
                                                        <fmt:formatDate type="date" value="${order.dateCreated}" pattern="dd/MM/yyyy" />
                                                    </span>
                                                </td>
                                                <td>${order.shipName}</td>
                                                <td>
                                                    <span class="price">
                                                        <fmt:setLocale value="en_US"/>
                                                        <fmt:formatNumber value="${order.grandTotal}" type="currency"/>
                                                    </span>
                                                </td>
                                                <td>
                                                    <em>
                                                        ${order.orderStatus}
                                                    </em>
                                                </td>
                                                <td class="a-center">
                                                    <span class="nobr">
                                                        <a href="<c:url value="/order-detail?orderid=${order.orderID}"/>">Detail</a>
                                                        <c:if test="${order.orderStatus != 'Completed' && order.orderStatus != 'Delivering' && order.orderStatus != 'Canceled'}">
                                                            <span class="separator">|</span>
                                                            <a href="<c:url value="/customer/order/cancel?orderid=${order.orderID}"/>">Cancel</a>
                                                        </c:if>
                                                        <c:if test="${order.orderStatus == 'Awaiting Payment'}">
                                                            <span class="separator">|</span>
                                                            <a href="<c:url value="/place-order/payment?orderid=${order.orderID}"/>">Payment</a>
                                                        </c:if>
                                                    </span>
                                                </td>
                                            </tr>
                                        </c:forEach>
                                    </tbody>
                                </table>
                                <script type="text/javascript">
                                    decorateTable('my-orders-table');
                                </script>
                                <div class="pager">
                                    <p class="amount"> <strong>${orderMaster.size()} Item(s)</strong> </p>
                                    <!--div class="limiter">
                                        <label>Show</label>
                                        <select onchange="setLocation(this.value)">
                                            <option value="" selected="selected"> 10 </option>
                                            <option value=""> 20 </option>
                                            <option value=""> 50 </option>
                                        </select>
                                        <span class="per-page"> per page</span>
                                    </div-->
                                </div>
                            </div>
                            <div class="buttons-set">
                                <p class="back-link"><a href=""><small>&laquo; </small>Back</a> </p>
                            </div>
                        </div>
                    </div>
                </div>
                <tiles:insertTemplate template="/client/customer/account/sidebar.jsp" />
                <div class="postscript"></div>
            </div>
        </div>
    </tiles:putAttribute>
</tiles:insertDefinition>
