<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="/admin/layout/taglib.jsp"%>
<tiles:insertDefinition name="page-client">
    <tiles:putAttribute name="title" value="Account Address" />
    <tiles:putAttribute name="body-class" value=" customer-account-index  wide cat-sidebar-popup-enabled" />
    <tiles:putAttribute name="page-css">
        <link rel="stylesheet" type="text/css" href="<c:url value="/" />resource/client/media/css/c78a949c76158a4d75f94587ee9c6aab.css" media="all" />
    </tiles:putAttribute>
    <tiles:putAttribute name="page-js" value="" />
    <tiles:putAttribute name="content">
        <div class="top-container">
            <div class="breadcrumbs bctype2">
                <div class="container">
                    <div class="breadcrumbs-inner">
                        <div class="bc-page-title">
                            <h1><span>Add New Address</span></h1>
                        </div>
                        <ul>
                            <li class="home"> <a href="/" title="Go to Home Page">Home</a> <span> / </span> </li>
                            <li class="my-account"> <strong>My Account</strong> </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="main-container col2-left-layout">
            <div class="main-before-top-container"></div>
            <div class="main container">
                <div class="preface"></div>
                <div class="col-main">
                    <div class="clearfix"></div>
                    <tiles:insertTemplate template="/client/layout/toggle.jsp" />
                    <div class="my-account">
                        <div class="page-title">
                            <h1>Add New Address</h1>
                        </div>
                        <c:if test="${not empty res_message}">
                            <ul class="messages">
                                <li class="success-msg">
                                    <ul>
                                        <li><span>${res_message}</span></li>
                                    </ul>
                                </li>
                            </ul>
                            <c:set var="res_message" value="" scope="session"/>
                        </c:if>
                        <form action="<c:url value="/" />customer/account/address" method="post" id="form-validate">
                            <div class="fieldset">
                                <h2>Address</h2>
                                <ul class="form-list">
                                    <input type="hidden" name="addressID" value="${address.addressID}" />
                                    <li class="fields">
                                        <div class="field">
                                            <label for="number" class="required"><em>*</em>Number</label>
                                            <div class="input-box">
                                                <input type="text" name="number" value="${address.number}" title="Number" id="number" class="input-text  required-entry" />
                                            </div>
                                        </div>
                                    </li>
                                    <li class="fields">
                                        <div class="field">
                                            <label for="street" class="required"><em>*</em>Street</label>
                                            <div class="input-box">
                                                <input type="text" name="street" value="${address.street}" title="Street" id="street" class="input-text  required-entry" />
                                            </div>
                                        </div>
                                    </li>
                                    <li class="fields">
                                        <div class="field">
                                            <label for="ward" class="required"><em>*</em>Ward</label>
                                            <div class="input-box">
                                                <input type="text" name="ward" value="${address.ward}" title="Ward" id="ward" class="input-text  required-entry" />
                                            </div>
                                        </div>
                                    </li>
                                    <li class="fields">
                                        <div class="field">
                                            <label for="district" class="required"><em>*</em>District</label>
                                            <div class="input-box">
                                                <input type="text" name="district" value="${address.district}" title="District" id="district" class="input-text  required-entry" />
                                            </div>
                                        </div>
                                    </li>
                                    <li class="fields">
                                        <div class="field">
                                            <label for="city" class="required"><em>*</em>City</label>
                                            <div class="input-box">
                                                <select name="city" id="city" class="validate-select" title="City">
                                                    <c:forEach items="${provinceList}" var="province">
                                                        <option value="${province.code}" <c:if test="${address.city eq province.name}">selected</c:if>>
                                                            ${province.name}
                                                        </option>
                                                    </c:forEach>
                                                </select>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div class="buttons-set">
                                <p class="required">* Required Fields</p>
                                <p class="back-link"><a href="<c:url value="/" />customer/account"><small>&laquo; </small>Back</a> </p>
                                <button data-action="save-customer-address" type="submit" title="Save Address" class="button">
                                    <span><span>Save Address</span></span>
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
                <tiles:insertTemplate template="/client/customer/account/sidebar.jsp" />
                <div class="postscript"></div>
            </div>
        </div>
    </tiles:putAttribute>
</tiles:insertDefinition>
