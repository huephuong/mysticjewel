<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="/admin/layout/taglib.jsp"%>
<tiles:insertDefinition name="page-client">
    <tiles:putAttribute name="title" value="Wishlist Sharing" />
    <tiles:putAttribute name="body-class" value=" customer-account-index  wide cat-sidebar-popup-enabled" />
    <tiles:putAttribute name="page-css">
        <link rel="stylesheet" type="text/css" href="<c:url value="/" />resource/client/media/css/c78a949c76158a4d75f94587ee9c6aab.css" media="all" />
    </tiles:putAttribute>
    <tiles:putAttribute name="page-js" value="" />
    <tiles:putAttribute name="content">
        <div class="top-container">
            <div class="breadcrumbs bctype2">
                <div class="container">
                    <div class="breadcrumbs-inner">
                        <div class="bc-page-title">
                            <h1><span>Wishlist Sharing</span></h1>
                        </div>
                        <ul>
                            <li class="home"> <a href="<c:url value="/" />" title="Go to Home Page">Home</a> <span> / </span> </li>
                            <li class="my-account"> <strong>My Account</strong> </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="main-container col2-left-layout">
            <div class="main-before-top-container"></div>
            <div class="main container">
                <div class="preface"></div>
                <div class="col-main">
                    <div class="clearfix"></div>
                    <tiles:insertTemplate template="/client/layout/toggle.jsp" />
                    <div class="my-account">
                        <div class="my-wishlist">
                            <div class="page-title title-buttons">
                                <h1>Share Your Wishlist</h1>
                            </div>
                            <c:if test="${not empty res_message}">
                                <ul class="messages">
                                    <li class="success-msg">
                                        <ul>
                                            <li><span>${res_message}</span></li>
                                        </ul>
                                    </li>
                                </ul>
                                <c:set var="res_message" value="" scope="session"/>
                            </c:if>
                            <form action="<c:url value="/" />customer/wishlist/share" id="form-validate" method="post">
                                <div class="fieldset">
                                    <h2 class="legend">Sharing Information</h2>
                                    <ul class="form-list">
                                        <li class="wide">
                                            <label for="email" class="required"><em>*</em>Email addresse</label>
                                            <div class="input-box">
                                                <input name="email" id="email" class="input-text validate-email required-entry" />
                                            </div>
                                        </li>
                                        <li class="wide">
                                            <label for="message">Message</label>
                                            <div class="input-box">
                                                <textarea id="message" name="message" cols="60" rows="5"></textarea>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="buttons-set form-buttons">
                                    <p class="required">* Required Fields</p>
                                    <p class="back-link"><a href="<c:url value="/" />customer/account"><small>&laquo; </small>Back</a> </p>
                                    <button type="submit" title="Share Wishlist" class="button">
                                        <span><span>Share Wishlist</span></span>
                                    </button>
                                </div>
                            </form>
                            <script type="text/javascript">
                                //<![CDATA[
                                Validation.addAllThese([
                                    ['validate-emails', 'Please enter a valid email addresses. For example johndoe@domain.com.', function(v) {
                                            if (Validation.get('IsEmpty').test(v)) {
                                                return true;
                                            }
                                            var valid_regexp = /^[a-z0-9\._-]{1,30}@([a-z0-9_-]{1,30}\.){1,5}[a-z]{2,4}$/i;
                                            var emails = v.split(',');
                                            for (var i = 0; i < emails.length; i++) {
                                                if (!valid_regexp.test(emails[i].strip())) {
                                                    return false;
                                                }
                                            }
                                            return true;
                                        }]
                                ]);
                                var dataForm = new VarienForm('form-validate', true);
                                //]]>
                            </script>
                        </div>
                    </div>
                </div>
                <tiles:insertTemplate template="/client/customer/account/sidebar.jsp" />
                <div class="postscript"></div>
            </div>
        </div>
    </tiles:putAttribute>
</tiles:insertDefinition>
