<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="/admin/layout/taglib.jsp"%>
<tiles:insertDefinition name="page-client">
    <tiles:putAttribute name="title" value="My Wishlist" />
    <tiles:putAttribute name="body-class" value=" customer-account-index  wide cat-sidebar-popup-enabled" />
    <tiles:putAttribute name="page-css">
        <link rel="stylesheet" type="text/css" href="<c:url value="/" />resource/client/media/css/c78a949c76158a4d75f94587ee9c6aab.css" media="all" />
    </tiles:putAttribute>
    <tiles:putAttribute name="page-js" value="" />
    <tiles:putAttribute name="content">
        <div class="top-container">
            <div class="breadcrumbs bctype2">
                <div class="container">
                    <div class="breadcrumbs-inner">
                        <div class="bc-page-title">
                            <h1><span>My Wishlist</span></h1>
                        </div>
                        <ul>
                            <li class="home"> <a href="/" title="Go to Home Page">Home</a> <span> / </span> </li>
                            <li class="my-account"> <strong>My Account</strong> </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="main-container col2-left-layout">
            <div class="main-before-top-container"></div>
            <div class="main container">
                <div class="preface"></div>
                <div class="col-main">
                    <div class="clearfix"></div>
                    <tiles:insertTemplate template="/client/layout/toggle.jsp" />
                    <div class="my-account">
                        <div class="my-wishlist">
                            <div class="page-title title-buttons">
                                <h1>My Wishlist</h1>
                            </div>
                            <c:if test="${not empty res_message}">
                                <ul class="messages">
                                    <li class="success-msg">
                                        <ul>
                                            <li><span>${res_message}</span></li>
                                        </ul>
                                    </li>
                                </ul>
                                <c:set var="res_message" value="" scope="session"/>
                            </c:if>
                            <form id="wishlist-view-form" action="#">
                                <fieldset>
                                    <table class="data-table" id="wishlist-table">
                                        <thead>
                                            <tr>
                                                <th></th>
                                                <th>Product Details</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <c:forEach items="${wishlists}" var="wishlist" varStatus="loop">
                                                <tr id="item_${loop.count}">
                                                    <c:set var="sku" value="${wishlist.product.skuList[0]}"/>
                                                    <c:set var="firstImg" value="${wishlist.product.productImageList[0]}"/>
                                                    <td width="1">
                                                        <a class="product-image" href="<c:url value="/product-detail?skuid=${sku.skuID}"/>" target="blank" title="${wishlist.product.productName}"> <img width="149" height="198" src="<c:url value="/" />resource/images/${firstImg.imageLink}" alt="${wishlist.product.productName}" /> </a>
                                                    </td>
                                                    <td>
                                                        <h3 class="product-name"><a href="<c:url value="/product-detail?skuid=${sku.skuID}"/>" target="blank" title="${wishlist.product.productName}">${wishlist.product.productName}</a></h3>
                                                        <div class="description std">
                                                            <div class="inner">${wishlist.product.productDesc}</div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <a href="<c:url value="/" />remove-wishlist?proID=${wishlist.product.productID}" onclick="return confirmRemoveWishlistItem();" title="Remove Item" class="btn-remove btn-remove2">Remove item</a> 
                                                    </td>
                                                </tr>
                                            </c:forEach>
                                        </tbody>
                                    </table>
                                    <script type="text/javascript">
                                        //<![CDATA[
                                        decorateTable('wishlist-table');

                                        function focusComment(obj) {
                                            if (obj.value == 'Please, enter your comments...') {
                                                obj.value = '';
                                            } else if (obj.value == '') {
                                                obj.value = 'Please, enter your comments...';
                                            }
                                        }

                                        function confirmRemoveWishlistItem() {
                                            return confirm('Are you sure you want to remove this product from your wishlist?');
                                        }
                                        //]]>
                                    </script>
                                    <script type="text/javascript">
                                        decorateTable('wishlist-table')
                                    </script>
                                    <div class="buttons-set buttons-set2">
                                        <a href="<c:url value="/" />customer/wishlist/share" title="Share Wishlist" class="button btn-grey btn-share no-margin-left">Share Wishlist</a>
                                    </div>
                                </fieldset>
                            </form>
                            <form id="wishlist-allcart-form" action="<c:url value="/" />wishlist/add-all-to-cart" method="post">
                                <div class="no-display">
                                    <input type="hidden" name="wishlist_id" id="wishlist_id" value="20" />
                                    <input type="hidden" name="qty" id="qty" value="" />
                                </div>
                            </form>
                            <script type="text/javascript">
                                //<![CDATA[
                                var wishlistForm = new Validation($('wishlist-view-form'));
                                var wishlistAllCartForm = new Validation($('wishlist-allcart-form'));

                                function calculateQty() {
                                    var itemQtys = new Array();
                                    $$('#wishlist-view-form .qty').each(function(input, index) {
                                        var idxStr = input.name;
                                        var idx = idxStr.replace(/[^\d.]/g, '');
                                        itemQtys[idx] = input.value;
                                    });
                                    $$('#qty')[0].value = JSON.stringify(itemQtys);
                                }

                                function addAllWItemsToCart() {
                                    calculateQty();
                                    wishlistAllCartForm.form.submit();
                                }
                                //]]>
                            </script>
                        </div>
                        <div class="buttons-set">
                            <p class="back-link"><a href="<c:url value="/" />customer/account/"><small>&laquo; </small>Back</a> </p>
                        </div>
                    </div>
                </div>
                <tiles:insertTemplate template="/client/customer/account/sidebar.jsp" />
                <div class="postscript"></div>
            </div>
        </div>
    </tiles:putAttribute>
</tiles:insertDefinition>
