<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="/admin/layout/taglib.jsp"%>
<tiles:insertDefinition name="page-client">
    <tiles:putAttribute name="title" value="Account Information" />
    <tiles:putAttribute name="body-class" value=" customer-account-index  wide cat-sidebar-popup-enabled" />
    <tiles:putAttribute name="page-css">
        <link rel="stylesheet" type="text/css" href="<c:url value="/" />resource/client/media/css/c78a949c76158a4d75f94587ee9c6aab.css" media="all" />
    </tiles:putAttribute>
    <tiles:putAttribute name="page-js" value="" />
    <tiles:putAttribute name="content">
        <div class="top-container">
            <div class="breadcrumbs bctype2">
                <div class="container">
                    <div class="breadcrumbs-inner">
                        <div class="bc-page-title">
                            <h1><span>My Account</span></h1>
                        </div>
                        <ul>
                            <li class="home"> <a href="/" title="Go to Home Page">Home</a> <span> / </span> </li>
                            <li class="my-account"> <strong>My Account</strong> </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="main-container col2-left-layout">
            <div class="main-before-top-container"></div>
            <div class="main container">
                <div class="preface"></div>
                <div class="col-main">
                    <div class="clearfix"></div>
                    <tiles:insertTemplate template="/client/layout/toggle.jsp" />
                    <div class="my-account">
                        <div class="dashboard">
                            <div class="page-title">
                                <h1>Edit Account Information</h1>
                            </div>
                            <c:if test="${not empty res_message}">
                                <ul class="messages">
                                    <li class="error-msg">
                                        <ul>
                                            <li><span>${res_message}</span></li>
                                        </ul>
                                    </li>
                                </ul>
                                <c:set var="res_message" value="" scope="session"/>
                            </c:if>
                            <form action="<c:url value="/" />customer/account/information" method="post" id="form-validate" autocomplete="off">
                                <div class="fieldset">
                                    <h2>Account Information</h2>
                                    <ul class="form-list">
                                        <li class="fields">
                                            <div class="customer-name">
                                                <div class="field name-firstname">
                                                    <label for="firstname" class="required"><em>*</em>First Name</label>
                                                    <div class="input-box">
                                                        <input type="text" id="firstname" name="firstname" value="${customer.firstName}" title="First Name" maxlength="255" class="input-text required-entry"  />
                                                    </div>
                                                </div>
                                                <div class="field name-lastname">
                                                    <label for="lastname" class="required"><em>*</em>Last Name</label>
                                                    <div class="input-box">
                                                        <input type="text" id="lastname" name="lastname" value="${customer.lastName}" title="Last Name" maxlength="255" class="input-text required-entry"  />
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <ul class="form-list no-margin">
                                            <label for="Gender" class="required"><em>*</em>Gender</label>
                                            <div class="input-box">
                                                <li class="control">
                                                    <input type="radio" name="gender" value="male" ${customer.gender eq 'male' ? 'checked' : ''} class="radio" />
                                                    <label for="Male" class="no-margin">Male</label>
                                                </li>
                                                <li class="control">
                                                    <input type="radio" name="gender" value="female" ${customer.gender eq 'female' ? 'checked' : ''} class="radio" />
                                                    <label for="Female" class="no-margin">Female</label>
                                                </li>
                                            </div>
                                        </ul>
                                        <li>
                                            <label for="dateOfBirth" class="required"><em>*</em>Date Of Birth</label>
                                            <div class="input-box">
                                                <input type="date" name="dateOfBirth" id="dateOfBirth" value="<fmt:formatDate value="${customer.dateOfBirth}" pattern = "YYYY-MM-dd" />" title="Email Address" class="input-text required-entry" />
                                            </div>
                                        </li>
                                        <li>
                                            <label for="phone" class="required"><em>*</em>Phone</label>
                                            <div class="input-box">
                                                <input type="text" id="phone" name="phone" value="${customer.phone1}" title="Phone" class="input-text required-entry"  />
                                            </div>
                                        </li>
                                        <li class="control">
                                            <input type="checkbox" name="change_password" id="change_password" value="1" onclick="setPasswordForm(this.checked)" title="Change Password" checked="checked" class="checkbox" /><label for="change_password">Change Password</label>
                                        </li>
                                    </ul>
                                </div>
                                <div class="fieldset" style="display:none;">
                                    <ul class="form-list">
                                        <li>
                                            <label for="current_password" class="required"><em>*</em>Current Password</label>
                                            <div class="input-box">
                                                <!-- This is a dummy hidden field to trick firefox from auto filling the password -->
                                                <input type="text" class="input-text no-display" name="dummy" id="dummy" />
                                                <input type="password" title="Current Password" class="input-text" name="current_password" id="current_password" />
                                            </div>
                                        </li>
                                        <li class="fields">
                                            <div class="field">
                                                <label for="password" class="required"><em>*</em>New Password</label>
                                                <div class="input-box">
                                                    <input type="password" title="New Password" class="input-text validate-password" name="password" id="password" />
                                                </div>
                                            </div>
                                            <div class="field">
                                                <label for="confirmation" class="required"><em>*</em>Confirm New Password</label>
                                                <div class="input-box">
                                                    <input type="password" title="Confirm New Password" class="input-text validate-cpassword" name="confirmation" id="confirmation" />
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="buttons-set">
                                    <p class="required">* Required Fields</p>
                                    <button type="submit" title="Save" class="button">
                                        <span><span>Save</span></span>
                                    </button>
                                </div>
                            </form>
                            <script type="text/javascript">
                                //<![CDATA[
                                var dataForm = new VarienForm('form-validate', true);
                                function setPasswordForm(arg) {
                                    if (arg) {
                                        $('current_password').up(3).show();
                                        $('current_password').addClassName('required-entry');
                                        $('password').addClassName('required-entry');
                                        $('confirmation').addClassName('required-entry');

                                    } else {
                                        $('current_password').up(3).hide();
                                        $('current_password').removeClassName('required-entry');
                                        $('password').removeClassName('required-entry');
                                        $('confirmation').removeClassName('required-entry');
                                    }
                                }
                                setPasswordForm(true);
                                //]]>
                            </script>
                        </div>
                    </div>
                </div>
                <tiles:insertTemplate template="/client/customer/account/sidebar.jsp" />
                <div class="postscript"></div>
            </div>
        </div>
    </tiles:putAttribute>
</tiles:insertDefinition>
