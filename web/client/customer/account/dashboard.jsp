<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="/admin/layout/taglib.jsp"%>
<tiles:insertDefinition name="page-client">
    <tiles:putAttribute name="title" value="Account dashboard" />
    <tiles:putAttribute name="body-class" value=" customer-account-index  wide cat-sidebar-popup-enabled" />
    <tiles:putAttribute name="page-css">
        <link rel="stylesheet" type="text/css" href="<c:url value="/" />resource/client/media/css/c78a949c76158a4d75f94587ee9c6aab.css" media="all" />
    </tiles:putAttribute>
    <tiles:putAttribute name="page-js" value="" />
    <tiles:putAttribute name="content">
        <div class="top-container">
            <div class="breadcrumbs bctype2">
                <div class="container">
                    <div class="breadcrumbs-inner">
                        <div class="bc-page-title">
                            <h1><span>My Account</span></h1>
                        </div>
                        <ul>
                            <li class="home"> <a href="/" title="Go to Home Page">Home</a> <span> / </span> </li>
                            <li class="my-account"> <strong>My Account</strong> </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="main-container col2-left-layout">
            <div class="main-before-top-container"></div>
            <div class="main container">
                <div class="preface"></div>
                <div class="col-main">
                    <div class="clearfix"></div>
                    <tiles:insertTemplate template="/client/layout/toggle.jsp" />
                    <div class="my-account">
                        <div class="dashboard">
                            <div class="page-title">
                                <h1>My Dashboard</h1>
                            </div>
                            <c:if test="${not empty res_message}">
                                <ul class="messages">
                                    <li class="success-msg">
                                        <ul>
                                            <li><span>${res_message}</span></li>
                                        </ul>
                                    </li>
                                </ul>
                                <c:set var="res_message" value="" scope="session"/>
                            </c:if>
                            <div class="welcome-msg">
                                <p class="hello"><strong>Hello, ${customer.firstName} ${customer.lastName}!</strong> </p>
                                <p>From your My Account Dashboard you have the ability to view a snapshot of your recent account activity and update your account information. Select a link below to view or edit information.</p>
                            </div>
                            <c:if test="${not empty orderMaster}">
                                <div class="box-account box-recent">
                                    <div class="box-head">
                                        <h2>Recent Orders</h2> <a href="/sales/order/history/">View All</a>
                                    </div>
                                    <div class="my-orders-table-wrapper">
                                        <table class="data-table" id="my-orders-table">
                                            <thead>
                                                <tr>
                                                    <th>Order #</th>
                                                    <th>Date</th>
                                                    <th>Ship To</th>
                                                    <th><span class="nobr">Order Total</span> </th>
                                                    <th>Status</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <c:forEach begin="0" end="2" items="${orderMaster}" var="order">
                                                    <tr>
                                                        <td>${order.orderID}</td>
                                                        <td><span class="nobr">
                                                                <fmt:formatDate type="date" value="${order.dateCreated}" pattern="dd/MM/yyyy" />
                                                            </span>
                                                        </td>
                                                        <td>${order.shipName}</td>
                                                        <td>
                                                            <span class="price">
                                                                <fmt:setLocale value="en_US"/>
                                                                <fmt:formatNumber value="${order.grandTotal}" type="currency"/>
                                                            </span>
                                                        </td>
                                                        <td>
                                                            <em>
                                                                ${order.orderStatus}
                                                            </em>
                                                        </td>
                                                        <td class="a-center">
                                                            <span class="nobr">
                                                                <a href="">Detail</a>
                                                                <a href="<c:url value="/customer/order/detail?orderid=${order.orderID}"/>">Detail</a>
                                                                <c:if test="${order.orderStatus != 'Completed' && order.orderStatus != 'Delivering' && order.orderStatus != 'Canceled'}">
                                                                    <span class="separator">|</span>
                                                                    <a href="<c:url value="/customer/order/cancel?orderid=${order.orderID}"/>">Cancel</a>
                                                                </c:if>
                                                                <c:if test="${order.orderStatus == 'Awaiting Payment'}">
                                                                    <span class="separator">|</span>
                                                                    <a href="<c:url value="/place-order/payment?orderid=${order.orderID}"/>">Payment</a>
                                                                </c:if>
                                                            </span>
                                                        </td>
                                                    </c:forEach>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <script type="text/javascript">
                                            decorateTable('my-orders-table')
                                        </script>
                                    </div>
                                </div>
                            </c:if>
                            <div class="box-account box-info">
                                <div class="box-head">
                                    <h2>Account Dashboard</h2>
                                </div>
                                <div class="col2-set">
                                    <div class="box">
                                        <div class="box-title">
                                            <h3 class="r-separator r-separator-2"><span>Contact Information</span></h3> <a href="<c:url value="/" />customer/account/information">Edit</a>
                                        </div>
                                        <div class="box-content">
                                            <p>
                                                Name: ${customer.firstName} ${customer.lastName}<br/>
                                                Email: ${customer.email}<br/>
                                                Phone: ${customer.phone1}<br/>
                                                <a href="<c:url value="/" />customer/account/information">Change Info</a>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col2-set">
                                    <div class="box">
                                        <div class="box-title">
                                            <h3 class="r-separator r-separator-2"><span>Address Book</span></h3> <a href="<c:url value="/" />customer/account/address">New</a>
                                        </div>
                                        <div class="box-content">
                                            <c:set var="numCols" value="3"/>
                                            <c:set var="numRows" value="3"/>
                                            <c:set var="rowCount" value="0"/>
                                            <div class="col-1">
                                                <c:forEach items="${customer.customerAddressList}" var="address" varStatus="status">
                                                    <c:if test="${rowCount lt numRows}">
                                                        <h4>Default Shipping Address</h4>
                                                        <address>
                                                            Number: ${address.number}<br/>
                                                            Street: ${address.street}<br/>
                                                            Ward: ${address.ward}<br/>
                                                            District: ${address.district}<br/>
                                                            City: ${address.city}<br/>
                                                            <c:if test="${not empty customerAddress}">
                                                                You have not set a default shipping address.<br/>
                                                            </c:if>
                                                            <a href="<c:url value="/" />customer/account/address?addressID=${address.addressID}">Edit Address</a>
                                                        </address>
                                                        <c:if test="${status.count ne 0 && status.count % numCols == 0}">
                                                            <c:set var="rowCount" value="${rowCount + 1}"/>
                                                        </div>
                                                        <div class="col-2">
                                                        </c:if>
                                                    </c:if>
                                                </c:forEach>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <tiles:insertTemplate template="/client/customer/account/sidebar.jsp" />
                <div class="postscript"></div>
            </div>
        </div>
    </tiles:putAttribute>
</tiles:insertDefinition>
