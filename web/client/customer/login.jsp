<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="/admin/layout/taglib.jsp"%>
<tiles:insertDefinition name="page-client">
    <tiles:putAttribute name="title" value="Login" />
    <tiles:putAttribute name="body-class" value=" customer-account-login  wide" />
    <tiles:putAttribute name="page-css">
        <link rel="stylesheet" type="text/css" href="<c:url value="/" />resource/client/media/css/c78a949c76158a4d75f94587ee9c6aab.css" media="all" />
    </tiles:putAttribute>
    <tiles:putAttribute name="page-js">
        <script type="text/javascript" src="resource/client/js/mage/captcha.js"></script>
    </tiles:putAttribute>
    <tiles:putAttribute name="content">
        <div class="top-container">
            <div class="breadcrumbs bctype2">
                <div class="container">
                    <div class="breadcrumbs-inner">
                        <div class="bc-page-title">
                            <h1><span>Customer Login</span></h1>
                        </div>
                        <ul>
                            <li class="home"> <a href="/" title="Go to Home Page">Home</a> <span> / </span> </li>
                            <li class="account-login"> <strong>Login</strong> </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="main-container col1-layout">
            <div class="main-before-top-container"></div>
            <div class="main container">
                <div class="preface"></div>
                <div class="col-main">
                    <div class="clearfix"></div>
                    <tiles:insertTemplate template="/client/layout/toggle.jsp" />
                    <div class="account-login">
                        <div class="page-title">
                            <h1>Login or Create an Account</h1>
                        </div>
                        <c:if test="${not empty res_message}">
                            <ul class="messages">
                                <li class="${res_status}-msg">
                                    <ul>
                                        <li>
                                            <span>${res_message}</span>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                            <c:set var="res_message" value="" scope="session"/>
                        </c:if>
                        <form action="<c:url value="/" />login" method="post" id="login-form">
                            <div class="col2-set">
                                <div class="col-1 registered-users grid-xs-full">
                                    <div class="content">
                                        <h2 class="r-separator"><span>Login</span></h2>
                                        <ul class="form-list">
                                            <li>
                                                <label for="email" class="required"><em>*</em>Email Address</label>
                                                <div class="input-box">
                                                    <input type="text" name="email" value="" id="email" class="input-text required-entry validate-email" title="Email Address" />
                                                </div>
                                            </li>
                                            <li>
                                                <label for="pass" class="required"><em>*</em>Password</label>
                                                <div class="input-box">
                                                    <input type="password" name="password" class="input-text required-entry validate-password" id="pass" title="Password" />
                                                </div>
                                            </li>
                                            <li>
                                                <div class="input-box input-checkbox">
                                                    <input type="checkbox" id="eg-licence" name="remember" />
                                                    <label for="eg-remember">Remember me </label>
                                                </div>
                                            </li>
                                        </ul>
                                        <div id="window-overlay" class="window-overlay" style="display:none;"></div>
                                        <div id="remember-me-popup" class="remember-me-popup" style="display:none;">
                                            <div class="remember-me-popup-head">
                                                <h3>What's this?</h3> <a href="#" class="remember-me-popup-close" title="Close">Close</a>
                                            </div>
                                            <div class="remember-me-popup-body">
                                                <p>Checking &quot;Remember Me&quot; will let you access your shopping cart on this computer when you are logged out</p>
                                                <div class="remember-me-popup-close-button a-right"> <a href="#" class="remember-me-popup-close button" title="Close"><span>Close</span></a> </div>
                                            </div>
                                        </div>
                                        <script type="text/javascript">
                                            //<![CDATA[
                                            function toggleRememberMepopup(event) {
                                                if ($('remember-me-popup')) {
                                                    var viewportHeight = document.viewport.getHeight(),
                                                            docHeight = $$('body')[0].getHeight(),
                                                            height = docHeight > viewportHeight ? docHeight : viewportHeight;
                                                    $('remember-me-popup').toggle();
                                                    $('window-overlay').setStyle({
                                                        height: height + 'px'
                                                    }).toggle();
                                                }
                                                Event.stop(event);
                                            }
                                            document.observe("dom:loaded", function() {
                                                new Insertion.Bottom($$('body')[0], $('window-overlay'));
                                                new Insertion.Bottom($$('body')[0], $('remember-me-popup'));
                                                $$('.remember-me-popup-close').each(function(element) {
                                                    Event.observe(element, 'click', toggleRememberMepopup);
                                                })
                                                $$('#remember-me-box a').each(function(element) {
                                                    Event.observe(element, 'click', toggleRememberMepopup);
                                                });
                                            });
                                            //]]>
                                        </script>
                                        <p class="required"><em>*</em> Required Fields</p>
                                    </div>
                                    <div class="buttons-set"> <a href="<c:url value="/" />forgot-password" class="f-left">Forgot Your Password?</a>
                                        <button type="submit" class="button" title="Login" name="send" id="send2">
                                            <span><span>Login</span></span>
                                        </button>
                                    </div>
                                    <style type="text/css">
                                        @media (min-width: 768px) {
                                            .omb_row-sm-offset-3 div:first-child[class*="col-"] {
                                                margin-left: 25%;
                                            }
                                        }

                                        .omb_login .omb_authTitle {
                                            text-align: center;
                                            line-height: 300%;
                                        }

                                        .omb_login .omb_socialButtons a {
                                            color: white; // In yourUse @body-bg 
                                            opacity: 0.9;
                                        }

                                        .omb_login .omb_socialButtons a:hover {
                                            color: white;
                                            opacity: 1;
                                        }

                                        .omb_login .omb_socialButtons .omb_btn-facebook {
                                            background: #3b5998;
                                        }

                                        .omb_login .omb_socialButtons .omb_btn-twitter {
                                            background: #00aced;
                                        }

                                        .omb_login .omb_socialButtons .omb_btn-google {
                                            background: #c32f10;
                                        }

                                        .omb_login .omb_loginOr {
                                            position: relative;
                                            font-size: 1.5em;
                                            color: #aaa;
                                            margin-top: 1em;
                                            margin-bottom: 1em;
                                            padding-top: 0.5em;
                                            padding-bottom: 0.5em;
                                        }

                                        .omb_login .omb_loginOr .omb_hrOr {
                                            background-color: #cdcdcd;
                                            height: 1px;
                                            margin-top: 0px !important;
                                            margin-bottom: 0px !important;
                                        }

                                        .omb_login .omb_loginOr .omb_spanOr {
                                            display: block;
                                            position: absolute;
                                            left: 50%;
                                            top: -0.6em;
                                            margin-left: -1.5em;
                                            background-color: white;
                                            width: 3em;
                                            text-align: center;
                                        }

                                        .omb_login .omb_loginForm .input-group.i {
                                            width: 2em;
                                        }

                                        .omb_login .omb_loginForm .help-block {
                                            color: red;
                                        }

                                        @media (min-width: 768px) {
                                            .omb_login .omb_forgotPwd {
                                                text-align: right;
                                                margin-top: 10px;
                                            }
                                        }
                                    </style>
                                    <div class="omb_login">
                                        <div class="row omb_row-sm-offset-3 omb_loginOr">
                                            <div class="col-xs-12 col-sm-6">
                                                <hr class="omb_hrOr"> <span class="omb_spanOr">or</span> </div>
                                        </div>
                                        <div class="row omb_row-sm-offset-3 omb_socialButtons">
                                            <div class="col-xs-4 col-sm-3">
                                                <a href="javascript:void(0)" onclick="window.open('${fbAuthUrl}','Login Facebook','menubar=1,resizable=1,width=700,height=500')" class="btn btn-lg btn-block omb_btn-facebook"> <i class="fa fa-facebook visible-xs"></i> <span class="hidden-xs">Facebook</span> </a>
                                            </div>
                                            <div class="col-xs-4 col-sm-3">
                                                <a href="javascript:void(0)" onclick="window.open('${ggAuthUrl}','Login Google','menubar=1,resizable=1,width=700,height=500')" class="btn btn-lg btn-block omb_btn-google"> <i class="fa fa-google-plus visible-xs"></i> <span class="hidden-xs">Google+</span> </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-2 new-users grid-xs-full">
                                    <div class="content">
                                        <h2 class="r-separator"><span>New Customers</span></h2>
                                        <p>By creating an account with our store, you will be able to move through the checkout process faster, store multiple shipping addresses, view and track your orders in your account and more.</p>
                                    </div>
                                    <div class="buttons-set">
                                        <button type="button" title="Create an Account" class="button" onclick="window.location = '<c:url value="/" />register';">
                                            <span><span>Create an Account</span></span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <script type="text/javascript">
                            //<![CDATA[
                            var dataForm = new VarienForm('login-form', true);
                            //]]>
                        </script>
                    </div>
                </div>
                <div class="postscript"></div>
            </div>
        </div>
    </tiles:putAttribute>
</tiles:insertDefinition>
