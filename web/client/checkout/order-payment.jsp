<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="/admin/layout/taglib.jsp"%>
<tiles:insertDefinition name="page-client">
    <tiles:putAttribute name="title" value="Cart" />
    <tiles:putAttribute name="body-class" value="" />
    <tiles:putAttribute name="page-css">
        <link rel="stylesheet" type="text/css" href="<c:url value="/" />resource/client/media/css/608a0436af3da7766b9a1705fe393cd8.css" media="all" />
        <link rel="stylesheet" type="text/css" href="<c:url value="/"/>resource/client/media/css/c78a949c76158a4d75f94587ee9c6aab.css" media="all" />
        <style type="text/css" media="screen">
            .has-error input {
                border-width: 2px;
            }

            .validation.text-danger:after {
                content: 'Validation failed';
            }

            .validation.text-success:after {
                content: 'Validation passed';
            }
        </style>
    </tiles:putAttribute>
    <tiles:putAttribute name="page-js">
        <script src="<c:url value="/resource/client/js/payment/jquery.payment.js"/>"></script>
        <script>
            jQuery(function($) {
                $('.cc-number').payment('formatCardNumber');
                $('.cc-exp').payment('formatCardExpiry');
                $('.cc-cvc').payment('formatCardCVC');

                $.fn.toggleInputError = function(erred) {
                    this.parent('.form-group').toggleClass('has-error', erred);
                    return this;
                };

                $('#payment-form').submit(function(e) {
                    var cardType = $.payment.cardType($('.cc-number').val());
                    $('.cc-number').toggleInputError(!$.payment.validateCardNumber($('.cc-number').val()));
                    $('.cc-exp').toggleInputError(!$.payment.validateCardExpiry($('.cc-exp').payment('cardExpiryVal')));
                    $('.cc-cvc').toggleInputError(!$.payment.validateCardCVC($('.cc-cvc').val(), cardType));
                    $('.cc-brand').text(cardType);
                    $('.validation').removeClass('text-danger text-success');
                    
                    if ($('.has-error').length) {
                        e.preventDefault();
                    }
                    $('.validation').addClass($('.has-error').length ? 'text-danger' : 'text-success');
                });

            });
        </script>
    </tiles:putAttribute>
    <tiles:putAttribute name="content">
        <div class="top-container">
            <div class="breadcrumbs bctype2">
                <div class="container">
                    <div class="breadcrumbs-inner">
                        <div class="bc-page-title">
                            <h1><span>Checkout</span></h1>
                        </div>
                        <ul>
                            <li class="home"> <a href="/" title="Go to Home Page">Home</a> <span> / </span> </li>
                            <li class="checkout"> <strong>Checkout</strong> </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="main-container col1-layout">
            <div class="main-before-top-container"></div>
            <div class="main container">
                <div class="preface"></div>
                <div class="col-main">
                    <div class="clearfix"></div>
                    <tiles:insertTemplate template="/client/layout/toggle.jsp" />
                    <div class="page-title category-title">
                        <h1>Card Payment</h1>
                    </div>
                    <div id="eglayerednav_container">
                        <div class="category-products">
                            <div class="toolbar clearfix">
                                <div class="toolbar-upside">
                                    <h3 class="text-center">Card Payment</h3>
                                </div>
                            </div>
                            <h4 class="text text-success">Please specify your payment information</h4>
                            <input type="hidden" name="orderid" value="${currentOrder.orderID}">
                            <div class="col-xs-5">
                                <form id="payment-form" novalidate autocomplete="on" method="POST" action="<c:url value="/place-order/payment"/>">
                                    <input type="hidden" name="orderid" value="${currentOrder.orderID}">
                                    <div class="form-group">
                                        <label for="cc-number" class="control-label"><strong>Card number </strong><span class="text-primary">[<span class="cc-brand"></span>]</span></label>
                                        <input id="cc-number" type="tel" class="input-lg form-control cc-number" autocomplete="cc-number" placeholder="•••• •••• •••• ••••" required>
                                    </div>

                                    <div class="form-group">
                                        <label for="cc-exp" class="control-label"><strong>Card expiry </strong></label>
                                        <input id="cc-exp" type="tel" class="input-lg form-control cc-exp" autocomplete="cc-exp" placeholder="•• / ••" required>
                                    </div>

                                    <div class="form-group">
                                        <label for="cc-cvc" class="control-label"><strong>Card CVC </strong></label>
                                        <input id="cc-cvc" type="tel" class="input-lg form-control cc-cvc" autocomplete="off" placeholder="•••" required>
                                    </div>

                                    <button type="submit" class="button"><span><span>Complete Payment</span></span></button>

                                    <h5 class="validation"></h5>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="postscript"></div>
            </div>
        </div>
    </tiles:putAttribute>
</tiles:insertDefinition>
