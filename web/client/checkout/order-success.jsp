<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="/admin/layout/taglib.jsp"%>
<tiles:insertDefinition name="page-client">
    <tiles:putAttribute name="title" value="Cart" />
    <tiles:putAttribute name="body-class" value="" />
    <tiles:putAttribute name="page-css">
        <link rel="stylesheet" type="text/css" href="<c:url value="/" />resource/client/media/css/cac245d5f9ec2b4cc971c4dbee1fcb61.css" media="all" />
    </tiles:putAttribute>
    <tiles:putAttribute name="page-js">
        <script type="text/javascript"></script>
    </tiles:putAttribute>
    <tiles:putAttribute name="content">
        <div class="top-container">
            <div class="breadcrumbs bctype2">
                <div class="container">
                    <div class="breadcrumbs-inner">
                        <div class="bc-page-title">
                            <h1><span>Checkout</span></h1>
                        </div>
                        <ul>
                            <li class="home"> <a href="/" title="Go to Home Page">Home</a> <span> / </span> </li>
                            <li class="checkout"> <strong>Checkout</strong> </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="main-container col1-layout">
            <div class="main-before-top-container"></div>
            <div class="main container">
                <div class="preface"></div>
                <div class="col-main">
                    <div class="clearfix"></div>
                    <tiles:insertTemplate template="/client/layout/toggle.jsp" />
                    <div class="cart-empty">
                        <h2>Thank you for purchasing!</h2>
                        <p>Your order is being processed. Tracking number: <span style="color: #00ad9c;">${newOrder.orderID}</span></p>
                        <p>You can track your order <a href="<c:url value="/order-detail?orderid=${newOrder.orderID}"/>" class="theme-color">HERE</a></p>
                    </div>
                </div>
                <div class="postscript"></div>
            </div>
        </div>
    </tiles:putAttribute>
</tiles:insertDefinition>
