<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="/admin/layout/taglib.jsp"%>
<tiles:insertDefinition name="page-client">
    <tiles:putAttribute name="title" value="Cart" />
    <tiles:putAttribute name="body-class" value=" checkout-cart-index  wide" />
    <tiles:putAttribute name="page-css">
        <link rel="stylesheet" type="text/css" href="<c:url value="/" />resource/client/media/css/cac245d5f9ec2b4cc971c4dbee1fcb61.css" media="all" />
        <link rel="stylesheet" type="text/css" href="<c:url value="/" />resource/client/media/css/c78a949c76158a4d75f94587ee9c6aab.css" media="all" />
    </tiles:putAttribute>
    <tiles:putAttribute name="page-js">
        <script type="text/javascript" src="<c:url value="/" />resource/client/js/varien/weee.js"></script>
    </tiles:putAttribute>
    <tiles:putAttribute name="content">
        <div class="top-container">
            <div class="breadcrumbs bctype2">
                <div class="container">
                    <div class="breadcrumbs-inner">
                        <div class="bc-page-title">
                            <h1><span>Shopping Cart</span></h1>
                        </div>
                        <ul>
                            <li class="home"> <a href="/" title="Go to Home Page">Home</a> <span> / </span> </li>
                            <li class="shopping-cart"> <strong>Shopping Cart</strong> </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="main-container col1-layout">
            <div class="main-before-top-container"></div>
            <div class="main container">
                <div class="preface"></div>
                <div class="col-main">
                    <div class="clearfix"></div>
                    <c:if test="${not empty res_message}">
                        <ul class="messages">
                            <li class="${res_status}-msg">
                                <ul>
                                    <li>
                                        <span>${res_message}</span>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                        <c:set var="res_message" value="" scope="session"/>
                    </c:if>
                    <tiles:insertTemplate template="/client/layout/toggle.jsp" />
                    <c:choose>
                        <c:when test="${empty cartItems}">
                            <div class="cart-empty">
                                <h2>Shopping Cart is Empty</h2>
                                <p>You have no items in your shopping cart.</p>
                                <p>Click <a href="/" class="theme-color">HERE</a> to continue shopping.</p>
                            </div>
                        </c:when>    
                        <c:otherwise>
                            <div class="cart">
                                <div class="page-title title-buttons">
                                    <h1>Shopping Cart</h1>
                                    <ul class="checkout-types" style="display:none;">
                                        <li>
                                            <button type="button" title="Proceed to Checkout" class="button btn-proceed-checkout btn-checkout" onclick="window.location = '../onepage/index.html';">
                                                <span><span>Proceed to Checkout</span></span>
                                            </button>
                                        </li>
                                    </ul>
                                </div>
                                <form action="<c:url value="/" />update-product-to-cart" method="post">
                                    <fieldset>
                                        <table id="shopping-cart-table" class="data-table cart-table">
                                            <tfoot>
                                                <tr>
                                                    <td colspan="50" class="a-right">
                                                        <button type="button" title="Continue Shopping" class="button btn-continue btn-grey" onclick="setLocation('/')"><span><span>Continue Shopping</span></span>
                                                        </button>
                                                        <button type="submit" name="update_cart_action" value="update_qty" title="Update Shopping Cart" class="button btn-update"><span><span>Update Shopping Cart</span></span>
                                                        </button>
                                                        <!--[if lt IE 8]>
                                                        <input type="hidden" id="update_cart_action_container" />
                                                        <script type="text/javascript">
                                                        //<![CDATA[
                                                        Event.observe(window, 'load', function()
                                                        {
                                                            // Internet Explorer (lt 8) does not support value attribute in button elements
                                                            $emptyCartButton = $('empty_cart_button');
                                                            $cartActionContainer = $('update_cart_action_container');
                                                            if ($emptyCartButton && $cartActionContainer) {
                                                                Event.observe($emptyCartButton, 'click', function()
                                                                {
                                                                    $emptyCartButton.setAttribute('name', 'update_cart_action_temp');
                                                                    $cartActionContainer.setAttribute('name', 'update_cart_action');
                                                                    $cartActionContainer.setValue('empty_cart');
                                                                });
                                                            }

                                                        });
                                                        //]]>
                                                        </script>
                                                        <![endif]-->
                                                    </td>
                                                </tr>
                                            </tfoot>
                                            <thead>
                                                <tr>
                                                    <th class="no-border-right" rowspan="1">&nbsp;</th>
                                                    <th rowspan="1"><span class="nobr">Product Name</span> </th>
                                                    <th class="a-center" colspan="1"><span class="nobr">Unit Price</span> </th>
                                                    <th rowspan="1" class="a-center">Qty</th>
                                                    <th class="a-center" colspan="1">Subtotal</th>
                                                    <th rowspan="1" class="a-center">
                                                        <a type="submit" title="Clear Shopping Cart" class="btn-empty btn-remove" onclick="ajaxRemoveCart('')"></a>
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <c:forEach items="${cartItems}" var="cartItem">
                                                    <tr>
                                                        <td class="no-padding-right no-border-right">
                                                            <a href="" title="${cartItem.key.productID.productName}" class="product-image"><img height="100" width="100" src="<c:url value="/"/>resource/images/${cartItem.key.productID.productImageList[0].imageLink}" alt="${cartItem.key.productID.productName}" /> </a>
                                                        </td>
                                                        <td style="padding-left:10px !important;">
                                                            <h2 class="product-name">
                                                                <a href="">${cartItem.key.productID.productName}</a>
                                                            </h2>
                                                        </td>
                                                        <td class="a-center">
                                                            <span class="cart-price">
                                                                <span class="price">
                                                                    <c:if test="${cartItem.key.productID.discountValue gt 0}">
                                                                        <span class="price" style="color: #cda85c">
                                                                            <fmt:setLocale value="en_US"/>
                                                                            <fmt:formatNumber value="${cartItem.key.unitPrice - (cartItem.key.unitPrice * cartItem.key.productID.discountValue / 100)}" type="currency"/>
                                                                        </span>
                                                                    </c:if>
                                                                    <c:if test="${cartItem.key.productID.discountValue eq 0}">
                                                                        <span class="price">
                                                                            <fmt:setLocale value="en_US"/>
                                                                            <fmt:formatNumber value="${cartItem.key.unitPrice}" type="currency"/>
                                                                        </span>
                                                                    </c:if>
                                                                </span>
                                                            </span>
                                                        </td>
                                                        <!-- inclusive price starts here -->
                                                        <td class="a-center">
                                                            <div class="qty-wrapper">
                                                                <input type="number" name="qty" value="${cartItem.value}" onchange="ajaxUpdateCart('${cartItem.key.skuID}', this.value)" size="4" title="Qty" class="input-text qty" maxlength="2" />
                                                                <div class="qty-btn qty-up-btn" onclick="ajaxUpdateCart('${cartItem.key.skuID}')">+</div>
                                                                <div class="qty-btn qty-down-btn" onclick="ajaxUpdateCart('${cartItem.key.skuID}')">-</div>
                                                            </div>
                                                        </td>
                                                        <!--Sub total starts here -->
                                                        <td class="a-center"> <span class="cart-price">
                                                                <span class="price">
                                                                    <c:if test="${cartItem.key.productID.discountValue gt 0}">
                                                                        <span class="price" style="color: #cda85c">
                                                                            <fmt:setLocale value="en_US"/>
                                                                            <fmt:formatNumber value="${(cartItem.key.unitPrice - (cartItem.key.unitPrice * cartItem.key.productID.discountValue / 100)) * cartItem.value}" type="currency"/>
                                                                        </span>
                                                                    </c:if>
                                                                    <c:if test="${cartItem.key.productID.discountValue eq 0}">
                                                                        <span class="price">
                                                                            <fmt:setLocale value="en_US"/>
                                                                            <fmt:formatNumber value="${(cartItem.key.unitPrice) * cartItem.value}" type="currency"/>
                                                                        </span>
                                                                    </c:if>
                                                                </span>
                                                            </span>
                                                        </td>
                                                        <td class="a-center">
                                                            <a onclick="ajaxRemoveCart('${cartItem.key.skuID}')" title="Remove item" class="btn-remove btn-remove2"></a>
                                                        </td>
                                                    </tr>
                                                </c:forEach>
                                            </tbody>
                                        </table>
                                        <script type="text/javascript">
                                            decorateTable('shopping-cart-table');

                                            function ajaxUpdateCart(skuID, qty) {
                                                jQuery.get('<c:url value="/" />update-product-to-cart', {
                                                    skuID: skuID,
                                                    qty: qty
                                                });
                                                location.reload();
                                            }

                                            function ajaxRemoveCart(skuID) {
                                                jQuery.get('<c:url value="/" />remove-product-to-cart', {
                                                    skuID: skuID
                                                });
                                                location.reload();
                                            }
                                        </script>
                                    </fieldset>
                                </form>
                                <div class="cart-collaterals">
                                    <div class="grid-container">
                                        <div class="grid12-6 grid-sm-full grid-xs-full">
                                            <div class="grid-container">
                                                <div class="grid-full">
                                                    <form id="discount-coupon-form" action="<c:url value="/checkout/apply-voucher"/>" method="post">
                                                        <div class="discount">
                                                            <h2>Have a Voucher?</h2>
                                                            <div class="discount-form clearfix">
                                                                <c:choose>
                                                                    <c:when test="${not empty customer}">
                                                                        <p class="text text-danger" style="margin-bottom: 10px">${voucherError}</p>
                                                                        <label class="no-display" for="coupon_code">Enter your voucher code if you have one.</label>
                                                                        <div class="input-box grid12-8 no-padding no-margin inline-block left">
                                                                            <input class="input-text required-entry" id="coupon_code" placeholder="Voucher Code" name="coupon_code" value="" />
                                                                        </div>
                                                                        <div class="buttons-set right no-margin inline-block">
                                                                            <button type="submit" title="Apply Coupon" class="button" value="Apply Voucher">
                                                                                <span><span>Apply Voucher</span></span>
                                                                            </button>
                                                                        </div>
                                                                    </c:when>
                                                                    <c:otherwise>
                                                                        <h5>Please log in to use voucher</h5>
                                                                    </c:otherwise>
                                                                </c:choose>
                                                            </div>
                                                        </div>
                                                    </form>
                                                    <script type="text/javascript">
                                                        var discountForm = new VarienForm('discount-coupon-form', true);
                                                    </script>
                                                    <div class="shipping">
                                                        <h2>Estimate Shipping Fee</h2>
                                                        <div class="reference">
                                                            <p class="no-margin eg-fsi"><a href="#" class="slide-trigger shipping-trigger">Click here</a> to calculate shipping and tax, based on your location or shipping address.</p>
                                                        </div>
                                                        <div class="shipping-form" style="display:none;">
                                                            <form action="<c:url value="/checkout/estimate-shipping-fee"/>" method="post" id="shipping-zip-form">
                                                                <p class="no-display">Enter your destination to get a shipping estimate.</p>
                                                                <ul class="form-list">
                                                                    <li class="wide">
                                                                        <c:if test="${not empty customer}">
                                                                            <c:forEach items="${customer.customerAddressList}" var="address">
                                                                                <p>
                                                                                    &nbsp;&nbsp;<input type="radio" name="chooseAddress" value="${address.addressID}">
                                                                                    <label class="radio-inline">${address.number} ${address.street}, Ward ${address.ward}, District ${address.district}, ${address.city}</label>
                                                                                </p>
                                                                            </c:forEach>
                                                                        </c:if>
                                                                        <p>
                                                                            &nbsp;&nbsp;<input type="radio" name="chooseAddress" value="new" checked>
                                                                            <label class="radio-inline">Unsaved Address</label>
                                                                        <p>
                                                                        </li>
                                                                        <!-- Number -->
                                                                        <li class="wide">
                                                                            <input type="text" id="billing-number" name="billing-number" value="${currentAddress.number}" placeholder="Number" class="input-text required-entry"/>
                                                                        </li>
                                                                        <!-- Street -->
                                                                        <li class="wide">
                                                                            <input type="text" id="billing-street" name="billing-street" value="${currentAddress.street}" placeholder="Street" class="input-text required-entry"/>
                                                                        </li>
                                                                        <!-- Ward -->
                                                                        <li class="wide">
                                                                            <input type="text" id="billing-ward" name="billing-ward" value="${currentAddress.ward}" placeholder="Ward" class="input-text required-entry"/>
                                                                        </li>
                                                                        <!-- District -->
                                                                        <li class="wide">
                                                                            <input type="text" id="billing-district" name="billing-district" value="${currentAddress.district}" placeholder="District" class="input-text required-entry"/>
                                                                        </li>
                                                                        <!-- Province -->
                                                                        <li class="wide">
                                                                            <div class="input-box">
                                                                                <select id="billing-city" name="billing-city" title="City/Province">
                                                                                    <c:forEach items="${provinceList}" var="province">
                                                                                        <option value="${province.code}" <c:if test="${currentAddress.city eq province.name}">selected</c:if>>
                                                                                            ${province.name}
                                                                                        </option>
                                                                                    </c:forEach>
                                                                                </select>
                                                                            </div>
                                                                        </li>
                                                                        <li class="wide">
                                                                            <div class="input-box">
                                                                                <div class="buttons-set">
                                                                                    <button type="submit" class="button"><span><span>Calculate Fee</span></span>
                                                                                    </button>
                                                                                </div>
                                                                            </div>
                                                                        </li>
                                                                    </ul>
                                                                </form>
                                                                <script type="text/javascript">
                                                                    //<![CDATA[
                                                                    jQuery(function($) {
                                                                        $('.shipping-trigger').click(function(e) {
                                                                            e.preventDefault();
                                                                            $('.shipping-form').slideToggle('slow');
                                                                        });
                                                                    });
                                                                    //]]>
                                                                </script>
                                                            </div>
                                                        </div>
                                                        <script type="text/javascript">
                                                            //<![CDATA[
                                                            jQuery(function($) {
                                                                $('.crosssell-trigger').click(function(e) {
                                                                    e.preventDefault();
                                                                    $('.crosssell-list').slideToggle('slow');
                                                                });
                                                                $('#crosssell-products-list').owlCarousel({
                                                                    lazyLoad: true,
                                                                    responsive: true,
                                                                    singleItem: false,
                                                                    items: 3,
                                                                    responsiveRefreshRate: 50,
                                                                    autoPlay: true,
                                                                    stopOnHover: true,
                                                                    rewindNav: true,
                                                                    rewindSpeed: 600,
                                                                    pagination: false,
                                                                    navigation: false,
                                                                    navigationText: ["<span class='fa fa-angle-left'></span>", "<span class='fa fa-angle-right'></span>"]
                                                                });
                                                            });
                                                            //]]>
                                                        </script>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="grid12-6 grid-sm-full grid-xs-full">
                                                <div class="grid-container">
                                                    <div class="grid-full totals">
                                                        <h2 class="totals-subtitle">Cart Totals</h2>
                                                        <table id="shopping-cart-totals-table">
                                                            <col />
                                                            <col />
                                                            <tfoot>
                                                                <tr>
                                                                    <td style="" class="a-right" colspan="1"> <strong>Grand Total</strong> </td>
                                                                    <td style="" class="a-right">
                                                                        <strong>
                                                                            <span class="price">
                                                                                <fmt:setLocale value="en_US"/>
                                                                                <fmt:formatNumber value="${subTotal + (subTotal * 0.1) + currentFee - (subTotal * currentVoucher.discountValue / 100)}" type="currency"/>
                                                                            </span>
                                                                        </strong>
                                                                    </td>
                                                                </tr>
                                                            </tfoot>
                                                            <tbody>
                                                                <tr>
                                                                    <td style="" class="a-right" colspan="1"> Subtotal </td>
                                                                    <td style="" class="a-right">
                                                                        <span class="price">
                                                                            <fmt:setLocale value="en_US"/>
                                                                            <fmt:formatNumber value="${subTotal}" type="currency"/>
                                                                        </span>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                            <tbody>
                                                                <tr>
                                                                    <td style="" class="a-right" colspan="1"> Voucher Discount </td>
                                                                    <td style="" class="a-right">
                                                                        <span class="price">
                                                                            <fmt:setLocale value="en_US"/>
                                                                            - <fmt:formatNumber value="${currentVoucher.discountValue * subTotal / 100}" type="currency"/>
                                                                        </span>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                            <tbody>
                                                                <tr>
                                                                    <td style="" class="a-right" colspan="1"> VAT </td>
                                                                    <td style="" class="a-right">
                                                                        <span class="price">
                                                                            <fmt:setLocale value="en_US"/>
                                                                            <fmt:formatNumber value="${subTotal * 0.1}" type="currency"/>
                                                                        </span>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                            <tbody>
                                                                <tr>
                                                                    <td style="" class="a-right" colspan="1"> Shipping </td>
                                                                    <td style="" class="a-right"> 
                                                                        <span class="price">
                                                                            <fmt:setLocale value="en_US"/>
                                                                            <fmt:formatNumber value="${currentFee}" type="currency"/>
                                                                        </span>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                        <ul class="checkout-types">
                                                            <li>
                                                                <button type="submit" title="Proceed to Checkout" class="button btn-proceed-checkout btn-checkout" onclick="window.location = '<c:url value="/checkout"/>';"><span><span>Proceed to Checkout</span></span>
                                                                </button>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </c:otherwise>
                        </c:choose>
                    </div>
                    <div class="postscript"></div>
                </div>
            </div>
        </tiles:putAttribute>
    </tiles:insertDefinition>
