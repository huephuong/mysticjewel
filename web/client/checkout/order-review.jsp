<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="/admin/layout/taglib.jsp"%>
<tiles:insertDefinition name="page-client">
    <tiles:putAttribute name="title" value="Checkout" />
    <tiles:putAttribute name="body-class" value="" />
    <tiles:putAttribute name="page-css">
        <link rel="stylesheet" type="text/css" href="<c:url value="/" />resource/client/media/css/608a0436af3da7766b9a1705fe393cd8.css" media="all" />
        <link rel="stylesheet" type="text/css" href="<c:url value="/"/>resource/client/media/css/c78a949c76158a4d75f94587ee9c6aab.css" media="all" />
        <link rel="stylesheet" id="select2-css" href="<c:url value="/" />resource/client/royal_checkout/select2.css" type="text/css" />
        <link rel="stylesheet" id="panel-style-css" href="<c:url value="/" />resource/client/royal_checkout/panel.css" type="text/css" />
        <link rel="stylesheet" id="parent-style-css" href="<c:url value="/" />resource/client/royal_checkout/style.css" type="text/css" />
    </tiles:putAttribute>
    <tiles:putAttribute name="page-js">
        <script type="text/javascript" src="<c:url value="/" />resource/client/royal_checkout/checkout.min.js"></script>
    </tiles:putAttribute>
    <tiles:putAttribute name="content">
        <div class="top-container">
            <div class="breadcrumbs bctype2">
                <div class="container">
                    <div class="breadcrumbs-inner">
                        <div class="bc-page-title">
                            <h1><span>Review Order</span></h1>
                        </div>
                        <ul>
                            <li class="home"> <a href="/" title="Go to Home Page">Home</a> <span> / </span> </li>
                            <li class="checkout"> <strong>Review Order</strong> </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="main-container col1-layout">
            <div class="main-before-top-container"></div>
            <div class="main container">
                <div class="preface"></div>
                <div class="col-main">
                    <div class="clearfix"></div>
                    <tiles:insertTemplate template="/client/layout/toggle.jsp" />
                    <div class="page-title">
                        <h1>Checkout</h1>
                    </div>
                    <div id="st-container" class="st-container">
                        <div class="st-pusher" style="background-color: rgb(255, 255, 255); min-height: 925px;">
                            <div>
                                <div class="row">
                                    <div class="content col-lg-12 col-md-12">				
                                        <div class="woocommerce">
                                            <div class="before-checkout-form">
                                                <c:if test="${not empty res_message}">
                                                    <ul class="messages">
                                                        <li class="error-msg">
                                                            <ul>
                                                                <li>
                                                                    <span>${res_message}</span>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                    <c:set var="res_message" value="" scope="session"/>
                                                </c:if>
                                            </div>
                                            <form name="checkout" method="post" class="checkout" action="<c:url value="/" />order-finish" id="order-form">
                                                <div class="row">
                                                    <div class="col-lg-7 col-md-7">
                                                        <div class="col2-set" id="customer_details">
                                                            <div>
                                                                <div class="woocommerce-billing-fields">
                                                                    <h3 class="step-title"><span>Billing Details</span></h3>
                                                                    <p class="form-row form-row form-row-first validate-required" id="billing_first_name_field">
                                                                        <label for="billing_first_name" class="">First Name <abbr class="required" title="required">*</abbr></label>
                                                                        <input type="text" class="input-text required-entry" name="billing_first_name" id="billing_first_name" placeholder="" value="${purchaseCustomer.firstName}" readonly>
                                                                    </p>
                                                                    <p class="form-row form-row form-row-last validate-required" id="billing_last_name_field">
                                                                        <label for="billing_last_name" class="">Last Name <abbr class="required" title="required">*</abbr></label>
                                                                        <input type="text" class="input-text required-entry" name="billing_last_name" id="billing_last_name" placeholder="" value="${purchaseCustomer.lastName}" readonly>
                                                                    </p>
                                                                    <div class="clear"></div>
                                                                    <p class="form-row form-row form-row-first validate-required validate-email" id="billing_email_field">
                                                                        <label for="billing_email" class="">Email Address <abbr class="required" title="required">*</abbr></label>
                                                                        <input type="text" class="input-text required-entry validate-email" name="billing_email" id="billing_email" placeholder="" value="${purchaseCustomer.email}" readonly>
                                                                    </p>
                                                                    <p class="form-row form-row form-row-last validate-required validate-phone" id="billing_phone_field">
                                                                        <label for="billing_phone" class="">Phone <abbr class="required" title="required">*</abbr></label>
                                                                        <input type="tel" class="input-text required-entry validate-phone" name="billing_phone" id="billing_phone" placeholder="" value="${purchaseCustomer.phone}" readonly>
                                                                    </p>
                                                                    <div class="clear"></div>
                                                                </div>		
                                                            </div>
                                                            <br/>
                                                            <div>
                                                                <div class="woocommerce-shipping-fields">
                                                                    <h3 class="step-title"><span>Shipping address</span></h3>
                                                                    <div class="clearfix"></div>
                                                                    <div id="ship-to-different-address">
                                                                        <c:if test="${not empty customer and isNewAddress == 'new'}">
                                                                            <input id="addAddress" value="addAddress" class="input-checkbox" type="checkbox" name="addAddress">
                                                                            <label for="addAddress" class="checkbox">Would you like a save this address for later?</label>
                                                                        </c:if>
                                                                    </div>
                                                                    <div class="shipping_address" style="display: block;">
                                                                        <p class="form-row form-row-first validate-required" id="shipping_first_name_field">
                                                                            <label for="shipping_add_number" class="">Number <abbr class="required" title="required">*</abbr></label>
                                                                            <input type="text" class="input-text required-entry" name="shipping_add_number" id="shipping_add_number" placeholder="Number" value="${currentAddress.number}" readonly>
                                                                        </p>
                                                                        <p class="form-row form-row form-row-last address-field validate-required" id="shipping_address_1_field">
                                                                            <label for="shipping_add_street" class="">Street <abbr class="required" title="required">*</abbr></label>
                                                                            <input type="text" class="input-text required-entry" name="shipping_add_street" id="shipping_add_street" placeholder="Street" value="${currentAddress.street}" readonly>
                                                                        </p>
                                                                        <div class="clear"></div>
                                                                        <p class="form-row form-row-first address-field validate-required" id="shipping_city_field">
                                                                            <label for="shipping_add_ward" class="">Ward <abbr class="required" title="required">*</abbr></label>
                                                                            <input type="text" class="input-text required-entry" name="shipping_add_ward" id="shipping_add_ward" placeholder="Ward" value="${currentAddress.ward}" readonly>
                                                                        </p>
                                                                        <p class="form-row form-row-last address-field validate-required">
                                                                            <label for="shipping_add_district" class="">District <abbr class="required" title="required">*</abbr></label>
                                                                            <input type="text" class="input-text required-entry" value="${currentAddress.district}" placeholder="District" name="shipping_add_district" id="shipping_add_district" readonly>
                                                                        </p>
                                                                        <div class="clear"></div>
                                                                        <p class="form-row form-row-wide address-field update_totals_on_change validate-required" id="billing_country_field">
                                                                            <label for="shipping_add_city" class="">City <abbr class="required" title="required">*</abbr></label>
                                                                            <select class="select2-container country_to_state country_select" id="shipping_add_city" name="shipping_add_city" style="width: 100%;">
                                                                                <c:forEach items="${provinceList}" var="province">
                                                                                    <option value="${province.code}" <c:if test="${currentAddress.city eq province.name}">selected</c:if>>
                                                                                        ${province.name}
                                                                                    </option>
                                                                                </c:forEach>
                                                                            </select>
                                                                        </p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-5 col-md-5">
                                                        <div class="order-review">
                                                            <h3 class="step-title"><span>Your order</span></h3>
                                                            <div id="order_review" class="woocommerce-checkout-review-order">
                                                                <table class="shop_table woocommerce-checkout-review-order-table">
                                                                    <thead>
                                                                        <tr>
                                                                            <th>Product</th>
                                                                            <th class="product-total">Total</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        <c:forEach items="${cartItems}" var="item">
                                                                            <tr class="cart_item">
                                                                                <td class="product-name" width="70%">
                                                                                    ${item.key.productID.productName} &nbsp;&nbsp;&nbsp;<strong class="product-quantity">× ${item.value}</strong></td>
                                                                                <td class="product-total">
                                                                                    <span class="amount">
                                                                                        <c:if test="${item.key.productID.discountValue gt 0}">
                                                                                            <span>
                                                                                                <fmt:setLocale value="en_US"/>
                                                                                                <fmt:formatNumber value="${(item.key.unitPrice - (item.key.unitPrice * item.key.productID.discountValue / 100)) * item.value}" type="currency"/>
                                                                                            </span>
                                                                                        </c:if>
                                                                                        <c:if test="${item.key.productID.discountValue eq 0}">
                                                                                            <span>
                                                                                                <fmt:setLocale value="en_US"/>
                                                                                                <fmt:formatNumber value="${item.key.unitPrice * item.value}" type="currency"/>
                                                                                            </span>
                                                                                        </c:if>
                                                                                    </span>
                                                                                </td>
                                                                            </tr>
                                                                        </c:forEach>
                                                                    </tbody>
                                                                    <tfoot>
                                                                        <tr class="cart-subtotal">
                                                                            <th>Subtotal</th>
                                                                            <td>
                                                                                <span class="amount">
                                                                                    <fmt:setLocale value="en_US"/>
                                                                                    <fmt:formatNumber value="${subTotal}" type="currency"/>
                                                                                </span>
                                                                            </td>
                                                                        </tr>
                                                                        <tr class="cart-subtotal">
                                                                            <th>Voucher</th>
                                                                            <td>
                                                                                <span class="amount">
                                                                                    <fmt:setLocale value="en_US"/>
                                                                                    - <fmt:formatNumber value="${currentVoucher.discountValue * subTotal / 100}" type="currency"/>
                                                                                </span>
                                                                            </td>
                                                                        </tr>
                                                                        <tr class="vat">
                                                                            <th>VAT</th>
                                                                            <td>
                                                                                <span class="amount">
                                                                                    <fmt:setLocale value="en_US"/>
                                                                                    <fmt:formatNumber value="${subTotal * 0.1}" type="currency"/>
                                                                                </span>
                                                                            </td>
                                                                        </tr>
                                                                        <tr class="shipping">
                                                                            <th>Shipping</th>
                                                                            <td>
                                                                                Flat Rate:
                                                                                <span class="amount">
                                                                                    <fmt:setLocale value="en_US"/>
                                                                                    <fmt:formatNumber value="${currentFee}" type="currency"/>
                                                                                </span>
                                                                                <input type="hidden" name="shipFee" value="${currentFee}"/>
                                                                            </td>
                                                                        </tr>
                                                                        <tr class="order-total">
                                                                            <th>Total</th>
                                                                            <td>
                                                                                <strong>
                                                                                    <span class="amount">
                                                                                        <fmt:setLocale value="en_US"/>
                                                                                        <fmt:formatNumber value="${subTotal + (subTotal * 0.1) + currentFee - (subTotal * currentVoucher.discountValue / 100)}" type="currency"/>
                                                                                    </span>
                                                                                </strong>
                                                                            </td>
                                                                        </tr>
                                                                    </tfoot>
                                                                </table>
                                                                <div id="payment" class="woocommerce-checkout-payment">
                                                                    <ul class="wc_payment_methods payment_methods methods">
                                                                        <c:choose>
                                                                            <c:when test="${purchaseCustomer.payment == 'COD'}">
                                                                                <li class="wc_payment_method payment_method_cheque">
                                                                                    <input id="payment_method_cheque" type="radio" class="input-radio" name="payment_method" value="COD" checked="checked">
                                                                                    <label for="payment_method_cod">
                                                                                        <span style="font-size: 15px;">Cash on Delivery</span>
                                                                                    </label>
                                                                                </li>
                                                                                <li class="wc_payment_method payment_method_paypal">
                                                                                    <input id="payment_method_paypal" type="radio" class="input-radio" name="payment_method" value="CARD">
                                                                                    <label for="payment_method_paypal">
                                                                                        <span style="font-size: 15px;">Debit or Credit</span>
                                                                                    </label>
                                                                                </li>
                                                                            </c:when>
                                                                            <c:otherwise>
                                                                                <li class="wc_payment_method payment_method_cheque">
                                                                                    <input id="payment_method_cheque" type="radio" class="input-radio" name="payment_method" value="COD">
                                                                                    <label for="payment_method_cod">
                                                                                        <span style="font-size: 15px;">Cash on Delivery</span>
                                                                                    </label>
                                                                                </li>
                                                                                <li class="wc_payment_method payment_method_paypal">
                                                                                    <input id="payment_method_paypal" type="radio" class="input-radio" name="payment_method" value="CARD" checked="checked">
                                                                                    <label for="payment_method_paypal">
                                                                                        <span style="font-size: 15px;">Debit or Credit</span>
                                                                                    </label>
                                                                                </li>
                                                                            </c:otherwise>
                                                                        </c:choose>
                                                                    </ul>
                                                                    <div class="form-row place-order">
                                                                        <noscript>
                                                                        Since your browser does not support JavaScript, or it is disabled, please ensure you click the <em>Update Totals</em> button before placing your order. You may be charged more than the amount stated above if you fail to do so.			<br/><input type="submit" class="button alt" name="woocommerce_checkout_update_totals" value="Update totals" />
                                                                        </noscript>
                                                                        <input type="submit" class="button alt" name="btnPlaceOrder" id="place_order" value="Place Order" data-value="Place Order">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="post-navigation">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="postscript"></div>
            </div>
        </div>
    </tiles:putAttribute>
</tiles:insertDefinition>
