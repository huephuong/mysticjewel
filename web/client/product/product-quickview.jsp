<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="/admin/layout/taglib.jsp"%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Mystic Jewel</title>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta name="description" content="&lt;p style=&quot;color: #767676;&quot;&gt;There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don’t look even slightly believable. If you are going to use a" />
        <meta name="keywords" content="Royal, Magento, Theme, Elegento, SoapTheme, Multi-purpose, Premium, Ultra" />
        <meta name="robots" content="INDEX,FOLLOW" />
        <!--
        <link rel="icon" href="<c:url value="/"/>resource/client/skin/frontend/base/default/favicon.ico" type="image/x-icon" />
        <link rel="shortcut icon" href="<c:url value="/"/>resource/client/skin/frontend/base/default/favicon.ico" type="image/x-icon" />
        -->
        <link rel="icon" href="<c:url value="/"/>resource/client/skin/frontend/royal/default/favicon.png" type="image/png" />
        <link rel="shortcut icon" href="<c:url value="/"/>resource/client/skin/frontend/royal/default/favicon.png" type="image/png" />
        <!--[if lt IE 7]>
        <script type="text/javascript">
        //<![CDATA[
            var BLANK_URL = '<c:url value="/"/>resource/client/js/blank.html';
            var BLANK_IMG = '<c:url value="/"/>resource/client/js/spacer.gif';
        //]]>
        </script>
        <![endif]-->
        <link rel="stylesheet" type="text/css" href="<c:url value="/"/>resource/client/media/css/443dad4d43b65d580c80a22025f226c5.css" />
        <link rel="stylesheet" type="text/css" href="<c:url value="/"/>resource/client/media/css/c78a949c76158a4d75f94587ee9c6aab.css" media="all" />
        <link rel="stylesheet" type="text/css" href="<c:url value="/"/>resource/client/media/css/0f7a1a9dc80d956761dcac5407b1e2c0.css" media="print" />
        <script type="text/javascript" src="<c:url value="/"/>resource/client/js/prototype/prototype.js"></script>
        <script type="text/javascript" src="<c:url value="/"/>resource/client/js/lib/ccard.js"></script>
        <script type="text/javascript" src="<c:url value="/"/>resource/client/js/prototype/validation.js"></script>
        <script type="text/javascript" src="<c:url value="/"/>resource/client/js/scriptaculous/builder.js"></script>
        <script type="text/javascript" src="<c:url value="/"/>resource/client/js/scriptaculous/effects.js"></script>
        <script type="text/javascript" src="<c:url value="/"/>resource/client/js/scriptaculous/dragdrop.js"></script>
        <script type="text/javascript" src="<c:url value="/"/>resource/client/js/scriptaculous/controls.js"></script>
        <script type="text/javascript" src="<c:url value="/"/>resource/client/js/scriptaculous/slider.js"></script>
        <script type="text/javascript" src="<c:url value="/"/>resource/client/js/varien/js.js"></script>
        <script type="text/javascript" src="<c:url value="/"/>resource/client/js/varien/form.js"></script>
        <script type="text/javascript" src="<c:url value="/"/>resource/client/js/varien/menu.js"></script>
        <script type="text/javascript" src="<c:url value="/"/>resource/client/js/mage/translate.js"></script>
        <script type="text/javascript" src="<c:url value="/"/>resource/client/js/mage/cookies.js"></script>
        <script type="text/javascript" src="<c:url value="/"/>resource/client/js/elegento/jquery/jquery-1.10.2.js"></script>
        <script type="text/javascript" src="<c:url value="/"/>resource/client/js/elegento/jquery/jquery-noconflict.js"></script>
        <script type="text/javascript" src="<c:url value="/"/>resource/client/js/elegento/jquery/jquery-ui-1.10.3.js"></script>
        <script type="text/javascript" src="<c:url value="/"/>resource/client/js/elegento/ajax/ajax.js"></script>
        <script type="text/javascript" src="<c:url value="/"/>resource/client/js/elegento/ajax/ajaxlogin.js"></script>
        <script type="text/javascript" src="<c:url value="/"/>resource/client/js/elegento/ajax/quickview.js"></script>
        <script type="text/javascript" src="<c:url value="/"/>resource/client/js/elegento/menu/menu.js"></script>
        <script type="text/javascript" src="<c:url value="/"/>resource/client/js/elegento/media.match.js"></script>
        <script type="text/javascript" src="<c:url value="/"/>resource/client/js/elegento/modernizr.js"></script>
        <script type="text/javascript" src="<c:url value="/"/>resource/client/js/elegento/enquire.js"></script>
        <script type="text/javascript" src="<c:url value="/"/>resource/client/js/elegento/jquery/plugins/plugins.js"></script>
        <script type="text/javascript" src="<c:url value="/"/>resource/client/js/elegento/twitterfetcher.js"></script>
        <script type="text/javascript" src="<c:url value="/"/>resource/client/js/elegento/bootstrap.min.js"></script>
        <script type="text/javascript" src="<c:url value="/"/>resource/client/js/elegento/pre-elegento.js"></script>
        <script type="text/javascript" src="<c:url value="/"/>resource/client/js/varien/product.js"></script>
        <script type="text/javascript" src="<c:url value="/"/>resource/client/js/varien/configurable.js"></script>
        <script type="text/javascript" src="<c:url value="/"/>resource/client/js/calendar/calendar.js"></script>
        <script type="text/javascript" src="<c:url value="/"/>resource/client/js/calendar/calendar-setup.js"></script>
        <script type="text/javascript" src="<c:url value="/"/>resource/client/js/elegento/jquery/plugins/jquery.form.js"></script>
        <script type="text/javascript" src="<c:url value="/"/>resource/client/js/nwdthemes/jquery-1.11.0.min.js"></script>
        <script type="text/javascript" src="<c:url value="/"/>resource/client/js/nwdthemes/jquery-migrate-1.2.1.min.js"></script>
        <script type="text/javascript" src="<c:url value="/"/>resource/client/js/nwdthemes/jquery.noconflict.js"></script>
        <script type="text/javascript" src="<c:url value="/"/>resource/client/skin/frontend/base/default/js/nwdthemes/revslider/rs/jquery.themepunch.tools.min.js"></script>
        <script type="text/javascript" src="<c:url value="/"/>resource/client/skin/frontend/base/default/js/nwdthemes/revslider/rs/jquery.themepunch.revolution.min.js"></script>
        <link href="<c:url value="/"/>resource/client/demo6/index.php/blog/rss/index/store_id/16/" title="Blog" rel="alternate" type="application/rss+xml" />
        <!--[if IE]>
        <link rel="stylesheet" type="text/css" href="<c:url value="/"/>resource/client/media/css/181d3565126faf10c805ee17032869b2.css" media="all" />
        <![endif]-->
        <!--[if lt IE 7]>
        <script type="text/javascript" src="<c:url value="/"/>resource/client/js/lib/ds-sleight.js"></script>
        <script type="text/javascript" src="<c:url value="/"/>resource/client/skin/frontend/base/default/js/ie6.js"></script>
        <![endif]-->
        <!--[if lte IE 8]>
        <link rel="stylesheet" type="text/css" href="<c:url value="/"/>resource/client/media/css/e94b61f13849fb09464e2151d5529a9a.css" media="all" />
        <![endif]-->
        <script type="text/javascript">
            //<![CDATA[
            Mage.Cookies.path = '/';
            Mage.Cookies.domain = '/';
            //]]>
        </script>
        <script type="text/javascript">
            //<![CDATA[
            optionalZipCountries = ["HK", "IE", "MO", "PA"];
            //]]>
        </script>
        <script type="text/javascript">
            //<![CDATA[
            var egAjaxTime;
            var egAjaxTimer;
            var EGAjaxObject = new EG.EgAjax();
            jQuery(function($) {
                var loaderClass = EGAjaxObject.options.loader.substring(1);
                $('body').append($('<div />', {
                    class: loaderClass,
                    css: {
                        "display": "none"
                    }
                }));
                var ajaxWishlistFn = function(event) {
                    event.preventDefault();
                    EGAjaxObject.ajaxWishlist($(this).attr('href'));
                };
                $('body').on('click', '.link-wishlist', ajaxWishlistFn);
                var ajaxCompareFn = function(event) {
                    event.preventDefault();
                    EGAjaxObject.ajaxCompare($(this).attr('href'));
                };
                $('body').on('click', '.link-compare', ajaxCompareFn);
                var ajaxQuickviewFn = function(event) {
                    event.preventDefault();
                    EGAjaxObject.ajaxQuickview($(this));
                };
                $('body').on('click', '.link-quickview', ajaxQuickviewFn);
            });
            //]]>
        </script>
        <script type="text/javascript">
            //<![CDATA[
            var Translator = new Translate([]);
            //]]>
        </script>
        <script>
            (function(i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function() {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
            ga('create', 'UA-66842336-1', 'auto');
            ga('send', 'pageview');
        </script>
        <!--[if lt IE 9]>
        <script type='text/javascript' src="<c:url value="/"/>resource/client/js/elegento/html5shiv.js"></script>
        <script type='text/javascript' src="<c:url value="/"/>resource/client/js/elegento/css3-mediaqueries.js"></script>
        <![endif]-->
        <link href='//fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900italic,900' rel='stylesheet' type='text/css' />
    </head>

    <body class="page-empty  egajax-quickview-index catalog-product-view product-men-jewellery">
        <div>
            <script type="text/javascript">
                var optionsPrice = new Product.OptionsPrice([]);
            </script>
            <div class="product-view">
                <div class="product-essential">
                    <form action="#" method="post" id="product_addtocart_form">
                        <div class="no-display">
                            <input type="hidden" name="skuID" value="${prod.skuList[0].skuID}" />
                            <input type="hidden" name="related_product" id="related-products-field" value="" />
                        </div>
                        <div class="product-img-box grid12-6 no-padding-left">
                            <div class="product-image product-image-zoom">
                                <div class="product-image-slider eg-gallery-popup">
                                    <c:forEach items="${prod.productImageList}" var="img">
                                        <div class="image-item">
                                            <img id="image-main" class="gallery-image" src="<c:url value="/resource/images/"/>${img.imageLink}" alt="PRODUCT_IMG" data-zoom-image="<c:url value="/resource/images/"/>${img.imageLink}" width="100%" height="100%" />
                                        </div>
                                    </c:forEach>
                                </div>
                            </div>
                            <script type="text/javascript">
                                jQuery('.product-image-slider').owlCarousel({
                                    items: 1,
                                    navigation: true,
                                    pagination: false,
                                    lazyLoad: false,
                                    rewindNav: false,
                                    addClassActive: true,
                                    autoHeight: true,
                                    itemsCustom: [1600, 1],
                                    navigationText: ["<span class='icon-royal icon-less'></span>", "<span class='icon-royal icon-greater'></span>"]
                                });
                            </script>
                        </div>
                        <div class="product-shop grid12-6">
                            <div class="product-name">
                                <h1>${prod.productName}</h1>
                            </div>
                            <div class="price-rating clearfix">
                                <c:choose>
                                    <c:when test="${prod.rating != 0}">
                                        <div class="ratings">
                                            <div class="rating-box">
                                                <c:if test="${prod.rating gt 0}">
                                                    <div class="rating theme-color" style="width:${prod.rating}%"></div>
                                                </c:if>
                                                <c:if test="${prod.rating eq 0}">
                                                    <div class="rating theme-color" style="width:0%"></div>
                                                </c:if>
                                            </div>
                                            <p class="rating-links"> <a href="/review/product/list/id/141/">${fn:length(prod.reviewList)} Review(s)</a> </p>
                                        </div>
                                    </c:when>    
                                    <c:otherwise>
                                        <p class="no-rating">
                                            <a href="<c:url value="/"/>resource/client/demo6/index.php/review/product/list/id/142/#review-form">Be the first to review this product</a>
                                        </p>
                                    </c:otherwise>
                                </c:choose>
                                <div class="price-box">
                                    <c:if test="${prod.discountValue gt 0}">
                                        <p class="old-price"> <span class="price-label">Regular Price:</span>
                                            <span class="price">
                                                <fmt:setLocale value="en_US"/>
                                                <fmt:formatNumber value="${curSku.unitPrice}" type="currency"/>
                                            </span>
                                        </p>
                                        <p class="special-price"> <span class="price-label">Special Price</span>
                                            <span class="price" style="color: #cda85c">
                                                <fmt:setLocale value="en_US"/>
                                                <fmt:formatNumber value="${curSku.unitPrice - (curSku.unitPrice * prod.discountValue / 100)}" type="currency"/>
                                            </span>
                                        </p>
                                    </c:if>
                                    <c:if test="${prod.discountValue eq 0}">
                                        <span class="regular-price">
                                            <span class="price">
                                                <fmt:setLocale value="en_US"/>
                                                <fmt:formatNumber value="${curSku.unitPrice}" type="currency"/>
                                            </span> 
                                        </span>
                                    </c:if>
                                </div>
                            </div>
                            <div class="short-description">${prod.productDesc}</div>
                            <c:if test="${!empty prod.categoryID.parentID.skuSizeList}">
                            <div class="add-to-box">
                                <div class="add-to-cart">
                                <div class="qty-wrapper">
                                    <label for="size">Size:</label>
                                    <select id="qty" name="size" class="input-text qty" onchange="location = this.value" style="width: 74px !important">
                                        <c:forEach items="${prod.skuList}" var="skuList">
                                            <option <c:if test="${skuList.sizeID.sizeValue eq curSku.sizeID.sizeValue}">selected</c:if> value="<c:url value="/"/>quickview?skuid=${skuList.skuID}">${skuList.sizeID.sizeValue}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                                </div>
                            </div>
                            </c:if>
                            <div class="add-to-box">
                                <div class="add-to-cart">
                                    <div class="qty-wrapper">
                                        <label for="qty">Qty:</label>
                                        <input type="text" name="qty" id="qty" maxlength="12" value="1" title="Qty" class="input-text qty" />
                                        <div id="qty_up_btn" class="qty-btn qty-up-btn">+</div>
                                        <div id="qty_down_btn" class="qty-btn qty-down-btn">-</div>
                                    </div>
                                    <button type="submit" title="Add to Cart" class="button btn-cart">
                                        <span><span>Add to Cart</span></span>
                                    </button>
                                    <div class="ajaxcart_loading" style="display:none"> <img src="<c:url value="/"/>resource/client/skin/frontend/base/default/images/elegento/ajax/ajax-loader(1).gif" alt="Loading" /> </div>
                                </div>
                            </div>
                        </div>
                        <div class="clearer"></div>
                    </form>
                    <script type="text/javascript">
                        //<![CDATA[
                        (function($) {
                            var form = $('#product_addtocart_form');
                            var status = $('.ajaxcart_loading');
                            var $body = form.parents('body').first();
                            form.ajaxForm({
                                url: '<c:url value="/" />add-product-to-cart',
                                dataType: 'json',
                                data: {
                                    'isAjax': 1
                                },
                                beforeSend: function() {
                                    $body.css({
                                        'overflow': 'hidden'
                                    });
                                    status.show();
                                },
                                success: function(data) {
                                    status.hide();
                                    $body.css({
                                        'overflow': 'initial'
                                    });
                                    parent.EGAjaxObject.updateAjaxData(data, '.block-cart', 'cart');
                                }
                            });
                        })(jQuery);
                        //]]>
                    </script>
                </div>
            </div>
            <script type="text/javascript" src="<c:url value="/"/>resource/client/js/elegento/elegento.js"></script>
        </div>
    </body>

</html>