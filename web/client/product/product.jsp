<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="/admin/layout/taglib.jsp"%>
<tiles:insertDefinition name="page-client">
    <tiles:putAttribute name="title" value="Products" />
    <tiles:putAttribute name="body-class" value=" catalog-category-view categorypath-fashion-html category-fashion  wide" />
    <tiles:putAttribute name="page-css">
        <link rel="stylesheet" type="text/css" href="<c:url value="/" />resource/client/media/css/608a0436af3da7766b9a1705fe393cd8.css" media="all" />
    </tiles:putAttribute>
    <tiles:putAttribute name="page-js">
        <script type="text/javascript" src="<c:url value="/" />resource/client/js/elegento/ajax/layerednav.js"></script>
    </tiles:putAttribute>
    <tiles:putAttribute name="content">
        <!-- TOP BANNER-->
        <div class="top-container">
            <div class="breadcrumbs bctype2">
                <div class="container">
                    <div class="breadcrumbs-inner">
                        <div class="bc-page-title">
                            <h1><span>Our Shop</span></h1>
                        </div>
                        <ul>
                            <li class="home"> <a href="<c:url value="/home" />" title="Go to Home Page">Home</a> <span> / </span> </li>
                            <li class="category3"> <strong>Shop</strong> </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="main-container col2-left-layout">
            <div class="main-before-top-container"></div>
            <div class="main container">
                <!-- BREAD CRUMBS BANNER -->
                <div class="preface">
                    <div class="banner-section category-banner clearfix eg-tac eg-fcf owl-carousel owlcarousel-slider singleItem pagination">
                        <div class="banner-column-wrapper grid-full">
                            <div class="banner-column">
                                <div class="banner eg-hoveropacity eg-hover-nobg no-margin" onclick="window.location = '<c:url value="/home" />'">
                                    <img src="resource/client/media/wysiwyg/elegento/category/category-banner-1.jpg" alt="" />
                                </div>
                            </div>
                        </div>
                        <div class="banner-column-wrapper grid-full">
                            <div class="banner-column">
                                <div class="banner eg-hoveropacity eg-hover-nobg no-margin" onclick="window.location = '<c:url value="/home" />'">
                                    <div class="banner-content">
                                        <div class="banner-inner">
                                            <h2 class="no-margin eg-fwb" style="font-size: 40px;line-height: 40px;letter-spacing: 10px;">LIFETIME</h2>
                                            <div class="horizontal-separator separator-2" style="background: #fff;margin: 15px auto;"></div>
                                            <p class="no-margin eg-fsi" style="line-height: 24px;font-size: 25px;font-family: Bodoni MT;">Check our new arrivals</p>
                                        </div>
                                    </div>
                                    <img src="resource/client/media/wysiwyg/elegento/category/category-banner-2.jpg" alt="" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- MAIN COL -->
                <div class="col-main">
                    <div class="clearfix"></div>
                    <tiles:insertTemplate template="/client/layout/toggle.jsp" />
                    <div class="page-title category-title">
                        <h1>Products</h1>
                    </div>
                    <div id="eglayerednav_container">
                        <div class="category-products">
                            <div class="toolbar clearfix">
                                <div class="toolbar-upside">
                                    <div class="sort-by">
                                        <label>Sort By:</label>
                                        <select onchange="catalog_toolbar_make_request(this.value)">
                                            <option value="?order=name&dir=asc" ${order eq 'name' ? 'selected' : ''}> Name </option>
                                            <option value="?order=price&dir=asc" ${order eq 'price' ? 'selected' : ''}> Price </option>
                                            <option value="?order=new&dir=asc" ${order eq 'new' ? 'selected' : ''}> New </option>
                                        </select>
                                        <a href="?order=${order}&dir=${dir eq 'desc' ? 'asc' : 'desc'}" title="Set Descending Direction" class="fa fa-long-arrow-${dir eq 'desc' ? 'down' : 'up'}"></a>
                                    </div>
                                    <div class="view-mode">
                                        <c:choose>
                                            <c:when test="${mode eq 'grid'}">
                                                <strong title="Grid" class="grid">
                                                    <i class="fa fa-th-large"></i>
                                                </strong>
                                                <a href="?mode=list" title="List" class="list">
                                                    <i class="fa fa-th-list"></i>
                                                </a>
                                            </c:when>
                                            <c:otherwise>
                                                <a href="?mode=grid" title="Grid" class="list">
                                                    <i class="fa fa-th-large"></i>
                                                </a>
                                                <strong title="List" class="grid">
                                                    <i class="fa fa-th-list"></i>
                                                </strong>
                                            </c:otherwise>
                                        </c:choose>
                                    </div>
                                    <div class="limiter">
                                        <label>Show:</label>
                                        <select onchange="catalog_toolbar_make_request(this.value)">
                                            <option value="?limit=6" ${limit == 6 ? 'selected' : ''}> 6 </option>
                                            <option value="?limit=9" ${limit == 9 ? 'selected' : ''}> 9 </option>
                                            <option value="?limit=12" ${limit == 12 ? 'selected' : ''}> 12 </option>
                                        </select>
                                    </div>
                                </div>
                                <div class="toolbar-downside no-display">
                                    <div class="pages gen-direction-arrows1"> <strong>Page:</strong>
                                        <ol>
                                            <c:forEach begin="1" end="${pageLimit}" varStatus="loop">
                                                <c:choose>
                                                    <c:when test="${loop.count == page}">
                                                        <li class="current">${loop.count}</li>
                                                        </c:when>
                                                        <c:otherwise>
                                                        <li><a href="?p=${loop.count}">${loop.count}</a> </li>
                                                        </c:otherwise>
                                                    </c:choose>
                                                </c:forEach>
                                                <c:if test="${page < pageLimit}">
                                                <li class="next">
                                                    <a class="next i-next" href="?p=${page + 1}" title="Next">
                                                        Next                        
                                                    </a>
                                                </li>
                                            </c:if>
                                        </ol>
                                    </div>
                                </div>
                            </div>
                            <c:choose>
                                <c:when test="${mode eq 'list'}">
                                    <ol class="products-list clearfix" id="products-list">
                                        <c:forEach items="${prodList}" var="prod">
                                            <c:set var="sku" value="${prod.skuList[0]}"/>
                                            <li class="item">
                                                <div class="media-section eg-image-slider">
                                                    <c:set var="firstImg" value="${prod.productImageList[0]}"/>
                                                    <a href="<c:url value="/product-detail?skuid=${sku.skuID}" />" title="${prod.productName}" class="product-image" data-images="<c:url value="/" />resource/images/${firstImg.imageLink}"> <img src="<c:url value="/" />resource/images/${firstImg.imageLink}" alt="${prod.productName}" style="width:100%" width="350" height="455" /> </a>
                                                    <div class="product-labels">
                                                        <c:if test="${prod.isNew}">
                                                            <span class="new-product">
                                                                <span>New</span>
                                                            </span>
                                                        </c:if>
                                                        <c:if test="${prod.discountValue gt 0}">
                                                            <span class="discount-code">
                                                                <span>-${prod.discountValue}%</span>
                                                            </span>
                                                        </c:if>
                                                    </div>
                                                    <div class="hover-section">
                                                        <div class="hover-links-wrapper a-center">
                                                            <div class="eg-tac link-icon">
                                                                <a href="<c:url value="/" />add-wishlist/${prod.productID}" class="link-wishlist" title="Wishlist"> <span class="link-ico"><span class="fa fa-heart"></span></span> <span class="link-label">Wishlist</span> </a>
                                                            </div>
                                                            <div class="eg-tac link-icon">
                                                                <a href="<c:url value="/" />product-compare/add?productid=${prod.productID}" class="" title="Compare"> <span class="link-ico"><span class="fa fa-exchange"></span></span> <span class="link-label">Compare</span> </a>
                                                            </div>
                                                            <div class="eg-tac link-both">
                                                                <a href="javascript:void(0);" data-target="<c:url value="/" />quickview?skuid=${sku.skuID}" class="link-quickview" title="Quick View"> <span class="link-ico"><span class="fa fa-search"></span></span> <span class="link-label">Quickview</span> </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="product-shop">
                                                    <div class="f-fix">
                                                        <div class="clearfix"></div>
                                                        <div class="product-category"> <a href="#" title="${prod.categoryID.categoryName}">${prod.categoryID.categoryName}</a> </div>
                                                        <h2 class="product-name"><a href="<c:url value="/product-detail?skuid=${sku.skuID}" />" title="${prod.productName}">${prod.productName}</a></h2>
                                                        <div class="review rating-only">
                                                            <div class="ratings">
                                                                <div class="rating-box">
                                                                    <c:if test="${prod.rating gt 0}">
                                                                        <div class="rating theme-color" style="width:${prod.rating}%"></div>
                                                                    </c:if>
                                                                    <c:if test="${prod.rating eq 0}">
                                                                        <div class="rating theme-color" style="width:0%"></div>
                                                                    </c:if>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="price-box">
                                                            <c:if test="${prod.discountValue gt 0}">
                                                                <p class="old-price"> <span class="price-label">Regular Price:</span>
                                                                    <span class="price">
                                                                        <fmt:setLocale value="en_US"/>
                                                                        <fmt:formatNumber value="${sku.unitPrice}" type="currency"/>
                                                                    </span>
                                                                </p>
                                                                <p class="special-price"> <span class="price-label">Special Price</span>
                                                                    <span class="price" style="color: #cda85c">
                                                                        <fmt:setLocale value="en_US"/>
                                                                        <fmt:formatNumber value="${sku.unitPrice - (sku.unitPrice * prod.discountValue / 100)}" type="currency"/>
                                                                    </span>
                                                                </p>
                                                            </c:if>
                                                            <c:if test="${prod.discountValue eq 0}">
                                                                <span class="regular-price">
                                                                    <span class="price">
                                                                        <fmt:setLocale value="en_US"/>
                                                                        <fmt:formatNumber value="${sku.unitPrice}" type="currency"/>
                                                                    </span> 
                                                                </span>
                                                            </c:if>
                                                        </div>
                                                        <div class="desc std"> ${prod.productDesc} <a href="<c:url value="/product-detail?skuid=${sku.skuID}" />" title="${prod.productName}" class="link-learn">Learn More</a> </div>
                                                    </div>
                                                </div>
                                            </li>
                                        </c:forEach>
                                    </ol>
                                    <script type="text/javascript">
                                        decorateList('products-list', 'none-recursive')
                                    </script>
                                    <script type="text/javascript">
                                        decorateGeneric($$('ul.products-grid'), ['odd', 'even', 'first', 'last'])
                                    </script>
                                </c:when>
                                <c:otherwise>
                                    <!-- PRODUCT GRIDS -->
                                    <div class="grid-container">
                                        <ul class="products-grid">
                                            <c:forEach items="${prodList}" var="prod">
                                                <c:set var="sku" value="${prod.skuList[0]}"/>
                                                <li class="grid-columns cols-3 item grid12-sm-4 grid12-xs-6 grid12-sxs-12">
                                                    <div class="media-section eg-image-slider">
                                                        <c:set var="firstImg" value="${prod.productImageList[0]}"/>
                                                        <a href="<c:url value="/product-detail?skuid=${sku.skuID}" />" title="${prod.productName}" class="product-image" data-images="${firstImg}"> <img src="<c:url value="/" />resource/images/${firstImg.imageLink}" alt="${prod.productName}" style="width:100%" width="350" height="455" /> </a>
                                                        <div class="product-labels">
                                                            <c:if test="${prod.isNew}">
                                                                <span class="new-product">
                                                                    <span>New</span>
                                                                </span>
                                                            </c:if>
                                                            <c:if test="${prod.discountValue gt 0}">
                                                                <span class="discount-code">
                                                                    <span>-${prod.discountValue}%</span>
                                                                </span>
                                                            </c:if>
                                                        </div>
                                                        <div class="hover-section">
                                                            <div class="hover-links-wrapper a-center">
                                                                <div class="eg-tac link-icon">
                                                                    <a href="<c:url value="/" />add-wishlist/${prod.productID}" class="link-wishlist" title="Wishlist"> <span class="link-ico"><span class="fa fa-heart"></span></span> <span class="link-label">Wishlist</span> </a>
                                                                </div>
                                                                <div class="eg-tac link-icon">
                                                                    <a href="<c:url value="/" />product-compare/add?productid=${prod.productID}" class="" title="Compare"> <span class="link-ico"><span class="fa fa-exchange"></span></span> <span class="link-label">Compare</span> </a>
                                                                </div>
                                                                <div class="eg-tac link-both">
                                                                    <a href="javascript:void(0);" data-target="<c:url value="/" />quickview?skuid=${sku.skuID}" class="link-quickview" title="Quick View"> <span class="link-ico"><span class="fa fa-search"></span></span> <span class="link-label">Quickview</span> </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="details-section">
                                                        <div class="product-category"> <a href="#" title="${prod.categoryID.categoryName}">${prod.categoryID.categoryName}</a> </div>
                                                        <h2 class="product-name">
                                                            <a href="<c:url value="/" />product-detail?skuid=${sku.skuID}" title="${prod.productName}">${prod.productName}</a>
                                                        </h2>
                                                        <div class="review rating-only">
                                                            <div class="ratings clearfix">
                                                                <div class="rating-box">
                                                                    <c:if test="${prod.rating gt 0}">
                                                                        <div class="rating theme-color" style="width:${prod.rating}%"></div>
                                                                    </c:if>
                                                                    <c:if test="${prod.rating eq 0}">
                                                                        <div class="rating theme-color" style="width:0%"></div>
                                                                    </c:if>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="price-box">
                                                            <c:if test="${prod.discountValue gt 0}">
                                                                <p class="old-price"> <span class="price-label">Regular Price:</span>
                                                                    <span class="price">
                                                                        <fmt:setLocale value="en_US"/>
                                                                        <fmt:formatNumber value="${sku.unitPrice}" type="currency"/>
                                                                    </span>
                                                                </p>
                                                                <p class="special-price"> <span class="price-label">Special Price</span>
                                                                    <span class="price" style="color: #cda85c">
                                                                        <fmt:setLocale value="en_US"/>
                                                                        <fmt:formatNumber value="${sku.unitPrice - (sku.unitPrice * prod.discountValue / 100)}" type="currency"/>
                                                                    </span>
                                                                </p>
                                                            </c:if>
                                                            <c:if test="${prod.discountValue eq 0}">
                                                                <span class="regular-price">
                                                                    <span class="price">
                                                                        <fmt:setLocale value="en_US"/>
                                                                        <fmt:formatNumber value="${sku.unitPrice}" type="currency"/>
                                                                    </span> 
                                                                </span>
                                                            </c:if>
                                                        </div>
                                                    </div>
                                                </li>
                                            </c:forEach>
                                        </ul>
                                    </div>
                                    <script type="text/javascript">
                                        decorateGeneric($$('ul.products-grid'), ['odd', 'even', 'first', 'last'])
                                    </script>
                                </c:otherwise>
                            </c:choose>
                            <div class="toolbar-bottom">
                                <div class="toolbar clearfix">
                                    <div class="toolbar-upside">
                                        <div class="sort-by">
                                            <label>Sort By:</label>
                                            <select onchange="catalog_toolbar_make_request(this.value)">
                                                <option value="?order=name&dir=asc" ${order eq 'name' ? 'selected' : ''}> Name </option>
                                                <option value="?order=price&dir=asc" ${order eq 'price' ? 'selected' : ''}> Price </option>
                                                <option value="?order=new&dir=asc" ${order eq 'new' ? 'selected' : ''}> New </option>
                                            </select>
                                            <a href="?order=${order}&dir=${dir eq 'desc' ? 'asc' : 'desc'}" title="Set Descending Direction" class="fa fa-long-arrow-${dir eq 'desc' ? 'down' : 'up'}"></a>
                                        </div>
                                        <div class="view-mode">
                                            <c:choose>
                                                <c:when test="${mode eq 'grid'}">
                                                    <strong title="Grid" class="grid">
                                                        <i class="fa fa-th-large"></i>
                                                    </strong>
                                                    <a href="?mode=list" title="List" class="list">
                                                        <i class="fa fa-th-list"></i>
                                                    </a>
                                                </c:when>
                                                <c:otherwise>
                                                    <a href="?mode=grid" title="Grid" class="list">
                                                        <i class="fa fa-th-large"></i>
                                                    </a>
                                                    <strong title="List" class="grid">
                                                        <i class="fa fa-th-list"></i>
                                                    </strong>
                                                </c:otherwise>
                                            </c:choose>
                                        </div>
                                        <div class="limiter">
                                            <label>Show:</label>
                                            <select onchange="catalog_toolbar_make_request(this.value)">
                                                <option value="?limit=6" ${limit == 6 ? 'selected' : ''}> 6 </option>
                                                <option value="?limit=9" ${limit == 9 ? 'selected' : ''}> 9 </option>
                                                <option value="?limit=12" ${limit == 12 ? 'selected' : ''}> 12 </option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="toolbar-downside no-display">
                                        <div class="pager">
                                            <div class="pages gen-direction-arrows1"> <strong>Page:</strong>
                                                <ol>
                                                    <c:forEach begin="1" end="${pageLimit}" varStatus="loop">
                                                        <c:choose>
                                                            <c:when test="${loop.count == page}">
                                                                <li class="current">${loop.count}</li>
                                                                </c:when>
                                                                <c:otherwise>
                                                                <li><a href="?p=${loop.count}">${loop.count}</a> </li>
                                                                </c:otherwise>
                                                            </c:choose>
                                                        </c:forEach>
                                                        <c:if test="${page < pageLimit}">
                                                        <li class="next">
                                                            <a class="next i-next" href="?p=${page + 1}" title="Next">
                                                                Next                        
                                                            </a>
                                                        </li>
                                                    </c:if>
                                                </ol>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="ajaxcart_loading" style="display:none"> <img src="resource/client/skin/frontend/base/default/images/elegento/ajax/ajax-loader.gif" alt="Loading" /> </div>
                        <script type="text/javascript">
                            //<![CDATA[
                            jQuery(function($) {
                                var columnCount = 3;
                                var getColumnCount = function(wrapper) {
                                    if (Modernizr.mq("screen and (min-width:768px) and (max-width:991px)")) {
                                        return 3;
                                    } else if (Modernizr.mq("screen and (min-width:481px) and (max-width:767px)")) {
                                        return 2;
                                    } else if (Modernizr.mq("screen and (max-width:480px)")) {
                                        return 1;
                                    } else {
                                        return columnCount;
                                    }
                                };
                                var resizeGrid = function() {
                                    $('.category-products .products-grid').each(function() {
                                        var $grid = $(this);
                                        var colCount = getColumnCount();
                                        $grid.find('.product-image img').imgpreload({
                                            all: function() {
                                                var h = 0;
                                                var index = 0;
                                                var size = $grid.find('.item').length;
                                                var items = new Array();
                                                $grid.find('.item').each(function() {
                                                    index++;
                                                    $(this).css('height', 'auto');
                                                    if (h < $(this).innerHeight() + 12)
                                                        h = $(this).innerHeight() + 12;
                                                    if (index % colCount == 0 || index == size) {
                                                        items.push(this);
                                                        $.each(items, function(index, value) {
                                                            $(value).css({
                                                                height: h + 'px'
                                                            });
                                                        });
                                                        h = 0;
                                                        items = new Array();
                                                    } else {
                                                        items.push(this);
                                                    }
                                                })
                                            }
                                        });
                                    });
                                };
                                resizeGrid();
                                $(window).on('delayed-resize', function(e, resizeEvent) {
                                    resizeGrid();
                                });
                                EG.commonfns();
                            });
                            //]]>
                        </script>
                    </div>
                </div>
                <!-- LEFT COL -->
                <div class="col-left sidebar">
                    <div class="eg-layerednav">
                        <div class="block block-categories">
                            <div class="block-title"> <strong><span>Find Jewellery</span></strong> </div>
                            <div class="block-content">
                                <ul class="eg-categories-accordion">
                                    <!-- Category -->
                                    <li id="category-${categoryID}" class="level0 parent first last">
                                        <a class="level0" href="#"><span>Categories</span></a>
                                        <span class="opener">
                                            <span class="icon-royal icon-plus"></span><span class="icon-royal icon-minus"></span>
                                        </span>
                                        <ul class="level0 parent clearfix">
                                            <c:forEach begin="0" items="${catList}" var="mainCat" varStatus="loop">
                                                <li id="category-${categoryID}" class="level1 ${categoryID == mainCat.categoryID ? 'current' : ''} parent ${loop.count == 1 ? 'first' : loop.count == catList.size() ? 'last' : ''}">
                                                    <a class="level1" href="<c:url value="/" />products?category=${mainCat.categoryID}"><span>${mainCat.categoryName}</span></a>
                                                    <span class="opener">
                                                        <span class="icon-royal icon-plus"></span><span class="icon-royal icon-minus"></span>
                                                    </span>
                                                    <ul class="level1 parent clearfix">
                                                        <c:forEach begin="0" items="${mainCat.categoryList}" var="subCat" varStatus="loop2">
                                                            <li id="category-${categoryID}" class="level2 ${categoryID == subCat.categoryID ? 'current' : ''} ${loop2.count == 1 ? 'first' : loop2.count == catList.size() ? 'last' : ''}">
                                                                <a class="level2" href="<c:url value="/" />products?category=${subCat.categoryID}"><span>${subCat.categoryName}</span></a>
                                                            </li>
                                                        </c:forEach>
                                                    </ul>
                                                </li>
                                            </c:forEach>
                                        </ul>
                                    </li>
                                    <!-- Collection -->
                                    <li id="category-${colID}" class="level0 parent first last">
                                        <a class="level0" href="#"><span>Collections</span></a>
                                        <span class="opener">
                                            <span class="icon-royal icon-plus"></span><span class="icon-royal icon-minus"></span>
                                        </span>
                                        <ul class="level0 parent clearfix">
                                            <c:forEach begin="0" items="${colList}" var="prodCol" varStatus="loop">
                                                <li class="level1 ${colID == prodCol.colID ? 'current' : ''} parent ${loop.count == 1 ? 'first' : loop.count == colList.size() ? 'last' : ''}">
                                                    <a class="level1" href="<c:url value="/" />products?collection=${prodCol.colID}"><span>${prodCol.colName}</span></a>
                                                </li>
                                            </c:forEach>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <script type="text/javascript">
                            jQuery('.eg-categories-accordion').accordion();
                            jQuery('.eg-categories-accordion').each(function(index) {
                                var activeItems = jQuery(this).find('li.active');
                                activeItems.each(function(i) {
                                    jQuery(this).children('ul').css('display', 'block');
                                    if (i == activeItems.length - 1) {
                                        jQuery(this).addClass("current");
                                    }
                                });
                            });
                        </script>
                        <script type="text/javascript">
                            var categoryUrl = "<c:url value="/" />";
                            var categoryUrlParams = "";
                            var layerednavAjaxUrl = "<c:url value="/" />product-filter";
                        </script>
                        <div class="block block-layered-nav">
                            <div class="block-title">
                                <strong><span>Shop by</span></strong>
                            </div>
                            <div class="block-content">
                                <div class="layers-container">
                                    <dl id="eglayerednav_dl">
                                        <c:forEach items="${attrGroupList}" var="attrGroup">
                                            <dt class="eglayerednav_dt">
                                            <c:if test="${attrID != 0}">
                                                <c:if test="${attrGroup.attributeGroupID == attr.attributeGroupID.attributeGroupID}">
                                                    <a id="attr-clear" href="?attr=clear" class="btn-remove eglayerednav_clear" title="Remove This Item">Remove This Item</a>
                                                </c:if>
                                            </c:if>
                                            ${attrGroup.attributeGroupName}
                                            </dt>
                                            <dd id="eglayerednav_attr" style="display: ${attrID != 0 ? attrGroup.attributeGroupID == attr.attributeGroupID.attributeGroupID ? "block" : "none" : "none"}">
                                                <ol class="clearfix">
                                                    <c:forEach items="${attrGroup.getAttributeList()}" var="attrChild">
                                                        <li class="grid-full no-padding-right clearfix">
                                                            <a href="#" id="attr-${attrChild.attributeID}" class="eglayerednav_attribute ${attrChild.attributeID == attrID ? 'eglayerednav_attribute_selected' : ''}">${attrChild.attributeValue}</a> <span class="f-right">${attrChild.getProductList().size()}</span>
                                                        </li>
                                                    </c:forEach>
                                                </ol>
                                            </dd>
                                        </c:forEach>
                                        <dt class="eglayerednav_dt">Price Filter</dt>
                                        <dd id="eglayerednav_price">
                                            <ol>
                                                <li>
                                                    <div id="eglayerednav_price_slider" class="price_slider"> </div> <span>Price: </span>
                                                    <input class="input-text" type="text" maxlength="12" size="4" id="price_from" value="$${priceFrom}" onclick="if (this.value == this.name)
                                                                this.value = '';" name="From" /> <span>-</span>
                                                    <input class="input-text" type="text" maxlength="12" size="4" id="price_to" value="$${priceTo}" onclick="if (this.value == this.name)
                                                                this.value = '';" name="To" />
                                                    <br />
                                                    <button type="button" class="button" id="price_button_ok">
                                                        <span><span>OK</span></span>
                                                    </button>
                                                    <c:if test="${priceFrom != 100 && priceTo != 10000}">
                                                        <button type="button" class="button eglayerednav_clear">
                                                            <span><span>CLEAR</span></span>
                                                        </button>
                                                    </c:if>
                                                </li>
                                            </ol>
                                            <script type="text/javascript">
                                                var priceQueryKey = "price";
                                                jQuery(function($) {
                                                    $("#eglayerednav_price_slider").slider({
                                                        range: true,
                                                        min: 100,
                                                        max: 10000,
                                                        values: [${priceFrom}, ${priceTo}],
                                                        slide: function(event, ui) {
                                                            $("#price_from").val("$" + ui.values[0]);
                                                            $("#price_to").val("$" + ui.values[1]);
                                                        },
                                                        change: function(event, ui) {
                                                            eglayerednav_add_params(priceQueryKey, ui.values[0] + ',' + ui.values[1], true);
                                                            eglayerednav_send_request();
                                                        }
                                                    });
                                                });
                                            </script>
                                        </dd>
                                    </dl>
                                </div>
                            </div>
                            <!-- <div class="layerednav_loading" style="display:none"></div> --></div>
                        <script type="text/javascript">
                            eglayerednav_init();
                        </script>
                        <script type="text/javascript">
                            decorateDataList('eglayer-filters');
                            catalog_toolbar_init();
                        </script>
                    </div>
                    <div class="cms-block vlist-slider-block">
                        <div class="block-title">
                            <h3><span>VIEWED PRODUCTS</span></h3>
                        </div>
                        <div class="itemslider-wrapper vlist-slider-wrapper clearfix">
                            <div class="upsell-vlist-slider top-right-nav">
                                <c:set var="numCols" value="3"/>
                                <c:set var="numRows" value="3"/>
                                <c:set var="rowCount" value="0"/>
                                <div class="column-item clearfix">
                                    <c:forEach items="${ckProductHistory}" var="chProd" varStatus="status">
                                        <c:if test="${rowCount lt numRows}">
                                            <c:set var="sku" value="${chProd.skuList[0]}"/>
                                            <div class="item clearfix">
                                                <a href="<c:url value="/product-detail?skuid=${sku.skuID}"/>" title="${chProd.productName}" class="product-image">
                                                    <c:set var="firstImg" value="${chProd.productImageList[0]}"/>
                                                    <img src="<c:url value="/" />resource/images/${firstImg.imageLink}" alt="${chProd.productName}" width="100" height="120" />
                                                </a>
                                                <div class="details-section">
                                                    <h2 class="product-name">
                                                        <a href="<c:url value="/product-detail?skuid=${sku.skuID}"/>" title="${chProd.productName}">${chProd.productName}</a>
                                                    </h2>
                                                    <div class="price-box">
                                                        <c:if test="${chProd.discountValue gt 0}">
                                                            <p class="old-price"> <span class="price-label">Regular Price:</span>
                                                                <span class="price">
                                                                    <fmt:setLocale value="en_US"/>
                                                                    <fmt:formatNumber value="${sku.unitPrice}" type="currency"/>
                                                                </span>
                                                            </p>
                                                            <p class="special-price"> <span class="price-label">Special Price</span>
                                                                <span class="price">
                                                                    <fmt:setLocale value="en_US"/>
                                                                    <fmt:formatNumber value="${sku.unitPrice - (sku.unitPrice * chProd.discountValue / 100)}" type="currency"/>
                                                                </span>
                                                            </p>
                                                        </c:if>
                                                        <c:if test="${chProd.discountValue eq 0}">
                                                            <span class="regular-price">
                                                                <span class="price">
                                                                    <fmt:setLocale value="en_US"/>
                                                                    <fmt:formatNumber value="${sku.unitPrice}" type="currency"/>
                                                                </span> 
                                                            </span>
                                                        </c:if>
                                                    </div>
                                                </div>
                                            </div>
                                            <c:if test="${status.count ne 0 && status.count % numCols == 0}">
                                                <c:set var="rowCount" value="${rowCount + 1}"/>
                                            </div>
                                            <div class="column-item clearfix">
                                            </c:if>
                                        </c:if>
                                    </c:forEach>
                                </div>
                                <!--div class="column-item clearfix">
                                    <div class="item clearfix">
                                        <a href="awesome-jewel-product-417.html" title="Awesome Jewel Product" class="product-image"> <img src="resource/client/media/catalog/product/cache/17/thumbnail/100x120/9df78eab33525d08d6e5fb8d27136e95/9/4/94-900x1200_2.jpg" alt="Awesome Jewel Product" width="100" height="120" /> </a>
                                        <div class="details-section">
                                            <h2 class="product-name">
                                                <a href="awesome-jewel-product-417.html" title="Awesome Jewel Product">Awesome Jewel Product</a>
                                            </h2>
                                            <div class="price-box"> <span class="regular-price" id="product-price-54">
                                                    <span class="price">$439.00</span> </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item clearfix">
                                        <a href="awesome-jewel-product.html" title="Awesome Jewel Product" class="product-image"> <img src="resource/client/media/catalog/product/cache/17/thumbnail/100x120/9df78eab33525d08d6e5fb8d27136e95/0/4/04-900x1200_2.jpg" alt="Awesome Jewel Product" width="100" height="120" /> </a>
                                        <div class="details-section">
                                            <h2 class="product-name">
                                                <a href="awesome-jewel-product.html" title="Awesome Jewel Product">Awesome Jewel Product</a>
                                            </h2>
                                            <div class="price-box"> <span class="regular-price" id="product-price-45">
                                                    <span class="price">$599.00</span> </span>
                                            </div>
                                        </div>
                                    </div>
                                </div-->
                            </div>
                        </div>
                        <script type="text/javascript">
                            //<![CDATA[
                            jQuery(function($) {
                                var upsellvlistslider = $('.upsell-vlist-slider');
                                upsellvlistslider.owlCarousel({
                                    lazyLoad: true,
                                    responsive: true,
                                    singleItem: true,
                                    responsiveRefreshRate: 50,
                                    autoPlay: true,
                                    stopOnHover: true,
                                    rewindNav: true,
                                    rewindSpeed: 600,
                                    pagination: false,
                                    navigation: true,
                                    navigationText: ["<span class='fa fa-angle-left'></span>", "<span class='fa fa-angle-right'></span>"]
                                });
                            });
                            //]]>
                        </script>
                    </div>
                </div>
                <div class="postscript"></div>
            </div>
        </div>
    </tiles:putAttribute>
</tiles:insertDefinition>
