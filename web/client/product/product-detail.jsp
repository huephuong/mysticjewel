<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="/admin/layout/taglib.jsp"%>
<tiles:insertDefinition name="page-client">
    <tiles:putAttribute name="title" value="${prod.productName}" />
    <tiles:putAttribute name="body-class" value=" catalog-product-view catalog-product-view product-awesome-jewel-product categorypath-fashion-html category-fashion  wide" />
    <tiles:putAttribute name="page-css">
        <link rel="stylesheet" type="text/css" href="<c:url value="/"/>resource/client/media/css/c78a949c76158a4d75f94587ee9c6aab.css" media="all" />
    </tiles:putAttribute>
    <tiles:putAttribute name="page-js">
        <script type="text/javascript" src="<c:url value="/"/>resource/client/js/varien/product.js"></script>
        <script type="text/javascript" src="<c:url value="/"/>resource/client/js/varien/configurable.js"></script>
        <script type="text/javascript" src="<c:url value="/"/>resource/client/js/calendar/calendar.js"></script>
        <script type="text/javascript" src="<c:url value="/"/>resource/client/js/calendar/calendar-setup.js"></script>
        <script type="text/javascript" src="<c:url value="/"/>resource/client/js/elegento/jquery/plugins/jquery.form.js"></script>
        <script type="text/javascript" src="<c:url value="/"/>resource/client/js/elegento/jquery/plugins/jquery.elevatezoom.min.js"></script>
    </tiles:putAttribute>
    <tiles:putAttribute name="content">
        <div class="top-container">
            <div class="breadcrumbs bctype2">
                <div class="container">
                    <div class="breadcrumbs-inner">
                        <div class="bc-page-title">
                            <h1><span>${prod.categoryID.categoryName}</span></h1>
                        </div>
                        <ul>
                            <li class="home"> <a href="<c:url value="/home"/>" title="Go to Home Page">Home</a> <span> / </span> </li>
                            <li class="category3"> <a href="<c:url value="/products?category=${prod.categoryID.parentID.categoryName}"/>" title="${prod.categoryID.parentID.categoryName}">${prod.categoryID.parentID.categoryName}</a> <span> / </span> </li>
                            <li class="product"> <strong>${prod.categoryID.categoryName}</strong> </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="main-container col1-layout">
            <div class="main-before-top-container"></div>
            <div class="main container">
                <div class="preface"></div>
                <div class="col-main">
                    <div class="clearfix"></div>
                    <tiles:insertTemplate template="/client/layout/toggle.jsp" />
                    <div id="messages_product_view"></div>
                    <div class="product-view">
                        <div class="product-essential">
                            <div class="grid-container">
                                <div class="grid12-9 no-padding-left-lg no-padding-left-md no-padding-xs no-padding-sm grid-xs-full grid-sm-full">
                                    <form action="#" method="post" id="product_addtocart_form">
                                        <div class="no-display">
                                            <input type="hidden" name="skuID" value="${curSku.skuID}" />
                                            <input type="hidden" name="related_product" id="related-products-field" value="" />
                                        </div>
                                        <div class="product-img-box grid12-6 grid-xs-full grid-sm-full">
                                            <!-- Product Image -->
                                            <div class="product-image product-image-zoom">
                                                <div class="product-image-slider eg-gallery-popup owl-carousel">
                                                    <c:forEach items="${prod.productImageList}" var="img">
                                                        <div class="image-item">
                                                            <img class="gallery-image" src="<c:url value="/resource/images/"/>${img.imageLink}" alt="PRODUCT_IMG" data-zoom-image="<c:url value="/resource/images/"/>${img.imageLink}" width="409" height="546" />
                                                            <a class="product-image-colorbox grid-xs-hide" href="<c:url value="/resource/images/"/>${img.imageLink}" data-rel="egtrig[eg-gallery-popup]"></a>
                                                        </div>
                                                    </c:forEach>
                                                </div>
                                                <script type="text/javascript">
                                                    jQuery('.product-image-slider').owlCarousel({
                                                        items: 1,
                                                        navigation: true,
                                                        pagination: false,
                                                        lazyLoad: false,
                                                        rewindNav: false,
                                                        addClassActive: true,
                                                        autoHeight: true,
                                                        itemsCustom: [1600, 1],
                                                        navigationText: ["<span class='icon-royal icon-less'></span>", "<span class='icon-royal icon-greater'></span>"],
                                                        afterMove: function(args) {
                                                            var owlMain = jQuery(".product-image-slider").data('owlCarousel');
                                                            var owlThumbs = jQuery("#more-views").data('owlCarousel');
                                                            jQuery('.active-thumbnail').removeClass('active-thumbnail')
                                                            jQuery("#more-views").find('.owl-item').eq(owlMain.currentItem).addClass('active-thumbnail');
                                                            if (typeof owlThumbs != 'undefined') {
                                                                owlThumbs.goTo(owlMain.currentItem - 1);
                                                            }
                                                        }
                                                    });
                                                    jQuery(document).ready(function() {
                                                        jQuery.fn.egImageZoom({
                                                            zoomOptions: {
                                                                responsive: true,
                                                                cursor: 'crosshair',
                                                                easing: true,
                                                                zoomType: 'inner',
                                                                zoomWindowFadeIn: '500',
                                                                zoomWindowFadeOut: '500',
                                                                scrollZoom: true,
                                                            }
                                                        });
                                                        var owlMain = jQuery(".product-image-slider").data('owlCarousel');
                                                        jQuery("#more-views").find('.owl-item').eq(owlMain.currentItem).addClass('active-thumbnail');
                                                    });
                                                    jQuery(window).on('delayed-resize', function() {
                                                        jQuery.fn.egImageZoom({
                                                            zoomOptions: {
                                                                responsive: true,
                                                                cursor: 'crosshair',
                                                                easing: true,
                                                                zoomType: 'inner',
                                                                zoomWindowFadeIn: '500',
                                                                zoomWindowFadeOut: '500',
                                                                scrollZoom: true,
                                                            }
                                                        });
                                                    });
                                                    jQuery('.product-image-slider a').click(function(e) {
                                                        e.preventDefault();
                                                    });
                                                </script>
                                            </div>
                                            <!-- More Views -->        
                                            <div class="more-views">
                                                <div id="more-views" class="owl-carousel">
                                                    <c:forEach items="${prod.productImageList}" var="img">
                                                        <a href="#"><img src="<c:url value="/resource/images/"/>${img.imageLink}" alt="PRODUCT_IMG" width="100" height="120" /></a>
                                                        </c:forEach>
                                                </div>
                                                <script type="text/javascript">
                                                    jQuery('#more-views').owlCarousel({
                                                        items: 3,
                                                        transitionStyle: "fade",
                                                        navigation: true,
                                                        pagination: false,
                                                        lazyLoad: true,
                                                        addClassActive: true,
                                                        navigationText: ["<span class='icon-royal icon-less'></span>", "<span class='icon-royal icon-greater'></span>"],
                                                        itemsCustom: [
                                                            [0, 2],
                                                            [479, 2],
                                                            [619, 2],
                                                            [768, 2],
                                                            [1200, 3],
                                                            [1600, 3]
                                                        ],
                                                    });
                                                    jQuery('#more-views .owl-item').click(function(e) {
                                                        var owlMain = jQuery(".product-image-slider").data('owlCarousel');
                                                        var owlThumbs = jQuery("#more-views").data('owlCarousel');
                                                        owlMain.goTo(jQuery(e.currentTarget).index());
                                                    });
                                                    jQuery('#more-views a').click(function(e) {
                                                        e.preventDefault();
                                                    });
                                                </script>
                                            </div>
                                        </div>
                                        <!-- PRODUCT -->        
                                        <div class="product-shop grid12-6 grid-xs-full grid-sm-full">
                                            <div class="product-name">
                                                <h1>${prod.productName}</h1>
                                            </div>
                                            <!-- Price & Rating -->
                                            <div class="price-rating clearfix">
                                                <div class="ratings">
                                                    <div class="rating-box">
                                                        <c:if test="${prod.rating gt 0}">
                                                            <div class="rating theme-color" style="width:${prod.rating}%"></div>
                                                        </c:if>
                                                        <c:if test="${prod.rating eq 0}">
                                                            <div class="rating theme-color" style="width:0%"></div>
                                                        </c:if>
                                                    </div>
                                                    <p class="rating-links"><a href="#reviews">${fn:length(prod.reviewList)} Review(s)</a> </p>
                                                </div>
                                                <div class="price-box">
                                                    <c:if test="${prod.discountValue gt 0}">
                                                        <p class="old-price"> <span class="price-label">Regular Price:</span>
                                                            <span class="price">
                                                                <fmt:setLocale value="en_US"/>
                                                                <fmt:formatNumber value="${curSku.unitPrice}" type="currency"/>
                                                            </span>
                                                        </p>
                                                        <p class="special-price"> <span class="price-label">Special Price</span>
                                                            <span class="price" style="color: #cda85c">
                                                                <fmt:setLocale value="en_US"/>
                                                                <fmt:formatNumber value="${curSku.unitPrice - (curSku.unitPrice * prod.discountValue / 100)}" type="currency"/>
                                                            </span>
                                                        </p>
                                                    </c:if>
                                                    <c:if test="${prod.discountValue eq 0}">
                                                        <span class="regular-price">
                                                            <span class="price">
                                                                <fmt:setLocale value="en_US"/>
                                                                <fmt:formatNumber value="${curSku.unitPrice}" type="currency"/>
                                                            </span> 
                                                        </span>
                                                    </c:if>
                                                </div>
                                            </div>
                                            <div class="short-description">${prod.productDesc}</div>
                                            <!-- Size -->
                                            <c:if test="${!empty prod.categoryID.parentID.skuSizeList}">
                                                <div class="add-to-box">
                                                    <div class="add-to-cart">
                                                        <div class="qty-wrapper">
                                                            <label for="size">Size:</label>
                                                            <select id="qty" name="size" class="input-text qty" onchange="location = this.value" style="width: 74px !important">
                                                                <c:forEach items="${prod.skuList}" var="skuList">
                                                                    <option <c:if test="${skuList.sizeID.sizeValue eq curSku.sizeID.sizeValue}">selected</c:if> value="<c:url value="/"/>product-detail?skuid=${skuList.skuID}">${skuList.sizeID.sizeValue}</option>
                                                                </c:forEach>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </c:if>
                                            <!-- Add To Cart -->
                                            <div class="single_variation_wrap">
                                                <div class="add-to-cart">
                                                    <div class="qty-wrapper">
                                                        <label for="qty">QTY:</label>
                                                        <input type="text" name="qty" id="qty" maxlength="12" value="1" title="Qty" class="input-text qty" />
                                                        <div id="qty_up_btn" class="qty-btn qty-up-btn">+</div>
                                                        <div id="qty_down_btn" class="qty-btn qty-down-btn">-</div>
                                                    </div>
                                                    <button type="submit" title="Add to Cart" class="button btn-cart">
                                                        <span><span>Add to Cart</span></span>
                                                    </button>
                                                    <div class="ajaxcart_loading" style="display:none"> <img src="resource/client/skin/frontend/base/default/images/elegento/ajax/ajax-loader(1).gif" alt="Loading" /> </div>
                                                </div>
                                                <div class="add-to-links">
                                                    <div class="wishlist-link">
                                                        <a href="<c:url value="/"/>add-wishlist/${prod.productID}" onclick="productAddToCartForm.submitLight(this, this.href);
                                                                return false;" class="link-wishlist">Add to Wishlist</a>
                                                    </div>
                                                    <div class="compare-link">
                                                        <a href="<c:url value="/product-compare/add?productid=${prod.productID}"/>" class="link-compare">Add to Compare</a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="supplement-detail single_variation_wrap">
                                                <p class="product-code">SKU: <span>${curSku.skuID}</span> </p>
                                                <p class="product-categories">Categories:
                                                    <a href="<c:url value="/"/>products?category=${prod.categoryID.parentID.categoryID}"> <span>${prod.categoryID.parentID.categoryName}</span> </a>,&nbsp;
                                                    <a href="<c:url value="/"/>products?category=${prod.categoryID.categoryID}"> <span>${prod.categoryID.categoryName}</span> </a>
                                                </p>
                                            </div>
                                        </div>
                                    </form>
                                    <div class="clearfix"></div>
                                    <!-- Addition Info & Review $ Custom Tab -->
                                    <div class="tabbable clearfix">
                                        <div class="nav-tabs-container grid-xs-full">
                                            <ul id="tabs" class="nav nav-tabs align-left grid-xs-full" data-tabs="tabs">
                                                <li class="active"><a href="#additional" data-toggle="tab">Additional Info</a></li>
                                                <li class=""><a href="#reviews" data-toggle="tab">Reviews</a></li>
                                            </ul>
                                        </div>
                                        <div id="tab-content" class="tab-content grid-xs-full no-padding-xs">
                                            <div class="tab-pane panel panel-default active" id="additional">
                                                <div class="panel panel-heading"><a href="#additional-accordion" data-parent="#tab-content" data-toggle="collapse" class="collapsed">Additional Info</a> </div>
                                                <div id="additional-accordion" class="panel-collapse collapse">
                                                    <div class="panel-body clearfix">
                                                        <table class="data-table" id="product-attribute-specs-table">
                                                            <tbody>
                                                                <c:forEach items="${prod.attributeList}" var="att">
                                                                    <tr>
                                                                        <th class="label">${att.attributeGroupID.attributeGroupName}</th>
                                                                        <td class="data">${att.attributeValue}</td>
                                                                    </tr>
                                                                </c:forEach>
                                                            </tbody>
                                                        </table>
                                                        <script type="text/javascript">
                                                            decorateTable('product-attribute-specs-table')
                                                        </script>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tab-pane panel panel-default" id="reviews">
                                                <div class="panel panel-heading"><a href="#reviews-accordion" data-parent="#tab-content" data-toggle="collapse" class="collapsed">Reviews</a> </div>
                                                <div id="reviews-accordion" class="panel-collapse collapse">
                                                    <div class="panel-body clearfix">
                                                        <div class="box-collateral box-reviews clearfix" id="customer-reviews">
                                                            <div class="reviews-list">
                                                                <h2>${fn:length(prod.reviewList)} Review(s) for <span>"${prod.productName}"</span></h2>
                                                                <c:forEach items="${prod.reviewList}" var="review">
                                                                    <div class="item clearfix">
                                                                        <div class="detail">
                                                                            <div class="review-title"> ${review.title} </div>
                                                                            <div class="ratings">
                                                                                <div class="rating-box">
                                                                                    <c:if test="${review.rating gt 0}">
                                                                                        <div class="rating theme-color" style="width:${review.rating}%"></div>
                                                                                    </c:if>
                                                                                    <c:if test="${review.rating eq 0}">
                                                                                        <div class="rating theme-color" style="width:0%"></div>
                                                                                    </c:if>
                                                                                </div>
                                                                            </div>
                                                                            <div class="date"><span>${review.customer.customerUserName}</span>, <fmt:formatDate type = "both" dateStyle = "short" timeStyle = "short" value = "${review.dateCreated}" /> </div>
                                                                            <div class="review-detail">${review.content}</div>
                                                                            <div class="review-detail" ${(review.adminResponse != null && review.adminResponse != '')  ? "" : "hidden"} ><span style="color:red">Admin Response:</span> ${review.adminResponse}</div>
                                                                        </div>
                                                                    </div>
                                                                </c:forEach>
                                                            </div>
                                                            <c:if test="${submitComment == 'yes'}">
                                                                <div class="form-add">
                                                                    <h2 class="r-separator"><span>Write Your Own Review</span></h2>
                                                                    <form action="<c:url value="/" />review-product" method="post" id="review-form">
                                                                        <input name="form_key" type="hidden" value="JYi3ceiZkC42i1tf" />
                                                                        <input type="hidden" name="prodID" value="${prod.productID}" />
                                                                        <input type="hidden" name="skuID" value="${prod.skuList[0].skuID}" />
                                                                        <fieldset> <span id="input-message-box"></span>
                                                                            <table class="data-table" id="product-review-table">
                                                                                <thead>
                                                                                    <tr>
                                                                                        <th>&nbsp;</th>
                                                                                        <th><span class="nobr">1 star</span> </th>
                                                                                        <th><span class="nobr">2 stars</span> </th>
                                                                                        <th><span class="nobr">3 stars</span> </th>
                                                                                        <th><span class="nobr">4 stars</span> </th>
                                                                                        <th><span class="nobr">5 stars</span> </th>
                                                                                    </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <th>Rating</th>
                                                                                        <td class="value">
                                                                                            <input type="radio" name="inputRating" id="Price_1" value="20" class="radio" />
                                                                                        </td>
                                                                                        <td class="value">
                                                                                            <input type="radio" name="inputRating" id="Price_2" value="40" class="radio" />
                                                                                        </td>
                                                                                        <td class="value">
                                                                                            <input type="radio" name="inputRating" id="Price_3" value="60" class="radio" />
                                                                                        </td>
                                                                                        <td class="value">
                                                                                            <input type="radio" name="inputRating" id="Price_4" value="80" class="radio" />
                                                                                        </td>
                                                                                        <td class="value">
                                                                                            <input type="radio" name="inputRating" id="Price_5" value="100" class="radio" />
                                                                                        </td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>
                                                                            <input type="hidden" name="validate_rating" class="validate-rating" value="" />
                                                                            <script type="text/javascript">
                                                                                decorateTable('product-review-table')
                                                                            </script>
                                                                            <ul class="form-list">
                                                                                <li>
                                                                                    <label for="summary_field" class="required"><em>*</em>Review Summary</label>
                                                                                    <div class="input-box">
                                                                                        <input type="text" name="title" id="summary_field" class="input-text required-entry" value="" />
                                                                                    </div>
                                                                                </li>
                                                                                <li>
                                                                                    <label for="review_field" class="required"><em>*</em>Write Your Review</label>
                                                                                    <div class="input-box">
                                                                                        <textarea name="detail" id="review_field" cols="5" rows="3" class="required-entry"></textarea>
                                                                                    </div>
                                                                                </li>
                                                                            </ul>
                                                                        </fieldset>
                                                                        <div class="buttons-set">
                                                                            <button type="submit" title="Submit Review" class="button" >
                                                                                <span><span>Submit Review</span></span>
                                                                            </button>
                                                                        </div>
                                                                    </form>
                                                                    <script type="text/javascript">
                                                                        //<![CDATA[
                                                                        var dataForm = new VarienForm('review-form');
                                                                        Validation.addAllThese(
                                                                                [
                                                                                    ['validate-rating', 'Please select one of each of the ratings above', function(v) {
                                                                                            var trs = $('product-review-table').select('tr');
                                                                                            var inputs;
                                                                                            var error = 1;
                                                                                            for (var j = 0; j < trs.length; j++) {
                                                                                                var tr = trs[j];
                                                                                                if (j > 0) {
                                                                                                    inputs = tr.select('input');
                                                                                                    for (i in inputs) {
                                                                                                        if (inputs[i].checked == true) {
                                                                                                            error = 0;
                                                                                                        }
                                                                                                    }
                                                                                                    if (error == 1) {
                                                                                                        return false;
                                                                                                    } else {
                                                                                                        error = 1;
                                                                                                    }
                                                                                                }
                                                                                            }
                                                                                            return true;
                                                                                        }]
                                                                                ]);
                                                                        //]]>
                                                                    </script>
                                                                </div>
                                                            </c:if>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Side Bar -->
                                <div class="grid12-3 grid-xs-full grid-sm-full">
                                    <div class="sidebar-content">
                                        <c:if test="${not empty prod.colID}">
                                            <div class="cms-block vlist-slider-block">
                                                <div class="block-title">
                                                    <h3><span>FROM COLLECTION</span></h3>
                                                </div>
                                                <div class="itemslider-wrapper vlist-slider-wrapper clearfix">
                                                    <div class="upsell-vlist-slider top-right-nav">
                                                        <c:set var="numCols" value="3"/>
                                                        <c:set var="numRows" value="3"/>
                                                        <c:set var="rowCount" value="0"/>
                                                        <div class="column-item clearfix">
                                                            <c:forEach items="${prod.colID.productList}" var="colProd" varStatus="status">
                                                                <c:if test="${rowCount lt numRows}">
                                                                    <c:set var="sku" value="${colProd.skuList[0]}"/>
                                                                    <div class="item clearfix">
                                                                        <a href="<c:url value="/product-detail?skuid=${sku.skuID}"/>" title="${colProd.productName}" class="product-image">
                                                                            <c:set var="firstImg" value="${colProd.productImageList[0]}"/>
                                                                            <img src="<c:url value="/" />resource/images/${firstImg.imageLink}" alt="${colProd.productName}" width="100" height="120" />
                                                                        </a>
                                                                        <div class="details-section">
                                                                            <h2 class="product-name">
                                                                                <a href="<c:url value="/product-detail?skuid=${sku.skuID}"/>" title="${colProd.productName}">${colProd.productName}</a>
                                                                            </h2>
                                                                            <div class="price-box">
                                                                                <c:if test="${colProd.discountValue gt 0}">
                                                                                    <p class="old-price"> <span class="price-label">Regular Price:</span>
                                                                                        <span class="price">
                                                                                            <fmt:setLocale value="en_US"/>
                                                                                            <fmt:formatNumber value="${sku.unitPrice}" type="currency"/>
                                                                                        </span>
                                                                                    </p>
                                                                                    <p class="special-price"> <span class="price-label">Special Price</span>
                                                                                        <span class="price">
                                                                                            <fmt:setLocale value="en_US"/>
                                                                                            <fmt:formatNumber value="${sku.unitPrice - (sku.unitPrice * colProd.discountValue / 100)}" type="currency"/>
                                                                                        </span>
                                                                                    </p>
                                                                                </c:if>
                                                                                <c:if test="${colProd.discountValue eq 0}">
                                                                                    <span class="regular-price">
                                                                                        <span class="price">
                                                                                            <fmt:setLocale value="en_US"/>
                                                                                            <fmt:formatNumber value="${sku.unitPrice}" type="currency"/>
                                                                                        </span> 
                                                                                    </span>
                                                                                </c:if>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <c:if test="${status.count ne 0 && status.count % numCols == 0}">
                                                                        <c:set var="rowCount" value="${rowCount + 1}"/>
                                                                    </div>
                                                                    <div class="column-item clearfix">
                                                                    </c:if>
                                                                </c:if>
                                                            </c:forEach>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </c:if>

                                        <!-- Recommended Product -->
                                        <c:if test="${not empty customer}">
                                            <div class="cms-block vlist-slider-block">
                                                <div class="block-title">
                                                    <h3><span style="color: #cda85c"><strong>RECOMMENDED FOR YOU</strong></span></h3>
                                                </div>
                                                <div class="itemslider-wrapper vlist-slider-wrapper clearfix">
                                                    <div class="upsell-vlist-slider top-right-nav">
                                                        <div class="column-item clearfix">
                                                            <c:set var="sku" value="${recProd.skuList[0]}"/>
                                                            <div class="item clearfix">
                                                                <a href="<c:url value="/product-detail?skuid=${sku.skuID}"/>" title="${recProd.productName}" class="product-image">
                                                                    <c:set var="firstImg" value="${recProd.productImageList[0]}"/>
                                                                    <img src="<c:url value="/" />resource/images/${firstImg.imageLink}" alt="${recProd.productName}" width="100" height="120" />
                                                                </a>
                                                                <div class="details-section">
                                                                    <h2 class="product-name">
                                                                        <a href="<c:url value="/product-detail?skuid=${sku.skuID}"/>" title="${recProd.productName}">${recProd.productName}</a>
                                                                    </h2>
                                                                    <div class="price-box">
                                                                        <c:if test="${recProd.discountValue gt 0}">
                                                                            <p class="old-price"> <span class="price-label">Regular Price:</span>
                                                                                <span class="price">
                                                                                    <fmt:setLocale value="en_US"/>
                                                                                    <fmt:formatNumber value="${sku.unitPrice}" type="currency"/>
                                                                                </span>
                                                                            </p>
                                                                            <p class="special-price"> <span class="price-label">Special Price</span>
                                                                                <span class="price">
                                                                                    <fmt:setLocale value="en_US"/>
                                                                                    <fmt:formatNumber value="${sku.unitPrice - (sku.unitPrice * recProd.discountValue / 100)}" type="currency"/>
                                                                                </span>
                                                                            </p>
                                                                        </c:if>
                                                                        <c:if test="${recProd.discountValue eq 0}">
                                                                            <span class="regular-price">
                                                                                <span class="price">
                                                                                    <fmt:setLocale value="en_US"/>
                                                                                    <fmt:formatNumber value="${sku.unitPrice}" type="currency"/>
                                                                                </span> 
                                                                            </span>
                                                                        </c:if>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <script type="text/javascript">
                                                    //<![CDATA[
                                                    jQuery(function($) {
                                                        var upsellvlistslider = $('.upsell-vlist-slider');
                                                    });
                                                    //]]>
                                                </script>
                                            </div>
                                        </c:if>

                                        <div class="cms-block vlist-slider-block">
                                            <div class="block-title">
                                                <h3><span>VIEWED PRODUCTS</span></h3>
                                            </div>
                                            <div class="itemslider-wrapper vlist-slider-wrapper clearfix">
                                                <div class="upsell-vlist-slider top-right-nav">
                                                    <c:set var="numCols" value="3"/>
                                                    <c:set var="numRows" value="3"/>
                                                    <c:set var="rowCount" value="0"/>
                                                    <div class="column-item clearfix">
                                                        <c:forEach items="${ckProductHistory}" var="chProd" varStatus="status">
                                                            <c:if test="${rowCount lt numRows}">
                                                                <c:set var="sku" value="${chProd.skuList[0]}"/>
                                                                <div class="item clearfix">
                                                                    <a href="<c:url value="/product-detail?skuid=${sku.skuID}"/>" title="${chProd.productName}" class="product-image">
                                                                        <c:set var="firstImg" value="${chProd.productImageList[0]}"/>
                                                                        <img src="<c:url value="/" />resource/images/${firstImg.imageLink}" alt="${chProd.productName}" width="100" height="120" />
                                                                    </a>
                                                                    <div class="details-section">
                                                                        <h2 class="product-name">
                                                                            <a href="<c:url value="/product-detail?skuid=${sku.skuID}"/>" title="${chProd.productName}">${chProd.productName}</a>
                                                                        </h2>
                                                                        <div class="price-box">
                                                                            <c:if test="${chProd.discountValue gt 0}">
                                                                                <p class="old-price"> <span class="price-label">Regular Price:</span>
                                                                                    <span class="price">
                                                                                        <fmt:setLocale value="en_US"/>
                                                                                        <fmt:formatNumber value="${sku.unitPrice}" type="currency"/>
                                                                                    </span>
                                                                                </p>
                                                                                <p class="special-price"> <span class="price-label">Special Price</span>
                                                                                    <span class="price">
                                                                                        <fmt:setLocale value="en_US"/>
                                                                                        <fmt:formatNumber value="${sku.unitPrice - (sku.unitPrice * chProd.discountValue / 100)}" type="currency"/>
                                                                                    </span>
                                                                                </p>
                                                                            </c:if>
                                                                            <c:if test="${chProd.discountValue eq 0}">
                                                                                <span class="regular-price">
                                                                                    <span class="price">
                                                                                        <fmt:setLocale value="en_US"/>
                                                                                        <fmt:formatNumber value="${sku.unitPrice}" type="currency"/>
                                                                                    </span> 
                                                                                </span>
                                                                            </c:if>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <c:if test="${status.count ne 0 && status.count % numCols == 0}">
                                                                    <c:set var="rowCount" value="${rowCount + 1}"/>
                                                                </div>
                                                                <div class="column-item clearfix">
                                                                </c:if>
                                                            </c:if>
                                                        </c:forEach>
                                                    </div>
                                                    <!--div class="column-item clearfix">
                                                        <div class="item clearfix">
                                                            <a href="awesome-jewel-product-417.html" title="Awesome Jewel Product" class="product-image"> <img src="resource/client/media/catalog/product/cache/17/thumbnail/100x120/9df78eab33525d08d6e5fb8d27136e95/9/4/94-900x1200_2.jpg" alt="Awesome Jewel Product" width="100" height="120" /> </a>
                                                            <div class="details-section">
                                                                <h2 class="product-name">
                                                                    <a href="awesome-jewel-product-417.html" title="Awesome Jewel Product">Awesome Jewel Product</a>
                                                                </h2>
                                                                <div class="price-box"> <span class="regular-price" id="product-price-54">
                                                                        <span class="price">$439.00</span> </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="item clearfix">
                                                            <a href="awesome-jewel-product.html" title="Awesome Jewel Product" class="product-image"> <img src="resource/client/media/catalog/product/cache/17/thumbnail/100x120/9df78eab33525d08d6e5fb8d27136e95/0/4/04-900x1200_2.jpg" alt="Awesome Jewel Product" width="100" height="120" /> </a>
                                                            <div class="details-section">
                                                                <h2 class="product-name">
                                                                    <a href="awesome-jewel-product.html" title="Awesome Jewel Product">Awesome Jewel Product</a>
                                                                </h2>
                                                                <div class="price-box"> <span class="regular-price" id="product-price-45">
                                                                        <span class="price">$599.00</span> </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div-->
                                                </div>
                                            </div>
                                            <script type="text/javascript">
                                                //<![CDATA[
                                                jQuery(function($) {
                                                    var upsellvlistslider = $('.upsell-vlist-slider');
                                                    upsellvlistslider.owlCarousel({
                                                        lazyLoad: true,
                                                        responsive: true,
                                                        singleItem: true,
                                                        responsiveRefreshRate: 50,
                                                        autoPlay: true,
                                                        stopOnHover: true,
                                                        rewindNav: true,
                                                        rewindSpeed: 600,
                                                        pagination: false,
                                                        navigation: true,
                                                        navigationText: ["<span class='fa fa-angle-left'></span>", "<span class='fa fa-angle-right'></span>"]
                                                    });
                                                });
                                                //]]>
                                            </script>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="clearer"></div>
                            <script type="text/javascript">
                                //<![CDATA[
                                (function($) {
                                    var form = $('#product_addtocart_form');
                                    var status = $('.ajaxcart_loading');
                                    form.ajaxForm({
                                        url: '<c:url value="/" />add-product-to-cart',
                                        dataType: 'json',
                                        data: {
                                            'isAjax': 1
                                        },
                                        beforeSend: function() {
                                            status.show();
                                        },
                                        success: function(data) {
                                            status.hide();
                                            EGAjaxObject.updateAjaxData(data, '.block-cart', 'cart');
                                        }
                                    });
                                })(jQuery);
                                //]]>
                            </script>
                        </div>
                        <!-- Related Product -->
                        <div class="product-collateral">
                            <div class="box-collateral box-related">
                                <div class="grid-container">
                                    <div class="block-title grid-full eg-tac eg-ttu">
                                        <h3 class="eg-fwn">RELATED PRODUCT</h3>
                                    </div>
                                    <div class="block-content">
                                        <div class="clearfix"></div>
                                        <div class="product-list-slider">
                                            <div class="home-productlist-slider owl-carousel">
                                                <!-- NEW ITEMS -->
                                                <c:forEach items="${relatedList}" var="relatedProd">
                                                    <c:set var="sku" value="${relatedProd.skuList[0]}"/>
                                                    <div class="column-item clearfix">
                                                        <div class="item clearfix">
                                                            <div class="media-section eg-image-slider">
                                                                <a href="<c:url value="/product-detail?skuid=${sku.skuID}"/>" title="${relatedProd.productName}" class="product-image" data-images="">
                                                                    <c:set var="firstImg" value="${relatedProd.productImageList[0]}"/>
                                                                    <img src="<c:url value="/"/>resource/images/${firstImg.imageLink}" alt="${relatedProd.productName}" style="width:100%" width="350" height="455" />
                                                                </a>
                                                                <div class="product-labels">
                                                                    <c:if test="${relatedProd.isNew}">
                                                                        <span class="new-product">
                                                                            <span>New</span>
                                                                        </span>
                                                                    </c:if>
                                                                    <c:if test="${relatedProd.discountValue gt 0}">
                                                                        <span class="discount-code">
                                                                            <span>-${relatedProd.discountValue}%</span>
                                                                        </span>
                                                                    </c:if>
                                                                </div>
                                                                <div class="hover-section">
                                                                    <div class="hover-links-wrapper a-center">
                                                                        <div class="eg-tac link-icon">
                                                                            <a href="<c:url value="/" />add-wishlist/${relatedProd.productID}" class="link-wishlist" title="Wishlist"> <span class="link-ico"><span class="fa fa-heart"></span></span> <span class="link-label">Wishlist</span> </a>
                                                                        </div>
                                                                        <div class="eg-tac link-icon">
                                                                            <a href="<c:url value="/product-compare/add?productid=${relatedProd.productID}"/>" class="" title="Compare"> <span class="link-ico"><span class="fa fa-exchange"></span></span> <span class="link-label">Compare</span> </a>
                                                                        </div>
                                                                        <div class="eg-tac link-both">
                                                                            <a href="javascript:void(0);" data-target="<c:url value="/quickview?skuid=${sku.skuID}"/>" class="link-quickview" title="Quick View"> <span class="link-ico"><span class="fa fa-search"></span></span> <span class="link-label">Quickview</span> </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="details-section">
                                                                <div class="product-category"> <a href="#" title="Jewellery">${relatedProd.categoryID.categoryName}</a> </div>
                                                                <h2 class="product-name">
                                                                    <a href="<c:url value="/product-detail?skuid=${sku.skuID}"/>" title="${relatedProd.productName}">${relatedProd.productName}</a>
                                                                </h2>
                                                                <div class="review rating-only">
                                                                    <div class="ratings clearfix">
                                                                        <div class="rating-box">
                                                                            <c:if test="${relatedProd.rating gt 0}">
                                                                                <div class="rating theme-color" style="width:${relatedProd.rating}%"></div>
                                                                            </c:if>
                                                                            <c:if test="${relatedProd.rating eq 0}">
                                                                                <div class="rating theme-color" style="width:0%"></div>
                                                                            </c:if>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="price-box">
                                                                    <c:if test="${relatedProd.discountValue gt 0}">
                                                                        <p class="old-price"> <span class="price-label">Regular Price:</span>
                                                                            <span class="price">
                                                                                <fmt:setLocale value="en_US"/>
                                                                                <fmt:formatNumber value="${sku.unitPrice}" type="currency"/>
                                                                            </span>
                                                                        </p>
                                                                        <p class="special-price"> <span class="price-label">Special Price</span>
                                                                            <span class="price" style="color: #cda85c">
                                                                                <fmt:setLocale value="en_US"/>
                                                                                <fmt:formatNumber value="${sku.unitPrice - (sku.unitPrice * relatedProd.discountValue / 100)}" type="currency"/>
                                                                            </span>
                                                                        </p>
                                                                    </c:if>
                                                                    <c:if test="${relatedProd.discountValue eq 0}">
                                                                        <span class="regular-price">
                                                                            <span class="price">
                                                                                <fmt:setLocale value="en_US"/>
                                                                                <fmt:formatNumber value="${sku.unitPrice}" type="currency"/>
                                                                            </span> 
                                                                        </span>
                                                                    </c:if>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </c:forEach>
                                                <!-- /ITEMS-->
                                            </div>
                                        </div>
                                        <script type="text/javascript">
                                            //<![CDATA[
                                            jQuery(function($) {
                                                var $homeproductlist = $('.home-productlist-slider');
                                                $homeproductlist.owlCarousel({
                                                    navigation: true,
                                                    lazyLoad: true,
                                                    autoPlay: false,
                                                    itemsCustom: [
                                                        [0, 1],
                                                        [320, 1],
                                                        [480, 1],
                                                        [751, 3],
                                                        [960, 3],
                                                        [1263, 4]
                                                    ],
                                                    pagination: false,
                                                    rewindNav: true,
                                                    rewindSpeed: 600,
                                                    transitionStyle: 'fade',
                                                    navigationText: ["<span class='fa fa-angle-left'></span>", "<span class='fa fa-angle-right'></span>"]
                                                });
                                            });
                                            //]]
                                        </script>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <script type="text/javascript">
                    var lifetime = 3600;
                    var expireAt = Mage.Cookies.expires;
                    if (lifetime > 0) {
                        expireAt = new Date();
                        expireAt.setTime(expireAt.getTime() + lifetime * 1000);
                    }
                    Mage.Cookies.set('external_no_cache', 1, expireAt);
                </script>
            </div>
            <div class="postscript"></div>
        </div>
    </tiles:putAttribute>
</tiles:insertDefinition>
