<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="/admin/layout/taglib.jsp"%>
<tiles:insertDefinition name="page-client">
    <tiles:putAttribute name="title" value="Product Compare" />
    <tiles:putAttribute name="body-class" value=" catalog-category-view categorypath-fashion-html category-fashion  wide" />
    <tiles:putAttribute name="page-css">
        <link rel="stylesheet" type="text/css" href="<c:url value="/" />resource/client/media/css/608a0436af3da7766b9a1705fe393cd8.css" media="all" />
        <link rel="stylesheet" type="text/css" href="<c:url value="/"/>resource/client/media/css/c78a949c76158a4d75f94587ee9c6aab.css" media="all" />
    </tiles:putAttribute>
    <tiles:putAttribute name="page-js">
        <script type="text/javascript" src="<c:url value="/" />resource/client/js/elegento/ajax/layerednav.js"></script>
    </tiles:putAttribute>
    <tiles:putAttribute name="content">
        <!-- TOP BANNER-->
        <div class="top-container">
            <div class="breadcrumbs bctype2">
                <div class="container">
                    <div class="breadcrumbs-inner">
                        <div class="bc-page-title">
                            <h1><span>Product Compare</span></h1> </div>
                        <ul>
                            <li class="home"> <a href="<c:url value="/home" />" title="Go to Home Page">Home</a> <span> / </span> </li>
                            <li class="category3"> <strong>Product Compare</strong> </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="main-container">
            <div class="main-before-top-container"></div>
            <div class="main container">

                <!-- MAIN COL -->
                <div class="col-main">
                    <div class="clearfix"></div>
                    <tiles:insertTemplate template="/client/layout/toggle.jsp" />
                    <div class="page-title category-title">
                        <h1>Product Compare</h1>
                    </div>
                    <div id="eglayerednav_container">
                        <div class="category-products">
                            <div class="toolbar clearfix">
                                <div class="toolbar-upside">
                                    <h3 class="text-center">Product Compare</h3>
                                </div>
                            </div>
                            <c:choose>
                                <c:when test="${empty comparedList}">
                                    <div><h4 class="text-center">Sorry. There aren't any products to compare</h4></div>
                                </c:when>    
                                <c:otherwise>
                                    <table class="data-table" id="product-review-table">
                                        <thead>
                                            <tr>
                                                <th>&nbsp;</th>
                                                    <c:forEach items="${comparedList}" var="prod">
                                                        <c:set var="sku" value="${prod.skuList[0]}"/>
                                                        <c:set var="firstImg" value="${prod.productImageList[0]}"/>
                                                    <th>
                                                        <a href="<c:url value="/product-detail?skuid=${sku.skuID}"/>" title="${prod.productName}" class="product-image" data-images="${firstImg}">
                                                            <img src="<c:url value="/"/>resource/images/${firstImg.imageLink}" alt="Product IMG" width="300" height="300" />
                                                        </a>
                                                    </th>
                                                </c:forEach>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <th>Name</th>
                                                    <c:forEach items="${comparedList}" var="prod">
                                                        <c:set var="sku" value="${prod.skuList[0]}"/>
                                                    <td>
                                                        <a href="<c:url value="/product-detail?skuid=${sku.skuID}"/>" title="${prod.productName}">
                                                            <h4>${prod.productName}</h4>
                                                        </a>
                                                    </td>
                                                </c:forEach>
                                            </tr>
                                            <tr>
                                                <th>Price</th>
                                                    <c:forEach items="${comparedList}" var="prod">
                                                        <c:set var="sku" value="${prod.skuList[0]}"/>
                                                    <td>
                                                        <div class="price-box">
                                                            <c:if test="${prod.discountValue gt 0}">
                                                                <p class="old-price"> <span class="price-label">Regular Price:</span>
                                                                    <span class="price">
                                                                        <fmt:setLocale value="en_US"/>
                                                                        <fmt:formatNumber value="${sku.unitPrice}" type="currency"/>
                                                                    </span>
                                                                </p>
                                                                <p class="special-price"> <span class="price-label">Special Price</span>
                                                                    <span class="price" style="color: #cda85c">
                                                                        <fmt:setLocale value="en_US"/>
                                                                        <fmt:formatNumber value="${sku.unitPrice - (sku.unitPrice * prod.discountValue / 100)}" type="currency"/>
                                                                    </span>
                                                                </p>
                                                            </c:if>
                                                            <c:if test="${prod.discountValue eq 0}">
                                                                <span class="regular-price">
                                                                    <span class="price">
                                                                        <fmt:setLocale value="en_US"/>
                                                                        <fmt:formatNumber value="${sku.unitPrice}" type="currency"/>
                                                                    </span> 
                                                                </span>
                                                            </c:if>
                                                        </div>
                                                    </td>
                                                </c:forEach>
                                            </tr>
                                            <tr>
                                                <th>Rating</th>
                                                    <c:forEach items="${comparedList}" var="prod">
                                                    <td>
                                                        <div class="review rating-only">
                                                            <div class="ratings clearfix">
                                                                <div class="rating-box">
                                                                    <c:if test="${prod.rating gt 0}">
                                                                        <div class="rating theme-color" style="width:${prod.rating}%"></div>
                                                                    </c:if>
                                                                    <c:if test="${prod.rating eq 0}">
                                                                        <div class="rating theme-color" style="width:0%"></div>
                                                                    </c:if>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </c:forEach>
                                            </tr>
                                            <tr>
                                                <th>Category</th>
                                                    <c:forEach items="${comparedList}" var="prod">
                                                    <td>
                                                        ${prod.categoryID.categoryName}
                                                    </td>
                                                </c:forEach>
                                            </tr>
                                            <c:forEach items="${attGroupList}" var="group">
                                                <tr>
                                                    <th>
                                                        ${group.attributeGroupName}
                                                    </th>
                                                    <c:forEach items="${comparedList}" var="prod">
                                                        <td>
                                                            <c:forEach items="${prod.attributeList}" var="att">
                                                                <c:if test="${att.attributeGroupID.attributeGroupID eq group.attributeGroupID}">
                                                                    ${att.attributeValue}
                                                                </c:if>
                                                            </c:forEach>
                                                        </td>
                                                    </c:forEach>
                                                </tr>
                                            </c:forEach>
                                            <tr>
                                                <th>&nbsp;</th>
                                                    <c:forEach items="${comparedList}" var="prod">
                                                    <td>
                                                        <a class="btn btn-default btn-block" href="<c:url value="/product-compare/remove?productid=${prod.productID}"/>">
                                                            Remove
                                                        </a>
                                                    </td>
                                                </c:forEach>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <script type="text/javascript">
                                        decorateTable('product-review-table')
                                    </script>
                                </c:otherwise>
                            </c:choose>
                        </div>
                    </div>
                </div>
                <div class="postscript"></div>
            </div>
        </div>
    </tiles:putAttribute>
</tiles:insertDefinition>
