<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="/admin/layout/taglib.jsp"%>
<tiles:insertDefinition name="page-client">
    <tiles:putAttribute name="title" value="Home" />
    <tiles:putAttribute name="body-class" value=" cms-index-index cms-royal-home-page6  wide cat-sidebar-popup-enabled" />
    <tiles:putAttribute name="page-css">
        <link rel="stylesheet" type="text/css" href="resource/client/media/css/e35fbc23d5f36d5f81767aa433faa7f4.css" media="all" />
    </tiles:putAttribute>
    <tiles:putAttribute name="page-js" value="" />
    <tiles:putAttribute name="content">
        <div class="main-container col1-layout">
            <div class="main-before-top-container"></div>
            <div class="main container">
                <div class="preface"></div>
                <div class="col-main">
                    <div class="clearfix"></div>
                    <tiles:insertTemplate template="/client/layout/toggle.jsp" />
                    <div class="std">
                        <div class="revslider-wrapper-eg" style="margin-top:-30px;margin-bottom:30px;">
                            <link href='http://fonts.googleapis.com/css?family=Dancing+Script:300,400,600,700,800|Open+Sans:300,400,600,700,800|Oswald:300,400,600,700,800|Roboto:300,400,600,700,800' rel='stylesheet' type='text/css' />
                            <div id="rev_slider_6_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" style="margin:0px auto;background-color:#E9E9E9;padding:0px;margin-top:0px;margin-bottom:0px;max-height:600px;">
                                <!-- START REVOLUTION SLIDER  fullwidth mode -->
                                <div id="rev_slider_6_1" class="rev_slider fullwidthabanner" style="display:none;max-height:600px;height:600px;">
                                    <ul>
                                        <!-- COLLECTION SLIDE  -->
                                        <c:forEach items="${colList}" var="trendCol">
                                            <c:if test="${trendCol.isTrending && trendCol.isActive}">
                                                <li data-transition="random" data-slotamount="7" data-masterspeed="300" data-saveperformance="on">
                                                    <!-- MAIN IMAGE --><img src="<c:url value="/"/>resource/images/${trendCol.colBanner}" alt="jewellery-slide02" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">
                                                    <!-- LAYERS -->
                                                    <!-- LAYER NR. 1 -->
                                                    <div class="tp-caption defaultsmallheading lft stt tp-resizeme" data-x="right" data-hoffset="-80" data-y="center" data-voffset="-160" data-speed="900" data-start="1000" data-easing="Power3.easeInOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="600" data-endeasing="Power3.easeIn" style="z-index: 5; max-width: auto; max-height: auto; white-space: nowrap;">MYSTIC JEWEL PRESENTS </div>
                                                    <!-- LAYER NR. 2 -->
                                                    <div class="tp-caption defaultsmallheading tp-fade stt tp-resizeme" data-x="right" data-hoffset="-175" data-y="center" data-voffset="-133" data-speed="600" data-start="1500" data-easing="Power3.easeInOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="300" data-endeasing="Power3.easeIn" style="z-index: 6; max-width: auto; max-height: auto; white-space: nowrap;">
                                                        <div class="horizontal-separator separator-1 grid-xs-hide">&nbsp;</div>
                                                    </div>
                                                    <!-- LAYER NR. 3 -->
                                                    <div class="tp-caption jewellerylargeheading1 skewfromleft stt tp-resizeme" data-x="right" data-hoffset="26" data-y="center" data-voffset="-77" data-speed="900" data-start="2000" data-easing="Power3.easeInOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="300" data-endeasing="Power3.easeIn" style="z-index: 7; max-width: auto; max-height: auto; white-space: nowrap;">${trendCol.colName} </div>
                                                    <!-- LAYER NR. 4 -->
                                                    <div class="tp-caption jewellerymediumheading skewfromright stt tp-resizeme" data-x="right" data-hoffset="-6" data-y="center" data-voffset="-16" data-speed="900" data-start="2500" data-easing="Power3.easeInOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="300" data-endeasing="Power3.easeIn" style="z-index: 8; max-width: auto; max-height: auto; white-space: nowrap;">NEW COLLECTION </div>
                                                    <!-- LAYER NR. 5 -->
                                                    <div class="tp-caption defaultsmallheading tp-fade stt tp-resizeme" data-x="right" data-hoffset="-175" data-y="center" data-voffset="30" data-speed="600" data-start="3000" data-easing="Power3.easeInOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="300" data-endeasing="Power3.easeIn" style="z-index: 9; max-width: auto; max-height: auto; white-space: nowrap;">
                                                        <div class="horizontal-separator separator-1 grid-xs-hide">&nbsp;</div>
                                                    </div>
                                                    <!-- LAYER NR. 8 -->
                                                    <div class="tp-caption defaultbtnlink randomrotate stt tp-resizeme" data-x="right" data-hoffset="-200" data-y="center" data-voffset="141" data-speed="600" data-start="4000" data-easing="Power3.easeInOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="300" data-endeasing="Power3.easeIn" style="z-index: 12; max-width: auto; max-height: auto; white-space: nowrap;"><a href='<c:url value="/"/>products?collection=${trendCol.colID}' class='button btn-link eg-fcf color-white'>SHOP NOW</a> </div>
                                                    <!-- LAYER NR. 10 -->
                                                    <div class="tp-caption tp-fade" data-x="474" data-y="142" data-speed="300" data-start="5200" data-easing="Power3.easeInOut" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="300" style="z-index: 14;"><img src="resource/client/media/revslider/jewellery/yellow-circle.png" alt=""> </div>
                                                    <!-- LAYER NR. 11 -->
                                                    <div class="tp-caption badgesmall skewfromleft stt tp-resizeme" data-x="501" data-y="165" data-speed="300" data-start="5200" data-easing="Power3.easeInOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="300" data-endeasing="Power3.easeIn" style="z-index: 15; max-width: auto; max-height: auto; white-space: nowrap;">SHOP </div>
                                                    <!-- LAYER NR. 12 -->
                                                    <div class="tp-caption badgelarge skewfromleft stt tp-resizeme" data-x="488" data-y="176" data-speed="300" data-start="5200" data-easing="Power3.easeInOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="300" data-endeasing="Power3.easeIn" style="z-index: 16; max-width: auto; max-height: auto; white-space: nowrap;">SALES </div>
                                                    <!-- LAYER NR. 13 -->
                                                    <div class="tp-caption badgesmall skewfromleft stt tp-resizeme" data-x="503" data-y="194" data-speed="300" data-start="5200" data-easing="Power3.easeInOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="300" data-endeasing="Power3.easeIn" style="z-index: 17; max-width: auto; max-height: auto; white-space: nowrap;">NOW </div>
                                                </li>
                                            </c:if>
                                        </c:forEach>
                                    </ul>
                                    <div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>
                                </div>
                                <style scoped>
                                    .tp-caption.defaultsmallheading,
                                    .defaultsmallheading {
                                        font-size: 14px;
                                        line-height: 18px;
                                        font-weight: 400;
                                        font-family: Roboto;
                                        color: #ffffff;
                                        text-decoration: none;
                                        padding: 0px 0px 0px 0px;
                                        background-color: transparent;
                                        letter-spacing: 4px;
                                        border-width: 0px;
                                        border-color: rgb(255, 214, 88);
                                        border-style: none
                                    }

                                    .tp-caption.defaultparagraph,
                                    .defaultparagraph {
                                        font-size: 13px;
                                        line-height: 23px;
                                        font-weight: 400;
                                        font-family: "Open Sans";
                                        color: rgb(255, 255, 255);
                                        text-decoration: none;
                                        padding: 0px;
                                        background-color: transparent;
                                        border-width: 0px;
                                        border-color: rgb(255, 214, 88);
                                        border-style: none
                                    }

                                    .tp-caption.defaultbtnlink,
                                    .defaultbtnlink {
                                        color: rgb(255, 255, 255);
                                        font-size: 13px;
                                        line-height: 23px;
                                        font-weight: 400;
                                        font-family: "Open Sans";
                                        text-decoration: none;
                                        padding: 0px;
                                        background-color: transparent;
                                        border-width: 0px;
                                        border-color: rgb(255, 214, 88);
                                        border-style: none
                                    }

                                    .tp-caption.badgesmall,
                                    .badgesmall {
                                        font-size: 10px;
                                        line-height: 8px;
                                        font-weight: 400;
                                        font-family: Roboto;
                                        color: rgb(255, 255, 255);
                                        text-decoration: none;
                                        padding: 0px;
                                        background-color: transparent;
                                        border-width: 0px;
                                        border-color: rgb(255, 214, 88);
                                        border-style: none
                                    }

                                    .tp-caption.badgelarge,
                                    .badgelarge {
                                        font-size: 18px;
                                        line-height: 15px;
                                        font-weight: 400;
                                        font-family: Roboto;
                                        color: rgb(255, 255, 255);
                                        text-decoration: none;
                                        padding: 0px;
                                        background-color: transparent;
                                        border-width: 0px;
                                        border-color: rgb(255, 214, 88);
                                        border-style: none
                                    }

                                    .tp-caption.jewellerysmallheading,
                                    .jewellerysmallheading {
                                        font-size: 14px;
                                        line-height: 18px;
                                        font-weight: 400;
                                        font-family: Roboto;
                                        color: rgb(175, 175, 175);
                                        text-decoration: none;
                                        padding: 0px;
                                        letter-spacing: 4px;
                                        background-color: transparent;
                                        border-width: 0px;
                                        border-color: rgb(255, 214, 88);
                                        border-style: none
                                    }

                                    .tp-caption.jewellerylargeheading,
                                    .jewellerylargeheading {
                                        font-size: 85px;
                                        line-height: 75px;
                                        font-weight: 400;
                                        font-family: "Century Gothic";
                                        color: rgb(37, 37, 37);
                                        text-decoration: none;
                                        padding: 0px;
                                        background-color: transparent;
                                        border-width: 0px;
                                        border-color: rgb(255, 214, 88);
                                        border-style: none
                                    }

                                    .tp-caption.jewellerymediumheading,
                                    .jewellerymediumheading {
                                        font-size: 60px;
                                        line-height: 54px;
                                        font-weight: 400;
                                        font-family: "Century Gothic";
                                        color: rgb(205, 168, 92);
                                        text-decoration: none;
                                        padding: 0px;
                                        background-color: transparent;
                                        border-width: 0px;
                                        border-color: rgb(255, 214, 88);
                                        border-style: none
                                    }

                                    .tp-caption.jewelleryparagraph,
                                    .jewelleryparagraph {
                                        font-size: 13px;
                                        line-height: 23px;
                                        font-weight: 400;
                                        font-family: "Open Sans";
                                        color: rgb(131, 131, 131);
                                        text-decoration: none;
                                        padding: 0px;
                                        background-color: transparent;
                                        border-width: 0px;
                                        border-color: rgb(255, 214, 88);
                                        border-style: none
                                    }

                                    .tp-caption.jewellerylargeheading1,
                                    .jewellerylargeheading1 {
                                        font-size: 85px;
                                        line-height: 75px;
                                        font-weight: 400;
                                        font-family: "Century Gothic";
                                        color: rgb(255, 255, 255);
                                        text-decoration: none;
                                        padding: 0px;
                                        background-color: transparent;
                                        border-width: 0px;
                                        border-color: rgb(255, 214, 88);
                                        border-style: none
                                    }
                                </style>
                                <script type="text/javascript">
                                    /******************************************
                                     -	PREPARE PLACEHOLDER FOR SLIDER	-
                                     ******************************************/
                                    (function(jQuery) {
                                        var setREVStartSize = function() {
                                            var tpopt = new Object();
                                            tpopt.startwidth = 900;
                                            tpopt.startheight = 600;
                                            tpopt.container = jQuery('#rev_slider_6_1');
                                            tpopt.fullScreen = "off";
                                            tpopt.forceFullWidth = "on";
                                            tpopt.container.closest(".rev_slider_wrapper").css({
                                                height: tpopt.container.height()
                                            });
                                            tpopt.width = parseInt(tpopt.container.width(), 0);
                                            tpopt.height = parseInt(tpopt.container.height(), 0);
                                            tpopt.bw = tpopt.width / tpopt.startwidth;
                                            tpopt.bh = tpopt.height / tpopt.startheight;
                                            if (tpopt.bh > tpopt.bw)
                                                tpopt.bh = tpopt.bw;
                                            if (tpopt.bh < tpopt.bw)
                                                tpopt.bw = tpopt.bh;
                                            if (tpopt.bw < tpopt.bh)
                                                tpopt.bh = tpopt.bw;
                                            if (tpopt.bh > 1) {
                                                tpopt.bw = 1;
                                                tpopt.bh = 1
                                            }
                                            if (tpopt.bw > 1) {
                                                tpopt.bw = 1;
                                                tpopt.bh = 1
                                            }
                                            tpopt.height = Math.round(tpopt.startheight * (tpopt.width / tpopt.startwidth));
                                            if (tpopt.height > tpopt.startheight && tpopt.autoHeight != "on")
                                                tpopt.height = tpopt.startheight;
                                            if (tpopt.fullScreen == "on") {
                                                tpopt.height = tpopt.bw * tpopt.startheight;
                                                var cow = tpopt.container.parent().width();
                                                var coh = jQuery(window).height();
                                                if (tpopt.fullScreenOffsetContainer != undefined) {
                                                    try {
                                                        var offcontainers = tpopt.fullScreenOffsetContainer.split(",");
                                                        jQuery.each(offcontainers, function(e, t) {
                                                            coh = coh - jQuery(t).outerHeight(true);
                                                            if (coh < tpopt.minFullScreenHeight)
                                                                coh = tpopt.minFullScreenHeight
                                                        })
                                                    } catch (e) {
                                                    }
                                                }
                                                tpopt.container.parent().height(coh);
                                                tpopt.container.height(coh);
                                                tpopt.container.closest(".rev_slider_wrapper").height(coh);
                                                tpopt.container.closest(".forcefullwidth_wrapper_tp_banner").find(".tp-fullwidth-forcer").height(coh);
                                                tpopt.container.css({
                                                    height: "100%"
                                                });
                                                tpopt.height = coh;
                                            } else {
                                                tpopt.container.height(tpopt.height);
                                                tpopt.container.closest(".rev_slider_wrapper").height(tpopt.height);
                                                tpopt.container.closest(".forcefullwidth_wrapper_tp_banner").find(".tp-fullwidth-forcer").height(tpopt.height);
                                            }
                                        };
                                        /* CALL PLACEHOLDER */
                                        setREVStartSize();
                                        var tpj = jQuery;
                                        tpj.noConflict();
                                        var revapi6;
                                        tpj(document).ready(function() {
                                            if (tpj('#rev_slider_6_1').revolution == undefined) {
                                                revslider_showDoubleJqueryError('#rev_slider_6_1');
                                            } else {
                                                revapi6 = tpj('#rev_slider_6_1').show().revolution({
                                                    dottedOverlay: "none",
                                                    delay: 9000,
                                                    startwidth: 900,
                                                    startheight: 600,
                                                    hideThumbs: 0,
                                                    thumbWidth: 100,
                                                    thumbHeight: 50,
                                                    thumbAmount: 2,
                                                    simplifyAll: "off",
                                                    navigationType: "none",
                                                    navigationArrows: "solo",
                                                    navigationStyle: "round",
                                                    touchenabled: "on",
                                                    onHoverStop: "on",
                                                    nextSlideOnWindowFocus: "on",
                                                    swipe_threshold: 75,
                                                    swipe_min_touches: 1,
                                                    drag_block_vertical: false,
                                                    keyboardNavigation: "off",
                                                    navigationHAlign: "center",
                                                    navigationVAlign: "bottom",
                                                    navigationHOffset: 0,
                                                    navigationVOffset: 20,
                                                    soloArrowLeftHalign: "left",
                                                    soloArrowLeftValign: "center",
                                                    soloArrowLeftHOffset: 45,
                                                    soloArrowLeftVOffset: 0,
                                                    soloArrowRightHalign: "right",
                                                    soloArrowRightValign: "center",
                                                    soloArrowRightHOffset: 45,
                                                    soloArrowRightVOffset: 0,
                                                    shadow: 0,
                                                    fullWidth: "on",
                                                    fullScreen: "off",
                                                    spinner: "spinner4",
                                                    stopLoop: "off",
                                                    stopAfterLoops: -1,
                                                    stopAtSlide: -1,
                                                    shuffle: "off",
                                                    autoHeight: "off",
                                                    forceFullWidth: "on",
                                                    hideTimerBar: "on",
                                                    hideThumbsOnMobile: "off",
                                                    hideNavDelayOnMobile: 1500,
                                                    hideBulletsOnMobile: "off",
                                                    hideArrowsOnMobile: "off",
                                                    hideThumbsUnderResolution: 0,
                                                    hideSliderAtLimit: 0,
                                                    hideCaptionAtLimit: 0,
                                                    hideAllCaptionAtLilmit: 0,
                                                    startWithSlide: 0
                                                });
                                            }
                                        }); /*ready*/
                                    })($nwd_jQuery);
                                </script>
                                <style type="text/css">
                                    #rev_slider_6_1_wrapper .tp-loader.spinner4 div {
                                        background-color: #FFFFFF !important;
                                    }
                                </style>
                                <style type="text/css">
                                    .tp-caption a {
                                        color: #000;
                                        border-color: #cccecf;
                                    }

                                    .tp-caption a:hover {
                                        color: #000;
                                        border-color: #000;
                                        background-color: rgba(255, 255, 255, 0);
                                    }

                                    .tp-caption a.color-white {
                                        color: #fff;
                                    }

                                    .tp-caption a.color-white:hover {
                                        color: #fff;
                                        border-color: #fff;
                                    }
                                </style>
                            </div>
                            <!-- END REVOLUTION SLIDER -->
                        </div>

                        <div class="grid-container eg-homeblock">
                            <div class="banner-section banner-section-5 clearfix eg-tal eg-fcf">
                                <div class="banner-column-wrapper grid12-6 grid-xs-full">
                                    <div class="banner-column">
                                        <div class="banner eg-hoveropacity eg-hover-nobg" onclick="window.location = '<c:url value="/"/>products?category=3'">
                                            <div class="banner-content">
                                                <div class="banner-inner a-center" style="padding:10px;">
                                                    <div class="grid12-8 grid-sm-full grid-xs-full">
                                                        <p class="eg-ttu" style="font-size: 14px;letter-spacing:4px;color: #a79782;">mystic jewel</p>
                                                        <div class="horizontal-separator separator-1" style="margin:13px auto;background-color: #a79782;"></div>
                                                        <h2 class="eg-ttu" style="font-family: Century Gothic;font-size: 42px;line-height: 40px;color: #5d554a;">Necklaces</h2>
                                                        <div class="horizontal-separator separator-1 grid-sm-hide grid-sxs-hide" style="margin:13px auto;background-color: #a79782;"></div>
                                                        <p class="grid-sm-hide grid-sxs-hide" style="line-height:22px;color: #a79782;"></p>
                                                    </div>
                                                </div>
                                            </div> <img src="<c:url value="/"/>resource/images/banner_necklace.jpg" alt="" /> </div>
                                    </div>
                                </div>
                                <div class="banner-column-wrapper grid12-6 grid-xs-full">
                                    <div class="banner-column">
                                        <div class="banner eg-hoveropacity eg-hover-nobg" onclick="window.location = '<c:url value="/"/>products?category=4'">
                                            <div class="banner-content">
                                                <div class="banner-inner a-center" style="padding:10px;">
                                                    <div class="grid12-8 grid-sm-full grid-xs-full">
                                                        <p class="eg-ttu" style="font-size: 14px;letter-spacing:4px;color: #a79782;">mystic jewel</p>
                                                        <div class="horizontal-separator separator-1" style="margin:13px auto;background-color: #a79782;"></div>
                                                        <h2 class="eg-ttu" style="font-family: Century Gothic;font-size: 42px;line-height: 40px;color: #5d554a;">Earrings</h2>
                                                        <div class="horizontal-separator separator-1 grid-sm-hide grid-sxs-hide" style="margin:13px auto;background-color: #a79782;"></div>
                                                        <p class="grid-sm-hide grid-sxs-hide" style="line-height:22px;color: #a79782;"></p>
                                                    </div>
                                                </div>
                                            </div> <img src="<c:url value="/"/>resource/images/banner_earrings.jpg" alt="" /> </div>
                                    </div>
                                </div>
                                <div class="banner-column-wrapper grid12-6 grid-xs-full">
                                    <div class="banner-column">
                                        <div class="banner eg-hoveropacity eg-hover-nobg" onclick="window.location = '<c:url value="/"/>products?category=1'">
                                            <div class="banner-content">
                                                <div class="banner-inner a-center" style="padding:10px;">
                                                    <div class="grid12-8 grid-sm-full grid-xs-full">
                                                        <p class="eg-ttu" style="font-size: 14px;letter-spacing:4px;color: #a79782;">mystic jewel</p>
                                                        <div class="horizontal-separator separator-1" style="margin:13px auto;background-color: #a79782;"></div>
                                                        <h2 class="eg-ttu" style="font-family: Century Gothic;font-size: 42px;line-height: 40px;color: #5d554a;">Rings</h2>
                                                        <div class="horizontal-separator separator-1 grid-sm-hide grid-sxs-hide" style="margin:13px auto;background-color: #a79782;"></div>
                                                        <p class="grid-sm-hide grid-sxs-hide" style="line-height:22px;color: #a79782;"></p>
                                                    </div>
                                                </div>
                                            </div> <img src="<c:url value="/"/>resource/images/banner_ring.jpg" alt="" /> </div>
                                    </div>
                                </div>
                                <div class="banner-column-wrapper grid12-6 grid-xs-full">
                                    <div class="banner-column">
                                        <div class="banner eg-hoveropacity eg-hover-nobg" onclick="window.location = '<c:url value="/"/>products?category=2'">
                                            <div class="banner-content">
                                                <div class="banner-inner a-center" style="padding:10px;">
                                                    <div class="grid12-8 grid-sm-full grid-xs-full">
                                                        <p class="eg-ttu" style="font-size: 14px;letter-spacing:4px;color: #a79782;">mystic jewel</p>
                                                        <div class="horizontal-separator separator-1" style="margin:13px auto;background-color: #a79782;"></div>
                                                        <h2 class="eg-ttu" style="font-family: Century Gothic;font-size: 42px;line-height: 40px;color: #5d554a;">Bracelets</h2>
                                                        <div class="horizontal-separator separator-1 grid-sm-hide grid-sxs-hide" style="margin:13px auto;background-color: #a79782;"></div>
                                                        <p class="grid-sm-hide grid-sxs-hide" style="line-height:22px;color: #a79782;"></p>
                                                    </div>
                                                </div>
                                            </div> <img src="<c:url value="/"/>resource/images/banner_bracelets.jpg" alt="" /> </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="grid-container eg-homeblock">
                            <div class="block-title grid-full eg-tac eg-ttu">
                                <h3><span style="color: #cda85c"><strong>New Products</strong></span></h3> </div>
                            <div class="block-content">
                                <div class="clearfix"></div>
                                <div class="product-list-slider">
                                    <div class="home-productlist-slider owl-carousel">
                                        <!-- NEW ITEMS -->
                                        <c:forEach items="${newProduct}" var="newProd">
                                            <c:set var="sku" value="${newProd.skuList[0]}"/>
                                            <div class="column-item clearfix">
                                                <div class="item clearfix">
                                                    <div class="media-section eg-image-slider">
                                                        <a href="<c:url value="/product-detail?skuid=${sku.skuID}"/>" title="${newProd.productName}" class="product-image" data-images="">
                                                            <c:set var="firstImg" value="${newProd.productImageList[0]}"/>
                                                            <img src="<c:url value="/"/>resource/images/${firstImg.imageLink}" alt="${newProd.productName}" style="width:100%" width="350" height="455" />
                                                        </a>
                                                        <div class="product-labels">
                                                            <c:if test="${newProd.isNew}">
                                                                <span class="new-product">
                                                                    <span>New</span>
                                                                </span>
                                                            </c:if>
                                                            <c:if test="${newProd.discountValue gt 0}">
                                                                <span class="discount-code">
                                                                    <span>-${newProd.discountValue}%</span>
                                                                </span>
                                                            </c:if>
                                                        </div>
                                                        <div class="hover-section">
                                                            <div class="hover-links-wrapper a-center">
                                                                <div class="eg-tac link-icon">
                                                                    <a href="<c:url value="/" />add-wishlist/${newProd.productID}" class="link-wishlist" title="Wishlist"> <span class="link-ico"><span class="fa fa-heart"></span></span> <span class="link-label">Wishlist</span> </a>
                                                                </div>
                                                                <div class="eg-tac link-icon">
                                                                    <a href="<c:url value="/product-compare/add?productid=${newProd.productID}"/>" title="Compare"> <span class="link-ico"><span class="fa fa-exchange"></span></span> <span class="link-label">Compare</span> </a>
                                                                </div>
                                                                <div class="eg-tac link-both">
                                                                    <a href="javascript:void(0);" data-target="<c:url value="/quickview?skuid=${sku.skuID}"/>" class="link-quickview" title="Quick View"> <span class="link-ico"><span class="fa fa-search"></span></span> <span class="link-label">Quickview</span> </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="details-section">
                                                        <div class="product-category"> <a href="#" title="Jewellery">${newProd.categoryID.categoryName}</a> </div>
                                                        <h2 class="product-name">
                                                            <a href="<c:url value="/product-detail?skuid=${sku.skuID}"/>" title="${newProd.productName}">${newProd.productName}</a>
                                                        </h2>
                                                        <div class="review rating-only">
                                                            <div class="ratings clearfix">
                                                                <div class="rating-box">
                                                                    <c:if test="${newProd.rating gt 0}">
                                                                        <div class="rating theme-color" style="width:${newProd.rating}%"></div>
                                                                    </c:if>
                                                                    <c:if test="${newProd.rating eq 0}">
                                                                        <div class="rating theme-color" style="width:0%"></div>
                                                                    </c:if>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="price-box">
                                                            <c:if test="${newProd.discountValue gt 0}">
                                                                <p class="old-price"> <span class="price-label">Regular Price:</span>
                                                                    <span class="price">
                                                                        <fmt:setLocale value="en_US"/>
                                                                        <fmt:formatNumber value="${sku.unitPrice}" type="currency"/>
                                                                    </span>
                                                                </p>
                                                                <p class="special-price"> <span class="price-label">Special Price</span>
                                                                    <span class="price" style="color: #cda85c">
                                                                        <fmt:setLocale value="en_US"/>
                                                                        <fmt:formatNumber value="${sku.unitPrice - (sku.unitPrice * newProd.discountValue / 100)}" type="currency"/>
                                                                    </span>
                                                                </p>
                                                            </c:if>
                                                            <c:if test="${newProd.discountValue eq 0}">
                                                                <span class="regular-price">
                                                                    <span class="price">
                                                                        <fmt:setLocale value="en_US"/>
                                                                        <fmt:formatNumber value="${sku.unitPrice}" type="currency"/>
                                                                    </span> 
                                                                </span>
                                                            </c:if>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </c:forEach>
                                        <!-- /ITEMS-->
                                    </div>
                                </div>
                                <script type="text/javascript">
                                    //<![CDATA[
                                    jQuery(function($) {
                                        var $homeproductlist = $('.home-productlist-slider');
                                        $homeproductlist.owlCarousel({
                                            navigation: true,
                                            lazyLoad: true,
                                            autoPlay: false,
                                            itemsCustom: [
                                                [0, 1],
                                                [320, 1],
                                                [480, 1],
                                                [751, 3],
                                                [960, 3],
                                                [1263, 4]
                                            ],
                                            pagination: false,
                                            rewindNav: true,
                                            rewindSpeed: 600,
                                            transitionStyle: 'fade',
                                            navigationText: ["<span class='fa fa-angle-left'></span>", "<span class='fa fa-angle-right'></span>"]
                                        });
                                    });
                                    //]]
                                </script>
                            </div>
                        </div>

                        <div class="grid-container eg-homeblock">
                            <div class="eg-cmsblock jewellery-block-section eg-fullwidth eg-fcf" style="background:url(resource/client/media/wysiwyg/elegento/home/jewellery/jewellery-image1.jpg);background-repeat:no-repeat;background-attachment:scroll;background-color:rgba(0,0,0,0);background-size:cover;background-position:50% 50%;">
                                <div class="jewellery-block-container">
                                    <div class="container">
                                        <div class="grid-container">
                                            <div class="grid-full eg-tac clearfix" style="padding:100px 15px;">
                                                <h2 class="eg-ttn">“Perfection is achieved, not when there is nothing more to add, but when there is nothing left to take away.”</h2>
                                                <div class="horizontal-separator separator-6" style="margin:20px auto;background-color: #fff;">&nbsp;</div>
                                                <p class="eg-ttu eg-fwb" style="font-size:14px;">ANTOINE DE SAINT-EXUPÉRY</p>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="grid-container eg-homeblock">
                            <div class="block-title grid-full eg-tac eg-ttu">
                                <h3><span style="color: #cda85c"><strong>Popular Products</strong></span></h3> </div>
                            <div class="block-content">
                                <div class="clearfix"></div>
                                <div class="product-list-slider">
                                    <div class="home-productlist-slider owl-carousel">
                                        <!-- ITEMS -->
                                        <c:forEach items="${hotProduct}" var="hotProd">
                                            <c:set var="sku" value="${hotProd.skuList[0]}"/>
                                            <div class="column-item clearfix">
                                                <div class="item clearfix">
                                                    <div class="media-section eg-image-slider">
                                                        <a href="<c:url value="/product-detail?skuid=${sku.skuID}"/>" title="${hotProd.productName}" class="product-image" data-images="">
                                                            <c:set var="firstImg" value="${hotProd.productImageList[0]}"/>
                                                            <img src="<c:url value="/"/>resource/images/${firstImg.imageLink}" alt="${hotProd.productName}" style="width:100%" width="350" height="455" />
                                                        </a>
                                                        <div class="product-labels">
                                                            <c:if test="${hotProd.isNew}">
                                                                <span class="new-product">
                                                                    <span>New</span>
                                                                </span>
                                                            </c:if>
                                                            <c:if test="${hotProd.discountValue gt 0}">
                                                                <span class="discount-code">
                                                                    <span>-${hotProd.discountValue}%</span>
                                                                </span>
                                                            </c:if>
                                                        </div>
                                                        <div class="hover-section">
                                                            <div class="hover-links-wrapper a-center">
                                                                <div class="eg-tac link-icon">
                                                                    <a href="<c:url value="/" />add-wishlist/${hotProd.productID}" class="link-wishlist" title="Wishlist"> <span class="link-ico"><span class="fa fa-heart"></span></span> <span class="link-label">Wishlist</span> </a>
                                                                </div>
                                                                <div class="eg-tac link-icon">
                                                                    <a href="<c:url value="/product-compare/add?productid=${hotProd.productID}"/>" title="Compare"> <span class="link-ico"><span class="fa fa-exchange"></span></span> <span class="link-label">Compare</span> </a>
                                                                </div>
                                                                <div class="eg-tac link-both">
                                                                    <a href="javascript:void(0);" data-target="<c:url value="/quickview?skuid=${sku.skuID}"/>" class="link-quickview" title="Quick View"> <span class="link-ico"><span class="fa fa-search"></span></span> <span class="link-label">Quickview</span> </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="details-section">
                                                        <div class="product-category"> <a href="#" title="Jewellery">${hotProd.categoryID.categoryName}</a> </div>
                                                        <h2 class="product-name">
                                                            <a href="<c:url value="/product-detail?skuid=${sku.skuID}"/>" title="${hotProd.productName}">${hotProd.productName}</a>
                                                        </h2>
                                                        <div class="review rating-only">
                                                            <div class="ratings clearfix">
                                                                <div class="rating-box">
                                                                    <c:if test="${hotProd.rating gt 0}">
                                                                        <div class="rating theme-color" style="width:${hotProd.rating}%"></div>
                                                                    </c:if>
                                                                    <c:if test="${hotProd.rating eq 0}">
                                                                        <div class="rating theme-color" style="width:0%"></div>
                                                                    </c:if>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="price-box">
                                                            <c:if test="${hotProd.discountValue gt 0}">
                                                                <p class="old-price"> <span class="price-label">Regular Price:</span>
                                                                    <span class="price">
                                                                        <fmt:setLocale value="en_US"/>
                                                                        <fmt:formatNumber value="${sku.unitPrice}" type="currency"/>
                                                                    </span>
                                                                </p>
                                                                <p class="special-price"> <span class="price-label">Special Price</span>
                                                                    <span class="price" style="color: #cda85c">
                                                                        <fmt:setLocale value="en_US"/>
                                                                        <fmt:formatNumber value="${sku.unitPrice - (sku.unitPrice * hotProd.discountValue / 100)}" type="currency"/>
                                                                    </span>
                                                                </p>
                                                            </c:if>
                                                            <c:if test="${hotProd.discountValue eq 0}">
                                                                <span class="regular-price">
                                                                    <span class="price">
                                                                        <fmt:setLocale value="en_US"/>
                                                                        <fmt:formatNumber value="${sku.unitPrice}" type="currency"/>
                                                                    </span> 
                                                                </span>
                                                            </c:if>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </c:forEach>
                                        <!-- /ITEMS-->
                                    </div>
                                </div>
                                <script type="text/javascript">
                                    //<![CDATA[
                                    jQuery(function($) {
                                        var $homeproductlist = $('.home-productlist-slider');
                                        $homeproductlist.owlCarousel({
                                            navigation: true,
                                            lazyLoad: true,
                                            autoPlay: false,
                                            itemsCustom: [
                                                [0, 1],
                                                [320, 1],
                                                [480, 1],
                                                [751, 3],
                                                [960, 3],
                                                [1263, 4]
                                            ],
                                            pagination: false,
                                            rewindNav: true,
                                            rewindSpeed: 600,
                                            transitionStyle: 'fade',
                                            navigationText: ["<span class='fa fa-angle-left'></span>", "<span class='fa fa-angle-right'></span>"]
                                        });
                                    });
                                    //]]
                                </script>
                            </div>
                        </div>

                        <div class="grid-container eg-homeblock">
                            <div class="eg-cmsblock jewellery-block-section" style="margin:0 15px;">
                                <div class="jewellery-block-container clearfix" style="border: 4px solid #f7f7f7;">
                                    <div class="grid12-7 grid-xs-full grid-sm-full">
                                        <div class="jewellery-block" style="padding-left:15px;padding-top: 37px;padding-bottom: 37px;">
                                            <h1 style="font-size: 24px;color: #333333;margin-bottom: 13px;">STATEMENT JEWELLERY</h1>
                                            <h5 class="eg-ttu theme-color" style="font-size: 12px; letter-spacing: 1px; font-weight: 600; text-shadow: none;">WE’VE GOT OUR FINGER ON THE PULSE WHEN IT COMES TO ON-TREND RINGS.</h5>
                                            <div class="horizontal-separator separator-4 theme-bg-color" style="margin: 12px 0;height: 1px;"></div>
                                            <p style="margin:0;font-size: 13px;color: #9c9c9c;line-height:1.8;"> Find friendship and classic charm bracelets, with also a range of contemporary bangles. View our stunning necklaces, earrings and rings too, all there to help </p>
                                        </div>
                                    </div>
                                    <div class="grid12-5 grid-xs-full grid-sm-full"> <img src="resource/client/media/wysiwyg/elegento/home/jewellery/jewellery-image2.jpg" alt=""> </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="postscript"></div>
            </div>
        </div>
    </tiles:putAttribute>
</tiles:insertDefinition>
