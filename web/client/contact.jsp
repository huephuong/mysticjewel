<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="/admin/layout/taglib.jsp"%>
<tiles:insertDefinition name="page-client">
    <tiles:putAttribute name="title" value="Contact" />
    <tiles:putAttribute name="body-class" value=" contacts-index-index  wide" />
    <tiles:putAttribute name="page-css">
        <link rel="stylesheet" type="text/css" href="resource/client/media/css/2fc494b88aeead24b11cc86b9013e125.css" media="all" />
    </tiles:putAttribute>
    <tiles:putAttribute name="page-js">
        <script type="text/javascript" src="resource/client/js/mage/captcha.js"></script>
    </tiles:putAttribute>
    <tiles:putAttribute name="content">
        <div class="main-container col1-layout">
            <div class="main-before-top-container"></div>
            <div class="main container">
                <div class="preface"></div>
                <div class="col-main">
                    <div class="clearfix"></div>
                    <style>
                        .gm-style img { max-width:none; }
                    </style>
                    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&key=AIzaSyDUIKjMFHWDcKTHBTVOJwJpQiXSR-e2gJ0"></script>
                    <script>
                        //<![CDATA[
                        var store = new google.maps.LatLng(10.7863467, 106.6641396);
                        var marker;
                        var map;

                        function initialize() {
                            var mapOptions = {
                                zoom: 15,
                                mapTypeId: google.maps.MapTypeId.ROADMAP,
                                center: store,
                                panControl: true,
                                zoomControl: true,
                                mapTypeControl: true,
                                scrollwheel: false,
                                scaleControl: true,
                                streetViewControl: true,
                                overviewMapControl: true
                            };

                            map = new google.maps.Map(document.getElementById('contacts-gmap'), mapOptions);

                            marker = new google.maps.Marker({
                                map: map,
                                draggable: true,
                                animation: google.maps.Animation.DROP,
                                position: store,
                                icon: 'http://www.soaptheme.net/magento/royal/media/elegento/gmap/icon_gmap.png'
                            });
                        }

                        google.maps.event.addDomListener(window, 'load', initialize);
                        google.maps.event.addDomListener(window, 'resize', initialize);
                        //]]>
                    </script>
                    <div class="grid-full no-padding eg-fullwidth"  data-revise="-15">
                        <div id="contacts-gmap">
                        </div>
                    </div>
                    <div id="messages_product_view"></div>
                    <div class="contact-form grid12-6 no-padding-left grid-xs-full">
                        <form action="<c:url value="/" />contact" id="contactForm" method="post">
                            <div class="fieldset">
                                <h2 class="r-separator"><span>Contact Information</span></h2>
                                <ul class="form-list">
                                    <li class="fields grid-full no-padding">
                                        <div class="field">
                                            <label for="name" class="required"><em>*</em>Name</label>
                                            <div class="input-box">
                                                <input name="name" id="name" title="Name" value="" class="input-text required-entry" type="text" />
                                            </div>
                                        </div>
                                        <div class="field">
                                            <label for="email" class="required"><em>*</em>Email</label>
                                            <div class="input-box">
                                                <input name="email" id="email" title="Email" value="" class="input-text required-entry validate-email" type="text" />
                                            </div>
                                        </div>
                                        <div class="field">
                                            <label for="telephone">Phone</label>
                                            <div class="input-box">
                                                <input name="phone" id="telephone" title="Phone" value="" class="input-text" type="text" />
                                            </div>
                                        </div>
                                    </li>
                                    <li class="fields grid-full no-padding">
                                        <label for="comment" class="required"><em>*</em>Comment</label>
                                        <div class="input-box">
                                            <textarea name="comment" id="comment" title="Comment" class="required-entry input-text" cols="5" rows="3"></textarea>
                                        </div>
                                        <p class="eg-fsi" style="margin-top:10px;color:#767676;">Your email address will not be published. Reguired fields are marked <span class="required">*</span></p>
                                        <input type="text" name="hideit" id="hideit" value="" style="display:none !important;" />
                                        <button type="submit" title="Submit" class="button"><span><span>Submit</span></span></button>
                                    </li>
                                </ul>
                            </div>
                        </form>
                    </div>
                    <script type="text/javascript">
                        //<![CDATA[
                        var contactForm = new VarienForm('contactForm');
                        //]]>
                    </script>
                    <div class="block block-contacts-details grid12-6 no-padding-right grid-xs-full">
                        <h2 class="r-separator">
                            <span>Our Office</span>
                        </h2>
                        <div class="block-content">
                            <div class="border-block border-style-1">
                                <div>
                                    <h4 class="text-center" style="margin-bottom:20px;font-size:25px;color:#252525;font-family:Bodoni;">
                                        <span>STORE ADDRESS</span>
                                    </h4>
                                    <p class="horizontal-separator separator-5" style="margin-bottom:30px;"></p>
                                    <p style="text-align: center;margin-bottom:20px;">Lorem ipsum dolor sit amet, consect etur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</p>
                                    <p style="color: #6c6c6c; text-align: center; font-size: 36px;margin-bottom:30px;line-height:60px;"><span style="color: #c1c1c1;">+78&nbsp;</span>123 456 789</p>
                                    <div class="grid-container">
                                        <div class="grid12-6">
                                            <p style="text-align: center;">
                                                <strong style="color:#252525;">
                                                    <span class="fa fa-home"></span> STORE ADRESS
                                                </strong><br>
                                                590 CACH MANG THANG 8, HCMC, VIETNAM<br>
                                                <span class="theme-color"><span class="fa fa-envelope-o"></span> mysticjewel.pnp@gmail.com</span>
                                            </p>
                                        </div>
                                        <div class="grid12-6">
                                            <p style="text-align: center;">
                                                <strong style="color:#252525;">
                                                    <span class="fa fa-clock-o"></span> WORKING HOURS
                                                </strong><br>
                                                <span class="theme-color">Mon-Fri : 8AM – 5PM</span><br>
                                                <span class="theme-color">Saturday : 9AM – 4PM</span><br>
                                                <span class="theme-color">Sunday : Closed</span>
                                            </p>
                                        </div><br>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="postscript"></div>
            </div>
        </div>
    </tiles:putAttribute>
</tiles:insertDefinition>
