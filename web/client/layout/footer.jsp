<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="/admin/layout/taglib.jsp"%>
<div class="footer-container">
    <div class="footer-top-container section-container">
        <div class="footer-top footer container">
            <div class="clearfix">
                <div class="grid12-3 grid-xs-full block_footer_top_col1">
                    <div class="cms-block vlist-slider-block">
                        <div class="block-title">
                            <h6><span>New Arrivals</span></h6> </div>
                        <div class="block-content">
                            <div class="vlist-wrapper clearfix">
                                <div class="vertical-list">
                                    <div class="column-item clearfix">
                                        <c:forEach items="${newProdFooter}" var="newProd">
                                            <c:set var="sku" value="${newProd.skuList[0]}"/>
                                            <div class="item clearfix">
                                                <a href="<c:url value="/product-detail?skuid=${sku.skuID}"/>" title="${newProd.productName}" class="product-image">
                                                    <c:set var="firstImg" value="${newProd.productImageList[0]}"/>
                                                    <img src="<c:url value="/" />resource/images/${firstImg.imageLink}" alt="${newProd.productName}" width="45" height="60" />
                                                </a>
                                                <div class="details-section">
                                                    <h2 class="product-name">
                                                        <a href="<c:url value="/product-detail?skuid=${sku.skuID}"/>" title="${newProd.productName}">${newProd.productName}</a>
                                                    </h2>
                                                    <div class="price-box">
                                                        <c:if test="${newProd.discountValue gt 0}">
                                                            <p class="old-price"> <span class="price-label">Regular Price:</span>
                                                                <span class="price">
                                                                    <fmt:setLocale value="en_US"/>
                                                                    <fmt:formatNumber value="${sku.unitPrice}" type="currency"/>
                                                                </span>
                                                            </p>
                                                            <p class="special-price"> <span class="price-label">Special Price</span>
                                                                <span class="price">
                                                                    <fmt:setLocale value="en_US"/>
                                                                    <fmt:formatNumber value="${sku.unitPrice - (sku.unitPrice * newProd.discountValue / 100)}" type="currency"/>
                                                                </span>
                                                            </p>
                                                        </c:if>
                                                        <c:if test="${newProd.discountValue eq 0}">
                                                            <span class="regular-price">
                                                                <span class="price">
                                                                    <fmt:setLocale value="en_US"/>
                                                                    <fmt:formatNumber value="${sku.unitPrice}" type="currency"/>
                                                                </span> 
                                                            </span>
                                                        </c:if>
                                                    </div>
                                                </div>
                                            </div>
                                        </c:forEach>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="grid12-3 grid-xs-full block_footer_top_col2">
                    <div class="cms-block vlist-slider-block">
                        <div class="block-title">
                            <h6><span>Favorite</span></h6> </div>
                        <div class="block-content">
                            <div class="vlist-wrapper clearfix">
                                <div class="vertical-list">
                                    <div class="column-item clearfix">
                                        <c:forEach items="${hotProdFooter}" var="hotProd">
                                            <c:set var="sku" value="${hotProd.skuList[0]}"/>
                                            <div class="item clearfix">
                                                <a href="<c:url value="/product-detail?skuid=${sku.skuID}"/>" title="${hotProd.productName}" class="product-image">
                                                    <c:set var="firstImg" value="${hotProd.productImageList[0]}"/>
                                                    <img src="<c:url value="/" />resource/images/${firstImg.imageLink}" alt="${hotProd.productName}" width="45" height="60" />
                                                </a>
                                                <div class="details-section">
                                                    <h2 class="product-name">
                                                        <a href="<c:url value="/product-detail?skuid=${sku.skuID}"/>" title="${hotProd.productName}">${hotProd.productName}</a>
                                                    </h2>
                                                    <div class="price-box">
                                                        <c:if test="${hotProd.discountValue gt 0}">
                                                            <p class="old-price"> <span class="price-label">Regular Price:</span>
                                                                <span class="price">
                                                                    <fmt:setLocale value="en_US"/>
                                                                    <fmt:formatNumber value="${sku.unitPrice}" type="currency"/>
                                                                </span>
                                                            </p>
                                                            <p class="special-price"> <span class="price-label">Special Price</span>
                                                                <span class="price">
                                                                    <fmt:setLocale value="en_US"/>
                                                                    <fmt:formatNumber value="${sku.unitPrice - (sku.unitPrice * hotProd.discountValue / 100)}" type="currency"/>
                                                                </span>
                                                            </p>
                                                        </c:if>
                                                        <c:if test="${hotProd.discountValue eq 0}">
                                                            <span class="regular-price">
                                                                <span class="price">
                                                                    <fmt:setLocale value="en_US"/>
                                                                    <fmt:formatNumber value="${sku.unitPrice}" type="currency"/>
                                                                </span> 
                                                            </span>
                                                        </c:if>       
                                                    </div>
                                                </div>
                                            </div>
                                        </c:forEach>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="grid12-3 grid-xs-full block_footer_top_col3">
                    <div class="cms-block twitter-feed">
                        <div class="block-title">
                            <h6><span>On Sale</span><!-- <a href="#" class="follow-button">Follow</a> --></h6>
                        </div>
                        <div class="block-content">
                            <div class="vlist-wrapper clearfix">
                                <div class="vertical-list">
                                    <div class="column-item clearfix">
                                        <c:forEach items="${saleProdFooter}" var="saleProd">
                                            <c:set var="sku" value="${saleProd.skuList[0]}"/>
                                            <div class="item clearfix">
                                                <a href="<c:url value="/product-detail?skuid=${sku.skuID}"/>" title="${saleProd.productName}" class="product-image">
                                                    <c:set var="firstImg" value="${saleProd.productImageList[0]}"/>
                                                    <img src="<c:url value="/" />resource/images/${firstImg.imageLink}" alt="${saleProd.productName}" width="45" height="45" />
                                                </a>
                                                <div class="details-section">
                                                    <h2 class="product-name">
                                                        <a href="<c:url value="/product-detail?skuid=${sku.skuID}"/>" title="${saleProd.productName}">${saleProd.productName}</a>
                                                    </h2>
                                                    <div class="price-box">
                                                        <c:if test="${saleProd.discountValue gt 0}">
                                                            <p class="old-price"> <span class="price-label">Regular Price:</span>
                                                                <span class="price">
                                                                    <fmt:setLocale value="en_US"/>
                                                                    <fmt:formatNumber value="${sku.unitPrice}" type="currency"/>
                                                                </span>
                                                            </p>
                                                            <p class="special-price"> <span class="price-label">Special Price</span>
                                                                <span class="price">
                                                                    <fmt:setLocale value="en_US"/>
                                                                    <fmt:formatNumber value="${sku.unitPrice - (sku.unitPrice * saleProd.discountValue / 100)}" type="currency"/>
                                                                </span>
                                                            </p>
                                                        </c:if>
                                                        <c:if test="${saleProd.discountValue eq 0}">
                                                            <span class="regular-price">
                                                                <span class="price">
                                                                    <fmt:setLocale value="en_US"/>
                                                                    <fmt:formatNumber value="${sku.unitPrice}" type="currency"/>
                                                                </span> 
                                                            </span>
                                                        </c:if>      
                                                    </div>
                                                </div>
                                            </div>
                                        </c:forEach>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="grid12-3 grid-xs-full block_footer_top_col4">
                    <div class="cms-block eg-tac banner-section no-padding">
                        <div class="block-title">
                            <h6><span>New Collection</span></h6>
                        </div>
                        <div class="block-content">
                            <div class="summer-collection-wrapper">
                                <div class="summer-collection banner eg-hover1 eg-hoverzoomout eg-hoveropacity" onclick="window.location = '<c:url value="/"/>products?collection=${colFooter.colID}'">
                                    <div class="banner-content">
                                        <div class="banner-inner">
                                            <h5 class="no-margin eg-fsi">Discover out new Collection</h5>
                                            <div class="horizontal-separator separator-1" style="margin-top:10px;margin-bottom:10px;"></div>
                                            <h1 class="no-margin" style="font-size: 48px;">${colFooter.colName}</h1>
                                            <div class="horizontal-separator separator-1" style="margin-top:10px;margin-bottom:10px;"></div>
                                            <h6 class="no-margin">Click here to learn more</h6> </div>
                                    </div> <img src="<c:url value="/" />resource/images/${colFooter.colImage}" alt="" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-main-container section-container">
        <div class="footer-main footer container">
            <div class="clearfix">
                <div class="grid12-3 grid-xs-full block_footer_main_col1">
                    <div class="cms-block">
                        <div class="cms-block-content">
                            <div class="cms-block-content-wrapper">
                                <div class="aboutus-img theme-color" style="font-size:71px;line-height:72px;"> <a class="f-left theme-color" href="#" style="margin-right:18px;"><span class="icon-royal icon-r"></span></a> </div>
                                <div class="aboutus-title">
                                    <h5 class="eg-ttu">About <span class="theme-color">Mystic</span></h5>
                                    <p style="font-size: 11px;line-height: 18px;color:#767676;">Say hello to the smartest and most flexible theme in the collection.</p>
                                </div>
                                <div class="clearfix"></div>
                                <div class="aboutus-content" style="margin: 19px 0 0;text-align: center;padding: 16px 10px 10px;border: 1px solid #3F3F3F;line-height: 22px;color: #767676;text-transform: uppercase;letter-spacing: 1px;"> <address class="address-company" style="margin-bottom:0;">590 Cach Mang Thang 8, HCMC, Vietnam<br></address> <span class="eg-fcf">Phone: </span> +78 123 456 789
                                    <br /> <span class="eg-fcf">Email: </span><a href="mailto:mysticjewel.pnp@gmail.com">mysticjewel.pnp@gmail.com</a>
                                    <br /> <a class="eg-fcf" href="#">www.MysticJewel.com</a>
                                    <ul class="social-icons footer-social-icons">
                                        <li>
                                            <a href="https://twitter.com/share?url=http://www.soaptheme.net/magento/royal/&amp;text=RoyalTheme" class="link-twitter tooltipster" target="_blank" title="Share On Twitter"> <span class="fa fa-twitter"></span> </a>
                                        </li>
                                        <li>
                                            <a href="http://www.facebook.com/sharer.php?u=http://www.soaptheme.net/magento/royal/&amp;u=http://www.soaptheme.net/magento/royal/&amp;t=RoyalTheme" class="link-facebook tooltipster" title="Share On Facebook" target="_blank"> <span class="fa fa-facebook"></span> </a>
                                        </li>
                                        <li>
                                            <a href="http://pinterest.com/pin/create/button/?url=http://www.soaptheme.net/magento/royal/&amp;description=RoyalTheme" class="link-pinterest tooltipster" title="Share On Pinterest" target="_blank"> <span class="fa fa-pinterest"></span> </a>
                                        </li>
                                        <li>
                                            <a href="http://plus.google.com/share?url=http://www.soaptheme.net/magento/royal/&amp;title=RoyalTheme" class="link-google-plus tooltipster" title="Share On Google" target="_blank"> <span class="fa fa-google-plus"></span> </a>
                                        </li>
                                        <li>
                                            <a href="mailto:enteryour@addresshere.com?subject=RoyalTheme&amp;body=Check%20this%20out:%20http://www.soaptheme.net/magento/royal/" class="link-email tooltipster" title="Share On Mail" target="_blank"> <span class="fa fa-envelope-o"></span> </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="grid12-3 grid-xs-full block_footer_main_col2">
                    <div class="cms-block">
                        <div class="cms-block-title">
                            <h6>Categories</h6>
                        </div>
                        <div class="cms-block-content clearfix">
                            <ul class="grid12-6 no-padding-left" style="line-height:30px;font-size:13px;">
                                <c:forEach begin="0" end="${catSubList.size()/2}" items="${catSubList}" var="catSub">
                                    <li><a href="<c:url value='/products?category=${catSub.categoryID}' />">${catSub.categoryName}</a></li>
                                </c:forEach>
                            </ul>
                            <ul class="grid12-6 no-padding-right" style="line-height:30px;font-size:13px;">
                                <c:forEach begin="${catSubList.size()/2+1}" end="${catSubList.size()}" items="${catSubList}" var="catSub">
                                    <li><a href="<c:url value='/products?category=${catSub.categoryID}' />">${catSub.categoryName}</a></li>
                                </c:forEach>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="grid12-3 grid-xs-full block_footer_main_col3">
                    <div class="cms-block">
                        <div class="cms-block-title">
                            <h6>Collections</h6> </div>
                        <div class="cms-block-content">
                            <c:forEach items="${colList}" var="col">
                                <div class="tag-item"> <a class="tag" href="<c:url value='/products?collection=${col.colID}' />">${col.colName}</a> </div>
                            </c:forEach>
                        </div>
                    </div>
                </div>
                <div class="grid12-3 grid-xs-full block_footer_main_col4">
                    <div class="cms-block blog-vlistslider-section">
                        <div class="blog-vlistslider-container">
                            <div class="cms-block-title">
                                <h6>From the Blog</h6> </div>
                            <div class="cms-block-content">
                                <div class="itemslider-wrapper blog-vlistslider-wrapper">
                                    <div id="blog-vlistslider" class="itemslider itemslider-responsive blog-vlistslider owl-carousel">
                                        <div class="column-item">
                                            <div class="item clearfix">
                                                <div class="post-date-image f-left">
                                                    <div class="post-date"> <span class="date">28</span> <span class="month">Apr</span> </div>
                                                </div>
                                                <div class="post-body">
                                                    <h4><a href="index.php/blog/conference-15.html/index.html" >NEW YORK CONFERENCE ‘15</a></h4> <span class="post-user-section">
                                                        by&nbsp;Mage Dev&nbsp;/&nbsp;                                    <span class="comments-count theme-color">0&nbsp;Comments</span> </span>
                                                </div>
                                            </div>
                                            <div class="item clearfix">
                                                <div class="post-date-image f-left">
                                                    <div class="post-date"> <span class="date">28</span> <span class="month">Apr</span> </div>
                                                </div>
                                                <div class="post-body">
                                                    <h4><a href="index.php/blog/conference-10.html/index.html" >NEW YORK CONFERENCE ‘10</a></h4> <span class="post-user-section">
                                                        by&nbsp;Mage Dev&nbsp;/&nbsp;                                    <span class="comments-count theme-color">0&nbsp;Comments</span> </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="column-item">
                                            <div class="item clearfix">
                                                <div class="post-date-image f-left">
                                                    <div class="post-date"> <span class="date">28</span> <span class="month">Apr</span> </div>
                                                </div>
                                                <div class="post-body">
                                                    <h4><a href="index.php/blog/conference-11.html/index.html" >NEW YORK CONFERENCE ‘11</a></h4> <span class="post-user-section">
                                                        by&nbsp;Mage Dev&nbsp;/&nbsp;                                    <span class="comments-count theme-color">0&nbsp;Comments</span> </span>
                                                </div>
                                            </div>
                                            <div class="item clearfix">
                                                <div class="post-date-image f-left">
                                                    <div class="post-date"> <span class="date">28</span> <span class="month">Apr</span> </div>
                                                </div>
                                                <div class="post-body">
                                                    <h4><a href="index.php/blog/conference-12.html/index.html" >NEW YORK CONFERENCE ‘12</a></h4> <span class="post-user-section">
                                                        by&nbsp;Mage Dev&nbsp;/&nbsp;                                    <span class="comments-count theme-color">0&nbsp;Comments</span> </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="column-item">
                                            <div class="item clearfix">
                                                <div class="post-date-image f-left">
                                                    <div class="post-date"> <span class="date">28</span> <span class="month">Apr</span> </div>
                                                </div>
                                                <div class="post-body">
                                                    <h4><a href="index.php/blog/conference-13.html/index.html" >NEW YORK CONFERENCE ‘13</a></h4> <span class="post-user-section">
                                                        by&nbsp;Mage Dev&nbsp;/&nbsp;                                    <span class="comments-count theme-color">0&nbsp;Comments</span> </span>
                                                </div>
                                            </div>
                                            <div class="item clearfix">
                                                <div class="post-date-image f-left">
                                                    <div class="post-date"> <span class="date">28</span> <span class="month">Apr</span> </div>
                                                </div>
                                                <div class="post-body">
                                                    <h4><a href="index.php/blog/conference-14.html/index.html" >NEW YORK CONFERENCE ‘14</a></h4> <span class="post-user-section">
                                                        by&nbsp;Mage Dev&nbsp;/&nbsp;                                    <span class="comments-count theme-color">0&nbsp;Comments</span> </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <script type="text/javascript">
                            //<![CDATA[
                            jQuery(function($) {
                                var owlVListBlog = $('#blog-vlistslider');
                                var owlVListBlogBreakpoints = [
                                    [0, 1],
                                    [320, 1],
                                    [480, 1],
                                    [751, 1],
                                    [960, 1],
                                    [1263, 1]
                                ];
                                owlVListBlog.owlCarousel({
                                    lazyLoad: true,
                                    responsive: true,
                                    items: 2,
                                    autoPlay: true,
                                    itemsCustom: owlVListBlogBreakpoints,
                                    stopOnHover: true,
                                    rewindNav: true,
                                    rewindSpeed: 600,
                                    pagination: false,
                                    navigation: false
                                });
                            });
                            //]]>
                        </script>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-bottom-container section-container">
        <div class="footer-blocks footer container">
            <div class="clearfix"> </div>
        </div>
        <div class="footer-bottom footer container">
            <div class="clearfix">
                <div class="grid12-6 grid-xs-full no-padding">
                    <p class="footer-copyright">&copy; Created with <span class="theme-color"><span class="fa fa-heart"></span></span> by <span><a href="http://www.soaptheme.net/" class="theme-color">soaptheme</a></span>. All right reserved</p>
                </div>
                <div class="grid12-6 grid-xs-full no-padding">
                    <div class="item item-right">
                        <div class="f-right"><img src="<c:url value="/" />resource/client/media/wysiwyg/elegento/footer/payments.png" alt="" style="width:100%;" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> <a href="#top" id="scroll-to-top"><span class="fa fa-angle-up"></span></a>
</div>
