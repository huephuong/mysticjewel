<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="/admin/layout/taglib.jsp"%>
<div class="header-ajaxlr-container">
    <div class="eg-ajaxlogin-loader"> </div>
    <div class="eg-login-window">
        <div class="eg-window-outside">
            <!-- <span class="close">×</span> -->
            <div class="eg-window-inside">
                <div class="eg-window-title">
                    <h2 class="r-separator">
                        <span>Login</span>
                    </h2>
                </div>
                <div class="eg-window-box first">
                    <div class="eg-window-content">
                        <div class="input-box">
                            <label for="eg-email-login">E-mail address <span>*</span> </label>
                            <input type="text" id="eg-email-login" class="input-text" name="eg-email" value="" />
                            <div class="eg-ajaxlogin-error err-email err-noemail err-wrongemail err-wronglogin err-baned"></div>
                        </div>
                        <div class="input-box">
                            <label for="eg-password-login">Password <span>*</span> </label>
                            <input type="password" id="eg-password-login" class="input-text" name="eg-password" value="" />
                            <div class="eg-ajaxlogin-error err-password err-dirtypassword err-nopassword err-longpassword"></div>
                        </div>
                        <div class="input-box input-checkbox">
                            <input type="checkbox" id="eg-remember" name="remember" />
                            <label for="eg-remember">Remember me </label>
                            <div class="eg-ajaxlogin-error"></div>
                        </div>
                    </div>
                </div>
                <div class="eg-window-box last">
                    <div class="eg-window-content">
                        <button type="button" class="button eg-ajaxlogin-button">
                            <span><span>Login</span></span>
                        </button>
                        <style type="text/css">
                            @media (min-width: 768px) {
                                .omb_row-sm-offset-3 div:first-child[class*="col-"] {
                                    margin-left: 25%;
                                }
                            }

                            .omb_login .omb_authTitle {
                                text-align: center;
                                line-height: 300%;
                            }

                            .omb_login .omb_socialButtons a {
                                color: white; // In yourUse @body-bg 
                                opacity: 0.9;
                            }

                            .omb_login .omb_socialButtons a:hover {
                                color: white;
                                opacity: 1;
                            }

                            .omb_login .omb_socialButtons .omb_btn-facebook {
                                background: #3b5998;
                            }

                            .omb_login .omb_socialButtons .omb_btn-twitter {
                                background: #00aced;
                            }

                            .omb_login .omb_socialButtons .omb_btn-google {
                                background: #c32f10;
                                margin-top: 1px;
                            }

                            .omb_login .omb_loginOr {
                                position: relative;
                                font-size: 1.5em;
                                color: #aaa;
                                margin-top: 1em;
                                margin-bottom: 1em;
                                padding-top: 0.5em;
                                padding-bottom: 0.5em;
                            }

                            .omb_login .omb_loginOr .omb_hrOr {
                                background-color: #cdcdcd;
                                height: 1px;
                                margin-top: 0px !important;
                                margin-bottom: 0px !important;
                            }

                            .omb_login .omb_loginOr .omb_spanOr {
                                display: block;
                                position: absolute;
                                left: 50%;
                                top: -0.6em;
                                margin-left: -1.5em;
                                background-color: white;
                                width: 3em;
                                text-align: center;
                            }

                            .omb_login .omb_loginForm .input-group.i {
                                width: 2em;
                            }

                            .omb_login .omb_loginForm .help-block {
                                color: red;
                            }

                            @media (min-width: 768px) {
                                .omb_login .omb_forgotPwd {
                                    text-align: right;
                                    margin-top: 10px;
                                }
                            }
                        </style>
                        <div class="omb_login">
                            <div class="row omb_row-sm-offset-3 omb_loginOr">
                                <div class="col-xs-12 col-sm-6">
                                    <hr class="omb_hrOr"> <span class="omb_spanOr">or</span> </div>
                            </div>
                            <div class="omb_row-sm-offset-3 omb_socialButtons">
                                <div>
                                    <a href="javascript:void(0)" onclick="window.open('${fbAuthUrl}','Login Facebook','menubar=1,resizable=1,width=700,height=500')" class="btn btn-lg btn-block omb_btn-facebook"> <i class="fa fa-facebook visible-xs"></i> <span class="hidden-xs">Facebook</span> </a>
                                </div>
                                <div>
                                    <a href="javascript:void(0)" onclick="window.open('${ggAuthUrl}','Login Google','menubar=1,resizable=1,width=700,height=500')" class="btn btn-lg btn-block omb_btn-google"> <i class="fa fa-google-plus visible-xs"></i> <span class="hidden-xs">Google+</span> </a>
                                </div>
                            </div>
                        </div>
                        <span class="grid12-6 no-padding eg-forgot-password">
                            <a href="<c:url value="/" />forgot-password">Forgot Password</a>
                        </span>
                        <p id="switch-to-register" class="grid12-6 no-padding eg-switch-window"> Register </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="eg-register-window">
        <div class="eg-window-outside">
            <!-- <span class="close">×</span> -->
            <div class="eg-window-inside">
                <div class="eg-window-title">
                    <h2 class="r-separator">
                        <span>Registration</span>
                    </h2>
                </div>
                <div class="eg-window-box first">
                    <div class="eg-window-content">
                        <div class="input-box">
                            <label for="eg-firstname">First Name <span>*</span> </label>
                            <input type="text" class="input-text" id="eg-firstname" name="eg-firstname" value="" />
                            <div class="eg-ajaxlogin-error err-firstname err-nofirstname err-dirtyfirstname"></div>
                        </div>
                        <div class="input-box">
                            <label for="eg-lastname">Last Name <span>*</span> </label>
                            <input type="text" class="input-text" id="eg-lastname" name="eg-lastname" value="" />
                            <div class="eg-ajaxlogin-error err-lastname err-nolastname err-dirtylastname"></div>
                        </div>
                        <div class="input-box">
                            <label for="eg-username">User Name <span>*</span> </label>
                            <input type="text" class="input-text" id="eg-username" name="eg-username" value="" />
                            <div class="eg-ajaxlogin-error err-username err-dirtyusername err-nousername err-shortusername err-longusername err-usernameisexist"></div>
                        </div>
                    </div>
                </div>
                <div class="eg-window-box second">
                    <div class="eg-window-content">
                        <div class="input-box">
                            <label for="eg-email">E-mail address <span>*</span> </label>
                            <input type="text" class="input-text" id="eg-email" name="eg-email" value="" />
                            <div class="eg-ajaxlogin-error err-email err-noemail err-wrongemail err-emailisexist"></div>
                        </div>
                        <div class="input-box">
                            <label for="eg-password">Password <span>*</span> </label>
                            <input type="password" class="input-text" id="eg-password" name="eg-password" value="" />
                            <div class="eg-ajaxlogin-error err-password err-dirtypassword err-nopassword err-shortpassword err-longpassword"></div>
                        </div>
                        <div class="input-box">
                            <label for="eg-passwordsecond">Password confirmation <span>*</span> </label>
                            <input type="password" class="input-text" id="eg-passwordsecond" name="eg-passwordsecond" value="" />
                            <div class="eg-ajaxlogin-error err-passwordsecond err-nopasswordsecond err-notsamepasswords"></div>
                        </div>
                        <div class="input-box input-checkbox">
                            <input type="checkbox" id="eg-licence" name="eg-licence" value="ok" />
                            <label for="eg-licence">I accept the <a href="#" target="_blank">Terms and Coditions</a> </label>
                            <div class="eg-ajaxlogin-error err-nolicence"></div>
                        </div>
                    </div>
                </div>
                <div class="eg-window-box last">
                    <div class="eg-window-content">
                        <button type="button" class="button eg-ajaxlogin-button">
                            <span><span>Register</span></span>
                        </button>
                        <p id="switch-to-login" class="no-padding eg-switch-window"> Login </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        //<![CDATA[
        jQuery(function($) {
            $.fn.egAjaxLogin({
                redirection: '0',
                profileUrl: '<c:url value="/" />customer/account',
                controllerUrlLogin: '<c:url value="/" />login',
                controllerUrlRegistration: '<c:url value="/" />register',
                errorMessages: {
                    "nofirstname": "First name is required!",
                    "nolastname": "Last name is required!",
                    "nousername": "User name is required!",
                    "dirtyfirstname": "First name is not valid!",
                    "dirtylastname": "Last name is not valid!",
                    "dirtyusername": "User name is not valid!",
                    "shortusername": "Please enter 6 or more characters!",
                    "longusername": "Please enter 16 or less characters!",
                    "usernameisexist": "This user name is already registered!",
                    "wrongemail": "This is not an email address!",
                    "noemail": "Email address is required!",
                    "emailisexist": "This email is already registered!",
                    "nopassword": "Password is required!",
                    "dirtypassword": "Enter a valid password!",
                    "shortpassword": "Please enter 6 or more characters!",
                    "longpassword": "Please enter 24 or less characters!",
                    "notsamepasswords": "Passwords are not same!",
                    "nolicence": "Terms and Conditions are required!",
                    "wronglogin": "Email or Password is wrong!",
                    "baned": "The account is locked, please contact the administrator!",
                },
                offsetLeft: 0,
                offsetTop: 23,
                parentElem: '.header-container',
                elemWrapper: '.header-ajaxlr-container',
                elemLogin: 'a[href*="customer/account/login"]',
                elemReg: 'a[href*="customer/account/register"]',
                elemOther: ''
            });
        });
        //]]>
    </script>
</div>
<div id="eg-ajaxcart-bglayer" class="eg-ajax-bglayer"></div>
<div id="eg-ajaxcart-success" class="border-block eg-ajax-success-msg">
    <div class="success-msg-container">
        <p class="msg-content">Product was successfully added to your shopping cart.</p>
        <p class="timer theme-color" data-time="5">5</p>
        <button type="button" name="eg-ajaxcart-continue" id="eg-ajaxcart-continue" class="button btn-continue btn-grey"> <span><span>Continue Shopping</span></span>
        </button>
        <button type="button" name="eg-ajaxcart-checkout" id="eg-ajaxcart-checkout" class="button btn-checkout"> <span><span>Checkout</span></span>
        </button>
    </div>
</div>
<script type="text/javascript">
    jQuery('#eg-ajaxcart-checkout').click(function() {
        location.href = "<c:url value="/"/>checkout/onepage/";
    });
    jQuery('#eg-ajaxcart-continue').click(function() {
        jQuery('#eg-ajaxcart-success').fadeOut(200);
        jQuery('#eg-ajaxcart-bglayer').fadeOut(200);
    });
</script>
<div id="eg-ajaxcomp-bglayer" class="eg-ajax-bglayer"></div>
<div id="eg-ajaxcomp-success" class="border-block eg-ajax-success-msg">
    <div class="success-msg-container">
        <p class="msg-content">Product was successfully added to your compare list.</p>
        <p class="timer theme-color" data-time="5">5</p>
        <button type="button" name="eg-ajaxcomp-continue" id="eg-ajaxcomp-continue" class="button btn-continue btn-grey"> <span><span>Continue Shopping</span></span>
        </button>
        <button type="button" name="eg-ajaxcomp-checkout" id="eg-ajaxcomp-checkout" class="button btn-checkout"> <span><span>Go to Compare</span></span>
        </button>
    </div>
</div>
<script type="text/javascript">
    jQuery('#eg-ajaxcomp-checkout').click(function() {
        location.href = "<c:url value="/"/>catalog/product_compare/index/uenc/aHR0cDovL3d3dy5zb2FwdGhlbWUubmV0L21hZ2VudG8vcm95YWwvZGVtbzYvaW5kZXgucGhwL2N1c3RvbWVyL2FjY291bnQvbG9naW4v/index.html";
    });
    jQuery('#eg-ajaxcomp-continue').click(function() {
        jQuery('#eg-ajaxcomp-success').fadeOut(200);
        jQuery('#eg-ajaxcomp-bglayer').fadeOut(200);
    });
</script>
<div id="eg-wishlist-bglayer" class="eg-ajax-bglayer"></div>
<div id="eg-wishlist-success" class="border-block eg-ajax-success-msg">
    <div class="success-msg-container">
        <p class="msg-content">Product was successfully added to your wishlist.</p>
        <p class="timer theme-color" data-time="5">5</p>
        <button type="button" name="eg-wishlist-continue" id="eg-wishlist-continue" class="button btn-continue btn-grey"> <span><span>Continue Shopping</span></span>
        </button>
        <button type="button" name="eg-wishlist-checkout" id="eg-wishlist-checkout" class="button btn-checkout"> <span><span>Go to Wishlist</span></span>
        </button>
    </div>
</div>
<script type="text/javascript">
    jQuery('#eg-wishlist-checkout').click(function() {
        location.href = "<c:url value="/"/>wishlist";
    });
    jQuery('#eg-wishlist-continue').click(function() {
        jQuery('#eg-wishlist-success').fadeOut(200);
        jQuery('#eg-wishlist-bglayer').fadeOut(200);
    });
</script>
