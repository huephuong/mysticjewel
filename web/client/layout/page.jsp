<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="/admin/layout/taglib.jsp"%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title><tiles:getAsString name="title" /> | Royal Premium Responsive Magento Theme</title>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta name="description" content="" />
        <meta name="keywords" content="Magento Responsive Theme, Responsive Magento Theme" />
        <meta name="robots" content="INDEX,FOLLOW" />
        <!--
        <link rel="icon" href="http://www.soaptheme.net/magento/royal/skin/frontend/base/default/favicon.ico" type="image/x-icon" />
        <link rel="shortcut icon" href="http://www.soaptheme.net/magento/royal/skin/frontend/base/default/favicon.ico" type="image/x-icon" />
        -->
        <link rel="icon" href="skin/frontend/royal/default/favicon.png" type="image/png" />
        <link rel="shortcut icon" href="skin/frontend/royal/default/favicon.png" type="image/png" />
        <!--[if lt IE 7]>
        <script type="text/javascript">
            //<![CDATA[
                var BLANK_URL = 'http://www.soaptheme.net/magento/royal/js/blank.html';
                var BLANK_IMG = 'http://www.soaptheme.net/magento/royal/js/spacer.gif';
            //]]>
        </script>
        <![endif]-->
        <link rel="stylesheet" type="text/css" href="<c:url value="/" />resource/client/media/css/0f7a1a9dc80d956761dcac5407b1e2c0.css" media="print" />
        <tiles:insertAttribute name="page-css" />
        <script type="text/javascript" src="<c:url value="/" />resource/client/js/prototype/prototype.js"></script>
        <script type="text/javascript" src="<c:url value="/" />resource/client/js/lib/ccard.js"></script>
        <script type="text/javascript" src="<c:url value="/" />resource/client/js/prototype/validation.js"></script>
        <script type="text/javascript" src="<c:url value="/" />resource/client/js/scriptaculous/builder.js"></script>
        <script type="text/javascript" src="<c:url value="/" />resource/client/js/scriptaculous/effects.js"></script>
        <script type="text/javascript" src="<c:url value="/" />resource/client/js/scriptaculous/dragdrop.js"></script>
        <script type="text/javascript" src="<c:url value="/" />resource/client/js/scriptaculous/controls.js"></script>
        <script type="text/javascript" src="<c:url value="/" />resource/client/js/scriptaculous/slider.js"></script>
        <script type="text/javascript" src="<c:url value="/" />resource/client/js/varien/js.js"></script>
        <script type="text/javascript" src="<c:url value="/" />resource/client/js/varien/form.js"></script>
        <script type="text/javascript" src="<c:url value="/" />resource/client/js/varien/menu.js"></script>
        <script type="text/javascript" src="<c:url value="/" />resource/client/js/mage/translate.js"></script>
        <script type="text/javascript" src="<c:url value="/" />resource/client/js/mage/cookies.js"></script>
        <script type="text/javascript" src="<c:url value="/" />resource/client/js/elegento/jquery/jquery-1.10.2.js"></script>
        <script type="text/javascript" src="<c:url value="/" />resource/client/js/elegento/jquery/jquery-noconflict.js"></script>
        <script type="text/javascript" src="<c:url value="/" />resource/client/js/elegento/jquery/jquery-ui-1.10.3.js"></script>
        <script type="text/javascript" src="<c:url value="/" />resource/client/js/elegento/ajax/ajax.js"></script>
        <script type="text/javascript" src="<c:url value="/" />resource/client/js/elegento/ajax/ajaxlogin.js"></script>
        <script type="text/javascript" src="<c:url value="/" />resource/client/js/elegento/ajax/quickview.js"></script>
        <script type="text/javascript" src="<c:url value="/" />resource/client/js/elegento/menu/menu.js"></script>
        <script type="text/javascript" src="<c:url value="/" />resource/client/js/elegento/media.match.js"></script>
        <script type="text/javascript" src="<c:url value="/" />resource/client/js/elegento/modernizr.js"></script>
        <script type="text/javascript" src="<c:url value="/" />resource/client/js/elegento/enquire.js"></script>
        <script type="text/javascript" src="<c:url value="/" />resource/client/js/elegento/jquery/plugins/plugins.js"></script>
        <script type="text/javascript" src="<c:url value="/" />resource/client/js/elegento/bootstrap.min.js"></script>
        <script type="text/javascript" src="<c:url value="/" />resource/client/js/elegento/pre-elegento.js"></script>
        <script type="text/javascript" src="<c:url value="/" />resource/client/js/nwdthemes/jquery-1.11.0.min.js"></script>
        <script type="text/javascript" src="<c:url value="/" />resource/client/js/nwdthemes/jquery-migrate-1.2.1.min.js"></script>
        <script type="text/javascript" src="<c:url value="/" />resource/client/js/nwdthemes/jquery.noconflict.js"></script>
        <script type="text/javascript" src="<c:url value="/" />resource/client/skin/frontend/base/default/js/nwdthemes/revslider/rs/jquery.themepunch.tools.min.js"></script>
        <script type="text/javascript" src="<c:url value="/" />resource/client/skin/frontend/base/default/js/nwdthemes/revslider/rs/jquery.themepunch.revolution.min.js"></script>
        <tiles:insertAttribute name="page-js" />
        <!--[if IE]>
        <link rel="stylesheet" type="text/css" href="http://www.soaptheme.net/magento/royal/media/css/181d3565126faf10c805ee17032869b2.css" media="all" />
        <![endif]-->
        <!--[if lt IE 7]>
        <script type="text/javascript" src="http://www.soaptheme.net/magento/royal/js/lib/ds-sleight.js"></script>
        <script type="text/javascript" src="http://www.soaptheme.net/magento/royal/skin/frontend/base/default/js/ie6.js"></script>
        <![endif]-->
        <!--[if lte IE 8]>
        <link rel="stylesheet" type="text/css" href="http://www.soaptheme.net/magento/royal/media/css/e94b61f13849fb09464e2151d5529a9a.css" media="all" />
        <![endif]-->
        <script type="text/javascript">
            //<![CDATA[
            Mage.Cookies.path = '/magento/royal/demo6';
            Mage.Cookies.domain = '.www.soaptheme.net';
            //]]>
        </script>
        <script type="text/javascript">
            //<![CDATA[
            optionalZipCountries = ["HK", "IE", "MO", "PA"];
            //]]>
        </script>
        <script type="text/javascript">
            //<![CDATA[
            var egAjaxTime;
            var egAjaxTimer;
            var EGAjaxObject = new EG.EgAjax();
            jQuery(function($) {
                var loaderClass = EGAjaxObject.options.loader.substring(1);
                $('body').append($('<div />', {
                    class: loaderClass,
                    css: {
                        "display": "none"
                    }
                }));
                var ajaxWishlistFn = function(event) {
                    event.preventDefault();
                    EGAjaxObject.ajaxWishlist($(this).attr('href'));
                };
                $('body').on('click', '.link-wishlist', ajaxWishlistFn);
                var ajaxCompareFn = function(event) {
                    event.preventDefault();
                    EGAjaxObject.ajaxCompare($(this).attr('href'));
                };
                $('body').on('click', '.link-compare', ajaxCompareFn);
                var ajaxQuickviewFn = function(event) {
                    event.preventDefault();
                    EGAjaxObject.ajaxQuickview($(this));
                };
                $('body').on('click', '.link-quickview', ajaxQuickviewFn);
            });
            //]]>
        </script>
        <script type="text/javascript">
            //<![CDATA[
            var Translator = new Translate([]);
            //]]>
        </script>
        <script>
            (function(i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function() {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                    m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
            ga('create', 'UA-66842336-1', 'auto');
            ga('send', 'pageview');
            // Load the SDK asynchronously
            window.fbAsyncInit = function() {
                FB.init({
                    appId: '152800598660318',
                    autoLogAppEvents: true,
                    xfbml: true,
                    version: 'v3.0'
                });
            };
            (function(d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) {
                    return;
                }
                js = d.createElement(s);
                js.id = id;
                js.src = "https://connect.facebook.net/en_US/sdk.js";
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));
        </script>
        <!--[if lt IE 9]>
        <script type='text/javascript' src="http://www.soaptheme.net/magento/royal/js/elegento/html5shiv.js"></script>
        <script type='text/javascript' src="http://www.soaptheme.net/magento/royal/js/elegento/css3-mediaqueries.js"></script>
        <![endif]-->
        <link href='http://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900italic,900' rel='stylesheet' type='text/css' />
    </head>

    <body class="<tiles:getAsString name="body-class" />">
        <div class="mobile-nav-wrapper">
            <ul class="elegento-mobile accordion">
                <li class="level0 parent"><a class="level0" href="#"><span>Our Shop</span></a><span class="opener"><span class="icon-royal icon-plus"></span><span class="icon-royal icon-minus"></span></span>
                    <ul class="level0 parent clearfix" style="display: none;">
                        <c:forEach items="${catList}" var="mainCat">
                            <c:if test="${not empty mainCat.categoryList}">
                                <li class="level1 parent"><a class="level1" href="<c:url value="/" />products?category=${mainCat.categoryID}"><span>${mainCat.categoryName}</span></a><span class="opener"><span class="icon-royal icon-plus"></span><span class="icon-royal icon-minus"></span></span>
                                    <ul class="level1 parent clearfix" style="display: none;">
                                        <c:forEach items="${mainCat.categoryList}" var="subCat">
                                        <li class="level2"><a class="level2" href="<c:url value="/" />products?category=${subCat.categoryID}"><span>${subCat.categoryName}</span></a> </li>
                                        </c:forEach>
                                    </ul>
                                </li>
                            </c:if>
                        </c:forEach>
                    </ul>
                </li>
                <li class="level0"><a class="level0" href="<c:url value="/" />new-arrivals"><span>New Arrivals</span></a> </li>
                <li class="level0"><a class="level0" href="<c:url value="/" />on-sale"><span>On Sale</span></a> </li>
                <li class="level0 parent"><a class="level0" href="#"><span>Collections</span></a><span class="opener"><span class="icon-royal icon-plus"></span><span class="icon-royal icon-minus"></span></span>
                    <ul class="level0 parent clearfix" style="display: none;">
                        <c:forEach items="${colList}" var="productCol">
                            <c:if test="${productCol.isActive}">
                                <li class="level2"><a class="level2" href="<c:url value="/" />products?collection=${productCol.colID}"><span>${productCol.colName}</span></a> </li>
                            </c:if>
                        </c:forEach>
                    </ul>
                </li>
                <li class="level0"><a href="<c:url value="/" />contact"><span>Contact</span></a> </li>
                <li class="first  eg-top-links"><a href="<c:url value="/" />customer/account" title="My Account">My Account</a> </li>
                <li class="eg-top-links"><a href="<c:url value="/" />customer/wishlist" title="My Wishlist">My Wishlist (${customer.getBookmarkList().size()})</a> </li>
                <li class="eg-top-links"><a href="<c:url value="/" />product-compare" title="Compare">Compare List<c:if test="${not empty comparedList}">(${fn:length(comparedList)})</c:if></a></li>
                <li class="eg-top-links"><a href="<c:url value="/" />checkout" title="Checkout" class="top-link-checkout">Checkout</a> </li>
                <c:choose>
                    <c:when test="${empty customer}">
                    <li class="last  eg-top-links"><a href="<c:url value="/" />customer/account/login" title="Log In" class="eg-login-link">Log In</a> </li>
                    </c:when>    
                    <c:otherwise>
                    <li class="last  eg-top-links"><a href="<c:url value="/" />customer/account/logout" title="Log Out" class="eg-logout-link">Log Out</a> </li>
                    </c:otherwise>
                </c:choose>
            </ul>
            <div class="search-wrapper">
                <form id="search_mini_form_mobile" style="overflow: auto" action="<c:url value="/" />catalogsearch/result" method="post">
                    <div class="form-search clearfix">
                        <input id="search_mobile" type="text" name="searchTitle" value="" class="input-text" maxlength="128" />
                        <button type="submit" title="Search" class="button"><span><span class="fa fa-search"></span></span>
                        </button>
                        <div id="search_autocomplete_mobile" class="search-autocomplete"></div>
                        <script type="text/javascript">
                            //<![CDATA[
                            var searchForm = new Varien.searchForm('search_mini_form_mobile', 'search_mobile', 'Find it here...');
                            searchForm.initAutocomplete('<c:url value="/" />catalogsearch', 'search_autocomplete_mobile');
                            //]]>
                        </script>
                    </div>
                </form>
            </div>
        </div>
        <div id="root-wrapper">
            <div class="wrapper">
                <noscript>
                    <div class="global-site-notice noscript">
                        <div class="notice-inner">
                            <p> <strong>JavaScript seems to be disabled in your browser.</strong>
                                <br /> You must have JavaScript enabled in your browser to utilize the functionality of this website. </p>
                        </div>
                    </div>
                </noscript>
                <div class="page">
                    <!-- STICKY HEADER -->
                    <div class="eg-stickyheader sticky-header-container eg-headerstyle6">
                        <div class="sticky-header container">
                            <div class="table">
                                <div class="table-row">
                                    <div class="nav-trigger-container table-cell">
                                        <div class="nav-trigger grid-xs-show grid-sm-show"> <span class="fa fa-align-justify"></span> </div>
                                    </div>
                                    <div class="sticky-left table-cell">
                                        <a href="<c:url value="/" />" title="Royal Store" class="logo"><img src="<c:url value="/resource/images/icons/logo_jewellery.png"/>" alt="Royal Store" />
                                        </a>
                                    </div>
                                    <div class="sticky-main table-cell"> </div>
                                    <div class="sticky-right table-cell">
                                        <div class="search-wrapper">
                                            <div class="eg-search-trigger"><span class="fa fa-search"></span>
                                            </div>
                                            <form id="search_mini_form_sticky" style="overflow: auto; min-height:150px;" action="<c:url value="/" />catalogsearch/result" method="post">
                                                <div class="form-search clearfix">
                                                    <input id="search_sticky" type="text" name="searchTitle" value="" class="input-text" maxlength="128" />
                                                    <button type="submit" title="Search" class="button"><span><span class="fa fa-search"></span></span>
                                                    </button>
                                                    <div id="search_autocomplete_sticky" class="search-autocomplete"></div>
                                                    <script type="text/javascript">
                                                        //<![CDATA[
                                                        var searchForm = new Varien.searchForm('search_mini_form_sticky', 'search_sticky', 'Find it here...');
                                                        searchForm.initAutocomplete('<c:url value="/" />catalogsearch', 'search_autocomplete_sticky');
                                                        //]]>
                                                    </script>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="mini-cart is-empty">
                                            <div class="mini-cart-trigger">
                                                <a href="<c:url value="/" />checkout/cart" class="cart-summary" title="CART">
                                                    <span class="cart-badge">
                                                        <span class="badge-icon icon-royal icon-soppingbag-large theme-color"></span>
                                                        <span class="badge-number">${cartItems.size()}</span>
                                                    </span>
                                                    <span class="cart-label empty">Cart : </span>
                                                    <span class="subtotal">
                                                        <span class="amount">
                                                            <span class="price">$${!empty subTotal ? subTotal : "0.0"}</span>
                                                        </span>
                                                    </span>
                                                </a>
                                            </div>
                                            <div class="mini-cart-content">
                                                <c:choose>
                                                    <c:when test="${empty cartItems}">
                                                        <div class="empty">No products in the cart.</div>
                                                    </c:when>    
                                                    <c:otherwise>
                                                        <h4 class="block-subtitle">Recently Added Item(s)</h4>
                                                        <ol class="mini-products-list">
                                                            <c:forEach items="${cartItems}" var="cartItem">
                                                                <li class="item">
                                                                    <div class="media-section">
                                                                        <a href="<c:url value="/" />product-detail?skuid=${cartItem.key.skuID}" title="Men Jewellery" class="product-image"><img src="<c:url value="/"/>resource/images/${cartItem.key.productID.productImageList[0].imageLink}" alt="${cartItem.key.productID.productName}" /> </a>
                                                                    </div>
                                                                    <div class="product-details clearfix">
                                                                        <a href="javascript:void(0);" title="Remove This Item" onclick="confirm('Are you sure you would like to remove this item from the shopping cart?');
                                                                                EGAjaxObject.ajaxCall('<c:url value="/"/>/remove-product-to-cart', '.block-cart', 'cart', 'skuID=${cartItem.key.skuID}');" class="btn-remove"></a>
                                                                        <p class="product-name"><a href="<c:url value="/" />product-detail?skuid=${cartItem.key.skuID}">${cartItem.key.productID.productName}</a> </p>
                                                                        <span class="mini-qty">${cartItem.value}</span> x <span class="price">$${cartItem.key.unitPrice}</span>
                                                                    </div>
                                                                </li>
                                                            </c:forEach>
                                                        </ol>
                                                        <div class="clearfix"></div>
                                                        <span class="subtotal-wrapper">
                                                            <span class="subtotal-title">Subtotal: </span>
                                                            <a class="summary" href="<c:url value="/" />checkout/cart/" title="View all items in your shopping cart">
                                                                <span class="subtotal">
                                                                    <span class="price">$${subTotal}</span>
                                                                </span>
                                                            </a>
                                                        </span>
                                                        <script type="text/javascript">
                                                            decorateList('cart-sidebar', 'none-recursive')
                                                        </script>
                                                        <div class="clearfix"></div>
                                                        <div class="buttons-set">
                                                            <div class="view-cart grid-full no-padding"> <a title="Cart" class="button btn-cart btn-grey" href="<c:url value="/" />checkout/cart"><span><span>Cart</span></span></a> </div>
                                                            <div class="checkout grid-full no-padding"> <a title="Checkout" class="button btn-checkout" href="<c:url value="/" />checkout/onepage"><span><span>Checkout</span></span></a> </div>
                                                        </div>
                                                    </c:otherwise>
                                                </c:choose>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="custom-block-headertop-container"> </div>
                    <div class="header-container eg-headerstyle6">
                        <div class="header-top-container">
                            <div class="header-top header container">
                                <div class="header-top-right clearfix">
                                    <div class="top-links">
                                        <ul class="links">
                                            <li class="first "><a href="<c:url value="/" />customer/account" title="My Account">My Account</a></li>
                                            <li class=""><a href="<c:url value="/" />customer/wishlist" title="My Wishlist (${customer.getBookmarkList().size()})">My Wishlist (${customer.getBookmarkList().size()})</a></li>
                                            <li class=""><a href="<c:url value="/" />product-compare" title="Compare">Compare List<c:if test="${not empty comparedList}">(${fn:length(comparedList)})</c:if></a></li>
                                            <li class=""><a href="<c:url value="/" />checkout" title="Checkout" class="top-link-checkout">Checkout</a></li>
                                                <c:choose>
                                                    <c:when test="${empty customer}">
                                                    <li class=" last "><a href="<c:url value="/" />customer/account/login" title="Log In" class="eg-login-link">Log In</a></li>
                                                    </c:when>    
                                                    <c:otherwise>
                                                    <li class=" last "><a href="<c:url value="/" />customer/account/logout" title="Log Out" class="eg-logout-link">Log Out</a></li>
                                                    </c:otherwise>
                                                </c:choose>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="header-main-container">
                            <div class="header-main header container">
                                <div class="table">
                                    <div class="table-row">
                                        <div class="nav-trigger-container table-cell">
                                            <div class="nav-trigger grid-xs-show grid-sm-show"> <span class="fa fa-align-justify"></span> </div>
                                        </div>
                                        <div class="header-main-left">
                                            <div class="welcome-content"> <span class="comment-icon theme-color"><span class="icon-royal icon-message"></span></span>
                                                    <c:choose>
                                                        <c:when test="${empty customer}">
                                                        <p class="welcome-msg"> Welcome to Mystic Jewel </p>
                                                    </c:when>    
                                                    <c:otherwise>
                                                        <p class="welcome-msg"> Welcome, ${customer.firstName} ${customer.lastName} </p>
                                                    </c:otherwise>
                                                </c:choose>
                                            </div>
                                        </div>
                                        <div class="header-main-center">
                                            <a href="<c:url value="/" />home" title="Mystic Jewel" class="logo"><img src="<c:url value="/" />resource/images/icons/logo_jewellery.png" alt="Mystic Jewel" />
                                            </a>
                                        </div>
                                        <div class="header-main-right clearfix">
                                            <div class="search-wrapper">
                                                <div class="eg-search-trigger"><span class="fa fa-search"></span>
                                                </div>
                                                <form id="search_mini_form" style="overflow: auto; height: 150px" action="<c:url value="/" />catalogsearch/result" method="post">
                                                    <div class="form-search clearfix">
                                                        <input id="search" type="text" name="searchTitle" value="" class="input-text" maxlength="128" />
                                                        <button type="submit" title="Search" class="button"><span><span class="fa fa-search"></span></span>
                                                        </button>
                                                        <div id="search_autocomplete" class="search-autocomplete">

                                                        </div>
                                                        <script type="text/javascript">
                                                            //<![CDATA[
                                                            var searchForm = new Varien.searchForm('search_mini_form', 'search', 'Find it here...');
                                                            searchForm.initAutocomplete('<c:url value="/" />catalogsearch', 'search_autocomplete');
                                                            //]]>
                                                        </script>
                                                    </div>
                                                </form>
                                            </div>
                                            <div class="mini-cart is-empty">
                                                <div class="mini-cart-trigger">
                                                    <a href="<c:url value="/" />checkout/cart" class="cart-summary" title="CART">
                                                        <span class="cart-badge">
                                                            <span class="badge-icon icon-royal icon-soppingbag-large theme-color"></span>
                                                            <span class="badge-number">${cartItems.size()}</span>
                                                        </span>
                                                        <span class="cart-label empty">Cart : </span>
                                                        <span class="subtotal">
                                                            <span class="amount">
                                                                <span class="price">$${!empty subTotal ? subTotal : "0.0"}</span>
                                                            </span>
                                                        </span>
                                                    </a>
                                                </div>
                                                <div class="mini-cart-content">
                                                    <c:choose>
                                                        <c:when test="${empty cartItems}">
                                                            <div class="empty">No products in the cart.</div>
                                                        </c:when>    
                                                        <c:otherwise>
                                                            <h4 class="block-subtitle">Recently Added Item(s)</h4>
                                                            <ol class="mini-products-list">
                                                                <c:forEach items="${cartItems}" var="cartItem">
                                                                    <li class="item">
                                                                        <div class="media-section">
                                                                            <a href="<c:url value="/" />product-detail?skuid=${cartItem.key.skuID}" title="${cartItem.key.productID.productName}" class="product-image"><img src="<c:url value="/"/>resource/images/${cartItem.key.productID.productImageList[0].imageLink}" alt="${cartItem.key.productID.productName}" /> </a>
                                                                        </div>
                                                                        <div class="product-details clearfix">
                                                                            <a href="javascript:void(0);" title="Remove This Item" onclick="confirm('Are you sure you would like to remove this item from the shopping cart?');
                                                                                    EGAjaxObject.ajaxCall('<c:url value="/"/>/remove-product-to-cart', '.block-cart', 'cart', 'skuID=${cartItem.key.skuID}');" class="btn-remove"></a>
                                                                            <p class="product-name"><a href="<c:url value="/" />product-detail?skuid=${cartItem.key.skuID}">${cartItem.key.productID.productName}</a> </p>
                                                                            <span class="mini-qty">${cartItem.value}</span> x <span class="price">$${cartItem.key.unitPrice}</span>
                                                                        </div>
                                                                    </li>
                                                                </c:forEach>
                                                            </ol>
                                                            <div class="clearfix"></div>
                                                            <span class="subtotal-wrapper">
                                                                <span class="subtotal-title">Subtotal: </span>
                                                                <a class="summary" href="<c:url value="/" />checkout/cart/" title="View all items in your shopping cart">
                                                                    <span class="subtotal">
                                                                        <span class="price">$${subTotal}</span>
                                                                    </span>
                                                                </a>
                                                            </span>
                                                            <script type="text/javascript">
                                                                decorateList('cart-sidebar', 'none-recursive')
                                                            </script>
                                                            <div class="clearfix"></div>
                                                            <div class="buttons-set">
                                                                <div class="view-cart grid-full no-padding"> <a title="Cart" class="button btn-cart btn-grey" href="<c:url value="/" />checkout/cart"><span><span>Cart</span></span></a> </div>
                                                                <div class="checkout grid-full no-padding"> <a title="Checkout" class="button btn-checkout" href="<c:url value="/" />checkout/onepage"><span><span>Checkout</span></span></a> </div>
                                                            </div>
                                                        </c:otherwise>
                                                    </c:choose>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="header-nav-container">
                            <div class="header-nav header container">
                                <div class="nav-container">
                                    <div class="egmenu">
                                        <style type="text/css" scoped>
                                            .elegento-menu .level0-wrapper.static {
                                                width: 950px;
                                                left: auto;
                                            }

                                            .elegento-menu .level0-wrapper.classic {
                                                width: 200px;
                                                left: auto;
                                            }

                                            .elegento-menu .level0-wrapper.classic li > ul {
                                                width: 200px;
                                            }
                                        </style>
                                        <ul class="elegento-menu clearfix">
                                            <li class="menu level-top parent"><a href=""><span>Our Shop</span></a>
                                                <div class="level0-wrapper static" style="background-repeat: no-repeat;background-image: url(resource/images/gems.png);background-position: 100% 100%;">
                                                    <div class="menu-main-block clearfix">
                                                        <div class="category-block grid12-9 clearfix ">
                                                            <!-- Load Category -->
                                                            <c:forEach items="${catList}" var="mainCat">
                                                                <div class="column" style="width: 25.00%">
                                                                    <ul class="level1">
                                                                        <li class="level1 parent"><a class="level1" href="<c:url value="/" />products?category=${mainCat.categoryID}"><span>${mainCat.categoryName}</span></a>
                                                                                    <c:if test="${not empty mainCat.categoryList}">
                                                                                <ul class="level2">
                                                                                    <!-- Sub-Category -->
                                                                                    <c:forEach items="${mainCat.categoryList}" var="subCat">
                                                                                        <li class="level2"><a class="level2" href="<c:url value="/" />products?category=${subCat.categoryID}"><span>${subCat.categoryName}</span></a></li>
                                                                                                </c:forEach>
                                                                                </ul>
                                                                            </c:if>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </c:forEach>
                                                        </div>
                                                        <div class="grid12-3 no-padding ">
                                                            <div></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="menu level-top"><a href="<c:url value="/" />new-arrivals"><span>New Arrivals</span></a>
                                            </li>
                                            <li class="menu level-top"><a href="<c:url value="/" />on-sale"><span>On Sale</span></a>
                                            </li>
                                            <li class="level-top parent"><a href="#"><span>Collections</span></a>
                                                <div class="level0-wrapper classic" style="display: none;">
                                                    <ul class="level1">
                                                        <c:forEach items="${colList}" var="productCol">
                                                            <c:if test="${productCol.isActive}">
                                                                <li class="level1">
                                                                    <a class="menu-tooltip level1" href="<c:url value="/" />products?collection=${productCol.colID}">
                                                                        <span>${productCol.colName}</span>
                                                                        <div class="item-tooltip"><img src="<c:url value="/"/>resource/images/${productCol.colBanner}" alt="" width="450" height="265" /></div>
                                                                    </a>
                                                                </li>
                                                            </c:if>
                                                        </c:forEach>
                                                    </ul>
                                                </div>
                                            </li>
                                            <li class="menu level-top"><a href="<c:url value="/" />contact"><span>Contact</span></a>
                                            </li>
                                        </ul>
                                        <script type="text/javascript">
                                            //<![CDATA[
                                            jQuery(function($) {
                                                $(window).on('load', function() {
                                                    $('.elegento-menu').egMenu({
                                                        delayDisplay: 150,
                                                        fadeIn: 150,
                                                        delayHide: 100,
                                                        fadeOut: 150,
                                                        maximlDepth: 3
                                                    });
                                                });
                                            });
                                            //]]>
                                        </script>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="custom-block-headerbottom-container"> </div>
                    <tiles:insertAttribute name="content" />
                    <div class="custom-block-footertop-container"> </div>
                    <tiles:insertAttribute name="footer" />
                    <div class="custom-block-footerbottom-container"> </div>
                    <script type="text/javascript" src="<c:url value="/" />resource/client/js/elegento/elegento.js"></script>
                </div>
            </div>
        </div>
    </body>
</html>