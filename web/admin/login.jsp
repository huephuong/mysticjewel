<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="layout/taglib.jsp"%>
<tiles:insertDefinition name="page">
    <tiles:putAttribute name="title" value="Login" />
    <tiles:putAttribute name="body">
        <div class="login-box">
            <div class="login-logo">
                <a href="/admin-page/dashboard"><b>Mystic</b>Jewel</a>
            </div>
            <div class="login-box-body">
                <p class="login-box-msg">${admin_login_message}</p>
                <form action="<c:url value="/" />admin-login" method="post" id="f_admin_login">
                    <div class="form-group has-feedback">
                        <input type="text" name="userName" class="form-control" placeholder="Username">
                        <span class="glyphicon glyphicon-user form-control-feedback"></span>
                    </div>
                    <div class="form-group has-feedback">
                        <input type="password" name="password" class="form-control" placeholder="Password">
                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    </div>
                    <div class="row">
                        <div class="col-xs-8">
                            <div class="checkbox icheck">
                                <label>
                                    <input type="checkbox" name="remember"> Remember Me
                                </label>
                            </div>
                        </div>
                        <div class="col-xs-4">
                            <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </tiles:putAttribute>
</tiles:insertDefinition>