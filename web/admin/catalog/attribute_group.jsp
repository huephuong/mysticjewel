<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="../layout/taglib.jsp"%>
<tiles:insertDefinition name="common">
    <tiles:putAttribute name="title" value="Attributes" />
    <tiles:putAttribute name="page-css">
        <link rel="stylesheet" href="<c:url value="/resource/admin/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css" />">
        <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.1/css/responsive.bootstrap.min.css">
    </tiles:putAttribute>
    <tiles:putAttribute name="body">
        <section class="content-header">
            <h1>
                Attributes
                <small>Attribute groups for products</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
                <li class="active">Attributes</li>
            </ol>
        </section>
        <section class="content">

            <div class="row">
                <!-- Attributes List -->
                <div class="col-xs-6">                   
                    <!-- Attributes - Child -->
                    <div class="box box-primary">
                        <div class="box-header">
                            <h3 class="box-title">
                                Attribute List
                        </div>
                        <div class="box-body">
                            <table id="attTable" class="table table-striped table-bordered table-hover dt-responsive nowrap" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Name</th>
                                        <th>Group</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <c:forEach items="${attList}" var="att">
                                        <tr>
                                            <td>${att.attributeID}</td>
                                            <td>${att.attributeValue}</td>
                                            <td>${att.attributeGroupID.attributeGroupName}</td>
                                            <td>
                                                <a class="btn btn-info" href="<c:url value="/admin-page/catalog/attributes/edit?id=${att.attributeID}"/>">Update</a>
                                                <c:if test="${empty att.productList}">
                                                    <a class="btn btn-danger" href="<c:url value="/admin-page/catalog/attributes/delete?id=${att.attributeID}"/>">Delete</a>
                                                </c:if>
                                            </td>
                                        </tr>
                                    </c:forEach>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <!-- Child Attribute -->
                <div class="col-xs-6">
                    <div class="box box-info">
                        <div class="box-header">
                            <h3 class="box-title">
                                ${saveFn} Attribute
                            </h3>
                        </div>
                        <form action="<c:url value="/admin-page/catalog/attributes/edit" />" method="POST" class="form-horizontal" id="form_add_attribute">
                            <div class="box-body">
                                <div class="form-group">
                                    <input type="hidden" name="id" value="${currentAtt.attributeID}">
                                    <label for="inputAttValue" class="col-sm-4 control-label">Attribute Name</label>
                                    <div class="col-sm-8">
                                        <input type="text" name="inputAttValue" class="form-control" id="inputAttValue" placeholder="Attribute Name" value="${currentAtt.attributeValue}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputNew" class="col-sm-4 control-label">New Attribute Group</label>
                                    <div class="col-sm-8">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <input type="checkbox" name="isNew" id="isNew">
                                            </span>
                                            <input type="text" class="form-control" name="inputNewGroup" id="inputNewGroup">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputParent" class="col-sm-4 control-label">Attribute Group</label>
                                    <div class="col-sm-8">
                                        <select class="form-control" name="inputParent" id="inputParent">
                                            <c:forEach items="${groupList}" var="group">
                                                <option value="${group.attributeGroupID}" <c:if test="${group.attributeGroupID eq currentAtt.attributeGroupID.attributeGroupID}">selected</c:if>>${group.attributeGroupName}</option>
                                            </c:forEach>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="box-footer">
                                <button type="submit" class="btn btn-info pull" name="saveFn" value="${saveFn}">Save</button>
                                <button type="reset" class="btn btn-default">Cancel</button>
                            </div>
                        </form>
                    </div>
                    <!-- Attribute Group -->
                    <div class="box box-success">
                        <div class="box-header">
                            <h3 class="box-title">
                                Attribute Groups
                            </h3>
                        </div>
                        <div class="box-body">
                            <table id="attGroupTable" class="table table-striped table-bordered table-hover dt-responsive nowrap" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Group Name</th>
                                        <th>Searchable</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <c:forEach items="${groupList}" var="group">
                                        <tr>
                                            <td>${group.attributeGroupID}</td>
                                            <td>${group.attributeGroupName}</td>
                                            <td>
                                                <c:choose>
                                                    <c:when test="${group.isSearchable}">
                                                        <span class="label label-success">Yes</span>
                                                    </c:when>
                                                    <c:otherwise>
                                                        <span class="label label-danger">No</span>
                                                    </c:otherwise>
                                                </c:choose>
                                            </td>
                                            <td>
                                                <div class="btn-group">
                                                    <button type="button" class="btn btn-info">Action</button>
                                                    <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
                                                        <span class="caret"></span>
                                                        <span class="sr-only">Toggle Dropdown</span>
                                                    </button>
                                                    <ul class="dropdown-menu" role="menu">
                                                        <c:choose>
                                                            <c:when test="${group.isSearchable}">
                                                                <li><a href="<c:url value="/"/>admin-page/catalog/attributes/updateGroup?id=${group.attributeGroupID}&status=false"><span class="text-red">Searchable: NO</span></a></li>
                                                                </c:when>
                                                                <c:otherwise>
                                                                <li><a href="<c:url value="/"/>admin-page/catalog/attributes/updateGroup?id=${group.attributeGroupID}&status=true"><span class="text-green">Searchable: YES</span></a></li>
                                                                </c:otherwise>
                                                            </c:choose>
                                                            <c:if test="${empty group.attributeList}">
                                                            <li class="divider"></li>
                                                            <li><a href="<c:url value="/admin-page/catalog/attributes/deleteGroup?id=${group.attributeGroupID}"/>"><span class="text-red">Delete</span></a></li>
                                                            </c:if>
                                                    </ul>
                                                </div>
                                            </td>
                                        </tr>
                                    </c:forEach>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </tiles:putAttribute>
    <tiles:putAttribute name="page-js">
        <script src="<c:url value="/resource/admin/bower_components/datatables.net/js/jquery.dataTables.min.js" />"></script>
        <script src="<c:url value="/resource/admin/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js" />"></script>
        <script src="https://cdn.datatables.net/responsive/2.2.1/js/dataTables.responsive.min.js"></script>
        <script src="https://cdn.datatables.net/responsive/2.2.1/js/responsive.bootstrap.min.js"></script>
        <script>
            $(function() {
                $('#attGroupTable').DataTable({
                    "pageLength": 10
                });
                $('#attTable').DataTable({
                    "pageLength": 10
                });
                $('#inputNewGroup').prop('disabled', true);
                $('#emptyGroup').DataTable({
                    "pageLength": 10
                });
            });
        </script>
        <script>
            $('#isNew').click(function() {
                if ($('#isNew').is(':checked')) {
                    $('#inputParent').prop('disabled', true);
                    $('#inputNewGroup').prop('disabled', false);
                }
                else {
                    $('#inputParent').prop('disabled', false);
                    $('#inputNewGroup').prop('disabled', true);
                }
            });

            $('#form_add_attribute').validate({
                errorElement: 'span', //default input error message container
                errorClass: 'help-block help-block-error', // default input error message class
                //focusInvalid: false, // do not focus the last invalid input
                //ignore: "",  // validate all fields including form hidden input
                messages: {
                    inputAttValue: {
                        required: "Attribute name can not blank."
                    },
                    inputNewGroup: {
                        required: "Attribute Group name can not blank."
                    }
                },
                rules: {
                    'inputAttValue': {
                        required: true
                    },
                    'inputNewGroup': {
                        required: true
                    }
                },
                errorPlacement: function(error, element) { // render error placement for each input type
                    element.after(error);
                },
                highlight: function(element) { // hightlight error inputs
                    $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
                },
                unhighlight: function(element) { // revert the change done by hightlight
                    $(element).closest('.form-group').removeClass('has-error'); // set error class to the control group
                },
                success: function(label) {
                    label.closest('.form-group').removeClass('has-error'); // set success class to the control group
                },
                submitHandler: function(form) {
                    form.submit();
                }
            });
        </script>
    </tiles:putAttribute>
</tiles:insertDefinition>
