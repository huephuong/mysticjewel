<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="../layout/taglib.jsp"%>
<tiles:insertDefinition name="common">
    <tiles:putAttribute name="title" value="Collection Overview" />
    <tiles:putAttribute name="page-css">
        <link rel="stylesheet" href="<c:url value="/resource/admin/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css" />">
        <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.1/css/responsive.bootstrap.min.css">
    </tiles:putAttribute>
    <tiles:putAttribute name="body">
        <section class="content-header">
            <h1>
                Collection Overview
                <small>All existing collections</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
                <li class="active">Collection List</li>
            </ol>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-xs-6">
                    <div class="box box-primary">
                        <div class="box-header">
                            <h3 class="box-title">
                                Collection List
                            </h3>
                        </div>
                        <div class="box-body">
                            <table id="coltable" class="table table-striped table-bordered table-hover dt-responsive wrap" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Name</th>
                                        <th>Banner</th>
                                        <th>Picture</th>
                                        <th>Trending</th>
                                        <th>Active</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <c:forEach items="${colList}" var="col">
                                        <tr>
                                            <td>${col.colID}</td>
                                            <td>${col.colName}</td>
                                            <td><img src="<c:url value="/resource/images/${col.colBanner}"/>" height="48"/></td>
                                            <td><img src="<c:url value="/resource/images/${col.colImage}"/>" height="48"/></td>
                                            <td><c:if test="${col.isTrending}">Yes</c:if></td>
                                            <td><c:if test="${col.isActive}">Yes</c:if></td>
                                            <td>
                                                    <div class="btn-group">
                                                        <button type="button" class="btn btn-info">Action</button>
                                                        <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
                                                            <span class="caret"></span>
                                                            <span class="sr-only">Toggle Dropdown</span>
                                                        </button>
                                                        <ul class="dropdown-menu" role="menu">
                                                            <li><a href="<c:url value="/admin-page/catalog/product/add" />">New Product</a></li>
                                                        <li><a href="<c:url value="/admin-page/catalog/product" />">Existing Product</a></li>
                                                        <li><a href="<c:url value="/admin-page/catalog/collection/edit?id=${col.colID}" />">Update</a></li>
                                                            <c:if test="${empty col.productList}">
                                                            <li class="divider"></li>
                                                            <li><a href="<c:url value="/admin-page/catalog/collection/delete?id=${col.colID}" />"><span class="text-red">Delete</span></a></li>
                                                            </c:if>
                                                    </ul>
                                                </div>
                                            </td>
                                        </tr>
                                    </c:forEach>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- Detail -->
                <div class="col-xs-6">
                    <div class="box box-info">
                        <div class="box-header">
                            <h3 class="box-title">
                                ${saveFn} Collection
                            </h3>
                        </div>
                        <form action="<c:url value="/admin-page/catalog/collection/edit"/>" method="POST" class="form-horizontal" id="form_add_collection">
                            <div class="box-body">
                                <div class="form-group">
                                    <input type="hidden" name="id" value="${currentCol.colID}">
                                    <label for="inputName" class="col-sm-4 control-label">Collection Name</label>
                                    <div class="col-sm-8">
                                        <input type="text" name="inputName" class="form-control" id="inputName" placeholder="Category Name" value="${currentCol.colName}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputDesc" class="col-sm-4 control-label">Collection Description</label>
                                    <div class="col-sm-8">
                                        <textarea class="form-control" rows="3" name="inputDesc" placeholder="Description" id="inputDesc">${currentCol.colDesc}</textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputBanner" class="col-sm-4 control-label">Banner</label>
                                    <div class="col-sm-8">
                                        <input type="hidden" value="${currentCol.colBanner}" name="oldBanner">
                                        <input type="file" class="form-control" id="inputBanner" name="inputBanner">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputImage" class="col-sm-4 control-label">Image</label>
                                    <div class="col-sm-8">
                                        <input type="hidden" value="${currentCol.colImage}" name="oldImage">
                                        <input type="file" class="form-control" id="inputImage" name="inputImage">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputTrending" class="col-sm-4 control-label">Trending status</label>
                                    <div class="col-sm-8">
                                        <input type="checkbox" name="inputTrending" id="inputTrending" ${currentCol.isTrending ? "checked" : ""}>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputActive" class="col-sm-4 control-label">Active status</label>
                                    <div class="col-sm-8">
                                        <input type="checkbox" name="inputActive" id="inputTrending" ${currentCol.isActive ? "checked" : ""}>
                                    </div>
                                </div>
                            </div>
                            <div class="box-footer">
                                <button type="submit" class="btn btn-info pull" name="saveFn" value="${saveFn}" id="btnSave">Save</button>
                                <button type="reset" class="btn btn-default">Cancel</button>
                            </div>
                        </form>
                    </div>

                    <!-- Product List -->
                    <c:if test="${currentCol ne null}">
                        <div class="box box-success">
                            <div class="box-header">
                                <h3 class="box-title">
                                    Product List for ${currentCol.colName}
                                </h3>
                            </div>
                            <div class="box-body">
                                <table id="producttable" class="table table-striped table-bordered table-hover dt-responsive wrap" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Name</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <c:forEach items="${currentCol.productList}" var="prod">
                                            <tr>
                                                <td>${prod.productID}</td>
                                                <td>${prod.productName}</td>
                                                <td>
                                                    <a class="btn btn-warning" href="<c:url value="/admin-page/catalog/collection/remove-product?id=${currentCol.colID}&prodid=${prod.productID}"/>">Remove</a>
                                                </td>
                                            </tr>
                                        </c:forEach>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </c:if>
                </div>
            </div>
        </section>
    </tiles:putAttribute>
    <tiles:putAttribute name="page-js">
        <script src="<c:url value="/resource/admin/bower_components/datatables.net/js/jquery.dataTables.min.js" />"></script>
        <script src="<c:url value="/resource/admin/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js" />"></script>
        <script src="https://cdn.datatables.net/responsive/2.2.1/js/dataTables.responsive.min.js"></script>
        <script src="https://cdn.datatables.net/responsive/2.2.1/js/responsive.bootstrap.min.js"></script>
        <script>
            $(function() {
                $('#coltable').DataTable({
                    "pageLength": 10
                });
                $('#producttable').DataTable({
                    "pageLength": 10
                });

                $('#form_add_collection').validate({
                    errorElement: 'span', //default input error message container
                    errorClass: 'help-block help-block-error', // default input error message class
                    //focusInvalid: false, // do not focus the last invalid input
                    //ignore: "",  // validate all fields including form hidden input
                    messages: {
                        inputName: {
                            required: "Product name can not blank."
                        },
                        inputDesc: {
                            required: "Description can not blank."
                        },
                        inputBanner: {
                            required: "Banner can not blank."
                        },
                        inputImage: {
                            required: "Image can not blank."
                        }
                    },
                    rules: {
                        'inputName': {
                            required: true
                        },
                        'inputDesc': {
                            required: true
                        },
                        'inputBanner': {
                            required: true
                        },
                        'inputImage': {
                            required: true
                        }
                    },
                    errorPlacement: function(error, element) { // render error placement for each input type
                        element.after(error);
                    },
                    highlight: function(element) { // hightlight error inputs
                        $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
                    },
                    unhighlight: function(element) { // revert the change done by hightlight
                        $(element).closest('.form-group').removeClass('has-error'); // set error class to the control group
                    },
                    success: function(label) {
                        label.closest('.form-group').removeClass('has-error'); // set success class to the control group
                    },
                    submitHandler: function(form) {
                        form.submit();
                    }
                });
            });
        </script>
    </tiles:putAttribute>
</tiles:insertDefinition>
