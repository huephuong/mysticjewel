<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="../layout/taglib.jsp"%>
<tiles:insertDefinition name="common">
    <tiles:putAttribute name="title" value="Product Detail" />
    <tiles:putAttribute name="page-css">
        <link rel="stylesheet" href="<c:url value="/resource/admin/plugins/bootstrap-datepicker/bootstrap-datepicker.min.css" />">
        <link rel="stylesheet" href="<c:url value="/resource/admin/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css" />">
        <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.1/css/responsive.bootstrap.min.css">
    </tiles:putAttribute>
    <tiles:putAttribute name="body">
        <section class="content-header">
            <h1>
                Product's Details
                <small>Product ID: ${prod.productID}</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
                <li class="active">Product's Details</li>
            </ol>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-xs-6">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Product: ${prod.productName}</h3>
                        </div>
                        <div class="box-body">
                            <dl class="dl-horizontal">
                                <dt>Product ID:</dt>
                                <dd>${prod.productID}</dd>
                                <dt>Product Name:</dt>
                                <dd>${prod.productName}</dd>
                                <dt>Product Description:</dt>
                                <dd>${prod.productDesc}</dd>
                                <dt>Average Rating:</dt>
                                <dd>${prod.rating / 100 * 5}/5</dd>
                                <dt>Is Visible</dt>
                                <dd>
                                    <c:if test="${prod.isActive}">Yes</c:if>
                                    <c:if test="${!prod.isActive}">No</c:if>
                                    </dd>
                                    <dt>Category:</dt>
                                    <dd>
                                    ${prod.categoryID.categoryName}
                                </dd>
                                <dd>
                                    ${prod.categoryID.parentID.categoryName}
                                </dd>
                                <dt>Discount Value:</dt>
                                <dd>-${prod.discountValue}%</dd>
                                <dt>Discount Valid:</dt>
                                <dd><fmt:formatDate type = "both" dateStyle = "short" timeStyle = "short" value = "${prod.discountValidFrom}" /> - <fmt:formatDate type = "both" dateStyle = "short" timeStyle = "short" value = "${prod.discountValidTo}" /></dd>
                                <dt>Created Date:</dt>
                                <dd><fmt:formatDate type = "both" dateStyle = "short" timeStyle = "short" value = "${prod.dateCreated}" /></dd>
                                <dt>Updated Date:</dt>
                                <dd><fmt:formatDate type = "both" dateStyle = "short" timeStyle = "short" value = "${prod.dateUpdated}" /></dd>
                            </dl>
                            <table id="atttable" class="table table-striped table-bordered table-hover dt-responsive nowrap" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>Attribute Group</th>
                                        <th>Attribute Value</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <c:forEach items="${prod.attributeList}" var="att">
                                        <tr>
                                            <td>${att.attributeGroupID.attributeGroupName}</td>
                                            <td>${att.attributeValue}</td>
                                        </tr>
                                    </c:forEach>
                                </tbody>
                            </table>
                        </div>
                        <div class="box-footer">
                            <a class="btn btn-info" href="<c:url value="/admin-page/catalog/product/update?productid=${prod.productID}"/>">Update Details</a>
                        </div>
                    </div>
                </div>
                <div class="col-xs-6">
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">SKU's Details</h3>
                        </div>
                        <div class="box-body">
                            <table id="skutable" class="table table-striped table-bordered table-hover dt-responsive nowrap" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>SKU</th>
                                        <th>Size</th>
                                        <th>Base Price</th>
                                        <th>Unit Price</th>
                                        <th>Stock</th>
                                        <th>Order</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <c:forEach items="${prod.skuList}" var="sku">
                                        <tr>
                                            <td>${sku.skuID}</td>
                                            <td>${sku.sizeID.sizeValue}</td>
                                            <td>
                                                <fmt:setLocale value="en_US"/>
                                                <fmt:formatNumber value="${sku.basePrice}" type="currency"/>
                                            </td>
                                            <td>
                                                <fmt:setLocale value="en_US"/>
                                                <fmt:formatNumber value="${sku.unitPrice}" type="currency"/>
                                            </td>
                                            <td>
                                                <span <c:if test="${sku.unitsInStock lt 5}">class="text-red"</c:if>>
                                                    ${sku.unitsInStock}
                                                </span>
                                            </td>
                                            <td>${sku.unitsOnOrder}</td>
                                            <td>
                                                <c:if test="${empty sku.orderDetailList}">
                                                    <a class="btn btn-danger" href="<c:url value="/admin-page/catalog/sku/delete?skuid=${sku.skuID}"/>">Delete</a>
                                                </c:if>
                                            </td>
                                        </tr>
                                    </c:forEach>
                                </tbody>
                            </table>
                        </div>
                        <div class="box-footer">
                            <a class="btn btn-success" href="<c:url value="/admin-page/catalog/sku/add?productid=${prod.productID}"/>">Add New Unit</a>
                        </div>
                    </div>
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">Product's Images</h3>
                        </div>
                        <div class="box-body">
                            <table id="imgtable" class="table table-striped table-bordered table-hover dt-responsive nowrap" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Image</th>
                                        <th>Name</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <c:forEach items="${prod.productImageList}" var="img">
                                        <tr>
                                            <td>${img.imageID}</td>
                                            <td><img src="<c:url value="/resource/images/${img.imageLink}"/>" height="64" width="64"/></td>
                                            <td>${img.imageLink}</td>
                                            <td>
                                                <a class="btn btn-danger" href="<c:url value="/admin-page/catalog/image/delete?id=${img.imageID}"/>">Delete</a>
                                            </td>
                                        </tr>
                                    </c:forEach>
                                </tbody>
                            </table>
                        </div>
                        <div class="box-footer">
                            <a class="btn btn-success" href="<c:url value="/admin-page/catalog/sku/add?productid=${prod.productID}"/>">Add Image</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">Customer Review</h3>
                        </div>
                        <div class="box-body">
                            <table id="reviewtable" class="table table-striped table-bordered table-hover dt-responsive wrap" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>Customer</th>
                                        <th>Title</th>
                                        <th>Content</th>
                                        <th>Rating</th>
                                        <th>Date created</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <c:forEach items="${prod.reviewList}" var="review">
                                        <tr>
                                            <td>${review.customer.customerUserName}</td>
                                            <td>${review.title}</td>
                                            <td>${review.content}</td>
                                            <td>${review.rating / 100 * 5}/5</td>
                                            <td><fmt:formatDate type = "both" dateStyle = "short" timeStyle = "short" value = "${review.dateCreated}" /></td>
                                        </tr>
                                    </c:forEach>
                                </tbody>
                            </table>
                        </div>
                        <div class="box-footer">
                            <a class="btn btn-success" href="<c:url value="/admin-page/catalog/reviews"/>">Review List</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </tiles:putAttribute>
    <tiles:putAttribute name="page-js">
        <script src="<c:url value="/resource/admin/bower_components/datatables.net/js/jquery.dataTables.min.js" />"></script>
        <script src="<c:url value="/resource/admin/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js" />"></script>
        <script src="https://cdn.datatables.net/responsive/2.2.1/js/dataTables.responsive.min.js"></script>
        <script src="https://cdn.datatables.net/responsive/2.2.1/js/responsive.bootstrap.min.js"></script>
        <script>
            $(function() {
                $('#skutable').DataTable({
                    "pageLength": 10
                });
                $('#reviewtable').DataTable({
                    "pageLength": 10
                });
                $('#imgtable').DataTable({
                    "pageLength": 10
                });
            });
        </script>
    </tiles:putAttribute>
</tiles:insertDefinition>
