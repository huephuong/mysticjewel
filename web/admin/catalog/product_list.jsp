<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="../layout/taglib.jsp"%>
<tiles:insertDefinition name="common">
    <tiles:putAttribute name="title" value="Product Overview" />
    <tiles:putAttribute name="page-css">
        <link rel="stylesheet" href="<c:url value="/resource/admin/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css" />">
        <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.1/css/responsive.bootstrap.min.css">
    </tiles:putAttribute>
    <tiles:putAttribute name="body">
        <section class="content-header">
            <h1>
                Product Overview
                <small>View and edit existing products</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
                <li class="active">Product list</li>
            </ol>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box box-primary">
                        <div class="box-header">
                            <h3 class="box-title">
                                Product list &nbsp;
                                <a class="btn btn-success" href="<c:url value="/"/>admin-page/catalog/product/add">New Product</a>
                            </h3>
                        </div>
                        <div class="box-body">
                            <table id="example" class="table table-striped table-bordered dt-responsive nowrap table-hover" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Product Name</th>
                                        <th>Image</th>
                                        <th>Category</th>
                                        <th>Collection</th>
                                        <th>Visible</th>
                                        <th>Discount</th>
                                        <th>Discount Valid Date</th>
                                        <th>Date created</th>
                                        <th>Data updated</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <c:forEach items="${prodList}" var="prod">
                                        <tr>
                                            <td>${prod.productID}</td>
                                            <td>${prod.productName}</td>
                                            <td>
                                                <c:set var="firstImg" value="${prod.productImageList[0]}"/>
                                                <img src="<c:url value="/"/>resource/images/${firstImg.imageLink}" alt="Product IMG" width="64" height="64" />
                                            </td>
                                            <td>${prod.categoryID.categoryName}</td>
                                            <td>${prod.colID.colName}</td>
                                            <td>${prod.isActive ? "Yes" : "No"}</td>
                                            <td><c:if test="${prod.discountValue gt 0}">-${prod.discountValue}%</c:if></td>
                                            <td><fmt:formatDate type = "both" dateStyle = "short" timeStyle = "short" value = "${prod.discountValidFrom}" /> - <fmt:formatDate type = "both" dateStyle = "short" timeStyle = "short" value = "${prod.discountValidTo}" /></td>
                                            <td><fmt:formatDate type = "both" dateStyle = "short" timeStyle = "short" value = "${prod.dateCreated}" /></td>
                                            <td><fmt:formatDate type = "both" dateStyle = "short" timeStyle = "short" value = "${prod.dateUpdated}" /></td>
                                            <td>
                                                <div class="btn-group">
                                                    <button type="button" class="btn btn-info">Action</button>
                                                    <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
                                                        <span class="caret"></span>
                                                        <span class="sr-only">Toggle Dropdown</span>
                                                    </button>
                                                    <ul class="dropdown-menu" role="menu">
                                                        <li><a href="<c:url value="/admin-page/catalog/product-detail?id=${prod.productID}"/>">Details</a></li>
                                                        <li><a href="#">Update</a></li>
                                                        <li class="divider"></li>
                                                        <li><a href="#"><span class="text-red">Delete Single</span></a></li>
                                                    </ul>
                                                </div>
                                            </td>
                                        </tr>
                                    </c:forEach>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </tiles:putAttribute>
    <tiles:putAttribute name="page-js">
        <script src="<c:url value="/resource/admin/bower_components/datatables.net/js/jquery.dataTables.min.js" />"></script>
        <script src="<c:url value="/resource/admin/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js" />"></script>
        <script src="https://cdn.datatables.net/responsive/2.2.1/js/dataTables.responsive.min.js"></script>
        <script src="https://cdn.datatables.net/responsive/2.2.1/js/responsive.bootstrap.min.js"></script>
        <script>
            $(function() {
                $('#example').DataTable({
                    "pageLength": 25
                });
            });
        </script>
    </tiles:putAttribute>
</tiles:insertDefinition>
