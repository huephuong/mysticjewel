<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="../layout/taglib.jsp"%>
<tiles:insertDefinition name="common">
    <tiles:putAttribute name="title" value="Response Review" />
    <tiles:putAttribute name="page-css">
        <link rel="stylesheet" href="<c:url value="/resource/admin/plugins/iCheck/all.css" />">
    </tiles:putAttribute>
    <tiles:putAttribute name="body">
        <section class="content-header">
            <h1>
                Review
                <small>Response</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-plus-square"></i> Home</a></li>
                <li class="active">Response Review</li>
            </ol>
        </section>
        <section class="content">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Response</h3>
                </div>
                <form action="<c:url value="/admin-page/catalog/reviews/response" />" method="POST" class="form-horizontal">
                    <div class="box-body">
                        <div class="form-group">
                            <label for="inputCusID" class="col-sm-2 control-label">Customer ID: </label>
                            <div class="col-sm-6">
                                <input type="text" name="cusID" class="form-control" id="inputCusID"  placeholder="Customer ID" value="${responseReview.customer.customerID}" readonly="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputProID" class="col-sm-2 control-label">Product ID: </label>
                            <div class="col-sm-6">
                                <input type="text" name="proID" class="form-control" id="inputProID"  placeholder="Product ID" value="${responseReview.product.productID}" readonly="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputTitle" class="col-sm-2 control-label">Title: </label>
                            <div class="col-sm-6">
                                <input type="text" name="title" class="form-control" id="inputTitle"  placeholder="Title" value="${responseReview.title}" readonly="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputContent" class="col-sm-2 control-label">Content: </label>
                            <div class="col-sm-6">
                                <textarea class="form-control" rows="5" name="content" placeholder="Content" id="inputContent" readonly="">${responseReview.content}</textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputRating" class="col-sm-2 control-label">Rating: </label>
                            <div class="col-sm-6">
                                <input type="number" name="rating" class="form-control" id="inputRating"  placeholder="Rating" value="${responseReview.rating}" readonly="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputAdminResponse" class="col-sm-2 control-label">Admin response: </label>
                            <div class="col-sm-6">
                                <textarea class="form-control" rows="5" name="adminResponse" placeholder="Admin Response" id="inputAdminResponse">${responseReview.adminResponse}</textarea>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-info pull">Save</button>
                        <button type="reset" class="btn btn-default">Cancel</button>
                        <label>${msg}</label>
                    </div>
                </form>
            </div>
        </section>
    </tiles:putAttribute>
    <tiles:putAttribute name="page-js">
        <script src="<c:url value="/resource/admin/plugins/iCheck/icheck.min.js" />"></script>
        <script>
            $(function() {
                $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
                    checkboxClass: 'icheckbox_minimal-blue',
                    radioClass: 'iradio_minimal-blue'
                })
            });
        </script>
    </tiles:putAttribute>
</tiles:insertDefinition>
