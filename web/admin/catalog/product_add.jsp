<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="../layout/taglib.jsp"%>
<tiles:insertDefinition name="common">
    <tiles:putAttribute name="title" value="New Product" />
    <tiles:putAttribute name="page-css">
        <link rel="stylesheet" href="<c:url value="/resource/admin/plugins/bootstrap-datepicker/bootstrap-datepicker.min.css" />">
        <link rel="stylesheet" href="<c:url value="/resource/admin/bower_components/bootstrap-daterangepicker/daterangepicker.css"/>">
        <link rel="stylesheet" href="<c:url value="/resource/admin/bower_components/select2/dist/css/select2.min.css"/>">
    </tiles:putAttribute>
    <tiles:putAttribute name="body">
        <section class="content-header">
            <h1>
                New Product
                <small>New products are inactive by default.</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
                <li class="active">New Product</li>
            </ol>
        </section>
        <section class="content">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">New Product Form</h3>
                </div>
                <form action="<c:url value="/admin-page/catalog/product/add" />" method="POST" class="form-horizontal" id="form_add_product">
                    <div class="box-body">
                        <div class="form-group">
                            <label for="inputName" class="col-sm-2 control-label">Product Name</label>
                            <div class="col-sm-6">
                                <input type="text" name="inputName" class="form-control" id="inputName" value="${prod.productName}" placeholder="Product Name">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputDescription" class="col-sm-2 control-label">Product Description</label>
                            <div class="col-sm-6">
                                <textarea class="form-control" rows="5" name="inputDescription" placeholder="Product Description" id="inputDescription">${prod.productDescription}</textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputCategory" class="col-sm-2 control-label">Select Category</label>
                            <div class="col-sm-6">
                                <select class="form-control select2" name="inputCategory" style="width: 100%;">
                                    <c:forEach items="${catList}" var="catList">
                                        <option value="${catList.categoryID}">${catList.categoryName}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputCollection" class="col-sm-2 control-label">Select Collection</label>
                            <div class="col-sm-6">
                                <select class="form-control" name="inputCollection">
                                    <option selected value="None">None</option>
                                    <c:forEach items="${colList}" var="prodCol">
                                        <option value="${prodCol.colID}">${prodCol.colName}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputDiscountValue" class="col-sm-2 control-label">Discount Value</label>
                            <div class="col-sm-6">
                                <input type="number" min="5" max="75" name="inputDiscountValue" class="form-control" id="inputDiscountValue" value="${prod.discountValue}" placeholder="Discount Value">
                            </div>
                        </div>
                        <div class="form-group" style="display: none" id="f_g_inputValidDate">
                            <label class="col-sm-2 control-label">Discount Valid Date</label>
                            <div class="col-sm-6">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-clock-o"></i>
                                    </div>
                                    <input type="text" name="inputValidDate" class="form-control pull-right" id="reservationtime">
                                </div>
                            </div>
                        </div>
                        <!-- Attribute -->
                        <c:forEach items="${attList}" var="attGroup">
                            <div class="form-group">
                                <label class="col-sm-2 control-label">${attGroup.attributeGroupName}</label>
                                <div class="col-sm-6">
                                    <select name="inputAttribute" class="form-control">
                                        <option value="none">None</option>
                                        <c:forEach items="${attGroup.attributeList}" var="att">
                                            <option value="${att.attributeID}">${att.attributeValue}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                            </div>
                        </c:forEach>
                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-info pull" id="btnSave">Save</button>
                        <button type="reset" class="btn btn-default">Cancel</button>
                    </div>
                </form>
            </div>
        </section>
    </tiles:putAttribute>
    <tiles:putAttribute name="page-js">
        <script src="<c:url value="/resource/admin/bower_components/select2/dist/js/select2.full.min.js" />"></script>
        <script src="<c:url value="/resource/admin/plugins/input-mask/jquery.inputmask.js" />"></script>
        <script src="<c:url value="/resource/admin/plugins/input-mask/jquery.inputmask.date.extensions.js" />"></script>
        <script src="<c:url value="/plugins/input-mask/jquery.inputmask.extensions.js" />"></script>
        <script src="<c:url value="/resource/admin/bower_components/moment/min/moment.min.js" />"></script>
        <script src="<c:url value="/resource/admin/bower_components/bootstrap-daterangepicker/daterangepicker.js" />"></script>
        <script>
            $(function() {
                $('.select2').select2();

                $('#reservationtime').daterangepicker({
                    timePicker: true,
                    timePickerIncrement: 30,
                    locale: {format: 'DD/MM/YYYY h:mm A'}
                }, function(start, end) {
                    $('#reservationtime span').html(start.format('DD/MM/YYYY h:mm A') + ' - ' + end.format('DD/MM/YYYY h:mm A'));
                    $('#inputValidFrom').val(start.format('DD/MM/YYYY h:mm A'));
                    $('#inputValidTo').val(end.format('DD/MM/YYYY h:mm A'));
                });

                $('#inputDiscountValue').change(function() {
                    if (this.value >= 5 && this.value <= 75) {
                        $('#f_g_inputValidDate').show();
                    } else {
                        $('#f_g_inputValidDate').hide();
                    }
                });

                $('#form_add_product').validate({
                    errorElement: 'span', //default input error message container
                    errorClass: 'help-block help-block-error', // default input error message class
                    //focusInvalid: false, // do not focus the last invalid input
                    //ignore: "",  // validate all fields including form hidden input
                    messages: {
                        inputName: {
                            required: "Product name can not blank."
                        },
                        inputDescription: {
                            required: "Description can not blank."
                        }
                    },
                    rules: {
                        'inputName': {
                            required: true
                        },
                        'inputDescription': {
                            required: true
                        }
                    },
                    errorPlacement: function(error, element) { // render error placement for each input type
                        element.after(error);
                    },
                    highlight: function(element) { // hightlight error inputs
                        $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
                    },
                    unhighlight: function(element) { // revert the change done by hightlight
                        $(element).closest('.form-group').removeClass('has-error'); // set error class to the control group
                    },
                    success: function(label) {
                        label.closest('.form-group').removeClass('has-error'); // set success class to the control group
                    },
                    submitHandler: function(form) {
                        form.submit();
                    }
                });
            });
        </script>
    </tiles:putAttribute>
</tiles:insertDefinition>
