<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="../layout/taglib.jsp"%>
<tiles:insertDefinition name="common">
    <tiles:putAttribute name="title" value="Category Overview" />
    <tiles:putAttribute name="page-css">
        <link rel="stylesheet" href="<c:url value="/resource/admin/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css" />">
        <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.1/css/responsive.bootstrap.min.css">
    </tiles:putAttribute>
    <tiles:putAttribute name="body">
        <section class="content-header">
            <h1>
                Category Overview
                <small>All existing categories</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
                <li class="active">Category List</li>
            </ol>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-xs-6">
                    <div class="box box-primary">
                        <div class="box-header">
                            <h3 class="box-title">
                                Category List
                            </h3>
                        </div>
                        <div class="box-body">
                            <table id="cattable" class="table table-striped table-bordered table-hover dt-responsive nowrap" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Category</th>
                                        <th>Parent Category</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <c:forEach items="${catList}" var="category">
                                        <tr>
                                            <td>${category.categoryID}</td>
                                            <td>${category.categoryName}</td>
                                            <td>
                                                <c:if test="${empty category.parentID.categoryName}">
                                                    <span class="text-green">PARENT</span>
                                                </c:if>
                                                ${category.parentID.categoryName}</td>
                                            <td>
                                                <c:if test="${not empty category.parentID}">
                                                    <a class="btn btn-info" href="<c:url value="/admin-page/catalog/category/edit?id=${category.categoryID}"/>">Update</a>
                                                    <c:if test="${empty category.productList}">
                                                        <a class="btn btn-danger" href="<c:url value="/admin-page/catalog/category/delete?id=${category.categoryID}"/>">Delete</a>
                                                    </c:if>
                                                </c:if>
                                            </td>
                                        </tr>
                                    </c:forEach>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- Add Cat -->
                <div class="col-xs-6">
                    <div class="box box-info">
                        <div class="box-header">
                            <h3 class="box-title">
                                ${saveFn} Category
                            </h3>
                        </div>
                        <form action="<c:url value="/admin-page/catalog/category/edit" />" method="POST" class="form-horizontal" id="form_add_category">
                            <div class="box-body">
                                <div class="form-group">
                                    <input type="hidden" name="id" value="${currentCat.categoryID}">
                                    <label for="inputName" class="col-sm-4 control-label">Category Name</label>
                                    <div class="col-sm-8">
                                        <input type="text" name="inputName" class="form-control" id="inputName" placeholder="Category Name" value="${currentCat.categoryName}">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="inputParent" class="col-sm-4 control-label">Select Parent</label>
                                    <div class="col-sm-8">
                                        <select class="form-control" name="inputParent" id="inputParent">
                                            <c:forEach items="${mainList}" var="mainCat">
                                                <option value="${mainCat.categoryID}" <c:if test="${currentCat.parentID.categoryID eq mainCat.categoryID}">selected</c:if>>${mainCat.categoryName}</option>
                                            </c:forEach>
                                        </select>
                                    </div>
                                </div>

                            </div>
                            <div class="box-footer">
                                <button type="submit" class="btn btn-info pull" name="saveFn" value="${saveFn}" id="btnSave">Save</button>
                                <button type="reset" class="btn btn-default">Cancel</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </tiles:putAttribute>
    <tiles:putAttribute name="page-js">
        <script src="<c:url value="/resource/admin/bower_components/datatables.net/js/jquery.dataTables.min.js" />"></script>
        <script src="<c:url value="/resource/admin/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js" />"></script>
        <script src="https://cdn.datatables.net/responsive/2.2.1/js/dataTables.responsive.min.js"></script>
        <script src="https://cdn.datatables.net/responsive/2.2.1/js/responsive.bootstrap.min.js"></script>
        <script>
            $(function() {
                $('#cattable').DataTable({
                    "pageLength": 25
                });

                $('#form_add_category').validate({
                    errorElement: 'span', //default input error message container
                    errorClass: 'help-block help-block-error', // default input error message class
                    //focusInvalid: false, // do not focus the last invalid input
                    //ignore: "",  // validate all fields including form hidden input
                    messages: {
                        inputName: {
                            required: "Category name can not blank."
                        }
                    },
                    rules: {
                        'inputName': {
                            required: true
                        }
                    },
                    errorPlacement: function(error, element) { // render error placement for each input type
                        element.after(error);
                    },
                    highlight: function(element) { // hightlight error inputs
                        $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
                    },
                    unhighlight: function(element) { // revert the change done by hightlight
                        $(element).closest('.form-group').removeClass('has-error'); // set error class to the control group
                    },
                    success: function(label) {
                        label.closest('.form-group').removeClass('has-error'); // set success class to the control group
                    },
                    submitHandler: function(form) {
                        form.submit();
                    }
                });
            });
        </script>
    </tiles:putAttribute>
</tiles:insertDefinition>
