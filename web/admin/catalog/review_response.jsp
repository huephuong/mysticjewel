<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="../layout/taglib.jsp"%>
<tiles:insertDefinition name="common">
    <tiles:putAttribute name="title" value="Response Review" />
    <tiles:putAttribute name="page-css">
        <link rel="stylesheet" href="<c:url value="/resource/admin/plugins/iCheck/all.css" />">
    </tiles:putAttribute>
    <tiles:putAttribute name="body">
        <section class="content-header">
            <h1>
                Review
                <small>Response</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-plus-square"></i> Home</a></li>
                <li class="active">Response Review</li>
            </ol>
        </section>
        <section class="content">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Response</h3>
                </div>
                <form action="<c:url value="/admin-page/catalog/reviews/response" />" method="POST" class="form-horizontal" id="form_response_review">
                    <div class="box-body">
                        <div class="form-group">
                            <label for="inputCusID" class="col-sm-2 control-label">Customer ID: </label>
                            <div class="col-sm-6">
                                <input type="text" name="cusID" class="form-control" id="inputCusID"  placeholder="Customer ID" value="${responseReview.customer.customerID}" readonly="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputProID" class="col-sm-2 control-label">Product ID: </label>
                            <div class="col-sm-6">
                                <input type="text" name="proID" class="form-control" id="inputProID"  placeholder="Product ID" value="${responseReview.product.productID}" readonly="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputTitle" class="col-sm-2 control-label">Title: </label>
                            <div class="col-sm-6">
                                <input type="text" name="title" class="form-control" id="inputTitle"  placeholder="Title" value="${responseReview.title}" readonly="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputContent" class="col-sm-2 control-label">Content: </label>
                            <div class="col-sm-6">
                                <textarea class="form-control" rows="5" name="content" placeholder="Content" id="inputContent" readonly="">${responseReview.content}</textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputRating" class="col-sm-2 control-label">Rating: </label>
                            <div class="col-sm-6">
                                <input type="number" name="rating" class="form-control" id="inputRating"  placeholder="Rating" value="${responseReview.rating}" readonly="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="adminResponse" class="col-sm-2 control-label">Admin response: </label>
                            <div class="col-sm-6">
                                <textarea class="form-control" rows="5" name="adminResponse" placeholder="Admin Response" id="adminResponse">${responseReview.adminResponse}</textarea>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-info pull">Save</button>
                        <button type="reset" class="btn btn-default">Cancel</button>
                        <label>${msg}</label>
                    </div>
                </form>
            </div>
        </section>
    </tiles:putAttribute>
    <tiles:putAttribute name="page-js">
        <script src="<c:url value="/resource/admin/plugins/iCheck/icheck.min.js" />"></script>
        <script>
            $(function() {
                $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
                    checkboxClass: 'icheckbox_minimal-blue',
                    radioClass: 'iradio_minimal-blue'
                });

                $('#form_response_review').validate({
                    errorElement: 'span', //default input error message container
                    errorClass: 'help-block help-block-error', // default input error message class
                    //focusInvalid: false, // do not focus the last invalid input
                    //ignore: "",  // validate all fields including form hidden input
                    messages: {
                        adminResponse: {
                            required: "Response content can not blank."
                        }
                    },
                    rules: {
                        'adminResponse': {
                            required: true
                        }
                    },
                    errorPlacement: function(error, element) { // render error placement for each input type
                        element.after(error);
                    },
                    highlight: function(element) { // hightlight error inputs
                        $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
                    },
                    unhighlight: function(element) { // revert the change done by hightlight
                        $(element).closest('.form-group').removeClass('has-error'); // set error class to the control group
                    },
                    success: function(label) {
                        label.closest('.form-group').removeClass('has-error'); // set success class to the control group
                    },
                    submitHandler: function(form) {
                        form.submit();
                    }
                });
            });
        </script>
    </tiles:putAttribute>
</tiles:insertDefinition>
