<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="../layout/taglib.jsp"%>
<tiles:insertDefinition name="common">
    <tiles:putAttribute name="title" value="Size Overview" />
    <tiles:putAttribute name="page-css">
        <link rel="stylesheet" href="<c:url value="/resource/admin/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css" />">
        <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.1/css/responsive.bootstrap.min.css">
    </tiles:putAttribute>
    <tiles:putAttribute name="body">
        <section class="content-header">
            <h1>
                Size Overview
                <small>Size management</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
                <li class="active">Size List</li>
            </ol>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-xs-6">
                    <div class="box box-primary">
                        <div class="box-header">
                            <h3 class="box-title">
                                Size List
                            </h3>
                        </div>
                        <div class="box-body">
                            <table id="sizetable" class="table table-striped table-bordered table-hover dt-responsive nowrap" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Category</th>
                                        <th>Value</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <c:forEach items="${sizeList}" var="size">
                                        <tr>
                                            <td>${size.sizeID}</td>
                                            <td>${size.categoryID.categoryName}</td>
                                            <td>${size.sizeValue}</td>
                                            <td>
                                                <a class="btn btn-info" href="<c:url value="/admin-page/catalog/size/edit?id=${size.sizeID}"/>">Update</a>
                                                <c:if test="${empty size.skuList}">
                                                    <a class="btn btn-danger" href="<c:url value="/admin-page/catalog/size/delete?id=${size.sizeID}"/>">Delete</a>
                                                </c:if>
                                            </td>
                                        </tr>
                                    </c:forEach>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- Add Size -->
                <div class="col-xs-6">
                    <div class="box box-info">
                        <div class="box-header">
                            <h3 class="box-title">
                                ${saveFn} Size
                            </h3>
                        </div>
                        <form action="<c:url value="/admin-page/catalog/size/edit" />" method="POST" class="form-horizontal" id="form_add_size">
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="inputCategory" class="col-sm-4 control-label">Select Category</label>
                                    <div class="col-sm-8">
                                        <input type="hidden" name="id" value="${currentSize.sizeID}">
                                        <select class="form-control" name="inputCategory" id="inputParent">
                                            <c:forEach items="${catList}" var="mainCat">
                                                <option value="${mainCat.categoryID}" <c:if test="${mainCat.categoryID eq currentSize.categoryID.categoryID}">selected</c:if>>${mainCat.categoryName}</option>
                                            </c:forEach>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="inputValue" class="col-sm-4 control-label">Size Value</label>
                                    <div class="col-sm-8">
                                        <input type="number" name="inputValue" class="form-control" id="inputName" min="1" placeholder="Size value" value="${currentSize.sizeValue}">
                                    </div>
                                </div>

                            </div>
                            <div class="box-footer">
                                <button type="submit" class="btn btn-info pull" id="saveFn" name="saveFn" value="${saveFn}">Save</button>
                                <button type="reset" class="btn btn-default">Cancel</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </tiles:putAttribute>
    <tiles:putAttribute name="page-js">
        <script src="<c:url value="/resource/admin/bower_components/datatables.net/js/jquery.dataTables.min.js" />"></script>
        <script src="<c:url value="/resource/admin/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js" />"></script>
        <script src="https://cdn.datatables.net/responsive/2.2.1/js/dataTables.responsive.min.js"></script>
        <script src="https://cdn.datatables.net/responsive/2.2.1/js/responsive.bootstrap.min.js"></script>
        <script>
            $(function() {
                $('#sizetable').DataTable({
                    "pageLength": 10
                });
                
                $('#form_add_size').validate({
                    errorElement: 'span', //default input error message container
                    errorClass: 'help-block help-block-error', // default input error message class
                    //focusInvalid: false, // do not focus the last invalid input
                    //ignore: "",  // validate all fields including form hidden input
                    messages: {
                        inputValue: {
                            required: "Size value can not blank."
                        }
                    },
                    rules: {
                        'inputValue': {
                            required: true
                        }
                    },
                    errorPlacement: function(error, element) { // render error placement for each input type
                        element.after(error);
                    },
                    highlight: function(element) { // hightlight error inputs
                        $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
                    },
                    unhighlight: function(element) { // revert the change done by hightlight
                        $(element).closest('.form-group').removeClass('has-error'); // set error class to the control group
                    },
                    success: function(label) {
                        label.closest('.form-group').removeClass('has-error'); // set success class to the control group
                    },
                    submitHandler: function(form) {
                        form.submit();
                    }
                });
            });
        </script>
    </tiles:putAttribute>
</tiles:insertDefinition>

