<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="../layout/taglib.jsp"%>
<tiles:insertDefinition name="common">
    <tiles:putAttribute name="title" value="Update Product" />
    <tiles:putAttribute name="page-css">
        <link rel="stylesheet" href="<c:url value="/resource/admin/plugins/bootstrap-datepicker/bootstrap-datepicker.min.css" />">
        <link rel="stylesheet" href="<c:url value="/resource/admin/bower_components/bootstrap-daterangepicker/daterangepicker.css"/>">
        <link rel="stylesheet" href="<c:url value="/resource/admin/bower_components/select2/dist/css/select2.min.css"/>">
    </tiles:putAttribute>
    <tiles:putAttribute name="body">
        <section class="content-header">
            <h1>
                Update Product
                <small></small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
                <li class="active">Update Product</li>
            </ol>
        </section>
        <section class="content">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Update Product - ID: ${prod.productID}</h3>
                </div>
                <form action="<c:url value="/admin-page/catalog/product/update" />" method="POST" class="form-horizontal">
                    <div class="box-body">
                        <div class="form-group">
                            <input type="hidden" name="id" value="${prod.productID}">
                            <label for="inputName" class="col-sm-2 control-label">Product Name</label>
                            <div class="col-sm-6">
                                <input type="text" name="inputName" class="form-control" id="inputName" value="${prod.productName}" placeholder="Product Name">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputDescription" class="col-sm-2 control-label">Product Description</label>
                            <div class="col-sm-6">
                                <textarea class="form-control" rows="5" name="inputDescription" placeholder="Product Description" id="inputDescription">${prod.productDesc}</textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputCategory" class="col-sm-2 control-label">Select Category</label>
                            <div class="col-sm-6">
                                <select class="form-control select2" name="inputCategory" style="width: 100%;">
                                    <c:forEach items="${catList}" var="catList">
                                        <option value="${catList.categoryID}" <c:if test="${catList.categoryID eq prod.categoryID.categoryID}">selected</c:if>>${catList.categoryName}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputCollection" class="col-sm-2 control-label">Select Collection</label>
                            <div class="col-sm-6">
                                <select class="form-control" name="inputCollection">
                                    <option value="None" <c:if test="${prod.colID.colID eq null}">selected</c:if>>None</option>
                                    <c:forEach items="${colList}" var="prodCol">
                                        <option value="${prodCol.colID}" <c:if test="${prodCol.colID eq prod.colID.colID}">selected</c:if>>${prodCol.colName}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputDiscountValue" class="col-sm-2 control-label">Discount Value</label>
                            <div class="col-sm-6">
                                <input type="text" name="inputDiscountValue" class="form-control" id="inputDiscountValue" value="${prod.discountValue}" placeholder="Discount Value">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Discount Valid Date</label>
                            <div class="col-sm-6">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-clock-o"></i>
                                    </div>
                                    <input type="text" name="inputValidDate" class="form-control pull-right" id="reservationtime">
                                </div>
                            </div>
                        </div>
                        <!-- Attribute -->
                        <c:forEach items="${attList}" var="attGroup">
                            <div class="form-group">
                                <label class="col-sm-2 control-label">${attGroup.attributeGroupName}</label>
                                <div class="col-sm-6">
                                    <select name="inputAttribute" class="form-control">
                                        <option value="none" selected>None</option>
                                        <c:forEach items="${attGroup.attributeList}" var="att">
                                            <option value="${att.attributeID}" 
                                                    <c:forEach items="${prod.attributeList}" var="prodAtt">
                                                    <c:if test="${(prodAtt.attributeGroupID.attributeGroupID eq attGroup.attributeGroupID) and (att.attributeID eq prodAtt.attributeID)}">selected</c:if>
                                                    </c:forEach>>
                                                ${att.attributeValue}
                                            </option>
                                        </c:forEach>
                                    </select>
                                </div>
                            </div>
                        </c:forEach>
                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-info pull" id="btnSave">Save</button>
                        <button type="reset" class="btn btn-default">Cancel</button>
                    </div>
                </form>
            </div>
        </section>
    </tiles:putAttribute>
    <tiles:putAttribute name="page-js">
        <script src="<c:url value="/resource/admin/bower_components/select2/dist/js/select2.full.min.js" />"></script>
        <script src="<c:url value="/resource/admin/plugins/input-mask/jquery.inputmask.js" />"></script>
        <script src="<c:url value="/resource/admin/plugins/input-mask/jquery.inputmask.date.extensions.js" />"></script>
        <script src="<c:url value="/plugins/input-mask/jquery.inputmask.extensions.js" />"></script>
        <script src="<c:url value="/resource/admin/bower_components/moment/min/moment.min.js" />"></script>
        <script src="<c:url value="/resource/admin/bower_components/bootstrap-daterangepicker/daterangepicker.js" />"></script>
        <script>
            $(function() {
                var start = <fmt:formatDate pattern = "yyyy-MM-dd" value = "${prod.discountValidFrom}" />
                var end = <fmt:formatDate pattern = "yyyy-MM-dd" value = "${prod.discountValidTo}" />
                $('.select2').select2();
                $('#reservationtime').daterangepicker(
                        {
                            timePicker: true,
                            timePickerIncrement: 30,
                            startDate: start,
                            endDate: end,
                            locale: {format: 'DD/MM/YYYY h:mm A'}
                        },
                function(start, end) {
                    $('#reservationtime span').html(start.format('DD/MM/YYYY h:mm A') + ' - ' + end.format('DD/MM/YYYY h:mm A'));
                    $('#inputValidFrom').val(start.format('DD/MM/YYYY h:mm A'));
                    $('#inputValidTo').val(end.format('DD/MM/YYYY h:mm A'));
                }
                );
            });
        </script>
    </tiles:putAttribute>
</tiles:insertDefinition>
