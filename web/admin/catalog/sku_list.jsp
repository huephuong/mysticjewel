<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="../layout/taglib.jsp"%>
<tiles:insertDefinition name="common">
    <tiles:putAttribute name="title" value="Inventory" />
    <tiles:putAttribute name="page-css">
        <link rel="stylesheet" href="<c:url value="/resource/admin/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css" />">
        <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.1/css/responsive.bootstrap.min.css">
    </tiles:putAttribute>
    <tiles:putAttribute name="body">
        <section class="content-header">
            <h1>
                Inventory Overview
                <small>Price and Stock</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
                <li class="active">Inventory</li>
            </ol>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box box-primary">
                        <div class="box-header">
                            <h3 class="box-title">
                                Inventory &nbsp;
                                <a class="btn btn-success" href="<c:url value="/"/>admin-page/catalog/sku/add">New Unit</a></h3>
                        </div>
                        <div class="box-body">
                            <table id="example" class="table table-striped table-bordered table-hover dt-responsive nowrap" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>SKU</th>
                                        <th>Product Name</th>
                                        <th>Size</th>
                                        <th>Category</th>
                                        <th>Base Price</th>
                                        <th>Unit Price</th>
                                        <th>Stock</th>
                                        <th>Order</th>
                                        <th>Date created</th>
                                        <th>Data updated</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <c:forEach items="${skuList}" var="sku">
                                        <tr>
                                            <td>${sku.skuID}</td>
                                            <td>${sku.productID.productName}</td>
                                            <td>${sku.sizeID.sizeValue}</td>
                                            <td>${sku.productID.categoryID.categoryName}</td>
                                            <td>
                                                <fmt:setLocale value="en_US"/>
                                                <fmt:formatNumber value="${sku.basePrice}" type="currency"/>
                                            </td>
                                            <td>
                                                <fmt:setLocale value="en_US"/>
                                                <fmt:formatNumber value="${sku.unitPrice}" type="currency"/>
                                            </td>
                                            <td>
                                                <span <c:if test="${sku.unitsInStock lt 5}">class="text-red"</c:if>>
                                                    ${sku.unitsInStock}
                                                </span>
                                            </td>
                                            <td>${sku.unitsOnOrder}</td>
                                            <td><fmt:formatDate type = "both" dateStyle = "short" timeStyle = "short" value = "${sku.dateCreated}" /></td>
                                            <td><fmt:formatDate type = "both" dateStyle = "short" timeStyle = "short" value = "${sku.dateUpdated}" /></td>
                                            <td>
                                                <div class="btn-group">
                                                    <button type="button" class="btn btn-info">Action</button>
                                                    <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
                                                        <span class="caret"></span>
                                                        <span class="sr-only">Toggle Dropdown</span>
                                                    </button>
                                                    <ul class="dropdown-menu" role="menu">
                                                        <li><a href="#">Details</a></li>
                                                        <li><a href="#">Update</a></li>
                                                            <c:if test="${empty sku.orderDetailList}">
                                                            <li class="divider"></li>
                                                            <li><a href="#"><span class="text-red">Delete</span></a></li>
                                                            </c:if>
                                                    </ul>
                                                </div>
                                            </td>
                                        </tr>
                                    </c:forEach>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>             
            </div>
        </section>
    </tiles:putAttribute>
    <tiles:putAttribute name="page-js">
        <script src="<c:url value="/resource/admin/bower_components/datatables.net/js/jquery.dataTables.min.js" />"></script>
        <script src="<c:url value="/resource/admin/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js" />"></script>
        <script src="https://cdn.datatables.net/responsive/2.2.1/js/dataTables.responsive.min.js"></script>
        <script src="https://cdn.datatables.net/responsive/2.2.1/js/responsive.bootstrap.min.js"></script>
        <script>
            $(function() {
                $('#example').DataTable({
                    "pageLength": 25
                });
                $('#sizetable').DataTable({
                    "pageLength": 10
                });
            });
        </script>
    </tiles:putAttribute>
</tiles:insertDefinition>

