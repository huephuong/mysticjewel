<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="../layout/taglib.jsp"%>
<tiles:insertDefinition name="common">
    <tiles:putAttribute name="title" value="SKU and Images" />
    <tiles:putAttribute name="page-css">
        <link rel="stylesheet" href="<c:url value="/resource/admin/plugins/bootstrap-datepicker/bootstrap-datepicker.min.css" />">
    </tiles:putAttribute>
    <tiles:putAttribute name="body">
        <section class="content-header">
            <h1>
                SKU and Images
                <small>Add stock keeping unit and images for product</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
                <li class="active">Sku and Images</li>
            </ol>
        </section>
        <section class="content">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Product ID: ${prod.productID} - Name: ${prod.productName}</h3>
                </div>
                <form action="<c:url value="/admin-page/catalog/product-detail/add" />" method="POST" class="form-horizontal">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-xs-8">
                                <table class="table table-bordered" id="tblSku">
                                    <thead>
                                        <tr>
                                            <th>SKU Code</th>
                                            <th>Size</th>
                                            <th>Base Price</th>
                                            <th>Unit Price</th>
                                            <th>Units in Stock</th>
                                            <th><button type="button" class="btn btn-primary" id="btnAddRow"><i class="fa fa-plus"></i> Add</button></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td><input class="form-control" type="text" name="inputCode" value="${generatedSKU}"/></td>
                                            <td>
                                                <select class="form-control" name="inputSize">
                                                    <c:forEach items="${prod.categoryID.parentID.skuSizeList}" var="size">
                                                        <option value="${size.sizeID}">${size.sizeValue}</option>
                                                    </c:forEach>
                                                </select>
                                            </td>
                                            <td><input class="form-control" type="text" name="inputBasePrice" /></td>
                                            <td><input class="form-control" type="text" name="inputUnitPrice" /></td>
                                            <td><input class="form-control" type="text" name="inputUnitsInStock" /></td>
                                            <td><button type="button" class="btnDeleteRow btn btn-danger"><i class="fa fa-minus"></i> Delete</button></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-xs-4">
                                <table class="table table-bordered" id="tblImage">
                                    <thead>
                                        <tr>
                                            <th>Product Image</th>
                                            <th><button type="button" class="btn btn-primary" id="btnAddImg"><i class="fa fa-plus"></i> Add</button></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td><input type="file" class="form-control" id="inputImage" name="inputImage"></td>
                                            <td><button type="button" class="btnDeleteImg btn btn-danger"><i class="fa fa-minus"></i> Delete</button></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="box-footer">
                            <input type="hidden" name="productID" value="${prod.productID}"/>
                            <button type="submit" class="btn btn-info pull-right">Save All</button>
                            <button type="reset" class="btn btn-default">Cancel</button>
                        </div>
                    </div>
                </form>
            </div>                        
        </section>
    </tiles:putAttribute>
    <tiles:putAttribute name="page-js">
        <script src="<c:url value="/resource/admin/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js" />"></script>
        <script>
            var maxRow = 10;
            var counter1 = 0;
            var counter2 = 0;
            
            $(document).ready(function() {
                $('#btnAddRow').on('click', function() {
                    if (counter1 >= maxRow) {
                        return;
                    }
                    var newRow = $('<tr>');
                    var cols = '';

                    cols += '<td><input type="text" class="form-control" name="inputCode" value="${generatedSKU}"/></td>';
                    cols += '<td><select class="form-control" name="inputSize"><c:forEach items="${prod.categoryID.parentID.skuSizeList}" var="size"><option value="${size.sizeID}">${size.sizeValue}</option></c:forEach></select></td>';
                    cols += '<td><input type="text" class="form-control" name="inputBasePrice"/></td>';
                    cols += '<td><input type="text" class="form-control" name="inputUnitPrice"/></td>';
                    cols += '<td><input type="text" class="form-control" name="inputUnitsInStock"/></td>';
                    cols += '<td><button type="button" class="btnDeleteRow btn btn-danger"><i class="fa fa-minus"></i> Delete</button></td>';
                    newRow.append(cols);
                    counter1 += 1;
                    $('#tblSku > tbody:last-child').append(newRow);
                });

                $('#tblSku').on('click', '.btnDeleteRow', function() {
                    $(this).closest('tr').remove();
                    counter1 -= 1;
                });
                
                $('#btnAddImg').on('click', function() {
                    if (counter2 >= maxRow) {
                        return;
                    }
                    var newRow = $('<tr>');
                    var cols = '';

                    cols += '<td><input type="file" class="form-control" id="inputImage" name="inputImage"></td>';
                    cols += '<td><button type="button" class="btnDeleteImg btn btn-danger"><i class="fa fa-minus"></i> Delete</button></td>';
                    newRow.append(cols);
                    counter2 += 1;
                    $('#tblImage > tbody:last-child').append(newRow);
                });

                $('#tblImage').on('click', '.btnDeleteImg', function() {
                    $(this).closest('tr').remove();
                    counter2 -= 1;
                });
            });
            </script>
    </tiles:putAttribute>
</tiles:insertDefinition>