<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="../layout/taglib.jsp"%>
<tiles:insertDefinition name="common">
    <tiles:putAttribute name="title" value="Add/edit customer" />
    <tiles:putAttribute name="page-css">
        <link rel="stylesheet" href="<c:url value="/resource/admin/plugins/bootstrap-datepicker/bootstrap-datepicker3.css" />">
    </tiles:putAttribute>
    <tiles:putAttribute name="body">
        <section class="content-header">
            <h1>
                Add/edit customer
                <small>Add/edit customer</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-user-plus"></i> Home</a></li>
                <li class="active">Add/edit customer</li>
            </ol>
        </section>
        <section class="content">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Add</h3>
                </div>
                <form action="<c:url value="/admin/customer/add" />" method="POST" class="form-horizontal">
                    <div class="box-body">
                        <div class="form-group">
                            <label for="inputUsername" class="col-sm-2 control-label">Username</label>
                            <div class="col-sm-10">
                                <input type="text" name="username" class="form-control" id="inputUsername" value="${customerById.customerUserName}" placeholder="Username">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPassword" class="col-sm-2 control-label">Password</label>
                            <div class="col-sm-10">
                                <input type="password" name="password" class="form-control" id="inputPassword" value="" placeholder="Password">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputAvatar" class="col-sm-2 control-label">Avatar</label>
                            <div class="col-sm-10">
                                <div class="input-group">
                                    <span class="input-group-btn">
                                        <a id="lfm" data-input="thumbnail" data-preview="holder" class="btn btn-default">
                                            <i class="fa fa-picture-o"></i> Upload
                                        </a>
                                    </span>
                                    <input type="text" name="avatar" value="" id="thumbnail" class="form-control" placeholder="Avatar">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputFirstName" class="col-sm-2 control-label">First name</label>
                            <div class="col-sm-10">
                                <input type="text" name="firstName" class="form-control" id="inputFirstName" value="" placeholder="First name">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputLastName" class="col-sm-2 control-label">Last name</label>
                            <div class="col-sm-10">
                                <input type="text" name="lastName" class="form-control" id="inputLastName" value="" placeholder="Last name">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputEmail" class="col-sm-2 control-label">Email</label>
                            <div class="col-sm-10">
                                <input type="text" name="email" class="form-control" id="inputEmail" value="" placeholder="Email">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputGame" class="col-sm-2 control-label">Gender</label>
                            <div class="col-sm-10">
                                <select name="gender" class="form-control">
                                    <option value="Male">Male</option>
                                    <option value="Female">Female</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPhone1" class="col-sm-2 control-label">Phone 1</label>
                            <div class="col-sm-10">
                                <input type="text" name="phone1" class="form-control" id="inputPhone1" value="" placeholder="Phone 1">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPhone2" class="col-sm-2 control-label">Phone 2</label>
                            <div class="col-sm-10">
                                <input type="text" name="phone2" class="form-control" id="inputPhone2" value="" placeholder="Phone 2">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputDateOfBirth" class="col-sm-2 control-label">Date of birth</label>
                            <div class="col-sm-10">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" name="dateOfBirth" class="form-control pull-right add-on" id="datepicker">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-info pull">Save</button>
                        <button type="reset" class="btn btn-default">Cancel</button>
                    </div>
                </form>
            </div>
        </section>
    </tiles:putAttribute>
    <tiles:putAttribute name="page-js">
        <script src="<c:url value="/resource/admin/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js" />"></script>
        <script>
            $(function () {
                $('#datepicker').datepicker({
                    autoclose: true,
                    format: 'dd/mm/yyyy'
                });
            });
        </script>
    </tiles:putAttribute>
</tiles:insertDefinition>
