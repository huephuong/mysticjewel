<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="../layout/taglib.jsp"%>
<tiles:insertDefinition name="common">
    <tiles:putAttribute name="title" value="Customer Overview" />
    <tiles:putAttribute name="page-css">
        <link rel="stylesheet" href="<c:url value="/resource/admin/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css" />">
        <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.1/css/responsive.bootstrap.min.css">
    </tiles:putAttribute>
    <tiles:putAttribute name="body">
        <section class="content-header">
            <h1>
                Customer Overview
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
                <li class="active">Customer Overview</li>
            </ol>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Customer Overview</h3>
                        </div>
                        <div class="box-body">
                            <table id="example" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Username</th>
                                        <th>First name</th>
                                        <th>Last name</th>
                                        <th>Email</th>
                                        <th>Gender</th>
                                        <th>Phone1</th>
                                        <th>Phone2</th>
                                        <th>Date of birth</th>
                                        <th>Data created</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <c:forEach items="${customers}" var="customer">
                                        <tr>
                                            <td>${customer.customerID}</td>
                                            <td>${customer.customerUserName}</td>
                                            <td>${customer.firstName}</td>
                                            <td>${customer.lastName}</td>
                                            <td>${customer.email}</td>
                                            <td>${customer.gender}</td>
                                            <td>${customer.phone1}</td>
                                            <td>${customer.phone2}</td>
                                            <td><fmt:formatDate value = "${customer.dateOfBirth}" pattern = "dd/MM/YYYY" /></td>
                                            <td><fmt:formatDate type = "both" dateStyle = "short" timeStyle = "short" value = "${customer.dateCreated}" /></td>
                                            <td>
                                                <c:choose>
                                                    <c:when test="${customer.isActive == true}">
                                                        <a class="btn btn-danger" href="<c:url value="/admin-page/change-status-customer?id=${customer.customerID}&status=0"/>">Ban</a>
                                                    </c:when>    
                                                    <c:otherwise>
                                                        <a class="btn btn-info" href="<c:url value="/admin-page/change-status-customer?id=${customer.customerID}&status=1"/>">Unban</a>
                                                    </c:otherwise>
                                                </c:choose>
                                            </td>
                                        </tr>
                                    </c:forEach>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </tiles:putAttribute>
    <tiles:putAttribute name="page-js">
        <script src="<c:url value="/resource/admin/bower_components/datatables.net/js/jquery.dataTables.min.js" />"></script>
        <script src="<c:url value="/resource/admin/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js" />"></script>
        <script src="https://cdn.datatables.net/responsive/2.2.1/js/dataTables.responsive.min.js"></script>
        <script src="https://cdn.datatables.net/responsive/2.2.1/js/responsive.bootstrap.min.js"></script>
        <script>
            $(function() {
                $('#example').DataTable({
                    "pageLength": 25
                });
            });
        </script>
    </tiles:putAttribute>
</tiles:insertDefinition>