<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="../layout/taglib.jsp"%>
<tiles:insertDefinition name="common">
    <tiles:putAttribute name="title" value="${empty adminAccount ? 'Add' : 'Edit'} admin" />
    <tiles:putAttribute name="page-css" value="" />
    <tiles:putAttribute name="body">
        <section class="content-header">
            <h1>
                ${empty adminAccount ? 'Add' : 'Edit'} admin
                <small>${empty adminAccount ? 'Add' : 'Edit'} admin</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-user-plus"></i> Home</a></li>
                <li class="active">${empty adminAccount ? 'Add' : 'Edit'} admin</li>
            </ol>
        </section>
        <section class="content">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">${empty adminAccount ? 'Add' : 'Edit'}</h3>
                </div>
                <form action="<c:url value="/admin-page/admin-account/form" />" method="POST" class="form-horizontal" id="form_add_admin">
                    <input type="hidden" name="adminID" value="${adminID}" />
                    <div class="box-body">
                        <div class="form-group">
                            <label for="inputUsername" class="col-sm-2 control-label">Username</label>
                            <div class="col-sm-10">
                                <input type="text" name="username" class="form-control" id="inputUsername" value="${adminAccount.adminUserName}" placeholder="Username">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPassword" class="col-sm-2 control-label">Password</label>
                            <div class="col-sm-10">
                                <input type="password" name="password" id="password" class="form-control" id="inputPassword" value="" placeholder="Password">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPasswordConfirmation" class="col-sm-2 control-label">Password Confirmation</label>
                            <div class="col-sm-10">
                                <input type="password" name="password_confirmation" id="password_confirmation" class="form-control" id="inputPasswordConfirmation" value="" placeholder="Password Confirmation">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputRole" class="col-sm-2 control-label">Role</label>
                            <div class="col-sm-10">
                                <select name="role" class="form-control">
                                    <option value="SuperAdmin" ${adminAccount.adminRole eq 'SuperAdmin' ? 'selected' : ''}>SuperAdmin</option>
                                    <option value="Admin" ${adminAccount.adminRole eq 'Admin' ? 'selected' : ''}>Admin</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-info pull">Save</button>
                        <button type="reset" class="btn btn-default">Cancel</button>
                    </div>
                </form>
            </div>
        </section>
    </tiles:putAttribute>
    <tiles:putAttribute name="page-js">
        <script>
            $(function() {
                $('#form_add_admin').validate({
                    errorElement: 'span', //default input error message container
                    errorClass: 'help-block help-block-error', // default input error message class
                    //focusInvalid: false, // do not focus the last invalid input
                    //ignore: "",  // validate all fields including form hidden input
                    messages: {
                        username: {
                            required: "Username can not blank.",
                            maxlength: jQuery.validator.format("Bạn chỉ được nhập nhiều nhất {0} ký tự."),
                            minlength: jQuery.validator.format("Bạn cần nhập ít nhất {0} ký tự.")
                        },
                        password: {
                            required: "Password can not blank.",
                            maxlength: jQuery.validator.format("Bạn chỉ được nhập nhiều nhất {0} ký tự."),
                            minlength: jQuery.validator.format("Bạn cần nhập ít nhất {0} ký tự.")
                        },
                        password_confirmation: {
                            equalTo: "Password does not match."
                        }
                    },
                    rules: {
                        'username': {
                            required: true,
                            minlength: 6,
                            maxlength: 20
                        },
                        'password': {
                            required: true,
                            minlength: 6,
                            maxlength: 32
                        },
                        'password_confirmation': {
                            equalTo: '#password'
                        }
                    },
                    errorPlacement: function(error, element) { // render error placement for each input type
                        element.after(error);
                    },
                    highlight: function(element) { // hightlight error inputs
                        $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
                    },
                    unhighlight: function(element) { // revert the change done by hightlight
                        $(element).closest('.form-group').removeClass('has-error'); // set error class to the control group
                    },
                    success: function(label) {
                        label.closest('.form-group').removeClass('has-error'); // set success class to the control group
                    },
                    submitHandler: function(form) {
                        form.submit();
                    }
                });
            });
        </script>
    </tiles:putAttribute>
</tiles:insertDefinition>
