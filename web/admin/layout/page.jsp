<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="../layout/taglib.jsp"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><tiles:getAsString name="title" /> | Admin LTE</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.7 -->
        <link rel="stylesheet" href="<c:url value="/resource/admin/bower_components/bootstrap/dist/css/bootstrap.min.css" />">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="<c:url value="/resource/admin/bower_components/font-awesome/css/font-awesome.min.css" />">
        <!-- Ionicons -->
        <link rel="stylesheet" href="<c:url value="/resource/admin/bower_components/Ionicons/css/ionicons.min.css" />">
        <!-- Theme style -->
        <link rel="stylesheet" href="<c:url value="/resource/admin/dist/css/AdminLTE.min.css" />">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- Google Font -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
        <tilesx:useAttribute id="stylesheets" name="stylesheets" classname="java.util.List" />
        <c:forEach var="css" items="${stylesheets}">
            <link rel="stylesheet" type="text/css" href="<c:url value="${css}"/>">
        </c:forEach>
    </head>
    <body class="hold-transition login-page">
        <tiles:insertAttribute name="body" />
        <!-- jQuery 3 -->
        <script src="<c:url value="/resource/admin/bower_components/jquery/dist/jquery.min.js" />"></script>
        <!-- Bootstrap 3.3.7 -->
        <script src="<c:url value="/resource/admin/bower_components/bootstrap/dist/js/bootstrap.min.js" />"></script>
        <!-- jQuery Validation 1.17.0 -->
        <script src="<c:url value="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js" />"></script>
        <script src="<c:url value="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/additional-methods.min.js" />"></script>
        <tilesx:useAttribute id="javascripts" name="javascripts" classname="java.util.List" />
        <c:forEach var="script" items="${javascripts}">
            <script src="<c:url value="${script}"/>"></script>
        </c:forEach>
        <script>
            $(function() {
                $('input').iCheck({
                    checkboxClass: 'icheckbox_square-blue',
                    radioClass: 'iradio_square-blue',
                    increaseArea: '20%' // optional
                });

                $('#f_admin_login').validate({
                    errorElement: 'span', //default input error message container
                    errorClass: 'help-block help-block-error', // default input error message class
                    //focusInvalid: false, // do not focus the last invalid input
                    //ignore: "",  // validate all fields including form hidden input
                    messages: {
                        userName: {
                            required: "Username can not blank.",
                            maxlength: jQuery.validator.format("Bạn chỉ được nhập nhiều nhất {0} ký tự."),
                            minlength: jQuery.validator.format("Bạn cần nhập ít nhất {0} ký tự.")
                        },
                        password: {
                            required: "Password can not blank.",
                            maxlength: jQuery.validator.format("Bạn chỉ được nhập nhiều nhất {0} ký tự."),
                            minlength: jQuery.validator.format("Bạn cần nhập ít nhất {0} ký tự.")
                        }
                    },
                    rules: {
                        'userName': {
                            required: true,
                            minlength: 5,
                            maxlength: 30
                        },
                        'password': {
                            required: true,
                            minlength: 6,
                            maxlength: 14
                        }
                    },
                    errorPlacement: function(error, element) { // render error placement for each input type
                        element.after(error);
                    },
                    highlight: function(element) { // hightlight error inputs
                        $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
                    },
                    unhighlight: function(element) { // revert the change done by hightlight
                        $(element).closest('.form-group').removeClass('has-error'); // set error class to the control group
                    },
                    success: function(label) {
                        label.closest('.form-group').removeClass('has-error'); // set success class to the control group
                    },
                    submitHandler: function(form) {
                        form.submit();
                    }
                });
            });
        </script>
    </body>
</html>
