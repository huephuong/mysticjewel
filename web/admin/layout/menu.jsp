<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="/admin/layout/taglib.jsp"%>
<ul class="sidebar-menu" data-widget="tree">
    <li class="header">NAVIGATION</li>
    <li>
        <a href="<c:url value="/admin-page" />">
            <i class="fa fa-dashboard"></i>
            <span>Dashboard</span>
        </a>
    </li>
    <!-- CATALOG: Category, Product, Attribute, Review -->
    <li class="treeview">
        <a href="#">
            <i class="fa fa-tags"></i>
            <span>Catalog</span>
            <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-circle-o"></i> Category
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li>
                        <a href="<c:url value="/admin-page/catalog/category" />">
                            <i class="fa fa-circle-o"></i> Category Overview
                        </a>
                    </li>
                    <li>
                        <a href="<c:url value="/admin-page/catalog/size" />">
                            <i class="fa fa-circle-o"></i> Size Overview
                        </a>
                    </li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-circle-o"></i> Product
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li>
                        <a href="<c:url value="/admin-page/catalog/product" />">
                            <i class="fa fa-circle-o"></i> Overview
                        </a>
                    </li>
                    <li>
                        <a href="<c:url value="/admin-page/catalog/product/inventory" />">
                            <i class="fa fa-circle-o"></i> Inventory
                        </a>
                    </li>
                    <li>
                        <a href="<c:url value="/admin-page/catalog/product/add" />">
                            <i class="fa fa-circle-o"></i> New product
                        </a>
                    </li>
                    <li>
                        <a href="<c:url value="/admin-page/catalog/product/discount" />">
                            <i class="fa fa-circle-o"></i> Discount
                        </a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="<c:url value="/admin-page/catalog/collection" />">
                    <i class="fa fa-circle-o"></i> Collection
                </a>
            </li>
            <li>
                <a href="<c:url value="/admin-page/catalog/attributes" />">
                    <i class="fa fa-circle-o"></i> Attributes
                </a>
            </li>
        </ul>
    </li>
    
    <!-- SALES -->
    <li class="treeview">
        <a href="#">
            <i class="fa fa-shopping-cart"></i>
            <span>Sales</span>
            <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
            <li>
                <a href="<c:url value="/admin-page/sales/order" />">
                    <i class="fa fa-circle-o"></i> Order
                </a>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-circle-o"></i> Voucher
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li>
                        <a href="<c:url value="/admin-page/sales/voucher" />">
                            <i class="fa fa-circle-o"></i> Voucher List
                        </a>
                    </li>
                    <li>
                        <a href="<c:url value="/admin-page/sales/voucher/form" />">
                            <i class="fa fa-circle-o"></i> Add Voucher
                        </a>
                    </li>
                </ul>
            </li>
        </ul>
    </li>

    <!-- Admin -->
    <li class="treeview">
        <a href="#">
            <i class="fa fa-user"></i>
            <span>Admin</span>
            <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
            <li>
                <a href="<c:url value="/admin-page/admin-account" />">
                    <i class="fa fa-circle-o"></i> Overview
                </a>
            </li>
        </ul>
    </li>
    </li>

    <!-- CUSTOMER -->
    <li class="treeview">
        <a href="#">
            <i class="fa fa-users"></i>
            <span>Customer</span>
            <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
            <li>
                <a href="<c:url value="/admin-page/customer" />">
                    <i class="fa fa-circle-o"></i> Overview
                </a>
            </li>
            <li>
                <a href="<c:url value="/admin-page/customer/review" />">
                    <i class="fa fa-circle-o"></i> Review
                </a>
            </li>
            <li>
                <a href="<c:url value="/admin-page/customer/bookmark" />">
                    <i class="fa fa-circle-o"></i> Bookmark
                </a>
            </li>
        </ul>
    </li>
    <!-- REPORT -->
    <li class="treeview">
        <a href="#">
            <i class="fa fa-bar-chart"></i>
            <span>Report</span>
            <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
            <li>
                <a href="<c:url value="/admin-page/report/sales" />">
                    <i class="fa fa-circle-o"></i> Sales
                </a>
            </li>
            <li>
                <a href="<c:url value="/admin-page/report/product" />">
                    <i class="fa fa-circle-o"></i> Product
                </a>
            </li>
            <li>
                <a href="<c:url value="/admin-page/report/customer" />">
                    <i class="fa fa-circle-o"></i> Customer
                </a>
            </li>
        </ul>
    </li>
    <li>
        <a href="<c:url value="/admin-page/log" />">
            <i class="fa fa-calendar"></i>
            <span>Activity Log</span>
        </a>
    </li>
</ul>