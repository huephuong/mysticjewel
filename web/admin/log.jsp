<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="layout/taglib.jsp"%>
<tiles:insertDefinition name="common">
    <tiles:putAttribute name="title" value="Activity Log" />
    <tiles:putAttribute name="page-css">
        <link rel="stylesheet" href="<c:url value="/resource/admin/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css" />">
        <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.1/css/responsive.bootstrap.min.css">
    </tiles:putAttribute>
    <tiles:putAttribute name="body">
        <section class="content-header">
            <h1>
                Activity Log
                <small>View admin's activity</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
                <li class="active">Activity Log</li>
            </ol>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box box-primary">
                        <div class="box-header">
                            <h3 class="box-title">
                                Activity Log
                            </h3>
                        </div>
                        <div class="box-body">
                            <table id="logTable" class="table table-striped table-bordered table-hover dt-responsive nowrap" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>Log ID</th>
                                        <th>Admin</th>
                                        <th>Detail</th>
                                        <th>Date Performed</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <c:forEach items="${log}" var="log">
                                        <tr>
                                            <td>${log.logID}</td>
                                            <td>${log.username}</td>
                                            <td>${log.logContent}</td>
                                            <td><fmt:formatDate type = "both" dateStyle = "short" timeStyle = "short" value = "${log.dateLogged}" /></td>
                                        </tr>
                                    </c:forEach>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- Order Detail -->
                <c:if test="${not empty orderDetail}">
                    <div class="col-xs-6">
                        <div class="box box-info">
                            <div class="box-header">
                                <h3 class="box-title">
                                    Order Detail - ID: ${orderDetail.orderID}
                                    <input type="hidden" name="currentID" value="${orderDetail.orderID}">
                                </h3>
                            </div>
                            <div class="box-body">
                                <table id="orderMasterTable" class="table table-striped dt-responsive" style="width:100%">
                                    <tbody>
                                        <tr>
                                            <th>Customer</th>
                                            <td>
                                                <c:choose>
                                                    <c:when test="${not empty orderDetail.customerID}">
                                                        ${orderDetail.customerID.firstName} ${orderDetail.customerID.lastName}
                                                    </c:when>
                                                    <c:otherwise>
                                                        Guest
                                                    </c:otherwise>
                                                </c:choose>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>Voucher</th>
                                            <td>
                                                <c:choose>
                                                    <c:when test="${not empty orderDetail.voucherID}">
                                                        ${orderDetail.voucherID.voucherID} <span class="badge bg-light-blue">${orderDetail.voucherID.discountValue}%</span>
                                                    </c:when>
                                                    <c:otherwise>
                                                        None
                                                    </c:otherwise>
                                                </c:choose>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>Shipping Fee</th>
                                            <td>
                                                <fmt:setLocale value="en_US"/>
                                                <fmt:formatNumber value="${orderDetail.shipFee}" type="currency"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>Shipping Date</th>
                                            <td><fmt:formatDate type = "both" dateStyle = "short" timeStyle = "short" value = "${orderDetail.shipDate}" /></td>
                                        </tr>
                                        <tr>
                                            <th>Shipping Name</th>
                                            <td>
                                                ${orderDetail.shipName}
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>Shipping Address</th>
                                            <td>
                                                ${orderDetail.shipAddress}
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>Shipping Phone</th>
                                            <td>
                                                ${orderDetail.shipPhone}
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>Payment method</th>
                                            <td>
                                                ${orderDetail.payment}
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>Grand Total</th>
                                            <td>
                                                <fmt:setLocale value="en_US"/>
                                                <fmt:formatNumber value="${orderDetail.grandTotal}" type="currency"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>Revenue</th>
                                            <td>
                                                <fmt:setLocale value="en_US"/>
                                                <fmt:formatNumber value="${orderDetail.orderRevenue}" type="currency"/>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <table id="detailTable" class="table table-bordered dt-responsive" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>SKU</th>
                                            <th>Sale Price</th>
                                            <th>Quantity</th>
                                            <th>Subtotal</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <c:forEach items="${orderDetail.orderDetailList}" var="detail">
                                            <tr>
                                                <td>${detail.sku.skuID}</td>
                                                <td>
                                                    <fmt:setLocale value="en_US"/>
                                                    <fmt:formatNumber value="${detail.unitPrice - (detail.unitPrice * detail.discountValue / 100)}" type="currency"/>
                                                </td>
                                                <td>${detail.quantity}</td>
                                                <td>
                                                    <fmt:setLocale value="en_US"/>
                                                    <fmt:formatNumber value="${(detail.unitPrice - (detail.unitPrice * detail.discountValue / 100)) * detail.quantity}" type="currency"/>
                                                </td>
                                            </tr>
                                        </c:forEach>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </c:if>
            </div>
        </section>
    </tiles:putAttribute>
    <tiles:putAttribute name="page-js">
        <script src="<c:url value="/resource/admin/bower_components/datatables.net/js/jquery.dataTables.min.js" />"></script>
        <script src="<c:url value="/resource/admin/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js" />"></script>
        <script src="https://cdn.datatables.net/responsive/2.2.1/js/dataTables.responsive.min.js"></script>
        <script src="https://cdn.datatables.net/responsive/2.2.1/js/responsive.bootstrap.min.js"></script>
        <script>
            $(function() {
                $('#logTable').DataTable({
                    "pageLength": 25
                });

            });
        </script>
    </tiles:putAttribute>
</tiles:insertDefinition>
