<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="../layout/taglib.jsp"%>
<tiles:insertDefinition name="common">
    <tiles:putAttribute name="title" value="403 error" />
    <tiles:putAttribute name="page-css" value="" />
    <tiles:putAttribute name="body">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                403 Error Page
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">403 error</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="error-page">
                <h2 class="headline text-red"> 403</h2>

                <div class="error-content">
                    <h3><i class="fa fa-warning text-red"></i> Access is denied.</h3>
                    <p>
                        We could not find the page you were looking for.
                        Meanwhile, you may <a href="<c:url value="/admin/dashboard" />">return to dashboard</a> or try using the search form.
                    </p>
                </div>
                <!-- /.error-content -->
            </div>
            <!-- /.error-page -->
        </section>
        <!-- /.content -->
    </tiles:putAttribute>
    <tiles:putAttribute name="page-js" value="" />
</tiles:insertDefinition>
