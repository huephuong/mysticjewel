<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="../layout/taglib.jsp"%>
<tiles:insertDefinition name="common">
    <tiles:putAttribute name="title" value="${voucherUpdate.voucherID == null ? 'Add' : 'Edit'} product voucher" />
    <tiles:putAttribute name="page-css">
        <link rel="stylesheet" href="<c:url value="/resource/admin/plugins/iCheck/all.css" />">
        <link rel="stylesheet" href="<c:url value="/resource/admin/plugins/bootstrap-datepicker/bootstrap-datepicker3.css" />">
        <link rel="stylesheet" href="<c:url value="/resource/admin/bower_components/bootstrap-daterangepicker/daterangepicker.css" />">
    </tiles:putAttribute>
    <tiles:putAttribute name="body">
        <section class="content-header">
            <h1>
                Product Voucher
                <small>${voucherUpdate.voucherID == null ? "Add" : "Edit"}</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa ${voucherUpdate.voucherID == null ? 'fa-plus-square' : 'fa-edit'}"></i> Home</a></li>
                <li class="active">${voucherUpdate.voucherID == null ? "Add" : "Edit"} product voucher</li>
            </ol>
        </section>
        <section class="content">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">${voucherUpdate.voucherID == null ? "Add" : "Edit"}</h3>
                </div>
                <form action="<c:url value="/admin-page/sales/voucher/form" />" method="POST" class="form-horizontal" id="form_add_voucher">
                    <input type="hidden" name="formName" value="${voucherUpdate.voucherID == null ? 'Add' : 'Edit'}">
                    <div class="box-body">
                        <div class="form-group">
                            <label for="inputVoucherID" class="col-sm-2 control-label">Voucher ID: </label>
                            <div class="col-sm-6">
                                <input type="text" name="inputVoucherID" class="form-control" id="inputVoucherID" value="${voucherUpdate.voucherID}" placeholder="VoucherID" 
                                       ${voucherUpdate.voucherID != null ? "readonly" : ""} >
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="inputValidDate">Discount Valid Date</label>
                            <div class="col-sm-6">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-clock-o"></i>
                                    </div>
                                    <input type="text" name="inputValidDate" class="form-control pull-right" id="inputValidDate" 
                                           value="<fmt:formatDate type = 'both' dateStyle = 'short' timeStyle = 'short' value = '${voucherUpdate.validFrom}' /> '-' <fmt:formatDate type = 'both' dateStyle = 'short' timeStyle = 'short' value = '${voucherUpdate.validTo}' />">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="inputDescription" class="col-sm-2 control-label">Description: </label>
                            <div class="col-sm-6">
                                <textarea class="form-control" rows="5" name="inputDescription" placeholder="Voucher Description" id="inputDescription">${voucherUpdate.voucherDesc}</textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="inputDiscountValue" class="col-sm-2 control-label">Discount value: </label>
                            <div class="col-sm-6">
                                <input type="number" min="5" max="75" name="inputDiscountValue" class="form-control" id="inputDiscountValue" value="${voucherUpdate.discountValue}" placeholder="Discount value">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="inputQuantity" class="col-sm-2 control-label">Quantity: </label>
                            <div class="col-sm-6">
                                <input type="number" name="inputQuantity" min="1" max="999" class="form-control" id="inputQuantity" value="${voucherUpdate.quantity}" placeholder="Quantity">
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-info pull">Save</button>
                        <button type="reset" class="btn btn-default">Cancel</button>
                        <label style="color: red">${msg != null ? msg : ""}</label>
                    </div>
                </form>
            </div>
        </section>
    </tiles:putAttribute>
    <tiles:putAttribute name="page-js">
        <script src="<c:url value="/resource/admin/plugins/iCheck/icheck.min.js" />"></script>
        <script src="<c:url value="/resource/admin/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js" />"></script>
        <script src="<c:url value="/resource/admin/bower_components/moment/min/moment.min.js" />"></script>
        <script src="<c:url value="/resource/admin/bower_components/bootstrap-daterangepicker/daterangepicker.js" />"></script>
        <script>
            $(function() {
                $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
                    checkboxClass: 'icheckbox_minimal-blue',
                    radioClass: 'iradio_minimal-blue'
                });

                $('#inputValidDate').daterangepicker(
                        {
                            timePicker: true,
                            timePickerIncrement: 30,
                            locale: {format: 'DD/MM/YYYY h:mm A'}
                        },
                function(start, end) {
                    $('#reservationtime span').html(start.format('DD/MM/YYYY h:mm A') + ' - ' + end.format('DD/MM/YYYY h:mm A'));
                    $('#inputValidFrom').val(start.format('DD/MM/YYYY h:mm A'));
                    $('#inputValidTo').val(end.format('DD/MM/YYYY h:mm A'));
                });

                $.validator.addMethod("formatID", function(value, element) {
                    return this.optional(element) || /^VOU+[0-9]{2,3}$/i.test(value);
                }, "Please enter Voucher ID follow format VOUxx");

                $('#form_add_voucher').validate({
                    errorElement: 'span', //default input error message container
                    errorClass: 'help-block help-block-error', // default input error message class
                    //focusInvalid: false, // do not focus the last invalid input
                    //ignore: "",  // validate all fields including form hidden input
                    messages: {
                        inputVoucherID: {
                            required: "Voucher ID can not blank.",
                            minlength: "Voucher id must consist's of at least 3 characters"
                        },
                        inputValidDate: {
                            required: "Valid date can not blank."
                        },
                        inputDescription: {
                            required: "Description can not blank.",
                            minlength: "Description must consist's of at least 10 characters"
                        },
                        inputDiscountValue: {
                            required: "Discount value can not blank."
                        },
                        inputQuantity: {
                            required: "Quantity value can not blank."
                        }
                    },
                    rules: {
                        'inputVoucherID': {
                            required: true,
                            minlength: 3,
                            formatID: true
                        },
                        'inputValidDate': {
                            required: true
                        },
                        'inputDescription': {
                            required: true,
                            minlength: 10
                        },
                        'inputDiscountValue': {
                            required: true
                        },
                        'inputQuantity': {
                            required: true
                        }
                    },
                    errorPlacement: function(error, element) { // render error placement for each input type
                        element.after(error);
                    },
                    highlight: function(element) { // hightlight error inputs
                        $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
                    },
                    unhighlight: function(element) { // revert the change done by hightlight
                        $(element).closest('.form-group').removeClass('has-error'); // set error class to the control group
                    },
                    success: function(label) {
                        label.closest('.form-group').removeClass('has-error'); // set success class to the control group
                    },
                    submitHandler: function(form) {
                        form.submit();
                    }
                });
            });
        </script>
    </tiles:putAttribute>
</tiles:insertDefinition>
