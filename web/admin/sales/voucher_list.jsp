<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="../layout/taglib.jsp"%>
<tiles:insertDefinition name="common">
    <tiles:putAttribute name="title" value="Voucher list" />
    <tiles:putAttribute name="page-css">
        <link rel="stylesheet" href="<c:url value="/resource/admin/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css" />">
        <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.1/css/responsive.bootstrap.min.css">
    </tiles:putAttribute>
    <tiles:putAttribute name="body">
        <section class="content-header">
            <h1>
                Product Voucher
                <small>List</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-users"></i> Home</a></li>
                <li class="active">Product Voucher List</li>
            </ol>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box box-primary">
                        <div class="box-header">
                            <h3 class="box-title">List</h3>
                        </div>
                        <div class="box-body">
                            <table id="example" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>Voucher ID</th>
                                        <th>Description</th>
                                        <th>Discount Value</th>
                                        <th>Quantity</th>
                                        <th>From date</th>
                                        <th>To date</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <c:forEach var="voucher" items="${voucherList}">
                                        <tr>
                                            <td>${voucher.voucherID}</td>
                                            <td>${voucher.voucherDesc}</td>
                                            <td>${voucher.discountValue}</td>
                                            <td>${voucher.quantity}</td>
                                            <td><fmt:formatDate type = "both" dateStyle = "short" timeStyle = "short" value = "${voucher.validFrom}" /></td>
                                            <td><fmt:formatDate type = "both" dateStyle = "short" timeStyle = "short" value = "${voucher.validTo}" /></td>
                                            <td>
                                                <div class="btn-group">
                                                    <button type="button" class="btn btn-info">Action</button>
                                                    <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
                                                        <span class="caret"></span>
                                                        <span class="sr-only">Toggle Dropdown</span>
                                                    </button>
                                                    <ul class="dropdown-menu" role="menu">
                                                        <li><a href="<c:url value="/" />admin-page/sales/voucher/form?voucherID=${voucher.voucherID}"><span class="text-primary">Update</span></a></li>
                                                        <li class="divider"></li>
                                                        <li><a href="<c:url value="/" />admin-page/sales/voucher?voucherID=${voucher.voucherID}"><span class="text-red">Delete</span></a></li>
                                                    </ul>
                                                </div>
                                            </td>
                                        </tr>
                                    </c:forEach>
                                </tbody>
                            </table>
                            <label>${msg}</label>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </tiles:putAttribute>
    <tiles:putAttribute name="page-js">
        <script src="<c:url value="/resource/admin/bower_components/datatables.net/js/jquery.dataTables.min.js" />"></script>
        <script src="<c:url value="/resource/admin/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js" />"></script>
        <script src="https://cdn.datatables.net/responsive/2.2.1/js/dataTables.responsive.min.js"></script>
        <script src="https://cdn.datatables.net/responsive/2.2.1/js/responsive.bootstrap.min.js"></script>
        <script>
            $(function() {
                $('#example').DataTable({
                    "pageLength": 25
                });
            });
        </script>
    </tiles:putAttribute>
</tiles:insertDefinition>